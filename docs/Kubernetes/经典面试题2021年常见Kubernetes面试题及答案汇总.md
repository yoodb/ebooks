# 经典面试题2021年常见Kubernetes面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Kubernetes

### 题1：[K8s 的 Service 是什么？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题1k8s-的-service-是什么)<br/>
pod每次重启或者重新部署，其IP地址都会产生变化，这使得pod间通信和pod与外部通信变得困难，这时候，就需要Service为pod提供一个固定的入口。

Service的Endpoint列表通常绑定了一组相同配置的pod，通过负载均衡的方式把外界请求分配到多个pod上。

### 题2：[Kubernetes 中如何使用 EFK 实现日志的统一管理？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题2kubernetes-中如何使用-efk-实现日志的统一管理)<br/>
在Kubernetes集群环境中，通常一个完整的应用或服务涉及组件过多，建议对日志系统进行集中化管理，通常采用EFK实现。

EFK是 Elasticsearch、Fluentd 和 Kibana 的组合，其各组件功能如下：

- Elasticsearch：是一个搜索引擎，负责存储日志并提供查询接口；

- Fluentd：负责从 Kubernetes 搜集日志，每个Node节点上面的Fluentd监控并收集该节点上面的系统日志，并将处理过后的日志信息发送给Elasticsearch；

- Kibana：提供了一个 Web GUI，用户可以浏览和搜索存储在 Elasticsearch 中的日志。

通过在每台Node上部署一个以DaemonSet方式运行的Fluentd来收集每台Node上的日志。Fluentd将Docker日志目录/var/lib/docker/containers和/var/log目录挂载到Pod中，然后Pod会在Node节点的/var/log/pods目录中创建新的目录，可以区别不同的容器日志输出，该目录下有一个日志文件链接到/var/lib/docker/contianers目录下的容器日志输出。

### 题3：[Kubernetes 有哪些应用功能？借助什么开源项目？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题3kubernetes-有哪些应用功能借助什么开源项目)<br/>
在生产环境中（尤其是面向云优化应用开发时）使用Kubernetes的主要优势在于，它提供了一个便捷有效的平台，可以在物理机和虚拟机集群上调度和运行容器。更广泛一点说，它可以帮助用户在生产环境中，完全实施并依托基于容器的基础架构运营。由于Kubernetes的实质在于实现操作任务自动化，所以可以将其它应用平台或管理系统分配给的许多相同任务交给容器来执行。

利用Kubernetes，能够达成以下目标：

- 跨多台主机进行容器编排。
- 更加充分地利用硬件，最大程度获取运行企业应用所需的资源。
- 有效管控应用部署和更新，并实现自动化操作。
- 挂载和增加存储，用于运行有状态的应用。
- 快速、按需扩展容器化应用及其资源。
- 对服务进行声明式管理，保证所部署的应用始终按照部署的方式运行。
- 利用自动布局、自动重启、自动复制以及自动扩展功能，对应用实施状况检查和自我修复。

但是，Kubernetes需要依赖其它项目来全面提供这些经过编排的服务。因此，借助其它开源项目可以有效的将Kubernetes的全部功用发挥出来。这些功能包括：

- 注册表，通过Atomic注册表或Docker注册表等项目实现。
- 联网，通过OpenvSwitch和智能边缘路由等项目实现。
- 遥测，通过heapster、kibana、hawkular和elastic等项目实现。
- 安全性，通过LDAP、SELinux、RBAC和OAUTH等项目以及多租户层来实现。
- 自动化，参照Ansible手册进行安装和集群生命周期管理。
- 服务，可通过自带预建版常用应用模式的丰富内容目录来提供。

### 题4：[为什么 Kubernetes 如此有用？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题4为什么-kubernetes-如此有用)<br/>
![image.png](https://jingxuan.yoodb.com/upload/images/f9a3a7a91be14a69b1158ac64b0c14a8.png)

**传统部署时代：**

早期，各个组织机构在物理服务器上运行应用程序。无法为物理服务器中的应用程序定义资源边界，这会导致资源分配问题。 例如，如果在物理服务器上运行多个应用程序，则可能会出现一个应用程序占用大部分资源的情况， 结果可能导致其他应用程序的性能下降。 一种解决方案是在不同的物理服务器上运行每个应用程序，但是由于资源利用不足而无法扩展， 并且维护许多物理服务器的成本很高。

**虚拟化部署时代：**

作为解决方案，引入了虚拟化。虚拟化技术允许你在单个物理服务器的 CPU 上运行多个虚拟机（VM）。 虚拟化允许应用程序在 VM 之间隔离，并提供一定程度的安全，因为一个应用程序的信息 不能被另一应用程序随意访问。

虚拟化技术能够更好地利用物理服务器上的资源，并且因为可轻松地添加或更新应用程序 而可以实现更好的可伸缩性，降低硬件成本等等。

每个 VM 是一台完整的计算机，在虚拟化硬件之上运行所有组件，包括其自己的操作系统。

**容器部署时代：**

容器类似于 VM，但是它们具有被放宽的隔离属性，可以在应用程序之间共享操作系统（OS）。 因此，容器被认为是轻量级的。容器与 VM 类似，具有自己的文件系统、CPU、内存、进程空间等。 由于它们与基础架构分离，因此可以跨云和 OS 发行版本进行移植。

容器因具有许多优势而变得流行起来。下面列出的是容器的一些好处：

- 敏捷应用程序的创建和部署：与使用 VM 镜像相比，提高了容器镜像创建的简便性和效率。
- 持续开发、集成和部署：通过快速简单的回滚（由于镜像不可变性），支持可靠且频繁的 容器镜像构建和部署。
- 关注开发与运维的分离：在构建/发布时而不是在部署时创建应用程序容器镜像， 从而将应用程序与基础架构分离。
- 可观察性：不仅可以显示操作系统级别的信息和指标，还可以显示应用程序的运行状况和其他指标信号。
- 跨开发、测试和生产的环境一致性：在便携式计算机上与在云中相同地运行。
- 跨云和操作系统发行版本的可移植性：可在 Ubuntu、RHEL、CoreOS、本地、 Google Kubernetes Engine 和其他任何地方运行。
- 以应用程序为中心的管理：提高抽象级别，从在虚拟硬件上运行 OS 到使用逻辑资源在 OS 上运行应用程序。
- 松散耦合、分布式、弹性、解放的微服务：应用程序被分解成较小的独立部分， 并且可以动态部署和管理 - 而不是在一台大型单机上整体运行。
- 资源隔离：可预测的应用程序性能。
- 资源利用：高效率和高密度。


### 题5：[Kubernetes Secret 有什么作用？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题5kubernetes-secret-有什么作用)<br/>
Secret对象，主要作用是保管私密数据，比如密码、OAuth Tokens、SSH Keys等信息。将这些私密信息放在Secret对象中比直接放在Pod或Docker Image中更安全，也更便于使用和分发。

### 题6：[什么是 Kubernetes 集群联邦？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题6什么是-kubernetes-集群联邦)<br/>
Kubernetes集群联邦可以将多个Kubernetes集群作为一个集群进行管理。因此，可以在一个数据中心/云中创建多个Kubernetes集群，并使用集群联邦在一个地方控制/管理所有集群。

### 题7：[Kubernetes CSI 模型是什么？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题7kubernetes-csi-模型是什么)<br/>
Kubernetes CSI是Kubernetes推出与容器对接的存储接口标准，存储提供方只需要基于标准接口进行存储插件的实现，就能使用Kubernetes的原生存储机制为容器提供存储服务。CSI使得存储提供方的代码能和Kubernetes代码彻底解耦，部署也与Kubernetes核心组件分离，显然，存储插件的开发由提供方自行维护，就能为Kubernetes用户提供更多的存储功能，也更加安全可靠。

CSI包括CSI Controller和CSI Node：

1、CSI Controller的主要功能是提供存储服务视角对存储资源和存储卷进行管理和操作。

2、CSI Node的主要功能是对主机（Node）上的Volume进行管理和操作。

### 题8：[简述 etcd 适应的场景？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题8简述-etcd-适应的场景)<br/>
etcd基于其优秀的特点，可广泛的应用于以下场景：

- 服务发现（Service Discovery）：服务发现主要解决在同一个分布式集群中的进程或服务，要如何才能找到对方并建立连接。本质上来说，服务发现就是想要了解集群中是否有进程在监听UDP或TCP端口，并且通过名字就可以查找和连接。

- 消息发布与订阅：在分布式系统中，最适用的一种组件间通信方式就是消息发布与订阅。即构建一个配置共享中心，数据提供者在这个配置中心发布消息，而消息使用者则订阅他们关心的主题，一旦主题有消息发布，就会实时通知订阅者。通过这种方式可以做到分布式系统配置的集中式管理与动态更新。应用中用到的一些配置信息放到etcd上进行集中管理。

- 负载均衡：在分布式系统中，为了保证服务的高可用以及数据的一致性，通常都会把数据和服务部署多份，以此达到对等服务，即使其中的某一个服务失效了，也不影响使用。etcd本身分布式架构存储的信息访问支持负载均衡。etcd集群化以后，每个etcd的核心节点都可以处理用户的请求。所以，把数据量小但是访问频繁的消息数据直接存储到etcd中也可以实现负载均衡的效果。

- 分布式通知与协调：与消息发布和订阅类似，都用到了etcd中的Watcher机制，通过注册与异步通知机制，实现分布式环境下不同系统之间的通知与协调，从而对数据变更做到实时处理。

- 分布式锁：因为etcd使用Raft算法保持了数据的强一致性，某次操作存储到集群中的值必然是全局一致的，所以很容易实现分布式锁。锁服务有两种使用方式，一是保持独占，二是控制时序。

- 集群监控与Leader竞选：通过etcd来进行监控实现起来非常简单并且实时性强。

### 题9：[Helm 是什么，有什么优势？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题9helm-是什么有什么优势)<br/>
Helm是Kubernetes的软件包管理工具。类似Ubuntu中使用的APT、CentOS中使用的yum 或者Python中的 pip 一样。

Helm能够将一组Kubernetes资源打包统一管理, 是查找、共享和使用为Kubernetes构建的软件的最佳方式。

Helm中通常每个包称为一个Chart，一个Chart是一个目录（一般情况下会将目录进行打包压缩，形成name-version.tgz格式的单一文件，方便传输和存储）。

在Kubernetes中部署一个可以使用的应用，需要涉及到很多的 Kubernetes 资源的共同协作。使用Helm则具有如下优势：

- 统一管理、配置和更新这些分散的Kubernetes的应用资源文件；

- 分发和复用一套应用模板；

- 将应用的一系列资源当做一个软件包管理。

- 对于应用发布者而言，可以通过 Helm 打包应用、管理应用依赖关系、管理应用版本并发布应用到软件仓库。

对于使用者而言，使用Helm后不用需要编写复杂的应用部署文件，可以以简单的方式在Kubernetes上查找、安装、升级、回滚、卸载应用程序。

### 题10：[描述 Kubernetes 创建一个 Pod 的主要流程？](/docs/Kubernetes/经典面试题2021年常见Kubernetes面试题及答案汇总.md#题10描述-kubernetes-创建一个-pod-的主要流程)<br/>
Kubernetes中创建一个Pod涉及多个组件之间联动，主要流程如下：

- 客户端提交Pod的配置信息（可以是yaml文件定义的信息）到kube-apiserver。

- Apiserver收到指令后，通知给controller-manager创建一个资源对象。

- Controller-manager通过api-server将Pod的配置信息存储到etcd数据中心中。

- Kube-scheduler检测到Pod信息会开始调度预选，会先过滤掉不符合Pod资源配置要求的节点，然后开始调度调优，主要是挑选出更适合运行Pod的节点，然后将Pod的资源配置单发送到Node节点上的kubelet组件上。

- Kubelet根据scheduler发来的资源配置单运行Pod，运行成功后，将Pod的运行信息返回给scheduler，scheduler将返回的Pod运行状况的信息存储到etcd数据中心。

### 题11：k8s-标签与标签选择器的作用是什么<br/>


### 题12：kubernetes-中-headless-service-有什么作用<br/>


### 题13：简述-kube-proxy-iptables-的原理<br/>


### 题14：描述一下-kubernetes-初始化容器init-container<br/>


### 题15：kubernetes-共享存储有什么作用<br/>


### 题16：-kubernetes-中如何处理容器日志-<br/>


### 题17：kubernetes-版本回滚相关的命令<br/>


### 题18：kubenetes-如何控制滚动更新过程<br/>


### 题19：k8s-常用的标签分类有哪些<br/>


### 题20：kubernetes-中-pv-和-pvc-是什么<br/>


### 题21：简述minikubekubectlkubelet-分别是什么<br/>


### 题22：说说你对-job-这种资源对象的了解<br/>


### 题23：kubernetes-中-pod-有哪些重启策略<br/>


### 题24：描述一下-kubernetes-deployment-升级过程<br/>


### 题25：kubernetes-kubele-t监控-worker-节点资源是使用什么组件来实现<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")