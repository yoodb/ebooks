# 最新2021年Kubernetes面试题及答案汇总版

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Kubernetes

### 题1：[描述一下 Kubernetes deployment 升级过程？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题1描述一下-kubernetes-deployment-升级过程)<br/>
1、初始创建Deployment时，系统创建了一个ReplicaSet，并按用户的需求创建了对应数量的Pod副本。

2、当更新Deployment时，系统创建了一个新的ReplicaSet，并将其副本数量扩展到1，然后将旧ReplicaSet缩减为2。

3、之后，系统继续按照相同的更新策略对新旧两个ReplicaSet进行逐个调整。

4、最后，新的ReplicaSet运行了对应个新版本Pod副本，旧的ReplicaSet副本数量则缩减为0。

### 题2：[简述 Kubernetes 网络策略？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题2简述-kubernetes-网络策略)<br/>
为实现细粒度的容器间网络访问隔离策略，Kubernetes引入Network Policy。

Network Policy的主要功能是对Pod间的网络通信进行限制和准入控制，设置允许访问或禁止访问的客户端Pod列表。Network Policy定义网络策略，配合策略控制器（Policy Controller）进行策略的实现。

### 题3：[Kubernetes Replica Set 和 Replication Controller 有什么区别？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题3kubernetes-replica-set-和-replication-controller-有什么区别)<br/>
Replica Set和Replication Controller类似，都是确保在任何给定时间运行指定数量的Pod副本。不同之处在于RS使用基于集合的选择器，而Replication Controller使用基于权限的选择器。

### 题4：[什么是 ETCD？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题4什么是-etcd)<br/>
Etcd是用Go编程语言编写的，是一个分布式键值存储，用于协调分布式工作。因此，Etcd存储Kubernetes集群的配置数据，表示在任何给定时间点的集群状态。

### 题5：[Kubernetes Secret 有哪些使用方式？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题5kubernetes-secret-有哪些使用方式)<br/>
创建完secret之后，可通过如下三种方式使用：

- 在创建Pod时，通过为Pod指定Service Account来自动使用该Secret。

- 通过挂载该Secret到Pod来使用它。

- 在Docker镜像下载时使用，通过指定Pod的spc.ImagePullSecrets来引用它。

### 题6：[为什么需要 Kubernetes，它能做什么？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题6为什么需要-kubernetes它能做什么)<br/>
容器是打包和运行应用程序的好方式。在生产环境中，管理运行应用程序的容器，并确保不会停机。例如，如果一个容器发生故障，则需要启动另一个容器。如果系统处理此行为，会不会更容易？

这就是Kubernetes来解决这些问题的方法！Kubernetes提供了一个可弹性运行分布式系统的框架。Kubernetes可以解决扩展要求、故障转移、部署模式等。 例如，Kubernetes可以轻松管理系统的Canary部署。

Kubernetes提供：

- 服务发现和负载均衡

Kubernetes可以使用DNS名称或自己的IP地址公开容器，如果进入容器的流量很大，Kubernetes可以负载均衡并分配网络流量，从而使部署稳定。

- 存储编排

Kubernetes支持自动挂载存储系统，例如本地存储、公共云提供商等。

- 自动部署和回滚

可以使用Kubernetes描述已部署容器的所需状态，它可以以受控的速率将实际状态 更改为期望状态。例如，可以自动化Kubernetes来实现部署创建新容器， 删除现有容器并将它们的所有资源用于新容器。

- 自动完成装箱计算

Kubernetes允许指定每个容器所需CPU和内存（RAM）。 当容器指定了资源请求时，Kubernetes可以做出更好的决策来管理容器的资源。

- 自我修复

Kubernetes 重新启动失败的容器、替换容器、杀死不响应用户定义的 运行状况检查的容器，并且在准备好服务之前不将其通告给客户端。

- 密钥与配置管理

Kubernetes允许存储和管理敏感信息，例如密码、OAuth 令牌和 ssh 密钥。 可以在不重建容器镜像的情况下部署和更新密钥和应用程序配置，也无需在堆栈配置中暴露密钥。

### 题7：[什么是 Heapster？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题7什么是-heapster)<br/>
Heapster是由每个节点上运行的Kubelet提供的集群范围的数据聚合器。此容器管理工具在Kubernetes集群上本机支持，并作为pod运行，就像集群中的任何其他pod一样。因此，它基本上发现集群中的所有节点，并通过机上Kubernetes代理查询集群中Kubernetes节点的使用信息。

### 题8：[Kubernetes 中 flannel 有什么作用？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题8kubernetes-中-flannel-有什么作用)<br/>
Flannel可以用于Kubernetes底层网络的实现，主要作用包括：

- Flannel能协助Kubernetes，给每一个Node上的Docker容器都分配互相不冲突的IP地址。

- Flannel能在这些IP地址之间建立一个覆盖网络（Overlay Network），通过这个覆盖网络，将数据包原封不动地传递到目标容器内。

### 题9：[Kubernetes 如何实现自动扩容机制？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题9kubernetes-如何实现自动扩容机制)<br/>
Kubernetes使用Horizontal Pod Autoscaler（HPA）的控制器实现基于CPU使用率进行自动Pod扩缩容的功能。HPA控制器周期性地监测目标Pod的资源性能指标，并与HPA资源对象中的扩缩容条件进行对比，在满足条件时对Pod副本数量进行调整。

Kubernetes中的某个Metrics Server（Heapster或自定义Metrics Server）持续采集所有Pod副本的指标数据。HPA控制器通过Metrics Server的API（Heapster的API或聚合API）获取这些数据，基于用户定义的扩缩容规则进行计算，得到目标Pod副本数量。

当目标Pod副本数量与当前副本数量不同时，HPA控制器就向Pod的副本控制器（Deployment、RC或ReplicaSet）发起scale操作，调整Pod的副本数量，完成扩缩容操作。

### 题10：[Kubernetes 中什么是静态 Pod？](/docs/Kubernetes/最新2021年Kubernetes面试题及答案汇总版.md#题10kubernetes-中什么是静态-pod)<br/>
静态Pod是由kubelet进行管理的仅存在于特定Node的Pod上，他们不能通过API Server进行管理，无法与ReplicationController、Deployment或者DaemonSet进行关联，并且kubelet无法对他们进行健康检查。静态Pod总是由kubelet进行创建，并且总是在kubelet所在的Node上运行。

### 题11：kubernetes-pod-的-livenessprobe-探针有哪些常见方式<br/>


### 题12：kubernetes-kubele-t监控-worker-节点资源是使用什么组件来实现<br/>


### 题13：kubernetes-中-metric-service-有什么作用<br/>


### 题14：kubernetes-pod-的常见调度方式有哪些<br/>


### 题15：daemonset-资源对象的特性<br/>


### 题16：如何解释-kubernetes-架构组件之间的不同-<br/>


### 题17：kubenetes-架构的组成是什么<br/>


### 题18：k8s-数据持久化的方式有哪些<br/>


### 题19：kubernetes-daemonset-类型的资源有什么特性<br/>


### 题20：简述-etcd-及其特点<br/>


### 题21：简述-kubernetes-cni-模型<br/>


### 题22：简述-etcd-适应的场景<br/>


### 题23：kubenetes-如何控制滚动更新过程<br/>


### 题24：k8s-集群外流量怎么访问-pod<br/>


### 题25：什么是-pod<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")