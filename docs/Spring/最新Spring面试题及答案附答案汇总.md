# 最新Spring面试题及答案附答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring

### 题1：[Spring 中什么是 bean 的自动装配？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题1spring-中什么是-bean-的自动装配)<br/>
Spring容器能够自动装配相互合作的bean，这意味着容器不需要\<constructor-arg>和\<property>配置，能通过Bean工厂自动处理bean之间的协作。

### 题2：[Spring 中如何开启注解装配？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题2spring-中如何开启注解装配)<br/>
注解装配在默认情况下是不开启的，为了使用注解装配，必须在Spring配置文件中配置\<context:annotation-config/>元素。

### 题3：[JDK 动态代理和 CGLIB 动态代理有什么区别？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题3jdk-动态代理和-cglib-动态代理有什么区别)<br/>
Spring AOP中的动态代理主要有两种方式，JDK动态代理和CGLIB动态代理：

1、JDK动态代理只提供接口的代理，不支持类的代理。核心InvocationHandler接口和Proxy类，InvocationHandler 通过invoke()方法反射来调用目标类中的代码，动态地将横切逻辑和业务编织在一起；接着，Proxy利用 InvocationHandler动态创建一个符合某一接口的的实例, 生成目标类的代理对象。

2、如果代理类没有实现InvocationHandler 接口，那么Spring AOP会选择使用CGLIB来动态代理目标类。CGLIB（Code Generation Library），是一个代码生成的类库，可以在运行时动态的生成指定类的一个子类对象，并覆盖其中特定方法并添加增强代码，从而实现AOP。CGLIB是通过继承的方式做的动态代理，因此如果某个类被标记为final，那么它是无法使用CGLIB做动态代理的。

**静态代理与动态代理区别**在于生成AOP代理对象的时机不同，相对来说AspectJ的静态代理方式具有更好的性能，但是AspectJ需要特定的编译器进行处理，而Spring AOP则无需特定的编译器处理。

InvocationHandler中的

```java
invoke(Object proxy,Method method,Object[] args);
```
proxy是最终生成的代理实例；method是被代理目标实例的某个具体方法；args是被代理目标实例某个方法的具体入参，在方法反射调用时使用。

### 题4：[Spring 支持哪几种 bean 作用域？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题4spring-支持哪几种-bean-作用域)<br/>
1）singleton：默认，每个容器中只有一个bean的实例，单例的模式由BeanFactory自身来维护。

2）prototype：为每一个bean请求提供一个实例。

3）request：为每一个网络请求创建一个实例，在请求完成后，bean会失效并被垃圾回收器回收。

4）session：与request范围类似，确保每个session中有一个bean的实例，在session过期后，bean会随之失效。

5）global-session：全局作用域，global-session和Portlet应用相关。

当应用部署在Portlet容器中工作时，它包含很多portlet。

如果想要声明让所有的portlet共用全局的存储变量的话，那么这全局变量需要存储在global-session中。

全局作用域与Servlet中的session作用域效果相同。

### 题5：[Spring 中自动装配 Bean 有哪些方式？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题5spring-中自动装配-bean-有哪些方式)<br/>
Spring中支持5中自动装配模式。

**no**

默认方式，缺省情况下，自动配置是通过“ref”属性手动设定。手动装配方式，需要通过ref设定bean的依赖关系，在项目中最常用。

**byName**

根据属性名称自动装配。如果一个bean的名称和其他bean属性的名称是一致，将会自装配它。

**byType**

按数据类型自动装配。如果一个bean的数据类型是用其它bean属性的数据类型，兼容并自动装配它。

**constructor**

根据构造器进行装配，与byType类似，如果bean的构造器有与其他bean类型相同的属性，则进行自动装配。

autodetect

如果找到默认的构造函数，使用“自动装配用构造”; 否则，使用“按类型自动装配”。

### 题6：[什么是 Spring AOP？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题6什么是-spring-aop)<br/>
Spring AOP(Aspect Oriented Programming，面向切面编程)是OOPs(面向对象编程)的补充，它也提供了模块化。

在面向对象编程中关键的单元是对象，AOP的关键单元是切面，或者说关注点（可以简单地理解为你程序中的独立模块）。

一些切面可能有集中的代码，但是有些可能被分散或者混杂在一起，例如日志或者事务。这些分散的切面被称为横切关注点。

一个横切关注点是一个可以影响到整个应用的关注点，而且应该被尽量地集中到代码的一个地方，例如事务管理、权限、日志、安全等。

AOP可以使用简单可插拔的配置，在实际逻辑执行之前、之后或周围动态添加横切关注点。这让代码在当下和将来都变得易于维护。

如果使用XML来使用切面的话，要添加或删除关注点，不用重新编译完整的源代码，只需修改配置文件就可以。

Spring AOP通过两种方式来使用。但是最广泛使用的方式是Spring AspectJ注解风格(Spring AspectJ Annotation Style)。

1）使用AspectJ 注解风格。

2）使用Spring XML 配置风格。

### 题7：[Spring AOP 代理模式是什么？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题7spring-aop-代理模式是什么)<br/>
代理模式是使用非常广泛的设计模式之一。

代理模式是指给某一个对象提供一个代理对象，并由代理对象控制对原对象的引用。通俗的来讲代理模式就是生活中常见的房产中介。

Spring AOP是基于代理实现的。

Spring AOP代理是一个由AOP框架创建的用于在运行时实现切面协议的对象。

Spring AOP默认为AOP代理使用标准的JDK动态代理，这使得任何接口（或者接口的集合）可以被代理。

Spring AOP也可以使用CGLIB代理，如果业务对象没有实现任何接口那么默认使用CGLIB。

### 题8：[Spring AOP 中切入点和连接点什么关系？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题8spring-aop-中切入点和连接点什么关系)<br/>
连接点是个虚的概念，可简单理解为切入点的集合，它只是对应用程序的所有需要进行插入切面的一个统称。

每个切入点都对应具体的连接点，在运行期Spring就是根据这些切入点的定义，将通知或者拦截器插入到具体的连接点上。

### 题9：[Spring 管理事务默认回滚的异常有哪些？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题9spring-管理事务默认回滚的异常有哪些)<br/>
Spring的事务管理默认只对出现运行期异常（java.lang.RuntimeException及其子类）、Error进行回滚。
 
假设一个方法抛出Exception或者Checked异常，Spring事务管理默认不进行回滚。

### 题10：[Spring 中事务有哪几种隔离级别？](/docs/Spring/最新Spring面试题及答案附答案汇总.md#题10spring-中事务有哪几种隔离级别)<br/>
Spring在TransactionDefinition接口类中定义了五个表示隔离级别的常量：

ISOLATION_DEFAULT：数据库默认隔离级别，Mysql默认采用REPEATABLE_READ隔离级别；Oracle默认采用的READ_COMMITTED隔离级别。

ISOLATION_READ_UNCOMMITTED：最低隔离级别，允许读取尚未提交的变更数据，可能会导致脏读、幻读或不可重复读。

ISOLATION_READ_COMMITTED：允许读取并发事务已经提交的数据，可以阻止脏读，但是幻读或不可重复读仍有可能发生。

ISOLATION_REPEATABLE_READ：对同字段多次读取结果都是一致的，除非数据是被本身事务所修改，可以阻止脏读和不可重复读，但幻读仍有可能发生。

ISOLATION_SERIALIZABLE：最高隔离级别，完全服从ACID的隔离级别。所有的事务依次逐个执行，这样事务之间完全不可能产生干扰，该级别可以防止脏读、不可重复读以及幻读。但是这将严重影响程序的性能。通常情况下不会使用该级别。

### 题11：spring-中常用的注解包含哪些<br/>


### 题12：spring-中-@component-和-@bean-注解有什么区别<br/>


### 题13：解释-spring-框架中-bean-的生命周期<br/>


### 题14：spring-中单例-bean-是线程安全的吗<br/>


### 题15：spring-native-和-jvm-有什么区别<br/>


### 题16：一个-spring-bean-定义包含什么<br/>


### 题17：spring-中事务如何指定回滚的异常<br/>


### 题18：spring-aop-中关注点和横切关注点有什么不同<br/>


### 题19：spring-中如何更有效地使用-jdbc<br/>


### 题20：哪些是重要的-bean-生命周期方法能否重载它们<br/>


### 题21：spring-事务管理的方式有几种<br/>


### 题22：spring-aop-连接点和切入点是什么<br/>


### 题23：spring-事务都有哪些特性<br/>


### 题24：spring-中支持那些-orm<br/>


### 题25：spring-框架有哪些特点<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")