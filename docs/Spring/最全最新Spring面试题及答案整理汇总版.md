# 最全最新Spring面试题及答案整理汇总版

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring

### 题1：[Spring 中什么是目标对象? ](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题1spring-中什么是目标对象?-)<br/>
被一个或者多个切面所通知的对象。它通常是一个代理对象。也指被通知（advised）对象。

### 题2：[Bean 工厂和 Application contexts  有什么区别？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题2bean-工厂和-application-contexts--有什么区别)<br/>
Application contexts提供一种方法处理文本消息，一个通常的做法是加载文件资源（比如镜像），它们可以向注册为监听器的bean发布事件。

另外，在容器或容器内的对象上执行的那些不得不由bean工厂以程序化方式处理的操作，可以在Application contexts中以声明的方式处理。

Application contexts实现了MessageSource接口，该接口的实现以可插拔的方式提供获取本地化消息的方法。

### 题3：[Spring 中内部 bean 是什么？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题3spring-中内部-bean-是什么)<br/>
当一个bean仅被用作另一个bean的属性时，它能被声明为一个内部bean，为了定义inner bean，在Spring的基于XML的配置元数据中，可以在\<property/>或\<constructor-arg/> 元素内使用\<bean/> 元素，内部bean通常是匿名的，它们的Scope一般是prototype。

### 题4：[Spring 中单例 bean 是线程安全的吗？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题4spring-中单例-bean-是线程安全的吗)<br/>
Spring中单例bean默认是线程安全的，前提是这个对象没有非静态成员变量。

Spring中单例bean是存在线程安全是因为多个线程操作同一个对象时，这个对象非静态成员变量的写操作会存在线程安全问题。


两种常见的解决方案：

1）bean对象中尽量避免定义可变的成员变量。

2）推荐在类中定义一个ThreadLocal成员变量，将需要的可变成员变量保存在ThreadLocal中。

3）设置scope="prototype"参数。

### 题5：[什么是Spring IOC 容器？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题5什么是spring-ioc-容器)<br/>
Spring IOC 负责创建对象，管理对象（通过依赖注入（DI），装配对象，配置对象，并且管理这些对象的整个生命周期。

### 题6：[什么是 Spring 框架？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题6什么是-spring-框架)<br/>
Spring中文翻译过来是春天的意思，被称为J2EE的春天，是一个开源的轻量级的Java开发框架， 具有控制反转（IoC）和面向切面（AOP）两大核心。Java Spring框架通过声明式方式灵活地进行事务的管理，提高开发效率和质量。

Spring框架不仅限于服务器端的开发。从简单性、可测试性和松耦合的角度而言，任何 Java 应用都可以从Spring中受益。Spring框架还是一个超级粘合平台，除了自己提供功能外，还提供粘合其他技术和框架的能力。

**1）IOC 控制反转**

对象创建责任的反转，在Spring中BeanFacotory是IOC容器的核心接口，负责实例化，定位，配置应用程序中的对象及建立这些对象间的依赖。XmlBeanFacotory实现BeanFactory接口，通过获取xml配置文件数据，组成应用对象及对象间的依赖关系。

Spring中有3中注入方式，一种是set注入，另一种是接口注入，另一种是构造方法注入。

**2）AOP面向切面编程**

AOP是指纵向的编程，比如两个业务，业务1和业务2都需要一个共同的操作，与其往每个业务中都添加同样的代码，通过写一遍代码，让两个业务共同使用这段代码。

Spring中面向切面编程的实现有两种方式，一种是动态代理，一种是CGLIB，动态代理必须要提供接口，而CGLIB实现是由=有继承。

### 题7：[Spring 中 IOC的优点是什么？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题7spring-中-ioc的优点是什么)<br/>
IOC 或 依赖注入把应用的代码量降到最低。它使应用容易测试，单元测试不再需要单例和JNDI查找机制。

最小的代价和最小的侵入性使松散耦合得以实现。IOC容器支持加载服务时的饿汉式初始化和懒加载。

### 题8：[一个 Spring Bean 定义包含什么？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题8一个-spring-bean-定义包含什么)<br/>
一个Spring Bean的定义包含容器必知的所有配置元数据，包括如何创建一个bean，它的生命周期详情及它的依赖。

### 题9：[Spring 中如何定义 Bean 的范围？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题9spring-中如何定义-bean-的范围)<br/>
在Spring中定义一个时，我们也可以为bean声明一个范围。它可以通过bean定义中的scope属性定义。

例如，当Spring每次需要生成一个新的bean实例时，bean的scope属性就是原型。另一方面，当每次需要Spring都必须返回相同的bean实例时，bean的scope属性必须设置为singleton。

注：bean的scope属性有prototype，singleton，request, session几个属性

### 题10：[Spring 框架中事务管理有哪些优点？](/docs/Spring/最全最新Spring面试题及答案整理汇总版.md#题10spring-框架中事务管理有哪些优点)<br/>
Spring框架事务管理为不同的事务API，如JTA、JDBC、Hibernate、JPA和JDO，提供一个不变的编程模式。

Spring框架为编程式事务管理提供了一套简单的API而不是一些复杂的事务API。

Spring框架支持声明式事务管理。

Spring框架和Spring各种数据访问抽象层很好得集成。

### 题11：spring-事务都有哪些特性<br/>


### 题12：spring-中-@component-和-@bean-注解有什么区别<br/>


### 题13：spring-中允许注入一个null-或一个空字符串吗<br/>


### 题14：spring-中自动装配-bean-有哪些方式<br/>


### 题15：spring-中什么是-bean-装配<br/>


### 题16：spring-框架有哪些特点<br/>


### 题17：spring-应用程序有哪些不同组件<br/>


### 题18：spring-框架中使用了哪些设计模式<br/>


### 题19：spring-中-applicationcontext-通常的实现是什么<br/>


### 题20：spring-中如何定义类的作用域?-<br/>


### 题21：spring-是由哪些模块组成的<br/>


### 题22：spring-中如何开启注解装配<br/>


### 题23：spring-中常用的注解包含哪些<br/>


### 题24：spring-中支持那些-orm<br/>


### 题25：为什么-spring-只支持方法级别的连接点<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")