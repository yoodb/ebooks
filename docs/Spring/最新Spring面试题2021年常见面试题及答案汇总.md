# 最新Spring面试题2021年常见面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring

### 题1：[Spring 中有几种不同类型的自动代理？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题1spring-中有几种不同类型的自动代理)<br/>
BeanNameAutoProxyCreator

DefaultAdvisorAutoProxyCreator

Metadata autoproxying

### 题2：[Spring 中什么是目标对象? ](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题2spring-中什么是目标对象?-)<br/>
被一个或者多个切面所通知的对象。它通常是一个代理对象。也指被通知（advised）对象。

### 题3：[Spring 中什么是 bean 的自动装配？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题3spring-中什么是-bean-的自动装配)<br/>
Spring容器能够自动装配相互合作的bean，这意味着容器不需要\<constructor-arg>和\<property>配置，能通过Bean工厂自动处理bean之间的协作。

### 题4：[Spring AOP 代理是什么？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题4spring-aop-代理是什么)<br/>
代理是使用非常广泛的设计模式。简单来说，代理是一个看其他像另一个对象的对象，但它添加了一些特殊的功能。

Spring AOP是基于代理实现的。AOP代理是一个由AOP框架创建的用于在运行时实现切面协议的对象。

Spring AOP默认为AOP代理使用标准的JDK 动态代理。这使得任何接口（或者接口的集合）可以被代理。

Spring AOP也可以使用CGLIB代理。这对代理类而不是接口是必须的。

如果业务对象没有实现任何接口那么默认使用CGLIB。

### 题5：[Spring 是由哪些模块组成的？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题5spring-是由哪些模块组成的)<br/>
Spring 总共大约有 20 个模块，Spring 框架的基本模块包括Core module、Bean module、Context module、Expression Language module、JDBC module、ORM module、OXM module、Java Messaging Service(JMS) module、Transaction module、Web module、Web-Servlet module、Web-Struts module、Web-Portlet module。由1300多个不同的文件构成，而这些组件被分别整合在核心容器（Core Container）、AOP（Aspect Oriented Programming）和设备支持（Instrmentation）、数据访问与集成（Data Access/Integeration）、Web、消息（Messaging）、Test等6个模块中。

以下是Spring 5的模块结构图：

![16434375481.jpg](https://jingxuan.yoodb.com/upload/images/33f2ad8edaf24a968d30aec975a2dcb5.jpg)

- Spring core：提供了框架的基本组成部分，包括控制反转（Inversion of Control，IOC）和依赖注入（Dependency Injection，DI）功能。

- Spring beans：提供了BeanFactory，是工厂模式的一个经典实现，Spring将管理对象称为Bean。

- Spring context：构建于 core 封装包基础上的 context 封装包，提供了一种框架式的对象访问方法。

- Spring jdbc：提供了一个JDBC的抽象层，消除了烦琐的JDBC编码和数据库厂商特有的错误代码解析， 用于简化JDBC。

- Spring aop：提供了面向切面的编程实现，让你可以自定义拦截器、切点等。

- Spring Web：提供了针对 Web 开发的集成特性，例如文件上传，利用 servlet listeners 进行 ioc 容器初始化和针对 Web 的 ApplicationContext。

- Spring test：主要为测试提供支持的，支持使用JUnit或TestNG对Spring组件进行单元测试和集成测试。

### 题6：[Spring 中自动装配有那些局限性？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题6spring-中自动装配有那些局限性)<br/>
**自动装配的局限性**

重写：仍需用\<constructor-arg>和\<property>配置来定义依赖，意味着总要重写自动装配。

基本数据类型：不能自动装配简单的属性，例如基本数据类型、String字符串、和类。

模糊特性：自动装配不如显式装配精确，如果有可能，建议使用显式装配。

### 题7：[哪些是重要的 bean 生命周期方法？能否重载它们？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题7哪些是重要的-bean-生命周期方法能否重载它们)<br/>
有两个重要的bean生命周期方法，第一个是setup，它是在容器加载bean的时候被调用。

第二个方法是teardown它是在容器卸载类的时候被调用。bean标签有两个重要的属性（init-method和destroy-method）。用它们可以自己定制初始化和注销方法。它们也有相应的注解（@PostConstruct和@PreDestroy）。

### 题8：[Spring 框架中事务管理有哪些优点？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题8spring-框架中事务管理有哪些优点)<br/>
Spring框架事务管理为不同的事务API，如JTA、JDBC、Hibernate、JPA和JDO，提供一个不变的编程模式。

Spring框架为编程式事务管理提供了一套简单的API而不是一些复杂的事务API。

Spring框架支持声明式事务管理。

Spring框架和Spring各种数据访问抽象层很好得集成。

### 题9：[Spring 中通知有哪些类型？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题9spring-中通知有哪些类型)<br/>
通知（advice）是在程序中想要应用在其他模块中横切关注点的实现。

Advice主要有5种类型：

**@Before注解使用Advice**

前置通知（BeforeAdvice）：在连接点之前执行的通知（advice），除非它抛出异常，否则没有能力中断执行流。

**@AfterReturning注解使用Advice**

返回之后通知（AfterRetuningAdvice）：如果一个方法没有抛出异常正常返回，在连接点正常结束之后执行的通知（advice）。

**@AfterThrowing注解使用Advice**

抛出（异常）后执行通知（AfterThrowingAdvice）：若果一个方法抛出异常来退出的话，这个通知（advice）就会被执行。

**@After注解使用Advice**

后置通知（AfterAdvice）：无论连接点是通过什么方式退出的正常返回或者抛出异常都会执行在结束后执行这些通知（advice）。

**注解使用Advice**

围绕通知（AroundAdvice）：围绕连接点执行的通知（advice），只有这一个方法调用。这是最强大的通知（advice）。

### 题10：[Spring 如何设计容器的？BeanFactory 和 ApplicationContext 两者关系？](/docs/Spring/最新Spring面试题2021年常见面试题及答案汇总.md#题10spring-如何设计容器的beanfactory-和-applicationcontext-两者关系)<br/>
Spring 作者 Rod Johnson 设计了两个接口用以表示容器。
- BeanFactory

- ApplicationContext

BeanFactory可以理解为就是个HashMap，Key是BeanName，Value是Bean实例。通常只提供注册（put），获取（get）这两个功能。我们可以称之为 “低级容器”。

ApplicationContext可以称之为 “高级容器”。因为它比BeanFactory多了更多的功能。它继承了多个接口。因此具备了更多的功能。例如资源的获取，支持多种消息（例如JSP tag的支持），对BeanFactory多了工具级别的支持等待。所以他的名称已经不是BeanFactory之类的工厂，而是 “应用上下文”， 代表着整个大容器的所有功能。该接口定义了一个refresh方法，此方法是所有阅读Spring源码的人的最熟悉的方法，用于刷新整个容器，即重新加载/刷新所有的bean。

当然，除了这两个大接口，还有其他的辅助接口，这里就不过多介绍。

BeanFactory和ApplicationContext的关系为了更直观的展示 “低级容器” 和 “高级容器” 的关系，这里通过常用的ClassPathXmlApplicationContext类来展示整个容器的层级UML关系。

![16434382941.jpg](https://jingxuan.yoodb.com/upload/images/4a98207e9dee43b090133b4ff32f5b8e.jpg)

**有点复杂，东哥解释一下：**

最上面的是BeanFactory，下面的3个绿色的，都是功能扩展接口，这里就不展开来讲，简单跳过。

看下面的隶属ApplicationContext粉红色的 “高级容器”，依赖着 “低级容器”，这里说的是依赖，不是继承，它依赖着 “低级容器” 的getBean功能。而高级容器有更多的功能：支持不同的信息源头，可以访问文件资源，支持应用事件（Observer 模式）。

通常用户看到的就是 “高级容器”。 但BeanFactory也非常够用！

左边灰色区域的是 “低级容器”， 只负载加载Bean，获取Bean。容器其他的高级功能是没有的。例如上图画的refresh刷新Bean工厂所有配置，生命周期事件回调等。

**小结**

说了这么多，不知道有没有理解Spring IoC？这里总结一下：IoC在Spring中，只需要低级容器就可以实现，2 个步骤：

- 加载配置文件，解析成BeanDefinition放在Map里。

- 调用getBean的时候，从BeanDefinition所属的Map里，拿出Class对象进行实例化，同时，如果有依赖关系，将递归调用getBean方法 —— 完成依赖注入。
上面就是Spring低级容器（BeanFactory）的IoC。

至于高级容器ApplicationContext，它包含了低级容器的功能，当他执行refresh模板方法的时候，将刷新整个容器的Bean。同时其作为高级容器，包含了太多的功能。一句话，它不仅仅是IoC。它支持不同信息源头，支持BeanFactory工具类，支持层级容器，支持访问文件资源，支持事件发布通知，支持接口回调等等。

### 题11：spring-支持哪几种-bean-作用域<br/>


### 题12：spring-aop-中切入点和连接点什么关系<br/>


### 题13：spring-中支持那些-orm<br/>


### 题14：spring-如何处理线程并发问题<br/>


### 题15：spring-中如何定义-bean-的范围<br/>


### 题16：spring-aop-连接点和切入点是什么<br/>


### 题17：spring-中单例-bean-是线程安全的吗<br/>


### 题18：spring-中事务有哪几种传播行为<br/>


### 题19：什么是基于注解的容器配置<br/>


### 题20：spring-aop-和-aspectj-aop-有什么区别<br/>


### 题21：什么是-spring-框架<br/>


### 题22：解释一下-spring-aop-中的几个名词<br/>


### 题23：spring-提供几种配置方式设置元数据<br/>


### 题24：spring-依赖注入有几种实现方式<br/>


### 题25：解释-spring-框架中-bean-的生命周期<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")