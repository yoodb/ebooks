# Spring 中级面试题汇总附答案，2021年 Spring 面试题及答案大全

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring

### 题1：[Spring 中 @Component 和 @Bean 注解有什么区别？](/docs/Spring/Spring%20中级面试题汇总附答案，2021年%20Spring%20面试题及答案大全.md#题1spring-中-@component-和-@bean-注解有什么区别)<br/>
1）作用对象不同。@Component注解作用于类，而@Bean注解作用于方法。

2）@Component注解一般是通过类路径扫描来自动侦测及自动装配到Spring容器中，定义要扫描的路径可以使用@ComponentScan注解。@Bean注解一般是在标有该注解的方法中定义产生这个bean，通知Spring这是某个类的实例。

3）@Bean注解相比较@Component注解的自定义性更强，而且很多地方只能通过@Bean注解来完成注册bean。比如引用第三方库的类装配到Spring容器的时候，只能通过@Bean注解来实现。

使用@Bean注解示例：

```java
@Configuration
public class SpringConfig {
    @Bean
    public IUserService iUserService() {
        return new UserServiceImpl();
    }
}
```

上述代码等同于XML配置：

```xml
<beans>
    <bean id="iUserService" class="com.yoodb.UserServiceImpl"/>
</beans>
```

使用@Component注解示例：

```java
@Component("iUserService")
public class IUserService implements UserServiceImpl {
    
}
```

### 题2：[什么是 Spring 框架？](/docs/Spring/Spring%20中级面试题汇总附答案，2021年%20Spring%20面试题及答案大全.md#题2什么是-spring-框架)<br/>
Spring中文翻译过来是春天的意思，被称为J2EE的春天，是一个开源的轻量级的Java开发框架， 具有控制反转（IoC）和面向切面（AOP）两大核心。Java Spring框架通过声明式方式灵活地进行事务的管理，提高开发效率和质量。

Spring框架不仅限于服务器端的开发。从简单性、可测试性和松耦合的角度而言，任何 Java 应用都可以从Spring中受益。Spring框架还是一个超级粘合平台，除了自己提供功能外，还提供粘合其他技术和框架的能力。

**1）IOC 控制反转**

对象创建责任的反转，在Spring中BeanFacotory是IOC容器的核心接口，负责实例化，定位，配置应用程序中的对象及建立这些对象间的依赖。XmlBeanFacotory实现BeanFactory接口，通过获取xml配置文件数据，组成应用对象及对象间的依赖关系。

Spring中有3中注入方式，一种是set注入，另一种是接口注入，另一种是构造方法注入。

**2）AOP面向切面编程**

AOP是指纵向的编程，比如两个业务，业务1和业务2都需要一个共同的操作，与其往每个业务中都添加同样的代码，通过写一遍代码，让两个业务共同使用这段代码。

Spring中面向切面编程的实现有两种方式，一种是动态代理，一种是CGLIB，动态代理必须要提供接口，而CGLIB实现是由=有继承。

### 题3：[Spring 事务管理的方式有几种？](/docs/Spring/Spring%20中级面试题汇总附答案，2021年%20Spring%20面试题及答案大全.md#题3spring-事务管理的方式有几种)<br/>
Spring事务管理的方式有两种，编程式事务和声明式事务。

1、编程式事务：在代码中硬编码（不推荐使用）。

2、声明式事务：在配置文件中配置（推荐使用），分为基于XML的声明式事务和基于注解的声明式事务。

声明式事务管理方式是基于AOP技术实现的，实质上就是在方法执行前后进行拦截，然后在目标方法开始之前创建并加入事务，执行完目标方法后根据执行情况提交或回滚事务。

**声明式事务管理分为两种方式**

1）基于XML配置文件的方式；

2）业务方法上添加@Transactional注解，将事务规则应用到业务逻辑中。

### 题4：[一个 Spring Bean 定义包含什么？](/docs/Spring/Spring%20中级面试题汇总附答案，2021年%20Spring%20面试题及答案大全.md#题4一个-spring-bean-定义包含什么)<br/>
一个Spring Bean的定义包含容器必知的所有配置元数据，包括如何创建一个bean，它的生命周期详情及它的依赖。

### 题5：[解释一下 Spring AOP 中的几个名词？](/docs/Spring/Spring%20中级面试题汇总附答案，2021年%20Spring%20面试题及答案大全.md#题5解释一下-spring-aop-中的几个名词)<br/>
1）切面（Aspect）：切面是通知和切点的结合。通知和切点共同定义了切面的全部内容。在Spring AOP中，切面可以使用通用类（基于模式的风格）或者在普通类中以@AspectJ注解来实现。

2）连接点（Join point）：指方法，在Spring AOP中，一个连接点总是代表一个方法的执行。应用可能有数以千计的时机应用通知。这些时机被称为连接点。连接点是在应用执行过程中能够插入切面的一个点。这个点可以是调用方法时、抛出异常时、甚至修改一个字段时。切面代码可以利用这些点插入到应用的正常流程之中，并添加新的行为。

3）通知（Advice）：在AOP术语中，切面的工作被称为通知。

4）切入点（Pointcut）：切点的定义会匹配通知所要织入的一个或多个连接点。我们通常使用明确的类和方法名称，或是利用正则表达式定义所匹配的类和方法名称来指定这些切点。

5）引入（Introduction）：引入允许我们向现有类添加新方法或属性。

6）目标对象（Target Object）：被一个或者多个切面（aspect）所通知（advise）的对象。它通常是一个代理对象。也有人把它叫做 被通知（adviced） 对象。 既然Spring AOP是通过运行时代理实现的，这个对象永远是一个被代理（proxied）对象。

7）织入（Weaving）：织入是把切面应用到目标对象并创建新的代理对象的过程。在目标对象的生命周期里有多少个点可以进行织入：

- 编译期：切面在目标类编译时被织入。AspectJ的织入编译器是以这种方式织入切面的。

- 类加载期：切面在目标类加载到JVM时被织入。需要特殊的类加载器，它可以在目标类被引入应用之前增强该目标类的字节码。AspectJ5的加载时织入就支持以这种方式织入切面。

- 运行期：切面在应用运行的某个时刻被织入。一般情况下，在织入切面时，AOP容器会为目标对象动态地创建一个代理对象。Spring AOP就是以这种方式织入切面。

### 题6：[Spring Native 有什么优缺点？](/docs/Spring/Spring%20中级面试题汇总附答案，2021年%20Spring%20面试题及答案大全.md#题6spring-native-有什么优缺点)<br/>
**Spring Native优点**

1）立即启动，一般启动时间小于100ms；

2）更低的内存消耗；

3）独立部署，不再需要JVM；

4）同样的峰值性能要比JVM消耗的内存小。

**Spring Native缺点**

1）构建时间长；

2）只支持新的Springboot版本（Spring Native 0.9.0 supports Spring Boot 2.4.3, Spring Native 0.9.1 will support Spring Boot 2.4.4, etc.）

### 题7：[Spring 中事务如何指定回滚的异常？](/docs/Spring/Spring%20中级面试题汇总附答案，2021年%20Spring%20面试题及答案大全.md#题7spring-中事务如何指定回滚的异常)<br/>
在@Transaction注解中定义noRollbackFor和RollbackFor参数指定某种异常是否回滚。 

```java
@Transaction(noRollbackFor=RuntimeException.class) 
@Transaction(RollbackFor=Exception.class) 
```

### 题8：[Spring 事务都有哪些特性？](/docs/Spring/Spring%20中级面试题汇总附答案，2021年%20Spring%20面试题及答案大全.md#题8spring-事务都有哪些特性)<br/>
原子性（Atomicity）：事务是一个原子操作，由一系列动作组成。事务的原子性确保动作要么全部完成，要么完全不起作用。

一致性（Consistency）：一旦事务完成（不管成功还是失败），系统必须确保它所建模的业务处于一致的状态，而不会是部分完成部分失败。在现实中的数据不应该被破坏。

隔离性（Isolation）：可能有许多事务会同时处理相同的数据，因此每个事务都应该与其他事务隔离开来，防止数据损坏。

持久性（Durability）：一旦事务完成，无论发生什么系统错误，它的结果都不应该受到影响，这样就能从任何系统崩溃中恢复过来。通常情况下，事务的结果被写到持久化存储器中。

### 题9：[Spring 中事务有哪几种隔离级别？](/docs/Spring/Spring%20中级面试题汇总附答案，2021年%20Spring%20面试题及答案大全.md#题9spring-中事务有哪几种隔离级别)<br/>
Spring在TransactionDefinition接口类中定义了五个表示隔离级别的常量：

ISOLATION_DEFAULT：数据库默认隔离级别，Mysql默认采用REPEATABLE_READ隔离级别；Oracle默认采用的READ_COMMITTED隔离级别。

ISOLATION_READ_UNCOMMITTED：最低隔离级别，允许读取尚未提交的变更数据，可能会导致脏读、幻读或不可重复读。

ISOLATION_READ_COMMITTED：允许读取并发事务已经提交的数据，可以阻止脏读，但是幻读或不可重复读仍有可能发生。

ISOLATION_REPEATABLE_READ：对同字段多次读取结果都是一致的，除非数据是被本身事务所修改，可以阻止脏读和不可重复读，但幻读仍有可能发生。

ISOLATION_SERIALIZABLE：最高隔离级别，完全服从ACID的隔离级别。所有的事务依次逐个执行，这样事务之间完全不可能产生干扰，该级别可以防止脏读、不可重复读以及幻读。但是这将严重影响程序的性能。通常情况下不会使用该级别。

### 题10：[Spring AOP 代理是什么？](/docs/Spring/Spring%20中级面试题汇总附答案，2021年%20Spring%20面试题及答案大全.md#题10spring-aop-代理是什么)<br/>
代理是使用非常广泛的设计模式。简单来说，代理是一个看其他像另一个对象的对象，但它添加了一些特殊的功能。

Spring AOP是基于代理实现的。AOP代理是一个由AOP框架创建的用于在运行时实现切面协议的对象。

Spring AOP默认为AOP代理使用标准的JDK 动态代理。这使得任何接口（或者接口的集合）可以被代理。

Spring AOP也可以使用CGLIB代理。这对代理类而不是接口是必须的。

如果业务对象没有实现任何接口那么默认使用CGLIB。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")