# 2022年最全Dubbo面试题附答案解析大汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Dubbo

### 题1：[Dubbo 和 Dubbox 有哪些区别？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题1dubbo-和-dubbox-有哪些区别)<br/>
Dubbox是继Dubbo停止维护后，当当网基于Dubbo做的一个扩展项目，例如增加了Restful调用，更新了开源组件等。

### 题2：[Dubbo 在大数据量情况下使用什么协议？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题2dubbo-在大数据量情况下使用什么协议)<br/>
Dubbo的设计目的是为了满足高并发且数据量小的rpc调用，在大数据量下的性能表现并不好，建议使用rmi或http协议。

### 题3：[什么是 Dubbo 框架？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题3什么是-dubbo-框架)<br/>
Dubbo（读音[ˈdʌbəʊ]）是阿里巴巴公司开源的一个高性能优秀的服务框架，使得应用可通过高性能的 RPC 实现服务的输出和输入功能，可以和Spring框架无缝集成。

Dubbo提供了六大核心能力：面向接口代理的高性能RPC调用，智能容错和负载均衡，服务自动注册和发现，高度可扩展能力，运行期流量调度，可视化的服务治理与运维。

Dubbo是一款高性能、轻量级的开源Java RPC框架，它提供了三大核心能力：面向接口的远程方法调用，智能容错和负载均衡，以及服务自动注册和发现。

**核心组件**

Remoting: 网络通信框架，实现了 sync-over-async 和request-response 消息机制；

RPC: 一个远程过程调用的抽象，支持负载均衡、容灾和集群功能；

Registry: 服务目录框架用于服务的注册和服务事件发布和订阅。

### 题4：[Provider 上配置 Consumer 端的属性有哪些？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题4provider-上配置-consumer-端的属性有哪些)<br/>
1）timeout：方法调用超时。

2）retries：失败重试次数，默认重试2次。

3）loadbalance：负载均衡算法，默认随机。

4）actives 消费者端，最大并发调用限制。

### 题5：[Dubbo 服务之间调用是阻塞的吗？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题5dubbo-服务之间调用是阻塞的吗)<br/>
Dubbo默认是同步等待结果阻塞的，支持异步调用。

Dubbo是基于NIO的非阻塞实现并行调用，客户端不需要启动多线程即可完成并行调用多个远程服务，相对多线程开销较小，异步调用会返回一个Future对象。

### 题6：[Dubbo 支持服务多协议吗？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题6dubbo-支持服务多协议吗)<br/>
Dubbo允许配置多协议，在不同服务上支持不同协议或同一服务上同时支持多种协议。

### 题7：[Dubbo 超时的实现原理是什么？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题7dubbo-超时的实现原理是什么)<br/>
dubbo默认采用了netty做为网络组件，它属于一种NIO的模式。消费端发起远程请求后，线程不会阻塞等待服务端的返回，而是马上得到一个ResponseFuture，消费端通过不断的轮询机制判断结果是否有返回。

轮询需要特别注要的就是避免死循环，因此为了解决这个问题引入了超时机制，只在一定时间范围内做轮询，如果超时时间就返回超时异常。

### 题8：[Dubbo 支持集成 Spring Boot 吗？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题8dubbo-支持集成-spring-boot-吗)<br/>
Dubbo支持集成Spring Boot。

Apache Dubbo Spring Boot项目可以使用Dubbo作为RPC框架轻松创建Spring Boot应用程序。关注Java精选公众号，源码分析持续更新。

项目地址：

>https://github.com/apache/dubbo-spring-boot-project

更重要的是提供：

自动配置功能（例如，注释驱动、自动配置、外部化配置）。

生产就绪功能（例如，安全性、健康检查、外部化配置）。

Apache Dubbo是一个高性能、轻量级、基于java的RPC框架。Dubbo提供了三个关键功能，包括基于接口的远程调用、容错和负载均衡以及自动服务注册和发现。

### 题9：[Dubbo 超时设置的优先级是什么？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题9dubbo-超时设置的优先级是什么)<br/>
dubbo支持非常细粒度的超时设置包括：方法级别、接口级别和全局。

如果各个级别同时配置，优先级为：消费端方法级 > 服务端方法级 > 消费端接口级 > 服务端接口级 > 消费端全局 > 服务端全局。

### 题10：[Dubbo 支持服务降级吗？](/docs/Dubbo/2022年最全Dubbo面试题附答案解析大汇总.md#题10dubbo-支持服务降级吗)<br/>
Dubbo 2.2.0以上版本支持服务降级。

通过dubbo:reference中设置mock="return null"实现服务降级。

mock的值可以修改为true，再跟接口同一个路径下实现一个Mock类，命名规则是 “接口名称+Mock” 后缀。最后在Mock类里实现降级逻辑。

### 题11：为什么-dubbo-不用-jdk-spi而是要自己实现<br/>


### 题12：dubbo-中服务上线如何兼容旧版本<br/>


### 题13：dubbo-中有哪些节点角色<br/>


### 题14：除了-dubbo还了解那些分布式框架吗<br/>


### 题15：dubbo-服务降级失败如何重试<br/>


### 题16：dubbo-调用超时问题如何处理<br/>


### 题17：dubbo-配置文件如何加载到-spring-中<br/>


### 题18：dubbo-中服务暴露的过程<br/>


### 题19：dubbo-需要-web-容器启动吗<br/>


### 题20：dubbo-中使用了哪些设计模式<br/>


### 题21：服务提供者服务失效踢出是什么原理<br/>


### 题22：dubbo-服务读写如何实现容错策略<br/>


### 题23：dubbo-telnet-命令有什么用处<br/>


### 题24：dubbo-支持分布式事务吗<br/>


### 题25：你了解过-dubbo-源码吗<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")