# 2021年最新版 Dubbo 面试题汇总附答案

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Dubbo

### 题1：[Dubbo内置服务容器都有哪些？](/docs/Dubbo/2021年最新版%20Dubbo%20面试题汇总附答案.md#题1dubbo内置服务容器都有哪些)<br/>
**Spring Container**

自动加载META-INF/spring目录下的所有Spring配置。这个在源码中已经“写死”：

```shell
DEFAULT_SPRING_CONFIG = "classpath*:META-INF/spring/*.xml" 
```

因此服务端主配置需放到META-INF/spring/目录下。

**Jetty Container**

启动一个内嵌Jetty，用于汇报状态，配置在java命令-D参数或dubbo.properties中

```shell
dubbo.jetty.port=8080 ----配置jetty启动端口 
dubbo.jetty.directory=/foo/bar ----配置可通过jetty直接访问的目录，用于存放静态文件 
dubbo.jetty.page=log,status,system ----配置显示的页面，缺省加载所有页面
```

**Log4j Container**

自动配置log4j的配置，在多进程启动时，自动给日志文件按进程分目录。 配置在java命令-D参数或者dubbo.properties中
```shell
dubbo.log4j.file=/foo/bar.log ----配置日志文件路径 
dubbo.log4j.level=WARN ----配置日志级别
dubbo.log4j.subdirectory=20880 ----配置日志子目录，用于多进程启动，避免冲突
```

### 题2：[服务提供者服务失效踢出是什么原理？](/docs/Dubbo/2021年最新版%20Dubbo%20面试题汇总附答案.md#题2服务提供者服务失效踢出是什么原理)<br/>
服务失效踢出基于Zookeeper的临时节点原理。

### 题3：[Dubbo 中如何解决服务调用链过长的问题？](/docs/Dubbo/2021年最新版%20Dubbo%20面试题汇总附答案.md#题3dubbo-中如何解决服务调用链过长的问题)<br/>
Dubbo中可以使用Pinpoint和Apache Skywalking实现分布式服务追踪等方式，解决解决服务调用链过长的问题。

### 题4：[你了解过 Dubbo 源码吗？](/docs/Dubbo/2021年最新版%20Dubbo%20面试题汇总附答案.md#题4你了解过-dubbo-源码吗)<br/>
面试时不管看没看过，都要说看过一些，还是多了解了解Dubbo源码吧。

如果要了解Dubbo是必须看源码的，熟悉其原理，需要花点功夫，可以在网上找一些相关的教程，后续“Java精选”公众号上也会分享Dubbo的源码分析。

### 题5：[Dubbo 超时设置的优先级是什么？](/docs/Dubbo/2021年最新版%20Dubbo%20面试题汇总附答案.md#题5dubbo-超时设置的优先级是什么)<br/>
dubbo支持非常细粒度的超时设置包括：方法级别、接口级别和全局。

如果各个级别同时配置，优先级为：消费端方法级 > 服务端方法级 > 消费端接口级 > 服务端接口级 > 消费端全局 > 服务端全局。

### 题6：[Dubbo 中服务暴露的过程？](/docs/Dubbo/2021年最新版%20Dubbo%20面试题汇总附答案.md#题6dubbo-中服务暴露的过程)<br/>
Dubbo在Spring实例化完bean之后，在刷新容器最后一步发布ContextRefreshEvent事件的时候，通知实现了ApplicationListener的ServiceBean类进行回调onApplicationEvent事件方法，Dubbo会在这个方法中调用ServiceBean父类ServiceConfig的export方法，而该方法真正实现了服务的（异步或者非异步）发布。

### 题7：[Dubbo 有哪几种集群容错方案，默认是哪种？](/docs/Dubbo/2021年最新版%20Dubbo%20面试题汇总附答案.md#题7dubbo-有哪几种集群容错方案默认是哪种)<br/>
Dubbo有六种集群容错方案，默认是Failover Cluster。

| 集群容错方案 | 说明 |
|-|-|
|Failover Cluster|失败自动切换，自动重试其它服务器（默认）|
|Failfast Cluster|快速失败，立即报错，只发起一次调用|
|Failsafe Cluster|失败安全，出现异常时，直接忽略|
|Failback Cluster|失败自动恢复，记录失败请求，定时重发|
|Forking Cluster|并行调用多个服务器，只要一个成功即返回|
|Broadcast Cluster|广播逐个调用所有提供者，任意一个报错则报错|

### 题8：[Dubbo 中服务上线如何兼容旧版本？](/docs/Dubbo/2021年最新版%20Dubbo%20面试题汇总附答案.md#题8dubbo-中服务上线如何兼容旧版本)<br/>
可以使用版本号（version）过渡，多个不同版本的服务注册到注册中心，版本号不同的服务相互间不引用。类似服务分组的概念。

### 题9：[Dubbo 中如何保证服务安全调用？](/docs/Dubbo/2021年最新版%20Dubbo%20面试题汇总附答案.md#题9dubbo-中如何保证服务安全调用)<br/>
1、Dubbo和Zookeeper部署在内网，不对外网开放。

2、Zookeeper的注册可以添加用户权限认证。

3、Dubbo通过Token令牌防止用户绕过注册中心直连。

4、在注册中心上管理授权。

5、增加对接口参数校验。

6、提供IP、服务黑白名单，来控制服务所允许的调用方。

### 题10：[除了 Dubbo，还了解那些分布式框架吗？](/docs/Dubbo/2021年最新版%20Dubbo%20面试题汇总附答案.md#题10除了-dubbo还了解那些分布式框架吗)<br/>
Spring cloud、Facebook（脸书）的Thrift、Twitter的Finagle等。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")