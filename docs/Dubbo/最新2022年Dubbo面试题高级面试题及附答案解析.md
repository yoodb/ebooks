# 最新2022年Dubbo面试题高级面试题及附答案解析

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Dubbo

### 题1：[Dubbo 中如何解决服务调用链过长的问题？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题1dubbo-中如何解决服务调用链过长的问题)<br/>
Dubbo中可以使用Pinpoint和Apache Skywalking实现分布式服务追踪等方式，解决解决服务调用链过长的问题。

### 题2：[Dubbo 服务接口多种实现，如何注册调用？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题2dubbo-服务接口多种实现如何注册调用)<br/>
使用Dubbo分组，通过不同的实现用不同的组。

**服务提供者**

```xml
<dubbo:service interface="…" ref="…" group="实现1" />
<dubbo:service interface="…" ref="…" group="实现2" />
```

**服务消费者**

```xml
<dubbo:reference id="…" interface="…" group="实现1" />
<dubbo:reference id="…" interface="…" group="实现2" />
```

### 题3：[Dubbo 超时设置的优先级是什么？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题3dubbo-超时设置的优先级是什么)<br/>
dubbo支持非常细粒度的超时设置包括：方法级别、接口级别和全局。

如果各个级别同时配置，优先级为：消费端方法级 > 服务端方法级 > 消费端接口级 > 服务端接口级 > 消费端全局 > 服务端全局。

### 题4：[Dubbo 支持对结果进行缓存吗？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题4dubbo-支持对结果进行缓存吗)<br/>
Dubbo支持对结果进行缓存。

Dubbo 提供了声明式缓存，用于提高数据的访问速度，以减少用户加缓存的工作量。

```xml
<dubbo:reference cache="true" />
```

比普通配置文件增加标签cache="true"即可。

### 题5：[Dubbo 需要 Web 容器启动吗？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题5dubbo-需要-web-容器启动吗)<br/>
Dubbo不需要Web容器启动。

如果硬要使用Web容器（类似Tomcat应用服务器等），只会增加复杂性，且浪费服务器资源。

### 题6：[Dubbo 配置文件如何加载到 Spring 中？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题6dubbo-配置文件如何加载到-spring-中)<br/>
Spring容器在启动的时候会读取到Spring默认的一些schema以及Dubbo自定义的schema，每个schema都会对应一个自己的NamespaceHandler，NamespaceHandler里面通过BeanDefinitionParser来解析配置信息并转化为需要加载的bean对象。


### 题7：[说说 Dubbo Monitor 实现原理？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题7说说-dubbo-monitor-实现原理)<br/>
Consumer端在发起调用之前会先进入filter链；provider端在接收到请求时也是先进入filter链，然后才进行真正的业务逻辑处理。默认情况下，在consumer和provider的filter链中都会有Monitorfilter。

1、MonitorFilter向DubboMonitor发送数据 

2、DubboMonitor将数据进行聚合后（默认聚合1分钟的统计数据）暂存到

```java
ConcurrentMap<Statistics, AtomicReference> statisticsMap
```

然后使用一个含有3个线程（线程名字：DubboMonitorSendTimer）的线程池每隔1分钟，调用SimpleMonitorService遍历发送statisticsMap中的统计数据，每发送完毕一个，就重置当前的Statistics的AtomicReference。

3、SimpleMonitorService将这些聚合数据塞入 BlockingQueue queue 中（队列大写为 100000）。

4、SimpleMonitorService使用一个后台线程（线程名为：DubboMonitorAsyncWriteLogThread）将queue中的数据写入文件（该线程以死循环的形式来写）。

5、SimpleMonitorService 还会使用一个含有 1 个线程（线程名字：DubboMonitorTimer）的线程池每隔5分钟，将文件中的统计数据画成图表。

### 题8：[Dubbo 支持服务降级吗？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题8dubbo-支持服务降级吗)<br/>
Dubbo 2.2.0以上版本支持服务降级。

通过dubbo:reference中设置mock="return null"实现服务降级。

mock的值可以修改为true，再跟接口同一个路径下实现一个Mock类，命名规则是 “接口名称+Mock” 后缀。最后在Mock类里实现降级逻辑。

### 题9：[Dubbo 和 Dubbox 有哪些区别？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题9dubbo-和-dubbox-有哪些区别)<br/>
Dubbox是继Dubbo停止维护后，当当网基于Dubbo做的一个扩展项目，例如增加了Restful调用，更新了开源组件等。

### 题10：[Dubbo 中服务上线如何兼容旧版本？](/docs/Dubbo/最新2022年Dubbo面试题高级面试题及附答案解析.md#题10dubbo-中服务上线如何兼容旧版本)<br/>
可以使用版本号（version）过渡，多个不同版本的服务注册到注册中心，版本号不同的服务相互间不引用。类似服务分组的概念。

### 题11：dubbo-中都有哪些核心的配置<br/>


### 题12：dubbo-和-spring-cloud-有哪些区别<br/>


### 题13：dubbo-服务读写如何实现容错策略<br/>


### 题14：dubbo-支持哪几种配置方式<br/>


### 题15：dubbo-支持集成-spring-boot-吗<br/>


### 题16：dubbo-适用于哪些场景<br/>


### 题17：dubbo-支持服务多协议吗<br/>


### 题18：dubbo-如何优雅停机<br/>


### 题19：你了解过-dubbo-源码吗<br/>


### 题20：dubbo-调用超时问题如何处理<br/>


### 题21：服务提供者服务失效踢出是什么原理<br/>


### 题22：dubbo-管理控制台有什么功能<br/>


### 题23：dubbo四种负载均衡策略<br/>


### 题24：dubbo-停止更新了吗<br/>


### 题25：dubbo-推荐使用什么序列化框架还有别的选择吗<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")