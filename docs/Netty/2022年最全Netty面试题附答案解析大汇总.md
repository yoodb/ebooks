# 2022年最全Netty面试题附答案解析大汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Netty

### 题1：[Bootstrap 和 ServerBootstrap 了解过吗？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题1bootstrap-和-serverbootstrap-了解过吗)<br/>
BootStarp和ServerBootstrap都是一个启动引导类，相当于如果要使用Netty需要把相关信息配置到启动引导类中，这样Netty才能正确工作。

Bootstrap是客户端引导类，核心方法是connect，传入一个EventLoopGroup就可以

ServerBootstrap是服务断引导类，核心方法是bind，需要传入两个EventLoopGroup，一个负责接受连接，一个负责处理具体的连接上的网络IO任务。

### 题2：[Reactor 模型中有哪几个关键组件？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题2reactor-模型中有哪几个关键组件)<br/>
1、Reactor Reactor在一个单独的线程中运行，负责监听和分发事件，分发给适当的处理程序来对IO事件做出反应。它就像公司的电话接线员，它接听来自客户的电话并将线路转移到适当的联系人

2、Handlers处理程序执行I/O事件要完成的实际事件，类似于客户想要与之交谈的公司中的实际官员。Reactor通过调度适当的处理程序来响应I/O事件，处理程序执行非阻塞操作。

### 题3：[JDK 原生 NIO 程序有什么问题？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题3jdk-原生-nio-程序有什么问题)<br/>
JDK原生也有一套网络应用程序API，但是存在一系列问题，主要如下：

1、NIO的类库和API繁杂，使用麻烦，你需要熟练掌握Selector、ServerSocketChannel、SocketChannel、ByteBuffer等。

2、需要具备其它的额外技能做铺垫，例如熟悉Java多线程编程，因为NIO编程涉及到Reactor模式，你必须对多线程和网路编程非常熟悉，才能编写出高质量的NIO程序

3、可靠性能力补齐，开发工作量和难度都非常大。例如客户端面临断连重连、网络闪断、半包读写、失败缓存、网络拥塞和异常码流的处理等等，NIO编程的特点是功能开发相对容易，但是可靠性能力补齐工作量和难度都非常大

4、JDK NIO的BUG，例如臭名昭著的epoll bug，它会导致Selector空轮询，最终导致CPU 100%。官方声称在JDK1.6版本的update18修复了该问题，但是直到JDK1.7版本该问题仍旧存在，只不过该bug发生概率降低了一些而已，它并没有被根本解决。

### 题4：[什么是 Netty 的零拷贝？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题4什么是-netty-的零拷贝)<br/>
Netty的零拷贝主要包含三个方面：

Netty的接收和发送ByteBuffer采用DIRECT BUFFERS，使用堆外直接内存进行Socket 读写，不需要进行字节缓冲区的二次拷贝。如果使用传统的堆内存（HEAP BUFFERS）进行Socket读写，JVM会将堆内存Buffer拷贝一份到直接内存中，然后才写入Socket 中。相比于堆外直接内存，消息在发送过程中多了一次缓冲区的内存拷贝。

Netty提供了组合Buffer对象，可以聚合多个 ByteBuffer对象，用户可以像操作一个Buffer 那样方便的对组合 Buffer 进行操作，避免了传统通过内存拷贝的方式将几个小Buffer合并成一个大的Buffer。

Netty 的文件传输采用了transferTo方法，它可以直接将文件缓冲区的数据发送到目标 Channel，避免了传统通过循环write方式导致的内存拷贝问题。

### 题5：[Netty 和 Tomcat 有什么区别？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题5netty-和-tomcat-有什么区别)<br/>
作用不同：Tomcat是Servlet容器，可以视为Web服务器，而Netty是异步事件驱动的网络应用程序框架和工具用于简化网络编程，例如TCP和UDP套接字服务器。

协议不同：Tomcat是基于http协议的Web服务器，而Netty能通过编程自定义各种协议，因为Netty本身自己能编码/解码字节流，所有Netty可以实现，HTTP服务器、FTP服务器、UDP服务器、RPC服务器、WebSocket服务器、Redis的Proxy服务器、MySQL的Proxy服务器等等。

### 题6：[Netty 高性能表现在哪些方面？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题6netty-高性能表现在哪些方面)<br/>
**传输：** IO模型在很大程度上决定了框架的性能，相比于bio，netty建议采用异步通信模式，因为nio一个线程可以并发处理N个客户端连接和读写操作，这从根本上解决了传统同步阻塞IO一连接一线程模型，架构的性能、弹性伸缩能力和可靠性都得到了极大的提升。正如代码中所示，使用的是NioEventLoopGroup和NioSocketChannel来提升传输效率。

**协议：** Netty默认提供了对Google Protobuf的支持，也可以通过扩展Netty的编解码接口，用户可以实现其它的高性能序列化框架。

**线程：** netty使用了Reactor线程模型，但Reactor模型不同，对性能的影响也非常大，下面介绍常用的Reactor线程模型有三种，分别如下：

1、Reactor单线程模型：单线程模型的线程即作为NIO服务端接收客户端的TCP连接，又作为NIO客户端向服务端发起TCP连接，即读取通信对端的请求或者应答消息，又向通信对端发送消息请求或者应答消息。理论上一个线程可以独立处理所有IO相关的操作，但一个NIO线程同时处理成百上千的链路，性能上无法支撑，即便NIO线程的CPU负荷达到100%，也无法满足海量消息的编码、解码、读取和发送，又因为当NIO线程负载过重之后，处理速度将变慢，这会导致大量客户端连接超时，超时之后往往会进行重发，这更加重了NIO线程的负载，最终会导致大量消息积压和处理超时，NIO线程会成为系统的性能瓶颈。

2、Reactor多线程模型：有专门一个NIO线程用于监听服务端，接收客户端的TCP连接请求；网络IO操作(读写)由一个NIO线程池负责，线程池可以采用标准的JDK线程池实现。但百万客户端并发连接时，一个nio线程用来监听和接受明显不够，因此有了主从多线程模型。

3、主从Reactor多线程模型：利用主从NIO线程模型，可以解决1个服务端监听线程无法有效处理所有客户端连接的性能不足问题，即把监听服务端，接收客户端的TCP连接请求分给一个线程池。因此，在代码中可以看到，我们在server端选择的就是这种方式，并且也推荐使用该线程模型。在启动类中创建不同的EventLoopGroup实例并通过适当的参数配置，就可以支持上述三种Reactor线程模型。

### 题7：[Netty 有哪些应用场景？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题7netty-有哪些应用场景)<br/>
**作为RPC框架的网络通信工具：** 分布式系统中，不同服务节点之间经常需要相互调用，这个时候就需要RPC框架。

不同服务节点之间的通信是如何做的呢？可以使用Netty来做。比如调用另外一个节点的方法的话，至少是要让对方知道调用的是哪个类中的哪个方法以及相关参数。

**实现一个自己的HTTP服务器：** 通过Netty可以实现一个简单的HTTP服务器。说到HTTP服务器的话，作为Java后端开发，一般使用Tomcat比较多。一个最基本的HTTP服务器可要以处理常见的HTTPMethod的请求，比如POST请求、GET请求等。

**实现一个即时通讯系统：** 使用Netty可以实现一个可以聊天类似微信的即时通讯系统，这方面的开源项目还是比较多的，可以自行去Github找一找。或者关注微信公众号Java精选，据说经常更新一些不错的框架。

**实现消息推送系统：** 市面上有很多消息推送系统都是基于Netty来实现的。

典型的应用还有：阿里分布式服务框架Dubbo，默认使用Netty作为基础通信组件，还有RocketMQ也是使用Netty作为通讯的基础。

### 题8：[Netty 中有那些重要组件？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题8netty-中有那些重要组件)<br/>
Channel：Netty网络操作抽象类，它除了包括基本的I/O操作，如bind、connect、read、write等。

EventLoop：主要是配合Channel处理I/O操作，用来处理连接的生命周期中所发生的事情。

ChannelFuture：Netty框架中所有的I/O操作都为异步的，因此我们需要ChannelFuture的addListener()注册一个ChannelFutureListener监听事件，当操作执行成功或者失败时，监听就会自动触发返回结果。

ChannelHandler：充当了所有处理入站和出站数据的逻辑容器。ChannelHandler主要用来处理各种事件，这里的事件很广泛，比如可以是连接、数据接收、异常、数据转换等。

ChannelPipeline：为ChannelHandler链提供了容器，当channel创建时，就会被自动分配到它专属的ChannelPipeline，这个关联是永久性的。

### 题9：[EventloopGroup 和 EventLoop 有什么联系？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题9eventloopgroup-和-eventloop-有什么联系)<br/>
EventLoop可以理解为线程，那么Group就是线程池或者线程组，Netty的线程模型中，每一个channel需要绑定到一个固定的EventLoop进行后续的处理，Netty就是根据某一个算法从线程组中选择一个线程进行绑定的。

### 题10：[Netty 支持哪些心跳类型设置？](/docs/Netty/2022年最全Netty面试题附答案解析大汇总.md#题10netty-支持哪些心跳类型设置)<br/>
readerIdleTime：为读超时时间（即测试端一定时间内未接受到被测试端消息）。

writerIdleTime：为写超时时间（即测试端一定时间内向被测试端发送消息）。

allIdleTime：所有类型的超时时间。

### 题11：默认情况-netty-起多少线程何时启动<br/>


### 题12：阻塞和非阻塞有什么区别<br/>


### 题13：什么是-reactor-线程模型<br/>


### 题14：reactor-线程模型有几种模式<br/>


### 题15：同步和异步有什么区别<br/>


### 题16：netty-中如何解决-tcp-粘包和拆包问题<br/>


### 题17：说一说-nioeventloopgroup-源码处理过程<br/>


### 题18：netty-和-java-nio-有什么区别为什么不直接使用-jdk-nio-类库<br/>


### 题19：java-nio-包括哪些组成部分<br/>


### 题20：netty-粘包和拆包是如何处理的有哪些实现<br/>


### 题21：netty-核⼼组件有哪些分别有什么作⽤<br/>


### 题22：nioeventloopgroup-默认构造方法启动几个线程<br/>


### 题23：java-中-bionioaio-有什么区别<br/>


### 题24：netty-发送消息有几种方式<br/>


### 题25：什么是-netty<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")