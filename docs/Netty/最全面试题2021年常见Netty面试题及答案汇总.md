# 最全面试题2021年常见Netty面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Netty

### 题1：[Java NIO 包括哪些组成部分？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题1java-nio-包括哪些组成部分)<br/>
Buffer：与Channel进行交互，数据是从Channel读入缓冲区，从缓冲区写入Channel中的。

flip方法 ：反转此缓冲区，将position给limit，然后将position置为0，其实就是切换读写模式。

clear方法 ：清除此缓冲区，将position置为0，把capacity的值给limit。

rewind方法 ：重绕此缓冲区，将position置为0。

DirectByteBuffer可减少一次系统空间到用户空间的拷贝。但Buffer创建和销毁的成本更高，不可控，通常会用内存池来提高性能。直接缓冲区主要分配给那些易受基础系统的本机I/O操作影响的大型、持久的缓冲区。如果数据量比较小的中小应用情况下，可以考虑使用heapBuffer，由JVM进行管理。

Channel：表示IO源与目标打开的连接，是双向的，但不能直接访问数据，只能与Buffer 进行交互。通过源码可知，FileChannel的read方法和write方法都导致数据复制了两次。

Selector可使一个单独的线程管理多个Channel，open方法可创建Selector，register方法向多路复用器器注册通道，可以监听的事件类型：读、写、连接、accept。注册事件后会产生一个SelectionKey：它表示SelectableChannel 和Selector 之间的注册关系，wakeup方法：使尚未返回的第一个选择操作立即返回，唤醒的。

原因是：注册了新的channel或者事件；channel关闭，取消注册；优先级更高的事件触发（如定时器事件），希望及时处理。

Selector在Linux的实现类是EPollSelectorImpl，委托给EPollArrayWrapper实现，其中三个native方法是对epoll的封装，而EPollSelectorImpl. implRegister方法，通过调用epoll_ctl向epoll实例中注册事件，还将注册的文件描述符(fd)与SelectionKey的对应关系添加到fdToKey中，这个map维护了文件描述符与SelectionKey的映射。

fdToKey有时会变得非常大，因为注册到Selector上的Channel非常多（百万连接）；过期或失效的Channel没有及时关闭。fdToKey总是串行读取的，而读取是在select方法中进行的，该方法是非线程安全的。

Pipe：两个线程之间的单向数据连接，数据会被写到sink通道，从source通道读取。

NIO的服务端建立过程：Selector.open()：打开一个Selector；ServerSocketChannel.open()：创建服务端的Channel；bind()：绑定到某个端口上。并配置非阻塞模式；register()：注册Channel和关注的事件到Selector上；select()轮询拿到已经就绪的事件。

### 题2：[什么是 Reactor 线程模型？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题2什么是-reactor-线程模型)<br/>
Reactor是反应堆的意思，Reactor模型，是指通过一个或多个输入同时传递给服务处理器的服务请求的事件驱动处理模式。

Reactor一种事件驱动处理模型，类似于多路复用IO模型，包括三种角色：Reactor、Acceptor和Handler。Reactor用来监听事件，包括：连接建立、读就绪、写就绪等。然后针对监听到的不同事件，将它们分发给对应的线程去处理。其中acceptor处理客户端建立的连接，handler对读写事件进行业务处理。

服务端程序处理传入多路请求，并将它们同步分派给请求对应的处理线程，Reactor模式也叫Dispatcher模式，即I/O多了复用统一监听事件，收到事件后分发（Dispatch给某进程），是编写高性能网络服务器的必备技术之一。

### 题3：[Netty 中有哪些线程模型？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题3netty-中有哪些线程模型)<br/>
Netty通过Reactor模型基于多路复用器接收并处理用户请求，内部实现了两个线程池，boss线程池和work线程池，其中boss线程池的线程负责处理请求的accept事件，当接收到accept事件的请求时，把对应的socket封装到一个NioSocketChannel中，并交给work线程池，其中work线程池负责请求的read和write事件，由对应的Handler处理。

**单线程模型：** 所有I/O操作都由一个线程完成，即多路复用、事件分发和处理都是在一个Reactor线程上完成的。既要接收客户端的连接请求,向服务端发起连接，又要发送/读取请求或应答/响应消息。一个NIO线程同时处理成百上千的链路，性能上无法支撑，速度慢，若线程进入死循环，整个程序不可用，对于高负载、大并发的应用场景不合适。

**多线程模型：** 有一个NIO线程（Acceptor）只负责监听服务端，接收客户端的TCP连接请求；NIO线程池负责网络IO的操作，即消息的读取、解码、编码和发送；1个NIO线程可以同时处理N条链路，但是1个链路只对应1个NIO线程，这是为了防止发生并发操作问题。但在并发百万客户端连接或需要安全认证时，一个Acceptor线程可能会存在性能不足问题。

**主从多线程模型：** Acceptor线程用于绑定监听端口，接收客户端连接，将SocketChannel从主线程池的Reactor线程的多路复用器上移除，重新注册到Sub线程池的线程上，用于处理I/O的读写等操作，从而保证mainReactor只负责接入认证、握手等操作。

### 题4：[Reactor 线程模型有几种模式？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题4reactor-线程模型有几种模式)<br/>
**单Reactor单线程模式**

仅由一个线程来进行事件监控和事件处理，即整个消息处理流程都在一个线程中完成。

**单Reactor多线程模式**

对于连接上的读写事件，会使用线程池中的线程来执行该连接上的handler操作，即对读写事件的处理不会阻塞Reactor线程。

**主从Reactor多线程模式**

在单Reactor多线程模式的基础上，使用两个Reactor线程分别对建立连接事件和读写事件进行监听，每个Reactor线程拥有一个多路复用器。当主Reactor线程监听到连接建立事件后，创建SocketChannel，然后将SocketChannel注册到子Reactor线程的多路复用器中，使子Reactor线程监听连接的读写事件。

### 题5：[Netty 核⼼组件有哪些？分别有什么作⽤？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题5netty-核⼼组件有哪些分别有什么作⽤)<br/>
Channel：Netty对网络操作的抽象，包括了一些常见的网络IO操作，比如read,write等等，最常见的实现类：NioServerSocketChannel和NioSocketChannel，对应Bio中的ServerSocketChannel和SocketChannel。

EventLoop：负责监听网络事件并调用事件处理器进行相关的处理。

ChannelFuture：Netty是异步的，所有操作都可以通过ChannelFuture来实现绑定一个监听器然后执行结果成功与否的业务逻辑，或者把通过调用sync方法，把异步方法变成同步的。

ChannelHandler和ChannelPipeline：Netty底层的处理器是一个处理器链，链条上的每一个处理器都可以对消息进行处理，选择继续传递或者到此为止，一般我们业务会实现自己的解码器/心跳处理器和实际的业务处理Handler，然后按照顺序绑定到链条上。

### 题6：[Netty 和 Tomcat 有什么区别？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题6netty-和-tomcat-有什么区别)<br/>
作用不同：Tomcat是Servlet容器，可以视为Web服务器，而Netty是异步事件驱动的网络应用程序框架和工具用于简化网络编程，例如TCP和UDP套接字服务器。

协议不同：Tomcat是基于http协议的Web服务器，而Netty能通过编程自定义各种协议，因为Netty本身自己能编码/解码字节流，所有Netty可以实现，HTTP服务器、FTP服务器、UDP服务器、RPC服务器、WebSocket服务器、Redis的Proxy服务器、MySQL的Proxy服务器等等。

### 题7：[Netty 中如何解决 TCP 粘包和拆包问题？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题7netty-中如何解决-tcp-粘包和拆包问题)<br/>
Netty提供了3种类型的解码器对TCP 粘包/拆包问题进行处理：

**定长消息解码器：** FixedLengthFrameDecoder。发送方和接收方规定一个固定的消息长度，不够用空格等字符补全，这样接收方每次从接受到的字节流中读取固定长度的字节即可，长度不够就保留本次接受的数据，再在下一个字节流中获取剩下数量的字节数据。

**分隔符解码器：** LineBasedFrameDecoder或DelimiterBasedFrameDecoder。LineBasedFrameDecoder是行分隔符解码器，分隔符为\n或\r\n；DelimiterBasedFrameDecoder是自定义分隔符解码器，可以定义一个或多个分隔符。接收端在收到的字节流中查找分隔符，然后返回分隔符之前的数据，没找到就继续从下一个字节流中查找。

**数据长度解码器：** LengthFieldBasedFrameDecoder。将发送的消息分为header和body，header存储消息的长度（字节数），body是发送的消息的内容。同时发送方和接收方要协商好这个header的字节数，因为int能表示长度，long也能表示长度。接收方首先从字节流中读取前n（header的字节数）个字节（header），然后根据长度读取等量的字节，不够就从下一个数据流中查找。

### 题8：[Reactor 模型中有哪几个关键组件？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题8reactor-模型中有哪几个关键组件)<br/>
1、Reactor Reactor在一个单独的线程中运行，负责监听和分发事件，分发给适当的处理程序来对IO事件做出反应。它就像公司的电话接线员，它接听来自客户的电话并将线路转移到适当的联系人

2、Handlers处理程序执行I/O事件要完成的实际事件，类似于客户想要与之交谈的公司中的实际官员。Reactor通过调度适当的处理程序来响应I/O事件，处理程序执行非阻塞操作。

### 题9：[Netty 支持哪些心跳类型设置？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题9netty-支持哪些心跳类型设置)<br/>
readerIdleTime：为读超时时间（即测试端一定时间内未接受到被测试端消息）。

writerIdleTime：为写超时时间（即测试端一定时间内向被测试端发送消息）。

allIdleTime：所有类型的超时时间。

### 题10：[JDK 原生 NIO 程序有什么问题？](/docs/Netty/最全面试题2021年常见Netty面试题及答案汇总.md#题10jdk-原生-nio-程序有什么问题)<br/>
JDK原生也有一套网络应用程序API，但是存在一系列问题，主要如下：

1、NIO的类库和API繁杂，使用麻烦，你需要熟练掌握Selector、ServerSocketChannel、SocketChannel、ByteBuffer等。

2、需要具备其它的额外技能做铺垫，例如熟悉Java多线程编程，因为NIO编程涉及到Reactor模式，你必须对多线程和网路编程非常熟悉，才能编写出高质量的NIO程序

3、可靠性能力补齐，开发工作量和难度都非常大。例如客户端面临断连重连、网络闪断、半包读写、失败缓存、网络拥塞和异常码流的处理等等，NIO编程的特点是功能开发相对容易，但是可靠性能力补齐工作量和难度都非常大

4、JDK NIO的BUG，例如臭名昭著的epoll bug，它会导致Selector空轮询，最终导致CPU 100%。官方声称在JDK1.6版本的update18修复了该问题，但是直到JDK1.7版本该问题仍旧存在，只不过该bug发生概率降低了一些而已，它并没有被根本解决。

### 题11：netty-都有哪些特点<br/>


### 题12：netty-和-java-nio-有什么区别为什么不直接使用-jdk-nio-类库<br/>


### 题13：java-中-bionioaio-有什么区别<br/>


### 题14：eventloopgroup-和-eventloop-有什么联系<br/>


### 题15：netty-粘包和拆包是如何处理的有哪些实现<br/>


### 题16：netty-有哪些应用场景<br/>


### 题17：netty-中有那些重要组件<br/>


### 题18：netty-高性能表现在哪些方面<br/>


### 题19：同步和异步有什么区别<br/>


### 题20：netty-有哪些优势<br/>


### 题21：阻塞和非阻塞有什么区别<br/>


### 题22：默认情况-netty-起多少线程何时启动<br/>


### 题23：什么是长连接<br/>


### 题24：netty-发送消息有几种方式<br/>


### 题25：reactor-线程模型消息处理流程<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")