# 常见Netty面试题整合汇总包含答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Netty

### 题1：[Netty 高性能表现在哪些方面？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题1netty-高性能表现在哪些方面)<br/>
**传输：** IO模型在很大程度上决定了框架的性能，相比于bio，netty建议采用异步通信模式，因为nio一个线程可以并发处理N个客户端连接和读写操作，这从根本上解决了传统同步阻塞IO一连接一线程模型，架构的性能、弹性伸缩能力和可靠性都得到了极大的提升。正如代码中所示，使用的是NioEventLoopGroup和NioSocketChannel来提升传输效率。

**协议：** Netty默认提供了对Google Protobuf的支持，也可以通过扩展Netty的编解码接口，用户可以实现其它的高性能序列化框架。

**线程：** netty使用了Reactor线程模型，但Reactor模型不同，对性能的影响也非常大，下面介绍常用的Reactor线程模型有三种，分别如下：

1、Reactor单线程模型：单线程模型的线程即作为NIO服务端接收客户端的TCP连接，又作为NIO客户端向服务端发起TCP连接，即读取通信对端的请求或者应答消息，又向通信对端发送消息请求或者应答消息。理论上一个线程可以独立处理所有IO相关的操作，但一个NIO线程同时处理成百上千的链路，性能上无法支撑，即便NIO线程的CPU负荷达到100%，也无法满足海量消息的编码、解码、读取和发送，又因为当NIO线程负载过重之后，处理速度将变慢，这会导致大量客户端连接超时，超时之后往往会进行重发，这更加重了NIO线程的负载，最终会导致大量消息积压和处理超时，NIO线程会成为系统的性能瓶颈。

2、Reactor多线程模型：有专门一个NIO线程用于监听服务端，接收客户端的TCP连接请求；网络IO操作(读写)由一个NIO线程池负责，线程池可以采用标准的JDK线程池实现。但百万客户端并发连接时，一个nio线程用来监听和接受明显不够，因此有了主从多线程模型。

3、主从Reactor多线程模型：利用主从NIO线程模型，可以解决1个服务端监听线程无法有效处理所有客户端连接的性能不足问题，即把监听服务端，接收客户端的TCP连接请求分给一个线程池。因此，在代码中可以看到，我们在server端选择的就是这种方式，并且也推荐使用该线程模型。在启动类中创建不同的EventLoopGroup实例并通过适当的参数配置，就可以支持上述三种Reactor线程模型。

### 题2：[Reactor 线程模型有几种模式？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题2reactor-线程模型有几种模式)<br/>
**单Reactor单线程模式**

仅由一个线程来进行事件监控和事件处理，即整个消息处理流程都在一个线程中完成。

**单Reactor多线程模式**

对于连接上的读写事件，会使用线程池中的线程来执行该连接上的handler操作，即对读写事件的处理不会阻塞Reactor线程。

**主从Reactor多线程模式**

在单Reactor多线程模式的基础上，使用两个Reactor线程分别对建立连接事件和读写事件进行监听，每个Reactor线程拥有一个多路复用器。当主Reactor线程监听到连接建立事件后，创建SocketChannel，然后将SocketChannel注册到子Reactor线程的多路复用器中，使子Reactor线程监听连接的读写事件。

### 题3：[什么是 Reactor 线程模型？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题3什么是-reactor-线程模型)<br/>
Reactor是反应堆的意思，Reactor模型，是指通过一个或多个输入同时传递给服务处理器的服务请求的事件驱动处理模式。

Reactor一种事件驱动处理模型，类似于多路复用IO模型，包括三种角色：Reactor、Acceptor和Handler。Reactor用来监听事件，包括：连接建立、读就绪、写就绪等。然后针对监听到的不同事件，将它们分发给对应的线程去处理。其中acceptor处理客户端建立的连接，handler对读写事件进行业务处理。

服务端程序处理传入多路请求，并将它们同步分派给请求对应的处理线程，Reactor模式也叫Dispatcher模式，即I/O多了复用统一监听事件，收到事件后分发（Dispatch给某进程），是编写高性能网络服务器的必备技术之一。

### 题4：[什么是长连接？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题4什么是长连接)<br/>
连接是指TCP协议中如果两端想要传递数据，首先需要通过三次握手建立连接，握手完毕，连接就建立完毕，但是这个过程是比较消耗网络资源的。

短连接是指一轮数据传输完毕后，就断开连接，实现和管理都很方便，但是频繁的建立断开连接比较消耗网络资源。

长连接是指数据传输完毕后，不断开连接，下次有数据发送需求的时候再使用这个连接，省去了握手的过程。

### 题5：[Netty 和 Tomcat 有什么区别？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题5netty-和-tomcat-有什么区别)<br/>
作用不同：Tomcat是Servlet容器，可以视为Web服务器，而Netty是异步事件驱动的网络应用程序框架和工具用于简化网络编程，例如TCP和UDP套接字服务器。

协议不同：Tomcat是基于http协议的Web服务器，而Netty能通过编程自定义各种协议，因为Netty本身自己能编码/解码字节流，所有Netty可以实现，HTTP服务器、FTP服务器、UDP服务器、RPC服务器、WebSocket服务器、Redis的Proxy服务器、MySQL的Proxy服务器等等。

### 题6：[Bootstrap 和 ServerBootstrap 了解过吗？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题6bootstrap-和-serverbootstrap-了解过吗)<br/>
BootStarp和ServerBootstrap都是一个启动引导类，相当于如果要使用Netty需要把相关信息配置到启动引导类中，这样Netty才能正确工作。

Bootstrap是客户端引导类，核心方法是connect，传入一个EventLoopGroup就可以

ServerBootstrap是服务断引导类，核心方法是bind，需要传入两个EventLoopGroup，一个负责接受连接，一个负责处理具体的连接上的网络IO任务。

### 题7：[Netty 支持哪些心跳类型设置？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题7netty-支持哪些心跳类型设置)<br/>
readerIdleTime：为读超时时间（即测试端一定时间内未接受到被测试端消息）。

writerIdleTime：为写超时时间（即测试端一定时间内向被测试端发送消息）。

allIdleTime：所有类型的超时时间。

### 题8：[什么是 Netty 的零拷贝？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题8什么是-netty-的零拷贝)<br/>
Netty的零拷贝主要包含三个方面：

Netty的接收和发送ByteBuffer采用DIRECT BUFFERS，使用堆外直接内存进行Socket 读写，不需要进行字节缓冲区的二次拷贝。如果使用传统的堆内存（HEAP BUFFERS）进行Socket读写，JVM会将堆内存Buffer拷贝一份到直接内存中，然后才写入Socket 中。相比于堆外直接内存，消息在发送过程中多了一次缓冲区的内存拷贝。

Netty提供了组合Buffer对象，可以聚合多个 ByteBuffer对象，用户可以像操作一个Buffer 那样方便的对组合 Buffer 进行操作，避免了传统通过内存拷贝的方式将几个小Buffer合并成一个大的Buffer。

Netty 的文件传输采用了transferTo方法，它可以直接将文件缓冲区的数据发送到目标 Channel，避免了传统通过循环write方式导致的内存拷贝问题。

### 题9：[Netty 有哪些应用场景？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题9netty-有哪些应用场景)<br/>
**作为RPC框架的网络通信工具：** 分布式系统中，不同服务节点之间经常需要相互调用，这个时候就需要RPC框架。

不同服务节点之间的通信是如何做的呢？可以使用Netty来做。比如调用另外一个节点的方法的话，至少是要让对方知道调用的是哪个类中的哪个方法以及相关参数。

**实现一个自己的HTTP服务器：** 通过Netty可以实现一个简单的HTTP服务器。说到HTTP服务器的话，作为Java后端开发，一般使用Tomcat比较多。一个最基本的HTTP服务器可要以处理常见的HTTPMethod的请求，比如POST请求、GET请求等。

**实现一个即时通讯系统：** 使用Netty可以实现一个可以聊天类似微信的即时通讯系统，这方面的开源项目还是比较多的，可以自行去Github找一找。或者关注微信公众号Java精选，据说经常更新一些不错的框架。

**实现消息推送系统：** 市面上有很多消息推送系统都是基于Netty来实现的。

典型的应用还有：阿里分布式服务框架Dubbo，默认使用Netty作为基础通信组件，还有RocketMQ也是使用Netty作为通讯的基础。

### 题10：[Netty 和 Java NIO 有什么区别，为什么不直接使用 JDK NIO 类库？](/docs/Netty/常见Netty面试题整合汇总包含答案.md#题10netty-和-java-nio-有什么区别为什么不直接使用-jdk-nio-类库)<br/>
1、NIO的类库和API繁杂，使用麻烦，你需要熟练掌握Selector,ServerSocketChannel、SocketChannel、ByteBuffer等。

2、需要具备其他的额外技能做铺垫，例如熟悉Java多线程编程。这是因为NIO编程涉及到Reactor模式，你必须对多线程和网络编程非常熟悉，才能写出高质量的NIO程序。

3、可靠性能力补齐，工作量和难度非常大。例如客户端面临断连重连、网络闪断、半包读写、失败缓存、网络拥塞和异常码流的处理等问题，NIO编程的特点是功能开发相对容易，但是可靠性能力补齐的工作量和难度都非常大。

4、JDK NIO的BUG，例如epoll bug，它会导致Selector空轮询，最终导致CPU 100%。官方验证例子基于以上原因，在大多数场景下，不建议直接使用JDK的NIO类库，除非你精通NIO编程或者有特殊的需求。在绝大多数的业务场景中，我们可以使用NIO框架Netty来进行NIO编程，它既可以作为客户端也可以作为服务端，同时支持UDP和异步文件传输，功能非常强大。

### 题11：同步和异步有什么区别<br/>


### 题12：netty-有哪些优势<br/>


### 题13：eventloopgroup-和-eventloop-有什么联系<br/>


### 题14：阻塞和非阻塞有什么区别<br/>


### 题15：netty-核⼼组件有哪些分别有什么作⽤<br/>


### 题16：默认情况-netty-起多少线程何时启动<br/>


### 题17：nioeventloopgroup-默认构造方法启动几个线程<br/>


### 题18：netty-粘包和拆包是如何处理的有哪些实现<br/>


### 题19：什么是-netty<br/>


### 题20：reactor-线程模型消息处理流程<br/>


### 题21：说一说-nioeventloopgroup-源码处理过程<br/>


### 题22：netty-都有哪些特点<br/>


### 题23：reactor-模型中有哪几个关键组件<br/>


### 题24：netty-中有那些重要组件<br/>


### 题25：java-nio-包括哪些组成部分<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")