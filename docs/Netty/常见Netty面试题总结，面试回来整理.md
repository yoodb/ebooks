# 常见Netty面试题总结，面试回来整理

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Netty

### 题1：[什么是 Reactor 线程模型？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题1什么是-reactor-线程模型)<br/>
Reactor是反应堆的意思，Reactor模型，是指通过一个或多个输入同时传递给服务处理器的服务请求的事件驱动处理模式。

Reactor一种事件驱动处理模型，类似于多路复用IO模型，包括三种角色：Reactor、Acceptor和Handler。Reactor用来监听事件，包括：连接建立、读就绪、写就绪等。然后针对监听到的不同事件，将它们分发给对应的线程去处理。其中acceptor处理客户端建立的连接，handler对读写事件进行业务处理。

服务端程序处理传入多路请求，并将它们同步分派给请求对应的处理线程，Reactor模式也叫Dispatcher模式，即I/O多了复用统一监听事件，收到事件后分发（Dispatch给某进程），是编写高性能网络服务器的必备技术之一。

### 题2：[Java 中 BIO、NIO、AIO 有什么区别？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题2java-中-bionioaio-有什么区别)<br/>
**BIO**

Block IO是指同步阻塞式IO，就是平常使用的传统IO，它的特点是模式简单使用方便，并发处理能力低。

服务器实现模式为一个连接一个线程，即客户端有连接请求时服务器端就需要启动一个线程进行处理，如果这个连接不做任何事情会造成不必要的线程开销，当然可以通过线程池机制改善。

BIO方式适用于连接数目比较小且固定的架构，这种方式对服务器资源要求比较高，并发局限于应用中，JDK1.4以前的唯一选择，但程序直观简单易理解。

**NIO**
Non IO是指同步非阻塞IO，是传统IO的升级，客户端和服务器端通过Channel（通道）通讯，实现了多路复用。

同步非阻塞，服务器实现模式为一个请求一个线程，即客户端发送的连接请求都会注册到多路复用器上，多路复用器轮询到连接有I/O请求时才启动一个线程进行处理。

NIO方式适用于连接数目多且连接比较短（轻操作）的架构，比如聊天服务器，并发局限于应用中，编程比较复杂，JDK1.4开始支持。

**AIO**

Asynchronous IO是指NIO的升级，也叫NIO2，实现了异步非堵塞IO，异步IO的操作基于事件和回调机制。

服务器实现模式为一个有效请求一个线程，客户端的I/O请求都是由OS先完成了再通知服务器应用去启动线程进行处理。

AIO方式适用于连接数目多且连接比较长（重操作）的架构，比如相册服务器，充分调用OS参与并发操作，编程比较复杂，JDK7开始支持。

### 题3：[EventloopGroup 和 EventLoop 有什么联系？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题3eventloopgroup-和-eventloop-有什么联系)<br/>
EventLoop可以理解为线程，那么Group就是线程池或者线程组，Netty的线程模型中，每一个channel需要绑定到一个固定的EventLoop进行后续的处理，Netty就是根据某一个算法从线程组中选择一个线程进行绑定的。

### 题4：[Netty 和 Tomcat 有什么区别？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题4netty-和-tomcat-有什么区别)<br/>
作用不同：Tomcat是Servlet容器，可以视为Web服务器，而Netty是异步事件驱动的网络应用程序框架和工具用于简化网络编程，例如TCP和UDP套接字服务器。

协议不同：Tomcat是基于http协议的Web服务器，而Netty能通过编程自定义各种协议，因为Netty本身自己能编码/解码字节流，所有Netty可以实现，HTTP服务器、FTP服务器、UDP服务器、RPC服务器、WebSocket服务器、Redis的Proxy服务器、MySQL的Proxy服务器等等。

### 题5：[Netty 发送消息有几种方式？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题5netty-发送消息有几种方式)<br/>
Netty 有两种发送消息的方式：

一种是直接写入Channel中，消息从ChannelPipeline当中尾部开始移动；

另一种是写入和ChannelHandler绑定的ChannelHandlerContext中，消息从ChannelPipeline中的下一个ChannelHandler中移动。

### 题6：[什么是 Netty 的零拷贝？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题6什么是-netty-的零拷贝)<br/>
Netty的零拷贝主要包含三个方面：

Netty的接收和发送ByteBuffer采用DIRECT BUFFERS，使用堆外直接内存进行Socket 读写，不需要进行字节缓冲区的二次拷贝。如果使用传统的堆内存（HEAP BUFFERS）进行Socket读写，JVM会将堆内存Buffer拷贝一份到直接内存中，然后才写入Socket 中。相比于堆外直接内存，消息在发送过程中多了一次缓冲区的内存拷贝。

Netty提供了组合Buffer对象，可以聚合多个 ByteBuffer对象，用户可以像操作一个Buffer 那样方便的对组合 Buffer 进行操作，避免了传统通过内存拷贝的方式将几个小Buffer合并成一个大的Buffer。

Netty 的文件传输采用了transferTo方法，它可以直接将文件缓冲区的数据发送到目标 Channel，避免了传统通过循环write方式导致的内存拷贝问题。

### 题7：[Reactor 模型中有哪几个关键组件？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题7reactor-模型中有哪几个关键组件)<br/>
1、Reactor Reactor在一个单独的线程中运行，负责监听和分发事件，分发给适当的处理程序来对IO事件做出反应。它就像公司的电话接线员，它接听来自客户的电话并将线路转移到适当的联系人

2、Handlers处理程序执行I/O事件要完成的实际事件，类似于客户想要与之交谈的公司中的实际官员。Reactor通过调度适当的处理程序来响应I/O事件，处理程序执行非阻塞操作。

### 题8：[什么是 Netty？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题8什么是-netty)<br/>
Netty是一款基于NIO（Nonblocking I/O，非阻塞IO）开发的网络通信框架，对比于BIO（Blocking I/O，阻塞IO），它的并发性能得到了很大提高。难能可贵的是，在保证快速和易用性的同时，并没有丧失可维护性和性能等优势。使用它可以快速简单地开发网络应用程序。

Netty极大地简化并优化了TCP和UDP套接字服务器等网络编程,并且性能以及安全性等很多方面甚至都要更好。

支持多种协议如FTP、SMTP、HTTP以及各种二进制和基于文本的传统协议。

官方话术就是Netty成功地找到了一种在不妥协可维护性和性能的情况下实现易于开发，性能，稳定性和灵活性的方法。

### 题9：[Netty 粘包和拆包是如何处理的，有哪些实现？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题9netty-粘包和拆包是如何处理的有哪些实现)<br/>
消息定长，报文大小固定长度，不够空格补全，发送和接收方遵循相同的约定，这样即使粘包了通过接收方编程实现获取定长报文也能区分。

包尾添加特殊分隔符，例如每条报文结束都添加回车换行符（例如FTP协议）或者指定特殊字符作为报文分隔符，接收方通过特殊分隔符切分报文区分。

将消息分为消息头和消息体，消息头中包含表示信息的总长度（或者消息体长度）的字段。

### 题10：[Reactor 线程模型有几种模式？](/docs/Netty/常见Netty面试题总结，面试回来整理.md#题10reactor-线程模型有几种模式)<br/>
**单Reactor单线程模式**

仅由一个线程来进行事件监控和事件处理，即整个消息处理流程都在一个线程中完成。

**单Reactor多线程模式**

对于连接上的读写事件，会使用线程池中的线程来执行该连接上的handler操作，即对读写事件的处理不会阻塞Reactor线程。

**主从Reactor多线程模式**

在单Reactor多线程模式的基础上，使用两个Reactor线程分别对建立连接事件和读写事件进行监听，每个Reactor线程拥有一个多路复用器。当主Reactor线程监听到连接建立事件后，创建SocketChannel，然后将SocketChannel注册到子Reactor线程的多路复用器中，使子Reactor线程监听连接的读写事件。

### 题11：同步和异步有什么区别<br/>


### 题12：netty-支持哪些心跳类型设置<br/>


### 题13：nioeventloopgroup-默认构造方法启动几个线程<br/>


### 题14：bootstrap-和-serverbootstrap-了解过吗<br/>


### 题15：netty-核⼼组件有哪些分别有什么作⽤<br/>


### 题16：netty-和-java-nio-有什么区别为什么不直接使用-jdk-nio-类库<br/>


### 题17：阻塞和非阻塞有什么区别<br/>


### 题18：netty-中有哪些线程模型<br/>


### 题19：jdk-原生-nio-程序有什么问题<br/>


### 题20：什么是长连接<br/>


### 题21：netty-中有那些重要组件<br/>


### 题22：reactor-线程模型消息处理流程<br/>


### 题23：java-nio-包括哪些组成部分<br/>


### 题24：默认情况-netty-起多少线程何时启动<br/>


### 题25：说一说-nioeventloopgroup-源码处理过程<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")