# 最新面试题2021年Spring MVC面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[Spring MVC 和 Struts2 有哪些区别？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题1spring-mvc-和-struts2-有哪些区别)<br/>
Spring MVC的入口是一个servlet即前端控制器（DispatchServlet），而Struts2入口是一个filter过虑器（StrutsPrepareAndExecuteFilter）。

Spring MVC是基于方法开发（一个url对应一个方法），请求参数传递到方法的形参，可以设计为单例或多例（建议单例），Struts2是基于类开发，传递参数是通过类的属性，只能设计为多例。

Struts采用值栈存储请求和响应的数据，通过OGNL存取数据，而Spring MVC通过参数解析器将request请求内容解析，并给方法形参赋值，将数据和视图封装成ModelAndView对象，最后将ModelAndView中的模型数据通过reques域传输到页面。其中Jsp视图解析器默认使用jstl。

### 题2：[Spring MVC 中文件上传有哪些需要注意事项？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题2spring-mvc-中文件上传有哪些需要注意事项)<br/>
Spring MVC提供了两种上传方式配置

基于commons-fileupload.jar

```java
org.springframework.web.multipart.commons.CommonsMultipartResolver
```

基于servlet3.0+

```java
org.springframework.web.multipart.support.StandardServletMultipartResolver
```

1、页面form中提交方式enctype="multipart/form-data"的数据时，需要springmvc对multipart类型的数据进行解析。

2、在springmvc.xml中配置multipart类型解析器。

3、方法中使用MultipartFile attach实现单个文件上传或者使用MultipartFile[] attachs实现多个文件上传。

### 题3：[Spring MVC 如何设置重定向和转发？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题3spring-mvc-如何设置重定向和转发)<br/>
在返回值前面加“forward:”参数，可以实现转发。

```java
"forward:getName.do?name=Java精选"
```

在返回值前面加“redirect:”参数，可以实现重定向。

```java
"redirect:https://blog.yoodb.com"

### 题4：[Spring MVC模块的作用是什么？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题4spring-mvc模块的作用是什么)<br/>
Spring提供了MVC框架来构建Web应用程序。

Spring可以很容易地与其他MVC框架集成，但是Spring的MVC框架是更好的选择，因为它使用IoC来提供控制器逻辑与业务对象的清晰分离。

使用Spring MVC，可以声明性地将请求参数绑定到业务对象。

### 题5：[如何开启注解处理器和适配器？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题5如何开启注解处理器和适配器)<br/>
在配置文件中（一般命名为springmvc.xml 文件）通过开启配置：

```xml
<mvc:annotation-driven>
```
来实现注解处理器和适配器的开启。

### 题6：[说一说对 RESTful 的理解及项目中的使用？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题6说一说对-restful-的理解及项目中的使用)<br/>
RESTful一种软件架构风格、设计风格，而不是标准，只是提供了一组设计原则和约束条件。

RESTful主要用于客户端和服务器交互类的软件。

REST指的是一组架构约束条件和原则。

满足这些约束条件和原则的应用程序或设计就是RESTful。

RESTful结构清晰、符合标准、易于理解、扩展方便，所以正得到越来越多网站的采用。


### 题7：[Spring MVC 中 @RequestMapping 注解有什么属性？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题7spring-mvc-中-@requestmapping-注解有什么属性)<br/>
RequestMapping是一个用来处理请求地址映射的注解，可用于类或方法上。用于类上，表示类中的所有响应请求的方法都是以该地址作为父路径。

**RequestMapping注解有六个属性**

value

指定请求的实际地址，指定的地址可以是URI Template 模式（后面将会说明）；

method

指定请求的method类型， GET、POST、PUT、DELETE等；

consumes

指定处理请求的提交内容类型（Content-Type），例如application/json, text/html;

produces

指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回；

params

指定request中必须包含某些参数值是，才让该方法处理。

headers

指定request中必须包含某些指定的header值，才能让该方法处理请求。


### 题8：[RequestMethod 可以同时支持POST和GET请求访问吗？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题8requestmethod-可以同时支持post和get请求访问吗)<br/>


POST请求访问

```java
@RequestMapping(value="/wx/getUserById",method = RequestMethod.POST)
```

GET请求访问

```java
@RequestMapping(value="/wx/getUserById/{userId}",method = RequestMethod.GET)
```

同时支持POST和GET请求访问

```java
@RequestMapping(value="/subscribe/getSubscribeList/{customerCode}/{sid}/{userId}")
```
**RequestMethod常用的参数**

GET（SELECT）：从服务器查询，在服务器通过请求参数区分查询的方式。
  
POST（CREATE）：在服务器新建一个资源，调用insert操作。 
 
PUT（UPDATE）：在服务器更新资源，调用update操作。  

DELETE（DELETE）：从服务器删除资源，调用delete语句。

### 题9：[Spring MVC 中系统是如何分层？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题9spring-mvc-中系统是如何分层)<br/>
表现层（UI）：数据的展现、操作页面、请求转发。

业务层（服务层）：封装业务处理逻辑。

持久层（数据访问层）：封装数据访问逻辑。

各层之间的关系：MVC是一种表现层的架构，表现层通过接口调用业务层，业务层通过接口调用持久层，当下一层发生改变，不影响上一层的数据。 

### 题10：[Spring MVC 中如何进行异常处理？](/docs/Spring%20MVC/最新面试题2021年Spring%20MVC面试题及答案汇总.md#题10spring-mvc-中如何进行异常处理)<br/>
**局部异常处理**

局部异常处理是指当类中发生异常时，由方法来处理，该方法的参数类型为Exception，而Exception是所有异常的父类，故由该参数来接收异常对象。

步骤说明

1）在controller类中定义处理异常的方法，添加注解@ExceptionHandler，方法的参数类型为Exception，并通过getMessage()方法获取异常信息，再将异常信息保存，最后跳转到错误页面，使用mv.setViewName("error")，这里通过ModelAndView将异常信息保存到request中。

2）创建error.jsp页面，在page中添加isErrorPage="true"，表示该页面为错误页面，再从request中获取错误信息，并显示到页面。

**定义全局异常类**

1）创建异常类，添加注解@ControllerAdvice表示当发生异常时由该异常类来处理，全局的异常类处理异常。

在异常类中定义处理异常的方法，该方法的定义与定义局部的异常处理方法一致。

### 题11：spring-mvc-中常用的注解包含哪些<br/>


### 题12：说一说-spring-mvc-注解原理<br/>


### 题13：spring-mvc-中日期类型参数如何接收<br/>


### 题14：spring-mvc-如何将-model-中数据存放到-session<br/>


### 题15：spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别<br/>


### 题16：spring-mvc-控制器是单例的吗?<br/>


### 题17：spring-mvc-执行流程是什么<br/>


### 题18：spring-mvc-中函数的返回值是什么<br/>


### 题19：spring-mvc-如何与-ajax-相互调用<br/>


### 题20：什么是-spring-mvc-框架<br/>


### 题21：spring-mvc-如何解决请求中文乱码问题<br/>


### 题22：spring-mvc-中如何拦截-get-方式请求<br/>


### 题23：spring-mvc-中拦截器如何使用<br/>


### 题24：spring-mvc-常用的注解有哪些<br/>


### 题25：spring-mvc-控制器注解一般适用什么可以适用什么替代<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")