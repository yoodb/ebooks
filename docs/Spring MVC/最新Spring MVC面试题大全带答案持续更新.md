# 最新Spring MVC面试题大全带答案持续更新

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[Spring MVC 中如何进行异常处理？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题1spring-mvc-中如何进行异常处理)<br/>
**局部异常处理**

局部异常处理是指当类中发生异常时，由方法来处理，该方法的参数类型为Exception，而Exception是所有异常的父类，故由该参数来接收异常对象。

步骤说明

1）在controller类中定义处理异常的方法，添加注解@ExceptionHandler，方法的参数类型为Exception，并通过getMessage()方法获取异常信息，再将异常信息保存，最后跳转到错误页面，使用mv.setViewName("error")，这里通过ModelAndView将异常信息保存到request中。

2）创建error.jsp页面，在page中添加isErrorPage="true"，表示该页面为错误页面，再从request中获取错误信息，并显示到页面。

**定义全局异常类**

1）创建异常类，添加注解@ControllerAdvice表示当发生异常时由该异常类来处理，全局的异常类处理异常。

在异常类中定义处理异常的方法，该方法的定义与定义局部的异常处理方法一致。

### 题2：[Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题2spring-mvc-中函数的返回值是什么)<br/>
Spring MVC的返回值可以有很多类型，如String、ModelAndView等，但事一般使用String比较友好。

### 题3：[Spring MVC模块的作用是什么？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题3spring-mvc模块的作用是什么)<br/>
Spring提供了MVC框架来构建Web应用程序。

Spring可以很容易地与其他MVC框架集成，但是Spring的MVC框架是更好的选择，因为它使用IoC来提供控制器逻辑与业务对象的清晰分离。

使用Spring MVC，可以声明性地将请求参数绑定到业务对象。

### 题4：[Spring MVC 控制器是单例的吗?](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题4spring-mvc-控制器是单例的吗?)<br/>
默认情况下是单例模式，在多线程进行访问时存在线程安全的问题。

解决方法可以在控制器中不要写成员变量，这是因为单例模式下定义成员变量是线程不安全的。

通过@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)或@Scope("prototype")可以实现多例模式。但是不建议使用同步，因为会影响性能。

使用单例模式是为了性能，无需频繁进行初始化操作，同时也没有必要使用多例模式。

### 题5：[Spring MVC 中 @RequestMapping 注解用在类上有什么作用？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题5spring-mvc-中-@requestmapping-注解用在类上有什么作用)<br/>
@RequestMapping注解是一个用来处理请求地址映射的注解，可用于类或方法上。

用于类上，则表示类中的所有响应请求的方法都是以该地址作为父路径。

比如

```java
@Controller
@RequestMapping("/test/")
public class TestController{

}
```

启动的是本地服务，默认端口是8080，通过浏览器访问的路径就是如下地址：

```shell
http://localhost:8080/test/
```

### 题6：[Spring MVC 如何解决请求中文乱码问题？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题6spring-mvc-如何解决请求中文乱码问题)<br/>
解决GET请求中文乱码问题

方式一：每次请求前使用encodeURI对URL进行编码。

方式二：在应用服务器上配置URL编码格式，在Tomcat配置文件server.xml中增加URIEncoding="UTF-8"，然后重启Tomcat即可。

```xml
<ConnectorURIEncoding="UTF-8" 
    port="8080"  maxHttpHeaderSize="8192"  maxThreads="150" 
    minSpareThreads="25"  maxSpareThreads="75"connectionTimeout="20000" 		
    disableUploadTimeout="true" URIEncoding="UTF-8" />
```

解决POST请求中文乱码问题

方式一：在request解析数据时设置编码格式：

```java
request.setCharacterEncoding("UTF-8");
```

方式二：使用Spring提供的编码过滤器

在web.xml文件中配置字符编码过滤器，增加如下配置：
```xml
<!--编码过滤器-->                                                                                                                
<filter>                                                                
	<filter-name>encodingFilter</filter-name>                           
	<filter-class>                                                      
		org.springframework.web.filter.CharacterEncodingFilter          
	</filter-class>                                                     
	<!-- 开启异步支持-->                                               
	<async-supported>true</async-supported>                             
	<init-param>                                                        
		<param-name>encoding</param-name>                               
		<param-value>utf-8</param-value>                                
	</init-param>                                                       
</filter>                                                               
<filter-mapping>                                                        
	<filter-name>encodingFilter</filter-name>                           
	<url-pattern>/*</url-pattern>                                       
</filter-mapping>     
```

该过滤器的作用就是强制所有请求和响应设置编码格式：
```java
request.setCharacterEncoding("UTF-8");
response.setCharacterEncoding("UTF-8");
```

### 题7：[Spring MVC 中 @PathVariable 和 @RequestParam 有哪些区别？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题7spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别)<br/>
@RequestParam与@PathVariable为spring的注解，都可以用于在Controller层接收前端传递的数据，不过两者的应用场景不同。

@PathVariable主要用于接收http://host:port/path/{参数值}数据。

@PathVariable请求接口时，URL是 http://www.yoodb.com/user/getUserById/2

@RequestParam主要用于接收http://host:port/path?参数名=值数据值。

@RequestParam请求接口时，URL是 http://www.yoodb.com/user/getUserById?userId=1


**@PathVariable用法**

```java
@RequestMapping(value = "/yoodb/{id}",method = RequestMethod.DELETE)
public Result getUserById(@PathVariable("id")String id) 
```

**@RequestParam用法**

```java
@RequestMapping(value = "/yoodb",method = RequestMethod.POST)
public Result getUserById(@RequestParam(value="id",required=false,defaultValue="0")String id)
```

注意@RequestParam用法当中的参数

>value参数表示接收数据的名称。
required参数表示接收的参数值是否必须，默认为true，既默认参数必须不为空，当传递过来的参数可能为空的时候可以设置required=false。
defaultValue参数表示如果此次参数未空则为其设置一个默认值。微信小程“Java精选面试题”，内涵 3000+ 道面试题。

@PathVariable主要应用场景：不少应用为了实现RestFul风格，采用@PathVariable方式。

@RequestParam应用场景：这种方式应用非常广，比如CSDN、博客园、开源中国等等。

### 题8：[Spring MVC 如何将 Model 中数据存放到 Session？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题8spring-mvc-如何将-model-中数据存放到-session)<br/>

在Controller类上增加@SessionAttributes注解，使得在调用该Controller时，将Model中的数据存入Session中，实例代码如下：

﻿```java
@Controller
@RequestMapping("/")
@SessionAttributes("isAdmin")
public class IndexController extends BasicController {
    ﻿//....
}
```

### 题9：[如何开启注解处理器和适配器？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题9如何开启注解处理器和适配器)<br/>
在配置文件中（一般命名为springmvc.xml 文件）通过开启配置：

```xml
<mvc:annotation-driven>
```
来实现注解处理器和适配器的开启。

### 题10：[Spring MVC 中系统是如何分层？](/docs/Spring%20MVC/最新Spring%20MVC面试题大全带答案持续更新.md#题10spring-mvc-中系统是如何分层)<br/>
表现层（UI）：数据的展现、操作页面、请求转发。

业务层（服务层）：封装业务处理逻辑。

持久层（数据访问层）：封装数据访问逻辑。

各层之间的关系：MVC是一种表现层的架构，表现层通过接口调用业务层，业务层通过接口调用持久层，当下一层发生改变，不影响上一层的数据。 

### 题11：spring-mvc-中如何实现拦截器<br/>


### 题12：spring-mvc-和-struts2-有哪些区别<br/>


### 题13：spring-mvc-中日期类型参数如何接收<br/>


### 题14：spring-mvc-如何与-ajax-相互调用<br/>


### 题15：spring-mvc-请求转发和重定向有什么区别<br/>


### 题16：说一说-spring-mvc-注解原理<br/>


### 题17：什么是-spring-mvc-框架<br/>


### 题18：spring-mvc-中-@requestmapping-注解有什么属性<br/>


### 题19：spring-mvc-中如何拦截-get-方式请求<br/>


### 题20：spring-mvc-常用的注解有哪些<br/>


### 题21：spring-mvc-执行流程是什么<br/>


### 题22：spring-mvc-中文件上传有哪些需要注意事项<br/>


### 题23：spring-mvc-中拦截器如何使用<br/>


### 题24：说一说对-restful-的理解及项目中的使用<br/>


### 题25：requestmethod-可以同时支持post和get请求访问吗<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")