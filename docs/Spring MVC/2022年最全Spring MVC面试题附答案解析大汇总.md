# 2022年最全Spring MVC面试题附答案解析大汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[说一说 Spring MVC 注解原理？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题1说一说-spring-mvc-注解原理)<br/>
注解本质是一个继承了Annotation的特殊接口，其具体实现类是JDK动态代理生成的代理类。

通过反射获取注解时，返回的也是Java运行时生成的动态代理对象。

通过代理对象调用自定义注解的方法，会最终调用AnnotationInvocationHandler的invoke方法，该方法会从memberValues这个Map中查询出对应的值，而memberValues的来源是Java常量池。

### 题2：[Spring MVC 中如何实现拦截器？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题2spring-mvc-中如何实现拦截器)<br/>
有两种写法，一种是实现HandlerInterceptor接口，另外一种是继承适配器类，接着在接口方法当中，实现处理逻辑；然后在SpringMvc的配置文件中配置拦截器即可。

```xml
<!-- 配置SpringMvc的拦截器 -->
<mvc:interceptors>
    <!-- 配置一个拦截器的Bean就可以了 默认是对所有请求都拦截 -->
    <bean id="myInterceptor" class="com.yoodb.action.MyHandlerInterceptor"></bean>
 
    <!-- 只针对部分请求拦截 -->
    <mvc:interceptor>
       <mvc:mapping path="/jingxuan.do" />
       <bean class="com.yoodb.action.MyHandlerInterceptorAdapter" />
    </mvc:interceptor>
</mvc:interceptors>
```

### 题3：[说一说对 RESTful 的理解及项目中的使用？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题3说一说对-restful-的理解及项目中的使用)<br/>
RESTful一种软件架构风格、设计风格，而不是标准，只是提供了一组设计原则和约束条件。

RESTful主要用于客户端和服务器交互类的软件。

REST指的是一组架构约束条件和原则。

满足这些约束条件和原则的应用程序或设计就是RESTful。

RESTful结构清晰、符合标准、易于理解、扩展方便，所以正得到越来越多网站的采用。


### 题4：[Spring MVC 和 Struts2 有哪些区别？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题4spring-mvc-和-struts2-有哪些区别)<br/>
Spring MVC的入口是一个servlet即前端控制器（DispatchServlet），而Struts2入口是一个filter过虑器（StrutsPrepareAndExecuteFilter）。

Spring MVC是基于方法开发（一个url对应一个方法），请求参数传递到方法的形参，可以设计为单例或多例（建议单例），Struts2是基于类开发，传递参数是通过类的属性，只能设计为多例。

Struts采用值栈存储请求和响应的数据，通过OGNL存取数据，而Spring MVC通过参数解析器将request请求内容解析，并给方法形参赋值，将数据和视图封装成ModelAndView对象，最后将ModelAndView中的模型数据通过reques域传输到页面。其中Jsp视图解析器默认使用jstl。

### 题5：[Spring MVC 中 @RequestMapping 注解用在类上有什么作用？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题5spring-mvc-中-@requestmapping-注解用在类上有什么作用)<br/>
@RequestMapping注解是一个用来处理请求地址映射的注解，可用于类或方法上。

用于类上，则表示类中的所有响应请求的方法都是以该地址作为父路径。

比如

```java
@Controller
@RequestMapping("/test/")
public class TestController{

}
```

启动的是本地服务，默认端口是8080，通过浏览器访问的路径就是如下地址：

```shell
http://localhost:8080/test/
```

### 题6：[什么是 Spring MVC 框架？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题6什么是-spring-mvc-框架)<br/>
Spring MVC属于Spring FrameWork的后续产品，已经融合在Spring Web Flow中。

Spring框架提供了构建Web应用程序的全功能MVC模块。

使用Spring可插入MVC架构，从而在使用Spring进行WEB开发时，可以选择使用Spring中的Spring MVC框架或集成其他MVC开发框架，如Struts1（已基本淘汰），Struts2（老项目还在使用或已重构）等。

通过策略接口，Spring框架是高度可配置的且包含多种视图技术，如JavaServer Pages（JSP）技术、Velocity、Tiles、iText和POI等。

Spring MVC 框架并不清楚或限制使用哪种视图，所以不会强迫开发者只使用JSP技术。

Spring MVC分离了控制器、模型对象、过滤器以及处理程序对象的角色，这种分离让它们更容易进行定制。

### 题7：[Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题7spring-mvc-请求转发和重定向有什么区别)<br/>
请求转发是浏览器发出一次请求，获取一次响应，而重定向是浏览器发出2次请求，获取2次请求。

请求转发是浏览器地址栏未发生变化，是第1次发出的请求，而重定向是浏览器地址栏发生变化，是第2次发出的请求。

请求转发称为服务器内跳转，重定向称为服务器外跳转。

请求转发是可以获取到用户提交请求中的数据，而重定向是不可以获取到用户提交请求中的数据，但可以获取到第2次由浏览器自动发出的请求中携带的数据。

请求转发是可以将请求转发到WEB-INF目录下的（内部）资源，而重定向是不可以将请求转发到WEB-INF目录下的资源。

### 题8：[Spring MVC 中如何拦截 get 方式请求？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题8spring-mvc-中如何拦截-get-方式请求)<br/>
@RequestMapping注解中加上method=RequestMethod.GET参数就可以实现拦截get方式请求。

```java
@RequestMapping("jingxuan/login")
public String login(){

}
```

### 题9：[Spring MVC 如何设置重定向和转发？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题9spring-mvc-如何设置重定向和转发)<br/>
在返回值前面加“forward:”参数，可以实现转发。

```java
"forward:getName.do?name=Java精选"
```

在返回值前面加“redirect:”参数，可以实现重定向。

```java
"redirect:https://blog.yoodb.com"

### 题10：[如何开启注解处理器和适配器？](/docs/Spring%20MVC/2022年最全Spring%20MVC面试题附答案解析大汇总.md#题10如何开启注解处理器和适配器)<br/>
在配置文件中（一般命名为springmvc.xml 文件）通过开启配置：

```xml
<mvc:annotation-driven>
```
来实现注解处理器和适配器的开启。

### 题11：spring-mvc-中-@requestmapping-注解有什么属性<br/>


### 题12：spring-mvc-中文件上传有哪些需要注意事项<br/>


### 题13：spring-mvc-常用的注解有哪些<br/>


### 题14：spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别<br/>


### 题15：spring-mvc-如何与-ajax-相互调用<br/>


### 题16：spring-mvc-中日期类型参数如何接收<br/>


### 题17：spring-mvc-中函数的返回值是什么<br/>


### 题18：spring-mvc-控制器注解一般适用什么可以适用什么替代<br/>


### 题19：spring-mvc-如何解决请求中文乱码问题<br/>


### 题20：spring-mvc-如何将-model-中数据存放到-session<br/>


### 题21：spring-mvc-中系统是如何分层<br/>


### 题22：spring-mvc-中拦截器如何使用<br/>


### 题23：spring-mvc-控制器是单例的吗?<br/>


### 题24：spring-mvc-中如何进行异常处理<br/>


### 题25：spring-mvc模块的作用是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")