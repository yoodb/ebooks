# 最新2022年Spring MVC面试题高级面试题及附答案解析

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[如何开启注解处理器和适配器？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题1如何开启注解处理器和适配器)<br/>
在配置文件中（一般命名为springmvc.xml 文件）通过开启配置：

```xml
<mvc:annotation-driven>
```
来实现注解处理器和适配器的开启。

### 题2：[说一说 Spring MVC 注解原理？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题2说一说-spring-mvc-注解原理)<br/>
注解本质是一个继承了Annotation的特殊接口，其具体实现类是JDK动态代理生成的代理类。

通过反射获取注解时，返回的也是Java运行时生成的动态代理对象。

通过代理对象调用自定义注解的方法，会最终调用AnnotationInvocationHandler的invoke方法，该方法会从memberValues这个Map中查询出对应的值，而memberValues的来源是Java常量池。

### 题3：[Spring MVC 如何将 Model 中数据存放到 Session？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题3spring-mvc-如何将-model-中数据存放到-session)<br/>

在Controller类上增加@SessionAttributes注解，使得在调用该Controller时，将Model中的数据存入Session中，实例代码如下：

﻿```java
@Controller
@RequestMapping("/")
@SessionAttributes("isAdmin")
public class IndexController extends BasicController {
    ﻿//....
}
```

### 题4：[Spring MVC 如何设置重定向和转发？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题4spring-mvc-如何设置重定向和转发)<br/>
在返回值前面加“forward:”参数，可以实现转发。

```java
"forward:getName.do?name=Java精选"
```

在返回值前面加“redirect:”参数，可以实现重定向。

```java
"redirect:https://blog.yoodb.com"

### 题5：[Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题5spring-mvc-请求转发和重定向有什么区别)<br/>
请求转发是浏览器发出一次请求，获取一次响应，而重定向是浏览器发出2次请求，获取2次请求。

请求转发是浏览器地址栏未发生变化，是第1次发出的请求，而重定向是浏览器地址栏发生变化，是第2次发出的请求。

请求转发称为服务器内跳转，重定向称为服务器外跳转。

请求转发是可以获取到用户提交请求中的数据，而重定向是不可以获取到用户提交请求中的数据，但可以获取到第2次由浏览器自动发出的请求中携带的数据。

请求转发是可以将请求转发到WEB-INF目录下的（内部）资源，而重定向是不可以将请求转发到WEB-INF目录下的资源。

### 题6：[Spring MVC 中常用的注解包含哪些？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题6spring-mvc-中常用的注解包含哪些)<br/>
@EnableWebMvc 在配置类中开启Web MVC的配置支持，如一些ViewResolver或者MessageConverter等，若无此句，重写WebMvcConfigurerAdapter方法（用于对SpringMVC的配置）。

@Controller 声明该类为SpringMVC中的Controller

@RequestMapping 用于映射Web请求，包括访问路径和参数（类或方法上）

@ResponseBody 支持将返回值放在response内，而不是一个页面，通常用户返回json数据（返回值旁或方法上）

@RequestBody 允许request的参数在request体中，而不是在直接连接在地址后面。（放在参数前）

@PathVariable 用于接收路径参数，比如@RequestMapping(“/hello/{name}”)申明的路径，将注解放在参数中前，即可获取该值，通常作为Restful的接口实现方法。

@RestController 该注解为一个组合注解，相当于@Controller和@ResponseBody的组合，注解在类上，意味着，该Controller的所有方法都默认加上了@ResponseBody。

@ControllerAdvice 通过该注解，我们可以将对于控制器的全局配置放置在同一个位置，注解了@Controller的类的方法可使用@ExceptionHandler、@InitBinder、@ModelAttribute注解到方法上， 

这对所有注解了 @RequestMapping的控制器内的方法有效。

@ExceptionHandler 用于全局处理控制器里的异常

@InitBinder 用来设置WebDataBinder，WebDataBinder用来自动绑定前台请求参数到Model中。

@ModelAttribute 本来的作用是绑定键值对到Model里，在@ControllerAdvice中是让全局的@RequestMapping都能获得在此处设置的键值对。

### 题7：[Spring MVC 中 @RequestMapping 注解用在类上有什么作用？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题7spring-mvc-中-@requestmapping-注解用在类上有什么作用)<br/>
@RequestMapping注解是一个用来处理请求地址映射的注解，可用于类或方法上。

用于类上，则表示类中的所有响应请求的方法都是以该地址作为父路径。

比如

```java
@Controller
@RequestMapping("/test/")
public class TestController{

}
```

启动的是本地服务，默认端口是8080，通过浏览器访问的路径就是如下地址：

```shell
http://localhost:8080/test/
```

### 题8：[Spring MVC 和 Struts2 有哪些区别？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题8spring-mvc-和-struts2-有哪些区别)<br/>
Spring MVC的入口是一个servlet即前端控制器（DispatchServlet），而Struts2入口是一个filter过虑器（StrutsPrepareAndExecuteFilter）。

Spring MVC是基于方法开发（一个url对应一个方法），请求参数传递到方法的形参，可以设计为单例或多例（建议单例），Struts2是基于类开发，传递参数是通过类的属性，只能设计为多例。

Struts采用值栈存储请求和响应的数据，通过OGNL存取数据，而Spring MVC通过参数解析器将request请求内容解析，并给方法形参赋值，将数据和视图封装成ModelAndView对象，最后将ModelAndView中的模型数据通过reques域传输到页面。其中Jsp视图解析器默认使用jstl。

### 题9：[Spring MVC 控制器是单例的吗?](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题9spring-mvc-控制器是单例的吗?)<br/>
默认情况下是单例模式，在多线程进行访问时存在线程安全的问题。

解决方法可以在控制器中不要写成员变量，这是因为单例模式下定义成员变量是线程不安全的。

通过@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)或@Scope("prototype")可以实现多例模式。但是不建议使用同步，因为会影响性能。

使用单例模式是为了性能，无需频繁进行初始化操作，同时也没有必要使用多例模式。

### 题10：[Spring MVC 中 @RequestMapping 注解有什么属性？](/docs/Spring%20MVC/最新2022年Spring%20MVC面试题高级面试题及附答案解析.md#题10spring-mvc-中-@requestmapping-注解有什么属性)<br/>
RequestMapping是一个用来处理请求地址映射的注解，可用于类或方法上。用于类上，表示类中的所有响应请求的方法都是以该地址作为父路径。

**RequestMapping注解有六个属性**

value

指定请求的实际地址，指定的地址可以是URI Template 模式（后面将会说明）；

method

指定请求的method类型， GET、POST、PUT、DELETE等；

consumes

指定处理请求的提交内容类型（Content-Type），例如application/json, text/html;

produces

指定返回的内容类型，仅当request请求头中的(Accept)类型中包含该指定类型才返回；

params

指定request中必须包含某些参数值是，才让该方法处理。

headers

指定request中必须包含某些指定的header值，才能让该方法处理请求。


### 题11：spring-mvc-如何解决请求中文乱码问题<br/>


### 题12：spring-mvc-如何与-ajax-相互调用<br/>


### 题13：spring-mvc-中拦截器如何使用<br/>


### 题14：spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别<br/>


### 题15：说一说对-restful-的理解及项目中的使用<br/>


### 题16：spring-mvc-中函数的返回值是什么<br/>


### 题17：requestmethod-可以同时支持post和get请求访问吗<br/>


### 题18：spring-mvc-执行流程是什么<br/>


### 题19：spring-mvc模块的作用是什么<br/>


### 题20：spring-mvc-中系统是如何分层<br/>


### 题21：spring-mvc-中如何进行异常处理<br/>


### 题22：spring-mvc-中文件上传有哪些需要注意事项<br/>


### 题23：spring-mvc-中如何实现拦截器<br/>


### 题24：spring-mvc-中如何拦截-get-方式请求<br/>


### 题25：spring-mvc-控制器注解一般适用什么可以适用什么替代<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")