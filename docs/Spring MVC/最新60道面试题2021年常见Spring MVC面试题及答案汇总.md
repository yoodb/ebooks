# 最新60道面试题2021年常见Spring MVC面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[Spring MVC 中 @RequestMapping 注解用在类上有什么作用？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题1spring-mvc-中-@requestmapping-注解用在类上有什么作用)<br/>
@RequestMapping注解是一个用来处理请求地址映射的注解，可用于类或方法上。

用于类上，则表示类中的所有响应请求的方法都是以该地址作为父路径。

比如

```java
@Controller
@RequestMapping("/test/")
public class TestController{

}
```

启动的是本地服务，默认端口是8080，通过浏览器访问的路径就是如下地址：

```shell
http://localhost:8080/test/
```

### 题2：[Spring MVC 如何解决请求中文乱码问题？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题2spring-mvc-如何解决请求中文乱码问题)<br/>
解决GET请求中文乱码问题

方式一：每次请求前使用encodeURI对URL进行编码。

方式二：在应用服务器上配置URL编码格式，在Tomcat配置文件server.xml中增加URIEncoding="UTF-8"，然后重启Tomcat即可。

```xml
<ConnectorURIEncoding="UTF-8" 
    port="8080"  maxHttpHeaderSize="8192"  maxThreads="150" 
    minSpareThreads="25"  maxSpareThreads="75"connectionTimeout="20000" 		
    disableUploadTimeout="true" URIEncoding="UTF-8" />
```

解决POST请求中文乱码问题

方式一：在request解析数据时设置编码格式：

```java
request.setCharacterEncoding("UTF-8");
```

方式二：使用Spring提供的编码过滤器

在web.xml文件中配置字符编码过滤器，增加如下配置：
```xml
<!--编码过滤器-->                                                                                                                
<filter>                                                                
	<filter-name>encodingFilter</filter-name>                           
	<filter-class>                                                      
		org.springframework.web.filter.CharacterEncodingFilter          
	</filter-class>                                                     
	<!-- 开启异步支持-->                                               
	<async-supported>true</async-supported>                             
	<init-param>                                                        
		<param-name>encoding</param-name>                               
		<param-value>utf-8</param-value>                                
	</init-param>                                                       
</filter>                                                               
<filter-mapping>                                                        
	<filter-name>encodingFilter</filter-name>                           
	<url-pattern>/*</url-pattern>                                       
</filter-mapping>     
```

该过滤器的作用就是强制所有请求和响应设置编码格式：
```java
request.setCharacterEncoding("UTF-8");
response.setCharacterEncoding("UTF-8");
```

### 题3：[Spring MVC 中 @PathVariable 和 @RequestParam 有哪些区别？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题3spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别)<br/>
@RequestParam与@PathVariable为spring的注解，都可以用于在Controller层接收前端传递的数据，不过两者的应用场景不同。

@PathVariable主要用于接收http://host:port/path/{参数值}数据。

@PathVariable请求接口时，URL是 http://www.yoodb.com/user/getUserById/2

@RequestParam主要用于接收http://host:port/path?参数名=值数据值。

@RequestParam请求接口时，URL是 http://www.yoodb.com/user/getUserById?userId=1


**@PathVariable用法**

```java
@RequestMapping(value = "/yoodb/{id}",method = RequestMethod.DELETE)
public Result getUserById(@PathVariable("id")String id) 
```

**@RequestParam用法**

```java
@RequestMapping(value = "/yoodb",method = RequestMethod.POST)
public Result getUserById(@RequestParam(value="id",required=false,defaultValue="0")String id)
```

注意@RequestParam用法当中的参数

>value参数表示接收数据的名称。
required参数表示接收的参数值是否必须，默认为true，既默认参数必须不为空，当传递过来的参数可能为空的时候可以设置required=false。
defaultValue参数表示如果此次参数未空则为其设置一个默认值。微信小程“Java精选面试题”，内涵 3000+ 道面试题。

@PathVariable主要应用场景：不少应用为了实现RestFul风格，采用@PathVariable方式。

@RequestParam应用场景：这种方式应用非常广，比如CSDN、博客园、开源中国等等。

### 题4：[Spring MVC 中系统是如何分层？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题4spring-mvc-中系统是如何分层)<br/>
表现层（UI）：数据的展现、操作页面、请求转发。

业务层（服务层）：封装业务处理逻辑。

持久层（数据访问层）：封装数据访问逻辑。

各层之间的关系：MVC是一种表现层的架构，表现层通过接口调用业务层，业务层通过接口调用持久层，当下一层发生改变，不影响上一层的数据。 

### 题5：[Spring MVC 如何设置重定向和转发？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题5spring-mvc-如何设置重定向和转发)<br/>
在返回值前面加“forward:”参数，可以实现转发。

```java
"forward:getName.do?name=Java精选"
```

在返回值前面加“redirect:”参数，可以实现重定向。

```java
"redirect:https://blog.yoodb.com"

### 题6：[Spring MVC 控制器是单例的吗?](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题6spring-mvc-控制器是单例的吗?)<br/>
默认情况下是单例模式，在多线程进行访问时存在线程安全的问题。

解决方法可以在控制器中不要写成员变量，这是因为单例模式下定义成员变量是线程不安全的。

通过@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)或@Scope("prototype")可以实现多例模式。但是不建议使用同步，因为会影响性能。

使用单例模式是为了性能，无需频繁进行初始化操作，同时也没有必要使用多例模式。

### 题7：[Spring MVC 中如何进行异常处理？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题7spring-mvc-中如何进行异常处理)<br/>
**局部异常处理**

局部异常处理是指当类中发生异常时，由方法来处理，该方法的参数类型为Exception，而Exception是所有异常的父类，故由该参数来接收异常对象。

步骤说明

1）在controller类中定义处理异常的方法，添加注解@ExceptionHandler，方法的参数类型为Exception，并通过getMessage()方法获取异常信息，再将异常信息保存，最后跳转到错误页面，使用mv.setViewName("error")，这里通过ModelAndView将异常信息保存到request中。

2）创建error.jsp页面，在page中添加isErrorPage="true"，表示该页面为错误页面，再从request中获取错误信息，并显示到页面。

**定义全局异常类**

1）创建异常类，添加注解@ControllerAdvice表示当发生异常时由该异常类来处理，全局的异常类处理异常。

在异常类中定义处理异常的方法，该方法的定义与定义局部的异常处理方法一致。

### 题8：[Spring MVC 中文件上传有哪些需要注意事项？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题8spring-mvc-中文件上传有哪些需要注意事项)<br/>
Spring MVC提供了两种上传方式配置

基于commons-fileupload.jar

```java
org.springframework.web.multipart.commons.CommonsMultipartResolver
```

基于servlet3.0+

```java
org.springframework.web.multipart.support.StandardServletMultipartResolver
```

1、页面form中提交方式enctype="multipart/form-data"的数据时，需要springmvc对multipart类型的数据进行解析。

2、在springmvc.xml中配置multipart类型解析器。

3、方法中使用MultipartFile attach实现单个文件上传或者使用MultipartFile[] attachs实现多个文件上传。

### 题9：[Spring MVC 中如何实现拦截器？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题9spring-mvc-中如何实现拦截器)<br/>
有两种写法，一种是实现HandlerInterceptor接口，另外一种是继承适配器类，接着在接口方法当中，实现处理逻辑；然后在SpringMvc的配置文件中配置拦截器即可。

```xml
<!-- 配置SpringMvc的拦截器 -->
<mvc:interceptors>
    <!-- 配置一个拦截器的Bean就可以了 默认是对所有请求都拦截 -->
    <bean id="myInterceptor" class="com.yoodb.action.MyHandlerInterceptor"></bean>
 
    <!-- 只针对部分请求拦截 -->
    <mvc:interceptor>
       <mvc:mapping path="/jingxuan.do" />
       <bean class="com.yoodb.action.MyHandlerInterceptorAdapter" />
    </mvc:interceptor>
</mvc:interceptors>
```

### 题10：[Spring MVC 中日期类型参数如何接收？](/docs/Spring%20MVC/最新60道面试题2021年常见Spring%20MVC面试题及答案汇总.md#题10spring-mvc-中日期类型参数如何接收)<br/>
**1、controller控制层**

日期类型参数接收时，需要注意Spring MVC在接收日期类型参数时，如不做特殊处理会出现400语法格式错误。

解决办法：定义全局日期处理。

```java
@RequestMapping("/test")
public String test(Date birthday){
	System.out.println(birthday);
	return "index";
}
```

**2、自定义类型转换规则**

```java
public class DateConvert implements Converter<String, Date> {

    @Override
    public Date convert(String stringDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return simpleDateFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
```

全局日期处理类，其中Convert<T,S>，泛型T：代表客户端提交的参数 String，泛型S：通过convert转换的类型。

**3、XML配置文件**

首先创建自定义日期转换规则，并创建convertion-Service ，注入dateConvert，最后注册处理器映射器、处理器适配器 ，添加conversion-service属性。

```xml
<mvc:annotation-driven conversion-service="conversionService"/>
<bean id="conversionService" class="org.springframework.format.support.FormattingConversionServiceFactoryBean">
	<property name="converters">
		<set>
			<ref bean="dateConvert"/>
		</set>
	</property>
</bean>
<bean id="dateConvert" class="com.yoodb.DateConvert"/>
```

### 题11：requestmethod-可以同时支持post和get请求访问吗<br/>


### 题12：spring-mvc-和-struts2-有哪些区别<br/>


### 题13：spring-mvc-如何与-ajax-相互调用<br/>


### 题14：说一说-spring-mvc-注解原理<br/>


### 题15：spring-mvc-中如何拦截-get-方式请求<br/>


### 题16：spring-mvc-控制器注解一般适用什么可以适用什么替代<br/>


### 题17：说一说对-restful-的理解及项目中的使用<br/>


### 题18：spring-mvc-请求转发和重定向有什么区别<br/>


### 题19：spring-mvc-中函数的返回值是什么<br/>


### 题20：如何开启注解处理器和适配器<br/>


### 题21：spring-mvc-中常用的注解包含哪些<br/>


### 题22：spring-mvc-如何将-model-中数据存放到-session<br/>


### 题23：spring-mvc模块的作用是什么<br/>


### 题24：spring-mvc-中-@requestmapping-注解有什么属性<br/>


### 题25：spring-mvc-执行流程是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")