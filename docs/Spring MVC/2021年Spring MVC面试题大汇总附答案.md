# 2021年Spring MVC面试题大汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[Spring MVC 中拦截器如何使用？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题1spring-mvc-中拦截器如何使用)<br/>
定义拦截器，实现HandlerInterceptor接口。

**接口中提供三个方法**

1、preHandle ：进入 Handler方法之前执行，用于身份认证、身份授权，比如身份认证，如果认证通过表示当前用户没有登陆，需要此方法拦截不再向下执行。

2、postHandle：进入Handler方法之后，返回modelAndView之前执行，应用场景从modelAndView出发：将公用的模型数据(比如菜单导航)在这里传到视图，也可以在这里统一指定视图。

3、afterCompletion：执行Handler完成执行此方法，应用场景：统一异常处理，统一日志处理。

**拦截器配置**

1、针对HandlerMapping配置(不推荐)：springmvc拦截器针对HandlerMapping进行拦截设置，如果在某个HandlerMapping中配置拦截，经过该 HandlerMapping映射成功的handler最终使用该 拦截器。（一般不推荐使用）。

2、类似全局的拦截器：springmvc配置类似全局的拦截器，springmvc框架将配置的类似全局的拦截器注入到每个HandlerMapping中。

### 题2：[Spring MVC 如何解决请求中文乱码问题？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题2spring-mvc-如何解决请求中文乱码问题)<br/>
解决GET请求中文乱码问题

方式一：每次请求前使用encodeURI对URL进行编码。

方式二：在应用服务器上配置URL编码格式，在Tomcat配置文件server.xml中增加URIEncoding="UTF-8"，然后重启Tomcat即可。

```xml
<ConnectorURIEncoding="UTF-8" 
    port="8080"  maxHttpHeaderSize="8192"  maxThreads="150" 
    minSpareThreads="25"  maxSpareThreads="75"connectionTimeout="20000" 		
    disableUploadTimeout="true" URIEncoding="UTF-8" />
```

解决POST请求中文乱码问题

方式一：在request解析数据时设置编码格式：

```java
request.setCharacterEncoding("UTF-8");
```

方式二：使用Spring提供的编码过滤器

在web.xml文件中配置字符编码过滤器，增加如下配置：
```xml
<!--编码过滤器-->                                                                                                                
<filter>                                                                
	<filter-name>encodingFilter</filter-name>                           
	<filter-class>                                                      
		org.springframework.web.filter.CharacterEncodingFilter          
	</filter-class>                                                     
	<!-- 开启异步支持-->                                               
	<async-supported>true</async-supported>                             
	<init-param>                                                        
		<param-name>encoding</param-name>                               
		<param-value>utf-8</param-value>                                
	</init-param>                                                       
</filter>                                                               
<filter-mapping>                                                        
	<filter-name>encodingFilter</filter-name>                           
	<url-pattern>/*</url-pattern>                                       
</filter-mapping>     
```

该过滤器的作用就是强制所有请求和响应设置编码格式：
```java
request.setCharacterEncoding("UTF-8");
response.setCharacterEncoding("UTF-8");
```

### 题3：[Spring MVC模块的作用是什么？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题3spring-mvc模块的作用是什么)<br/>
Spring提供了MVC框架来构建Web应用程序。

Spring可以很容易地与其他MVC框架集成，但是Spring的MVC框架是更好的选择，因为它使用IoC来提供控制器逻辑与业务对象的清晰分离。

使用Spring MVC，可以声明性地将请求参数绑定到业务对象。

### 题4：[RequestMethod 可以同时支持POST和GET请求访问吗？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题4requestmethod-可以同时支持post和get请求访问吗)<br/>


POST请求访问

```java
@RequestMapping(value="/wx/getUserById",method = RequestMethod.POST)
```

GET请求访问

```java
@RequestMapping(value="/wx/getUserById/{userId}",method = RequestMethod.GET)
```

同时支持POST和GET请求访问

```java
@RequestMapping(value="/subscribe/getSubscribeList/{customerCode}/{sid}/{userId}")
```
**RequestMethod常用的参数**

GET（SELECT）：从服务器查询，在服务器通过请求参数区分查询的方式。
  
POST（CREATE）：在服务器新建一个资源，调用insert操作。 
 
PUT（UPDATE）：在服务器更新资源，调用update操作。  

DELETE（DELETE）：从服务器删除资源，调用delete语句。

### 题5：[什么是 Spring MVC 框架？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题5什么是-spring-mvc-框架)<br/>
Spring MVC属于Spring FrameWork的后续产品，已经融合在Spring Web Flow中。

Spring框架提供了构建Web应用程序的全功能MVC模块。

使用Spring可插入MVC架构，从而在使用Spring进行WEB开发时，可以选择使用Spring中的Spring MVC框架或集成其他MVC开发框架，如Struts1（已基本淘汰），Struts2（老项目还在使用或已重构）等。

通过策略接口，Spring框架是高度可配置的且包含多种视图技术，如JavaServer Pages（JSP）技术、Velocity、Tiles、iText和POI等。

Spring MVC 框架并不清楚或限制使用哪种视图，所以不会强迫开发者只使用JSP技术。

Spring MVC分离了控制器、模型对象、过滤器以及处理程序对象的角色，这种分离让它们更容易进行定制。

### 题6：[如何开启注解处理器和适配器？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题6如何开启注解处理器和适配器)<br/>
在配置文件中（一般命名为springmvc.xml 文件）通过开启配置：

```xml
<mvc:annotation-driven>
```
来实现注解处理器和适配器的开启。

### 题7：[Spring MVC 控制器是单例的吗?](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题7spring-mvc-控制器是单例的吗?)<br/>
默认情况下是单例模式，在多线程进行访问时存在线程安全的问题。

解决方法可以在控制器中不要写成员变量，这是因为单例模式下定义成员变量是线程不安全的。

通过@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)或@Scope("prototype")可以实现多例模式。但是不建议使用同步，因为会影响性能。

使用单例模式是为了性能，无需频繁进行初始化操作，同时也没有必要使用多例模式。

### 题8：[Spring MVC 执行流程是什么？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题8spring-mvc-执行流程是什么)<br/>
1）客户端发送请求到前端控制器DispatcherServlet。

2）DispatcherServlet收到请求调用HandlerMapping处理器映射器。

3）处理器映射器找到具体的处理器，根据xml配置、注解进行查找，生成处理器对象及处理器拦截器，并返回给DispatcherServlet。

4）DispatcherServlet调用HandlerAdapter处理器适配器。

5）HandlerAdapter经过适配调用具体的后端控制器Controller。

6）Controller执行完成返回ModelAndView。

7）HandlerAdapter将controller执行结果ModelAndView返回给DispatcherServlet。

8）DispatcherServlet将ModelAndView传给ViewReslover视图解析器。

9）ViewReslover解析后返回具体View。

10）DispatcherServlet根据View进行渲染视图，将模型数据填充至视图中。

11）DispatcherServlet响应客户端。

**组件说明**

DispatcherServlet：作为前端控制器，整个流程控制的中心，控制其它组件执行，统一调度，降低组件之间的耦合性，提高每个组件的扩展性。

HandlerMapping：通过扩展处理器映射器实现不同的映射方式，例如：配置文件方式，实现接口方式，注解方式等。 

HandlAdapter：通过扩展处理器适配器，支持更多类型的处理器。

ViewResolver：通过扩展视图解析器，支持更多类型的视图解析，例如：jsp、freemarker、pdf、excel等。

### 题9：[Spring MVC 中如何进行异常处理？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题9spring-mvc-中如何进行异常处理)<br/>
**局部异常处理**

局部异常处理是指当类中发生异常时，由方法来处理，该方法的参数类型为Exception，而Exception是所有异常的父类，故由该参数来接收异常对象。

步骤说明

1）在controller类中定义处理异常的方法，添加注解@ExceptionHandler，方法的参数类型为Exception，并通过getMessage()方法获取异常信息，再将异常信息保存，最后跳转到错误页面，使用mv.setViewName("error")，这里通过ModelAndView将异常信息保存到request中。

2）创建error.jsp页面，在page中添加isErrorPage="true"，表示该页面为错误页面，再从request中获取错误信息，并显示到页面。

**定义全局异常类**

1）创建异常类，添加注解@ControllerAdvice表示当发生异常时由该异常类来处理，全局的异常类处理异常。

在异常类中定义处理异常的方法，该方法的定义与定义局部的异常处理方法一致。

### 题10：[Spring MVC 中如何拦截 get 方式请求？](/docs/Spring%20MVC/2021年Spring%20MVC面试题大汇总附答案.md#题10spring-mvc-中如何拦截-get-方式请求)<br/>
@RequestMapping注解中加上method=RequestMethod.GET参数就可以实现拦截get方式请求。

```java
@RequestMapping("jingxuan/login")
public String login(){

}
```

### 题11：spring-mvc-中系统是如何分层<br/>


### 题12：spring-mvc-常用的注解有哪些<br/>


### 题13：spring-mvc-如何设置重定向和转发<br/>


### 题14：spring-mvc-中日期类型参数如何接收<br/>


### 题15：spring-mvc-中-@requestmapping-注解用在类上有什么作用<br/>


### 题16：spring-mvc-请求转发和重定向有什么区别<br/>


### 题17：说一说-spring-mvc-注解原理<br/>


### 题18：说一说对-restful-的理解及项目中的使用<br/>


### 题19：spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别<br/>


### 题20：spring-mvc-中函数的返回值是什么<br/>


### 题21：spring-mvc-中常用的注解包含哪些<br/>


### 题22：spring-mvc-如何将-model-中数据存放到-session<br/>


### 题23：spring-mvc-中-@requestmapping-注解有什么属性<br/>


### 题24：spring-mvc-控制器注解一般适用什么可以适用什么替代<br/>


### 题25：spring-mvc-中如何实现拦截器<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")