# 最新2021年Spring MVC面试题及答案汇总版

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring MVC

### 题1：[什么是 Spring MVC 框架？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题1什么是-spring-mvc-框架)<br/>
Spring MVC属于Spring FrameWork的后续产品，已经融合在Spring Web Flow中。

Spring框架提供了构建Web应用程序的全功能MVC模块。

使用Spring可插入MVC架构，从而在使用Spring进行WEB开发时，可以选择使用Spring中的Spring MVC框架或集成其他MVC开发框架，如Struts1（已基本淘汰），Struts2（老项目还在使用或已重构）等。

通过策略接口，Spring框架是高度可配置的且包含多种视图技术，如JavaServer Pages（JSP）技术、Velocity、Tiles、iText和POI等。

Spring MVC 框架并不清楚或限制使用哪种视图，所以不会强迫开发者只使用JSP技术。

Spring MVC分离了控制器、模型对象、过滤器以及处理程序对象的角色，这种分离让它们更容易进行定制。

### 题2：[如何开启注解处理器和适配器？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题2如何开启注解处理器和适配器)<br/>
在配置文件中（一般命名为springmvc.xml 文件）通过开启配置：

```xml
<mvc:annotation-driven>
```
来实现注解处理器和适配器的开启。

### 题3：[Spring MVC 控制器注解一般适用什么？可以适用什么替代？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题3spring-mvc-控制器注解一般适用什么可以适用什么替代)<br/>
一般用@Controller注解，也可以使用@RestController，@RestController注解相当于@ResponseBody+@Controller，表示是表现层，除此之外，一般不用别的注解代替。

### 题4：[Spring MVC 中函数的返回值是什么？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题4spring-mvc-中函数的返回值是什么)<br/>
Spring MVC的返回值可以有很多类型，如String、ModelAndView等，但事一般使用String比较友好。

### 题5：[Spring MVC模块的作用是什么？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题5spring-mvc模块的作用是什么)<br/>
Spring提供了MVC框架来构建Web应用程序。

Spring可以很容易地与其他MVC框架集成，但是Spring的MVC框架是更好的选择，因为它使用IoC来提供控制器逻辑与业务对象的清晰分离。

使用Spring MVC，可以声明性地将请求参数绑定到业务对象。

### 题6：[Spring MVC 中如何拦截 get 方式请求？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题6spring-mvc-中如何拦截-get-方式请求)<br/>
@RequestMapping注解中加上method=RequestMethod.GET参数就可以实现拦截get方式请求。

```java
@RequestMapping("jingxuan/login")
public String login(){

}
```

### 题7：[Spring MVC 中日期类型参数如何接收？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题7spring-mvc-中日期类型参数如何接收)<br/>
**1、controller控制层**

日期类型参数接收时，需要注意Spring MVC在接收日期类型参数时，如不做特殊处理会出现400语法格式错误。

解决办法：定义全局日期处理。

```java
@RequestMapping("/test")
public String test(Date birthday){
	System.out.println(birthday);
	return "index";
}
```

**2、自定义类型转换规则**

```java
public class DateConvert implements Converter<String, Date> {

    @Override
    public Date convert(String stringDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return simpleDateFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
```

全局日期处理类，其中Convert<T,S>，泛型T：代表客户端提交的参数 String，泛型S：通过convert转换的类型。

**3、XML配置文件**

首先创建自定义日期转换规则，并创建convertion-Service ，注入dateConvert，最后注册处理器映射器、处理器适配器 ，添加conversion-service属性。

```xml
<mvc:annotation-driven conversion-service="conversionService"/>
<bean id="conversionService" class="org.springframework.format.support.FormattingConversionServiceFactoryBean">
	<property name="converters">
		<set>
			<ref bean="dateConvert"/>
		</set>
	</property>
</bean>
<bean id="dateConvert" class="com.yoodb.DateConvert"/>
```

### 题8：[Spring MVC 如何与 Ajax 相互调用？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题8spring-mvc-如何与-ajax-相互调用)<br/>
通过Jackson框架就可以把Java里面的对象直接转化成Js可以识别的Json对象。具体步骤如下：

1、加入Jackson.jar

2、在配置文件中配置json的映射

3、在接受Ajax方法里面可以直接返回Object、List等，但方法前面要加上@ResponseBody注解。

### 题9：[Spring MVC 请求转发和重定向有什么区别？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题9spring-mvc-请求转发和重定向有什么区别)<br/>
请求转发是浏览器发出一次请求，获取一次响应，而重定向是浏览器发出2次请求，获取2次请求。

请求转发是浏览器地址栏未发生变化，是第1次发出的请求，而重定向是浏览器地址栏发生变化，是第2次发出的请求。

请求转发称为服务器内跳转，重定向称为服务器外跳转。

请求转发是可以获取到用户提交请求中的数据，而重定向是不可以获取到用户提交请求中的数据，但可以获取到第2次由浏览器自动发出的请求中携带的数据。

请求转发是可以将请求转发到WEB-INF目录下的（内部）资源，而重定向是不可以将请求转发到WEB-INF目录下的资源。

### 题10：[Spring MVC 中文件上传有哪些需要注意事项？](/docs/Spring%20MVC/最新2021年Spring%20MVC面试题及答案汇总版.md#题10spring-mvc-中文件上传有哪些需要注意事项)<br/>
Spring MVC提供了两种上传方式配置

基于commons-fileupload.jar

```java
org.springframework.web.multipart.commons.CommonsMultipartResolver
```

基于servlet3.0+

```java
org.springframework.web.multipart.support.StandardServletMultipartResolver
```

1、页面form中提交方式enctype="multipart/form-data"的数据时，需要springmvc对multipart类型的数据进行解析。

2、在springmvc.xml中配置multipart类型解析器。

3、方法中使用MultipartFile attach实现单个文件上传或者使用MultipartFile[] attachs实现多个文件上传。

### 题11：spring-mvc-执行流程是什么<br/>


### 题12：spring-mvc-中-@requestmapping-注解有什么属性<br/>


### 题13：spring-mvc-控制器是单例的吗?<br/>


### 题14：spring-mvc-如何将-model-中数据存放到-session<br/>


### 题15：spring-mvc-如何设置重定向和转发<br/>


### 题16：说一说-spring-mvc-注解原理<br/>


### 题17：说一说对-restful-的理解及项目中的使用<br/>


### 题18：spring-mvc-中-@pathvariable-和-@requestparam-有哪些区别<br/>


### 题19：spring-mvc-中如何进行异常处理<br/>


### 题20：spring-mvc-中系统是如何分层<br/>


### 题21：spring-mvc-中-@requestmapping-注解用在类上有什么作用<br/>


### 题22：spring-mvc-如何解决请求中文乱码问题<br/>


### 题23：spring-mvc-中常用的注解包含哪些<br/>


### 题24：spring-mvc-中拦截器如何使用<br/>


### 题25：spring-mvc-中如何实现拦截器<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")