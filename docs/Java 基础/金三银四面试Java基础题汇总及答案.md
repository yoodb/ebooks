# 金三银四面试Java基础题汇总及答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java 基础

### 题1：[对称加密主要有哪些实现方式？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题1对称加密主要有哪些实现方式)<br/>
**DES（Data Encryption Standard）加密**

DES是一种对称加密算法，是一种非常简便的加密算法，但是密钥长度比较短。

DES加密算法出自IBM的研究，后来被美国政府正式采用，之后开始广泛流传，但是近些年使用越来越少，因为DES使用56位密钥，以现代计算能力，24小时内即可被破解。

**3DES加密**

3DES是一种对称加密算法，在DES的基础上，使用三重数据加密算法，对数据进行加密，它相当于是对每个数据块应用三次DES加密算法。由于计算机运算能力的增强，原版DES密码的密钥长度变得容易被暴力破解；3DES即是设计用来提供一种相对简单的方法，即通过增加DES的密钥长度来避免类似的攻击，而不是设计一种全新的块密码算法这样来说，破解的概率就小了很多。

缺点由于使用了三重数据加密算法，可能会比较耗性能。

**AES加密**

AES是一种对称加密算法，相比于3DES拥有更好的安全性。

**PBE（Password Based Encryption）加密**

PBE是基于口令的加密，结合了DES和AES的优点。

### 题2：[== 和 equals 两者有什么区别？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题2==-和-equals-两者有什么区别)<br/>
**使用==比较**

用于对比基本数据类型的变量，是直接比较存储的 “值”是否相等；

用于对比引用类型的变量，是比较的所指向的对象地址。

**使用equals比较**

equals方法不能用于对比基本数据类型的变量；

如果没对Object中equals方法进行重写，则是比较的引用类型变量所指向的对象地址，反之则比较的是内容。

### 题3：[Java 中什么是重载（Overload）？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题3java-中什么是重载overload)<br/>
重载(Overload)：是在一个类里面，方法名字相同，而参数不同。返回类型可以相同也可以不同

重载规则

1、被重载的方法必须改变参数列表(参数个数或类型不一样)；

2、被重载的方法可以改变返回类型；

3、被重载的方法可以改变访问修饰符；

4、被重载的方法可以声明新的或更广的检查异常；

5、方法能够在同一个类中或者在一个子类中被重载；

6、无法以返回值类型作为重载函数的区分标准；

实例：

```java
public class OverloadDemo {
	
	// 方法1 无参
	public int test() {
		System.out.println("公众号Java精选，3000+面试题!");
		return 1;
	}

	// 方法2 单一参数
	public void test(int a) {
		System.out.println("公众号Java精选，3000+面试题!!");
	}

	// 方法3 下面两个参数类型顺序不同
	public String test(long a, String s) {
		System.out.println(s);
		return "方法3返回->" + s;
	}

	//方法4
	public String test(String s, int a) {
		System.out.println(s);
		return "方法4返回->" + s;
	}

	public static void main(String[] args) {
		OverloadDemo od = new OverloadDemo();
		System.out.println(od.test());//方法1
		od.test(2);//方法2
		System.err.println(od.test(3l, "公众号Java精选，3000+面试题!"));//方法3
		System.err.println(od.test("公众号Java精选，3000+面试题!!!", 4));//方法4
	}
}
```

执行结果

```shell
公众号Java精选，3000+面试题!
1
公众号Java精选，3000+面试题!!
公众号Java精选，3000+面试题!
方法3返回->公众号Java精选，3000+面试题!
方法4返回->公众号Java精选，3000+面试题!!!
公众号Java精选，3000+面试题!!!
```

### 题4：[父类中静态方法能否被子类重写？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题4父类中静态方法能否被子类重写)<br/>
父类中静态方法不能被子类重写。

重写只适用于实例方法，不能用于静态方法，而且子类当中含有和父类相同签名的静态方法，一般称之为隐藏。

```java
public class A {
	
	public static String a = "这是父类静态属性";
	
	public static String getA() {
		return "这是父类静态方法";
	}
	
}
```

```java
public class B extends A{
	
	public static String a = "这是子类静态属性";
	
	public static String getA() {
		return "这是子类静态方法";
	}
	
	public static void main(String[] args) {
		A a =  new B();
		System.out.println(a.getA());
	}
}
```

如上述代码所示，如果能够被重写，则输出的应该是“这是子类静态方法”。与此类似的是，静态变量也不能被重写。如果想要调用父类的静态方法，应该使用类来直接调用。


### 题5：[String 编码 UTF-8 和 GBK 有什么区别?](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题5string-编码-utf-8-和-gbk-有什么区别?)<br/>
GBK全称《汉字内码扩展规范》（GBK即“国标”、“扩展”汉语拼音的第一个字母，英文名称：Chinese Internal Code Specification） ，中华人民共和国全国信息技术标准化技术委员会1995年12月1日制订，国家技术监督局标准化司、电子工业部科技与质量监督司1995年12月15日联合以技监标函1995 229号文件的形式，将它确定为技术规范指导性文件。经实际测试和查阅文档，GBK是采用单双字节变长编码，英文使用单字节编码，完全兼容ASCII字符编码，中文部分采用双字节编码。简洁一点就是是指中国的中文字符，其它它包含了简体中文与繁体中文字符，另外还有一种字符“gb2312”，这种字符仅能存储简体中文字符。

UTF-8（8位元，Universal Character Set/Unicode Transformation Format）是针对Unicode的一种可变长度字符编码。它可以用来表示Unicode标准中的任何字符，而且其编码中的第一个字节仍与ASCII相容，使得原来处理ASCII字符的软件无须或只进行少部份修改后，便可继续使用。因此，它逐渐成为电子邮件、网页及其他存储或传送文字的应用中，优先采用的编码。它是一种全国家通过的一种编码，如果你的网站涉及到多个国家的语言，那么建议你选择UTF-8编码。

**两者的区别：**

UTF8编码格式很强大，支持所有国家的语言，正是因为它的强大，才会导致它占用的空间大小要比GBK大，对于网站打开速度而言，也是有一定影响的。

GBK编码格式，它的功能少，仅限于中文字符，当然它所占用的空间大小会随着它的功能而减少，打开网页的速度比较快。

### 题6：[Java 中 static 可以修饰局部变量吗？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题6java-中-static-可以修饰局部变量吗)<br/>
Java中static关键字不可以修饰局部变量。

static是用于修饰成员变量和成员方法的，它随着类的加载而加载，随着类的消失而消失，存在于方法区的静态区，被其修饰的成员能被类的所有对象共享，即作用域为全局；而局部变量存在于栈，用完后就会释放。作用域为局部代码块。

### 题7：[a==b 与 a.equals(b) 有什么区别？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题7a==b-与-a.equals(b)-有什么区别)<br/>
a==b 与 a.equals(b) 有什么区别？


假设a和b都是对象

a==b是比较两个对象内存地址，当a和b指向的是堆中的同一个对象才会返回true。

a.equals(b)是比较的两个值内容，其比较结果取决于equals()具体实现。

多数情况下需要重写这个方法，如String类重写equals()用于比较两个不同对象，但是包含的字母相同的比较：

```java
public boolean equals(Object obj) {
	if (this == obj) {// 相同对象直接返回true
		return true;
	}
	if (obj instanceof String) {
		String anotherString = (String)obj;
		int n = value.length;
		if (n == anotherString.value.length) {
			char v1[] = value;
			char v2[] = anotherString.value;
			int i = 0;
			while (n-- != 0) {
				if (v1[i] != v2[i])
					return false;
				i++;
			}
			return true;
		}
	}
	return false;
}
```

### 题8：[什么是 Java 内部类？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题8什么是-java-内部类)<br/>
内部类是指把A类定义在另一个B类的内部。

例如：把类User定义在类Role中，类User就被称为内部类。
```java
class Role {
    class User {
    }
}
```
**1、内部类的访问规则**
​
1）可以直接访问外部类的成员，包括私有

​2）外部类要想访问内部类成员，必须创建对象

**2、内部类的分类**

​1）成员内部类

​2）局部内部类

​3）静态内部类

​4）匿名内部类

### 题9：[RMI 的 stub扮演了什么样的角色？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题9rmi-的-stub扮演了什么样的角色)<br/>
远程对象的stub扮演了远程对象的代表或者代理的角色。调用者在本地stub上调用方法，它负责在远程对象上执行方法。当stub的方法被调用的时候，会经历以下几个步骤：

1）初始化到包含了远程对象的JVM的连接。

2）序列化参数到远程的JVM。

3）等待方法调用和执行的结果。

4）反序列化返回的值或者是方法没有执行成功情况下的异常。

5）把值返回给调用者。

### 题10：[访问修饰符 public、private、protected 及不写（默认）时的区别？](/docs/Java%20基础/金三银四面试Java基础题汇总及答案.md#题10访问修饰符-publicprivateprotected-及不写默认时的区别)<br/>

区别如下：

|作用域|当前类|同包|子类|其他|
|-|-|-|-|-|
|public|√|√|√|√|
|protected|√|√|√|×|
|default|√|√|×|×|
|private|√|×|×|×|

类的成员不写访问修饰时默认为default。默认对于同一个包中的其他类相当于公开（public），对于不是同一个包中的其他类相当于私有（private）。受保护（protected）对子类相当于公开，对不是同一包中的没有父子关系的类相当于私有。

### 题11：java-中-error-和-exception都有哪些区别<br/>


### 题12：java-中-this-和-super-有哪些用法区别<br/>


### 题13：java-中常量和变量有哪些区别<br/>


### 题14：java-编程语言有哪些特点<br/>


### 题15：面向对象编程有哪些特征<br/>


### 题16：-oop-中的-组合聚合和关联有什么区别<br/>


### 题17：java-中常量有哪几种类型<br/>


### 题18：为什么静态方法中不能调用非静态方法或变量<br/>


### 题19：java-中常见都有哪些-runtimeexception<br/>


### 题20：java-如何实现字符串中查找某字符出现的次数<br/>


### 题21：什么是非对称加密<br/>


### 题22：java-中-@xmltransient-和-@transient-有什么区别<br/>


### 题23：了解过字节码的编译过程吗<br/>


### 题24：java-中-i-和-i-有什么区别<br/>


### 题25：java-变量命名规则是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")