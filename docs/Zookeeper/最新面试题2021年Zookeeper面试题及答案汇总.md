# 最新面试题2021年Zookeeper面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Zookeeper

### 题1：[Zookeeper 中都有哪些默认端口？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题1zookeeper-中都有哪些默认端口)<br/>
Zookeeper的默认端口有3个，如下：

2181：对Client端提供服务的端口。

3888：选举Leader。

2888：集群内的机器通讯使用。（Leader使用此端口）

### 题2：[Zookeeper 中 Stat 记录有哪些版本相关数据？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题2zookeeper-中-stat-记录有哪些版本相关数据)<br/>
version：当前ZNode版本

cversion：当前ZNode子节点版本

aversion：当前ZNode的ACL版本

### 题3：[Zookeeper 中什么是 ZAB 协议？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题3zookeeper-中什么是-zab-协议)<br/>
ZAB协议是为分布式协调服务Zookeeper专门设计的一种支持崩溃恢复的原子广播协议。

ZAB协议包括两种基本的模式：崩溃恢复和消息广播。

崩溃恢复：在正常情况下运行非常良好，一旦Leader出现崩溃或者由于网络原因导致Leader服务器失去了与过半Follower的联系，那么就会进入崩溃恢复模式。为了程序的正确运行，整个恢复过程后需要选举出一个新的Leader,因此需要一个高效可靠的选举方法快速选举出一个Leader。

消息广播：类似一个两阶段提交过程，针对客户端的事务请求， Leader服务器会为其生成对应的事务Proposal,并将其发送给集群中的其余所有机器，再分别收集各自的选票，最后进行事务提交。

当整个zookeeper集群刚刚启动或者Leader服务器宕机、重启或者网络故障导致不存在过半的服务器与Leader服务器保持正常通信时，所有进程（服务器）进入崩溃恢复模式，首先选举产生新的Leader服务器，然后集群中Follower服务器开始与新的Leader服务器进行数据同步，当集群中超过半数机器与该Leader服务器完成数据同步之后，退出恢复模式进入消息广播模式，Leader服务器开始接收客户端的事务请求生成事物提案来进行事务请求处理。

### 题4：[Zookeeper 中支持自动清理日志？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题4zookeeper-中支持自动清理日志)<br/>
Zookeeper不支持自动清理日志，需要运维人员手动或编写shell脚本清理日志。

### 题5：[Zookeeper 常用命令都有哪些？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题5zookeeper-常用命令都有哪些)<br/>
**1、ZooKeeper服务命令**

1）启动ZK服务: bin/zkServer.sh start

2）查看ZK服务状态: bin/zkServer.sh status

3）停止ZK服务: bin/zkServer.sh stop

4）重启ZK服务: bin/zkServer.sh restart

5）连接服务器: zkCli.sh -server 127.0.0.1:2181

**2、连接ZooKeeper**

启动ZooKeeper服务后可以使用命令连接到ZooKeeper服务：
```shell
zkCli.sh -server 127.0.0.1:2181
```
**3、ZooKeeper客户端命令**

1）ls -- 查看某个目录包含的所有文件：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] ls /
```
2）ls2 -- 查看某个目录包含的所有文件，与ls不同的是它查看到time、version等信息：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] ls2 /
```
3）create -- 创建znode，并设置初始内容：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] create /test "test"
Created /test
```
创建一个新的znode节点“test”以及与它关联的字符串

4）get -- 获取znode的数据：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] get /test
```
5）set -- 修改znode内容：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] set /test "ricky"
```
6）delete -- 删除znode：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] delete /test
```
7）quit -- 退出客户端

8）help -- 帮助命令

### 题6：[ZooKeeper 中 什么是 ACL 权限控制机制？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题6zookeeper-中-什么是-acl-权限控制机制)<br/>
**什么是ACL？**

ACL全称Access Control List即访问控制列表，用于控制资源的访问权限。

ZooKeeper利用ACL策略控制节点的访问权限，如节点数据读写、节点创建、节点删除、读取子节点列表、设置节点权限等。

在传统文件系统中，ACL分为两个维度。一个是属组，一个是权限。属组包含多个权限，一个文件或目录拥有某个组的权限即拥有了组里的所有权限，文件或子目录默认会继承自父目录的ACL。

Zookeeper中znode的ACL是没有继承关系的，每个znode的权限都是独立控制的，只有客户端满足znode设置的权限要求时，才能完成相应的操作。

Zookeeper的ACL，分为三个维度：scheme、id、permission。

通常表示方式：

```shell
scheme:id:permission
```
schema代表授权策略，id代表授权对象，permission代表权限。

ACL（Access Control List）访问控制列表，包括三个方面：

**权限模式（Scheme）**

IP：从IP地址粒度进行权限控制。

Digest：最常用，用类似于username:password的权限标识来进行权限配置，便于区分不同应用来进行权限控制。

World：最开放的权限控制方式，是一种特殊的digest模式，只有一个权限标识“world:anyone”。

Super：超级用户。

**授权对象（Id）**

授权对象指权限赋予的用户或一个指定实体。

**权限（Permission）**

CREATE：数据节点创建权限，允许授权对象在该Znode下创建子节点。

DELETE：子节点删除权限，允许授权对象删除该数据节点的子节点。

READ：数据节点的读取权限，允许授权对象访问该数据节点并读取其数据内容或子节点列表等。

WRITE：数据节点更新权限，允许授权对象对该数据节点进行更新操作。

ADMIN：数据节点管理权限，允许授权对象对该数据节点进行ACL相关设置操作。

### 题7：[Zookeeper 中如何选取主 leader 的？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题7zookeeper-中如何选取主-leader-的)<br/>
当leader崩溃或者leader失去大多数的follower，这时zk进入恢复模式，恢复模式需要重新选举出一个新的leader，让所有的Server都恢复到一个正确的状态。

Zk的选举算法有两种：一种是基于basic paxos实现的，另外一种是基于fast paxos算法实现的。系统默认的选举算法为fast paxos。

1、Zookeeper选主流程(basic paxos)

1）选举线程由当前Server发起选举的线程担任，其主要功能是对投票结果进行统计，并选出推荐的Server；

2）选举线程首先向所有Server发起一次询问(包括自己)；

3）选举线程收到回复后，验证是否是自己发起的询问(验证zxid是否一致)，然后获取对方的id(myid)，并存储到当前询问对象列表中，最后获取对方提议的leader相关信息(id,zxid)，并将这些信息存储到当次选举的投票记录表中；

4）收到所有Server回复以后，就计算出zxid最大的那个Server，并将这个Server相关信息设置成下一次要投票的Server；

5）线程将当前zxid最大的Server设置为当前Server要推荐的Leader，如果此时获胜的Server获得n/2 + 1的Server票数，设置当前推荐的leader为获胜的Server，将根据获胜的Server相关信息设置自己的状态，否则，继续这个过程，直到leader被选举出来。 通过流程分析我们可以得出：要使Leader获得多数Server的支持，则Server总数必须是奇数2n+1，且存活的Server的数目不得少于n+1. 每个Server启动后都会重复以上流程。在恢复模式下，如果是刚从崩溃状态恢复的或者刚启动的server还会从磁盘快照中恢复数据和会话信息，zk会记录事务日志并定期进行快照，方便在恢复时进行状态恢复。

### 题8：[ZooKeeper 客户端如何注册 Watcher 实现？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题8zookeeper-客户端如何注册-watcher-实现)<br/>
1）客户端调用getData()、getChildren()、exist()三个接口，传入Watcher对象。

2）标记请求request，封装Watcher到WatchRegistration。

3）封装成Packet对象，发送request至服务端。

4）收到服务端响应后，将Watcher注册到ZKWatcherManager中进行管理。

5）请求返回，完成注册。

### 题9：[Zookeeper 队列有哪些类型？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题9zookeeper-队列有哪些类型)<br/>
两种类型的队列：
1、同步队列，当一个队列的成员都聚齐时，这个队列才可用，否则一直等待所有成员到达。
2、队列按照 FIFO 方式进行入队和出队操作。
第一类，在约定目录下创建临时目录节点，监听节点数目是否是我们要求的数目。
第二类，和分布式锁服务中的控制时序场景基本原理一致，入列有编号，出列按编号。在特定的目录下创建PERSISTENT_SEQUENTIAL节点，创建成功时Watcher通知等待的队列，队列删除序列号最小的节点用以消费。此场景下Zookeeper的znode用于消息存储，znode存储的数据就是消息队列中的消息内容，SEQUENTIAL序列号就是消息的编号，按序取出即可。由于创建的节点是持久化的，所以不必担心队列消息的丢失问题。

### 题10：[Zookeeper 中 Chroot 特性有什么作用？](/docs/Zookeeper/最新面试题2021年Zookeeper面试题及答案汇总.md#题10zookeeper-中-chroot-特性有什么作用)<br/>
Zookeeper3.2.0版本后，添加了Chroot特性，该特性允许每个客户端为自己设置一个命名空间。

如果一个客户端设置了Chroot，那么该客户端对服务器的任何操作，都将会被限制在其自己的命名空间下。

通过设置Chroot，能够将一个客户端应用于Zookeeper服务端的一颗子树相对应，在那些多个应用公用一个Zookeeper进群的场景下，对实现不同应用间的相互隔离非常有帮助。

### 题11：paxos-和-zab-算法有什么区别和联系<br/>


### 题12：zookeeper-中是否支持禁止某一-ip-访问<br/>


### 题13：zookeeper-提供了什么<br/>


### 题14：zookeeper-和-nginx-的负载均衡有什么区别<br/>


### 题15：zookeeper-中对节点是永久watch监听通知吗<br/>


### 题16：zookeeper-中数据复制有什么优点<br/>


### 题17：zookeeper-中什么情况下删除临时节点<br/>


### 题18：zookeeper-有哪几种部署模式<br/>


### 题19：zookeeper-中节点增多时什么情况导致-ptbalancer-速度变慢<br/>


### 题20：zookeeper-有哪些典型应用场景<br/>


### 题21：zookeeper-服务端如何处理-watcher-实现<br/>


### 题22：zookeeper-中支持临时节点创建子节点吗<br/>


### 题23：zookeeper-中如何识别请求的先后顺序<br/>


### 题24：zookeeper-节点存储数据有没有限制<br/>


### 题25：说一说-zookeeper-中数据同步流程<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")