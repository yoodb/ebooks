# 2022年初，常见Zookeeper面试题汇总及答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Zookeeper

### 题1：[ZooKeeper 客户端如何注册 Watcher 实现？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题1zookeeper-客户端如何注册-watcher-实现)<br/>
1）客户端调用getData()、getChildren()、exist()三个接口，传入Watcher对象。

2）标记请求request，封装Watcher到WatchRegistration。

3）封装成Packet对象，发送request至服务端。

4）收到服务端响应后，将Watcher注册到ZKWatcherManager中进行管理。

5）请求返回，完成注册。

### 题2：[Zookeeper  中 Server 都有哪些工作状态？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题2zookeeper--中-server-都有哪些工作状态)<br/>
Zookeeper服务器具有四种状态，分别是LOOKING、FOLLOWING、LEADING、OBSERVING。

**LOOKING：寻找Leader状态**

当服务器处于该状态时，它会认为当前集群中没有Leader，因此需要进入Leader选举状态，也就是正在进行选举Leader的过程的server所处状态

**LEADING：领导者状态**

表示当前Zookeeper服务器角色是Leader，已被选举为Leader后的server所处状态。

**FOLLOWING：跟随者状态**

表示当前Zookeeper服务器角色是Follower，成为Follower后的server所处状态。

**OBSERVING：观察者状态**

表示当前Zookeeper服务器角色是Observer，在一个很大的集群中没有投票权的server所处状态。

### 题3：[Zookeeper 中都有哪些默认端口？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题3zookeeper-中都有哪些默认端口)<br/>
Zookeeper的默认端口有3个，如下：

2181：对Client端提供服务的端口。

3888：选举Leader。

2888：集群内的机器通讯使用。（Leader使用此端口）

### 题4：[Zookeeper 节点存储数据有没有限制？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题4zookeeper-节点存储数据有没有限制)<br/>
Zookeeper中每个结点默认的数据量上限是1M，如果需要存入大于1M的数据量，则要修改jute.maxbuffer参数。

jute.maxbuffer：默认值1048575，单位字节，用于配置单个数据节点（ZNode）上可以存储的最大数据大小。

需要注意的是在修改该参数的时候，需要在zookeeper集群的所有服务端以及客户端上设置才能生效。

### 题5：[Zookeeper 和 Nginx 的负载均衡有什么区别？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题5zookeeper-和-nginx-的负载均衡有什么区别)<br/>
Zookeeper的负载均衡是可以调控，而Nginx的负载均衡只是能调整权重，其他可控的都需要自己写Lua插件；但是Nginx的吞吐量比Zookeeper大很多，具体按业务应用场景选择用哪种方式。

### 题6：[ZooKeeper 中是否支持禁止某一 IP 访问？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题6zookeeper-中是否支持禁止某一-ip-访问)<br/>
ZK本身不提供这样的功能，它仅仅提供了对单个IP的连接数的限制。

但是可以通过修改iptables等方式来实现对单个ip的限制。

### 题7：[Zookeeper 中什么是 ZAB 协议？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题7zookeeper-中什么是-zab-协议)<br/>
ZAB协议是为分布式协调服务Zookeeper专门设计的一种支持崩溃恢复的原子广播协议。

ZAB协议包括两种基本的模式：崩溃恢复和消息广播。

崩溃恢复：在正常情况下运行非常良好，一旦Leader出现崩溃或者由于网络原因导致Leader服务器失去了与过半Follower的联系，那么就会进入崩溃恢复模式。为了程序的正确运行，整个恢复过程后需要选举出一个新的Leader,因此需要一个高效可靠的选举方法快速选举出一个Leader。

消息广播：类似一个两阶段提交过程，针对客户端的事务请求， Leader服务器会为其生成对应的事务Proposal,并将其发送给集群中的其余所有机器，再分别收集各自的选票，最后进行事务提交。

当整个zookeeper集群刚刚启动或者Leader服务器宕机、重启或者网络故障导致不存在过半的服务器与Leader服务器保持正常通信时，所有进程（服务器）进入崩溃恢复模式，首先选举产生新的Leader服务器，然后集群中Follower服务器开始与新的Leader服务器进行数据同步，当集群中超过半数机器与该Leader服务器完成数据同步之后，退出恢复模式进入消息广播模式，Leader服务器开始接收客户端的事务请求生成事物提案来进行事务请求处理。

### 题8：[说一说 Zookeeper 中数据同步流程？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题8说一说-zookeeper-中数据同步流程)<br/>
整个集群完成Leader选举之后，Learner（Follower和Observer的统称）回向Leader服务器进行注册。当Learner服务器想Leader服务器完成注册后，进入数据同步环节。

数据同步流程：（均以消息传递的方式进行）

>1）Learner向Learder注册
2）数据同步
3）同步确认

Zookeeper的数据同步通常分为四类：

>直接差异化同步（DIFF同步）
先回滚再差异化同步（TRUNC+DIFF同步）
仅回滚同步（TRUNC同步）
全量同步（SNAP同步）
在进行数据同步前，Leader服务器会完成数据同步初始化：

peerLastZxid：从learner服务器注册时发送的ACKEPOCH消息中提取lastZxid（该Learner服务器最后处理的ZXID）

minCommittedLog：Leader服务器Proposal缓存队列committedLog中最小ZXID

maxCommittedLog：Leader服务器Proposal缓存队列committedLog中最大ZXID

**直接差异化同步（DIFF同步）**

场景：peerLastZxid介于minCommittedLog和maxCommittedLog之间

**先回滚再差异化同步（TRUNC+DIFF同步）**

场景：当新的Leader服务器发现某个Learner服务器包含了一条自己没有的事务记录，那么就需要让该Learner服务器进行事务回滚--回滚到Leader服务器上存在的，同时也是最接近于peerLastZxid的ZXID

**仅回滚同步（TRUNC同步）**

场景：peerLastZxid 大于 maxCommittedLog

**全量同步（SNAP同步）**

场景一：peerLastZxid 小于 minCommittedLog

场景二：Leader服务器上没有Proposal缓存队列且peerLastZxid不等于lastProcessZxid

### 题9：[Zookeeper 中如何识别请求的先后顺序？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题9zookeeper-中如何识别请求的先后顺序)<br/>
ZooKeeper会给每个更新请求，分配一个全局唯一的递增编号（zxid)，编号的大小体现事务操作的先后顺序。

### 题10：[Zookeeper 中 Watcher 工作机制和特性？](/docs/Zookeeper/2022年初，常见Zookeeper面试题汇总及答案.md#题10zookeeper-中-watcher-工作机制和特性)<br/>
Zookeeper允许客户端向服务端的某个Znode注册一个Watcher监听，当服务端的一些指定事件触发了这个Watcher，服务端会向指定客户端发送一个事件通知来实现分布式的通知功能，然后客户端根据Watcher通知状态和事件类型做出业务上的改变。

**工作机制**

客户端注册watcher
服务端处理watcher
客户端回调watcher

**Watcher特性**

1）一次性

无论是服务端还是客户端，一旦一个Watcher被触发，Zookeeper都会将其从相应的存储中移除。这样的设计有效的减轻了服务端的压力，不然对于更新非常频繁的节点，服务端会不断的向客户端发送事件通知，无论对于网络还是服务端的压力都非常大。

2）客户端串行执行

客户端Watcher回调的过程是一个串行同步的过程。

3）轻量

Watcher通知非常简单，只会告诉客户端发生了事件，而不会说明事件的具体内容。
客户端向服务端注册Watcher的时候，并不会把客户端真实的Watcher对象实体传递到服务端，仅仅是在客户端请求中使用boolean类型属性进行了标记。

watcher event异步发送watcher的通知事件从server发送到client是异步的，这就存在一个问题，不同的客户端和服务器之间通过socket进行通信，由于网络延迟或其他因素导致客户端在不通的时刻监听到事件，由于Zookeeper本身提供了ordering guarantee，即客户端监听事件后，才会感知它所监视znode发生了变化。所以我们使用Zookeeper不能期望能够监控到节点每次的变化。Zookeeper只能保证最终的一致性，而无法保证强一致性。

注册watcher getData、exists、getChildren

触发watcher create、delete、setData

当一个客户端连接到一个新的服务器上时，watch将会被以任意会话事件触发。当与一个服务器失去连接的时候，是无法接收到watch的。而当client重新连接时，如果需要的话，所有先前注册过的watch，都会被重新注册。通常这是完全透明的。只有在一个特殊情况下，watch可能会丢失：对于一个未创建的znode的exist watch，如果在客户端断开连接期间被创建了，并且随后在客户端连接上之前又删除了，这种情况下，这个watch事件可能会被丢失。

### 题11：zookeeper-有哪几种部署模式<br/>


### 题12：zookeeper-支持哪种类型的数据节点<br/>


### 题13：zookeeper-中什么情况下导致-zab-进入恢复模式并选取新的-leader?<br/>


### 题14：分布式集群中为什么会有-master<br/>


### 题15：zookeeper-提供了什么<br/>


### 题16：zookeeper-中-什么是-acl-权限控制机制<br/>


### 题17：zookeeper-中-java-客户端都有哪些<br/>


### 题18：paxos-和-zab-算法有什么区别和联系<br/>


### 题19：zookeeper-节点宕机如何处理<br/>


### 题20：zookeeper-中如何实现通知机制的<br/>


### 题21：zookeeper-怎么保证主从节点的状态同步<br/>


### 题22：zookeeper-中支持临时节点创建子节点吗<br/>


### 题23：zookeeper-中都有哪些服务器角色<br/>


### 题24：zookeeper-集群支持动态添加服务器吗<br/>


### 题25：zookeeper-命名服务是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")