# 2021年最新最全面Zookeeper面试题（总结全面的面试题）

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Zookeeper

### 题1：[ZooKeeper 客户端如何注册 Watcher 实现？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题1zookeeper-客户端如何注册-watcher-实现)<br/>
1）客户端调用getData()、getChildren()、exist()三个接口，传入Watcher对象。

2）标记请求request，封装Watcher到WatchRegistration。

3）封装成Packet对象，发送request至服务端。

4）收到服务端响应后，将Watcher注册到ZKWatcherManager中进行管理。

5）请求返回，完成注册。

### 题2：[Zookeeper 集群最少要几台服务器，什么规则？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题2zookeeper-集群最少要几台服务器什么规则)<br/>
集群最少要3台服务器，集群规则为2N+1台，N>0，即3台。

### 题3：[说一说 Zookeeper 工作原理？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题3说一说-zookeeper-工作原理)<br/>
Zookeeper的核心是原子广播，这个机制保证了各个Server之间的同步。

实现这个机制的协议叫做Zab协议。

Zab协议有两种模式，它们分别是恢复模式（选主）和广播模式（同步）。

当服务启动或者在领导者崩溃后，Zab就进入了恢复模式，当领导者被选举出来，且大多数Server完成了和 leader的状态同步以后，恢复模式就结束了。

状态同步保证了leader和Server具有相同的系统状态。 

为了保证事务的顺序一致性，zookeeper采用了递增的事务id号（zxid）来标识事务。

所有的提议（proposal）都在被提出的时候加上了zxid。

实现中zxid是一个64位的数字，它高32位是epoch用来标识leader关系是否改变，每次一个leader被选出来，它都会有一个新的epoch，标识当前属于那个leader的统治时期。低32位用于递增计数。

### 题4：[ZooKeeper 客户端如何回调 Watcher？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题4zookeeper-客户端如何回调-watcher)<br/>
ZooKeeper客户端SendThread线程接收事件通知，交由EventThread线程回调Watcher。

ZooKeeper客户端的Watcher机制是一次性的，一旦被触发后这个Watcher就失效了。

### 题5：[Zookeeper 中如何选取主 leader 的？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题5zookeeper-中如何选取主-leader-的)<br/>
当leader崩溃或者leader失去大多数的follower，这时zk进入恢复模式，恢复模式需要重新选举出一个新的leader，让所有的Server都恢复到一个正确的状态。

Zk的选举算法有两种：一种是基于basic paxos实现的，另外一种是基于fast paxos算法实现的。系统默认的选举算法为fast paxos。

1、Zookeeper选主流程(basic paxos)

1）选举线程由当前Server发起选举的线程担任，其主要功能是对投票结果进行统计，并选出推荐的Server；

2）选举线程首先向所有Server发起一次询问(包括自己)；

3）选举线程收到回复后，验证是否是自己发起的询问(验证zxid是否一致)，然后获取对方的id(myid)，并存储到当前询问对象列表中，最后获取对方提议的leader相关信息(id,zxid)，并将这些信息存储到当次选举的投票记录表中；

4）收到所有Server回复以后，就计算出zxid最大的那个Server，并将这个Server相关信息设置成下一次要投票的Server；

5）线程将当前zxid最大的Server设置为当前Server要推荐的Leader，如果此时获胜的Server获得n/2 + 1的Server票数，设置当前推荐的leader为获胜的Server，将根据获胜的Server相关信息设置自己的状态，否则，继续这个过程，直到leader被选举出来。 通过流程分析我们可以得出：要使Leader获得多数Server的支持，则Server总数必须是奇数2n+1，且存活的Server的数目不得少于n+1. 每个Server启动后都会重复以上流程。在恢复模式下，如果是刚从崩溃状态恢复的或者刚启动的server还会从磁盘快照中恢复数据和会话信息，zk会记录事务日志并定期进行快照，方便在恢复时进行状态恢复。

### 题6：[Zookeeper 中什么是 ZAB 协议？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题6zookeeper-中什么是-zab-协议)<br/>
ZAB协议是为分布式协调服务Zookeeper专门设计的一种支持崩溃恢复的原子广播协议。

ZAB协议包括两种基本的模式：崩溃恢复和消息广播。

崩溃恢复：在正常情况下运行非常良好，一旦Leader出现崩溃或者由于网络原因导致Leader服务器失去了与过半Follower的联系，那么就会进入崩溃恢复模式。为了程序的正确运行，整个恢复过程后需要选举出一个新的Leader,因此需要一个高效可靠的选举方法快速选举出一个Leader。

消息广播：类似一个两阶段提交过程，针对客户端的事务请求， Leader服务器会为其生成对应的事务Proposal,并将其发送给集群中的其余所有机器，再分别收集各自的选票，最后进行事务提交。

当整个zookeeper集群刚刚启动或者Leader服务器宕机、重启或者网络故障导致不存在过半的服务器与Leader服务器保持正常通信时，所有进程（服务器）进入崩溃恢复模式，首先选举产生新的Leader服务器，然后集群中Follower服务器开始与新的Leader服务器进行数据同步，当集群中超过半数机器与该Leader服务器完成数据同步之后，退出恢复模式进入消息广播模式，Leader服务器开始接收客户端的事务请求生成事物提案来进行事务请求处理。

### 题7：[Paxos 和 ZAB 算法有什么区别和联系？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题7paxos-和-zab-算法有什么区别和联系)<br/>
**相同点**

两者都存在一个类似于Leader进程的角色，由其负责协调多个Follower进程的运行；

Leader进程都会等待超过半数的Follower做出正确的反馈后，才会将一个提案进行提交；

ZAB协议中每个Proposal中都包含一个epoch值来代表当前的Leader周期，而Paxos中名字为Ballot。

**不同点**

Paxos是用来构建分布式一致性状态机系统，而ZAB用来构建高可用的分布式数据主备系统（Zookeeper）。

### 题8：[Zookeeper 有哪几种部署模式？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题8zookeeper-有哪几种部署模式)<br/>
部署模式：单机模式、伪集群模式、集群模式。

### 题9：[Zookeeper 中 Java 客户端都有哪些？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题9zookeeper-中-java-客户端都有哪些)<br/>
Java客户端：

1）zk自带的zkclient

2）Apache开源的Curator

### 题10：[Zookeeper 中如何识别请求的先后顺序？](/docs/Zookeeper/2021年最新最全面Zookeeper面试题（总结全面的面试题）.md#题10zookeeper-中如何识别请求的先后顺序)<br/>
ZooKeeper会给每个更新请求，分配一个全局唯一的递增编号（zxid)，编号的大小体现事务操作的先后顺序。

### 题11：zookeeper-中会话管理使用什么策略和分配原则<br/>


### 题12：zookeeper-和-nginx-的负载均衡有什么区别<br/>


### 题13：zookeeper-服务端如何处理-watcher-实现<br/>


### 题14：zookeeper-支持哪种类型的数据节点<br/>


### 题15：分布式集群中为什么会有-master<br/>


### 题16：zookeeper-和文件系统有哪些区别<br/>


### 题17：zookeeper-常用命令都有哪些<br/>


### 题18：zookeeper-中对节点是永久watch监听通知吗<br/>


### 题19：zookeeper-中-chroot-特性有什么作用<br/>


### 题20：zookeeper-队列有哪些类型<br/>


### 题21：zookeeper-节点宕机如何处理<br/>


### 题22：zookeeper-中支持临时节点创建子节点吗<br/>


### 题23：zookeeper-中-stat-记录有哪些版本相关数据<br/>


### 题24：zookeeper-中都有哪些默认端口<br/>


### 题25：chubby-和-zookeeper-有哪些区别<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")