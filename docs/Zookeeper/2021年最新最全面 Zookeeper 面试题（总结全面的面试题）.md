# 2021年最新最全面 Zookeeper 面试题（总结全面的面试题）

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Zookeeper

### 题1：[Zookeeper 和文件系统有哪些区别？](/docs/Zookeeper/2021年最新最全面%20Zookeeper%20面试题（总结全面的面试题）.md#题1zookeeper-和文件系统有哪些区别)<br/>
Zookeeper提供一个多层级的节点命名空间（节点称为znode）。与文件系统不同的是，这些节点都可以设置关联的数据，而文件系统中只有文件节点可以存放数据而目录节点不行。

Zookeeper为了保证高吞吐和低延迟，在内存中维护了这个树状的目录结构，这种特性使得Zookeeper 不能用于存放大量的数据，每个节点的存放数据上限为1M。

### 题2：[Zookeeper 中如何选取主 leader 的？](/docs/Zookeeper/2021年最新最全面%20Zookeeper%20面试题（总结全面的面试题）.md#题2zookeeper-中如何选取主-leader-的)<br/>
当leader崩溃或者leader失去大多数的follower，这时zk进入恢复模式，恢复模式需要重新选举出一个新的leader，让所有的Server都恢复到一个正确的状态。

Zk的选举算法有两种：一种是基于basic paxos实现的，另外一种是基于fast paxos算法实现的。系统默认的选举算法为fast paxos。

1、Zookeeper选主流程(basic paxos)

1）选举线程由当前Server发起选举的线程担任，其主要功能是对投票结果进行统计，并选出推荐的Server；

2）选举线程首先向所有Server发起一次询问(包括自己)；

3）选举线程收到回复后，验证是否是自己发起的询问(验证zxid是否一致)，然后获取对方的id(myid)，并存储到当前询问对象列表中，最后获取对方提议的leader相关信息(id,zxid)，并将这些信息存储到当次选举的投票记录表中；

4）收到所有Server回复以后，就计算出zxid最大的那个Server，并将这个Server相关信息设置成下一次要投票的Server；

5）线程将当前zxid最大的Server设置为当前Server要推荐的Leader，如果此时获胜的Server获得n/2 + 1的Server票数，设置当前推荐的leader为获胜的Server，将根据获胜的Server相关信息设置自己的状态，否则，继续这个过程，直到leader被选举出来。 通过流程分析我们可以得出：要使Leader获得多数Server的支持，则Server总数必须是奇数2n+1，且存活的Server的数目不得少于n+1. 每个Server启动后都会重复以上流程。在恢复模式下，如果是刚从崩溃状态恢复的或者刚启动的server还会从磁盘快照中恢复数据和会话信息，zk会记录事务日志并定期进行快照，方便在恢复时进行状态恢复。

### 题3：[Zookeeper 中 Stat 记录有哪些版本相关数据？](/docs/Zookeeper/2021年最新最全面%20Zookeeper%20面试题（总结全面的面试题）.md#题3zookeeper-中-stat-记录有哪些版本相关数据)<br/>
version：当前ZNode版本

cversion：当前ZNode子节点版本

aversion：当前ZNode的ACL版本

### 题4：[ZooKeeper 客户端如何回调 Watcher？](/docs/Zookeeper/2021年最新最全面%20Zookeeper%20面试题（总结全面的面试题）.md#题4zookeeper-客户端如何回调-watcher)<br/>
ZooKeeper客户端SendThread线程接收事件通知，交由EventThread线程回调Watcher。

ZooKeeper客户端的Watcher机制是一次性的，一旦被触发后这个Watcher就失效了。

### 题5：[Zookeeper 中会话管理使用什么策略和分配原则？](/docs/Zookeeper/2021年最新最全面%20Zookeeper%20面试题（总结全面的面试题）.md#题5zookeeper-中会话管理使用什么策略和分配原则)<br/>
分桶策略：将类似的会话放在同一区块中进行管理，以便于Zookeeper对会话进行不同区块的隔离处理以及同一区块的统一处理。

分配原则：每个会话的“下次超时时间点”（ExpirationTime）

计算公式：

```java
ExpirationTime_ = currentTime + sessionTimeout
ExpirationTime = (ExpirationTime_ / ExpirationInrerval + 1) * ExpirationInterval
```
ExpirationInterval是指Zookeeper会话超时检查时间间隔，默认tickTime

### 题6：[Zookeeper 怎么保证主从节点的状态同步？](/docs/Zookeeper/2021年最新最全面%20Zookeeper%20面试题（总结全面的面试题）.md#题6zookeeper-怎么保证主从节点的状态同步)<br/>
Zookeeper的核心是原子广播机制，这个机制保证了各个server之间的同步。实现这个机制的协议叫做Zab协议。

Zab协议有两种模式，分别是恢复模式和广播模式。

**恢复模式**

当服务启动或者在领导者崩溃后，Zab就进入了恢复模式，当领导者被选举出来，且大多数server完成了和leader的状态同步以后，恢复模式就结束了。

状态同步保证了leader和server具有相同的系统状态。

**广播模式**

一旦leader已经和多数的follower进行了状态同步后，它就可以开始广播消息了，即进入广播状态。

此时当一个server加入ZooKeeper服务中，它会在恢复模式下启动，发现leader，并和leader进行状态同步。

待到同步结束，它也参与消息广播。

ZooKeeper服务一直维持在Broadcast状态，直到leader崩溃了或者leader失去了大部分的followers支持。

### 题7：[Zookeeper 集群支持动态添加服务器吗？](/docs/Zookeeper/2021年最新最全面%20Zookeeper%20面试题（总结全面的面试题）.md#题7zookeeper-集群支持动态添加服务器吗)<br/>
动态添加服务器等同于水平扩容，而Zookeeper在这方面的表现并不是太好。两种方式：

**1）全部重启**

关闭所有Zookeeper服务，修改配置之后启动。不影响之前客户端的会话。

**2）逐一重启**

在过半存活即可用的原则下，一台机器重启不影响整个集群对外提供服务。这是比较常用的方式，在Zookeeper 3.5版本开始支持动态扩容。

### 题8：[Zookeeper 中 Watcher 工作机制和特性？](/docs/Zookeeper/2021年最新最全面%20Zookeeper%20面试题（总结全面的面试题）.md#题8zookeeper-中-watcher-工作机制和特性)<br/>
Zookeeper允许客户端向服务端的某个Znode注册一个Watcher监听，当服务端的一些指定事件触发了这个Watcher，服务端会向指定客户端发送一个事件通知来实现分布式的通知功能，然后客户端根据Watcher通知状态和事件类型做出业务上的改变。

**工作机制**

客户端注册watcher
服务端处理watcher
客户端回调watcher

**Watcher特性**

1）一次性

无论是服务端还是客户端，一旦一个Watcher被触发，Zookeeper都会将其从相应的存储中移除。这样的设计有效的减轻了服务端的压力，不然对于更新非常频繁的节点，服务端会不断的向客户端发送事件通知，无论对于网络还是服务端的压力都非常大。

2）客户端串行执行

客户端Watcher回调的过程是一个串行同步的过程。

3）轻量

Watcher通知非常简单，只会告诉客户端发生了事件，而不会说明事件的具体内容。
客户端向服务端注册Watcher的时候，并不会把客户端真实的Watcher对象实体传递到服务端，仅仅是在客户端请求中使用boolean类型属性进行了标记。

watcher event异步发送watcher的通知事件从server发送到client是异步的，这就存在一个问题，不同的客户端和服务器之间通过socket进行通信，由于网络延迟或其他因素导致客户端在不通的时刻监听到事件，由于Zookeeper本身提供了ordering guarantee，即客户端监听事件后，才会感知它所监视znode发生了变化。所以我们使用Zookeeper不能期望能够监控到节点每次的变化。Zookeeper只能保证最终的一致性，而无法保证强一致性。

注册watcher getData、exists、getChildren

触发watcher create、delete、setData

当一个客户端连接到一个新的服务器上时，watch将会被以任意会话事件触发。当与一个服务器失去连接的时候，是无法接收到watch的。而当client重新连接时，如果需要的话，所有先前注册过的watch，都会被重新注册。通常这是完全透明的。只有在一个特殊情况下，watch可能会丢失：对于一个未创建的znode的exist watch，如果在客户端断开连接期间被创建了，并且随后在客户端连接上之前又删除了，这种情况下，这个watch事件可能会被丢失。

### 题9：[Zookeeper 中支持自动清理日志？](/docs/Zookeeper/2021年最新最全面%20Zookeeper%20面试题（总结全面的面试题）.md#题9zookeeper-中支持自动清理日志)<br/>
Zookeeper不支持自动清理日志，需要运维人员手动或编写shell脚本清理日志。

### 题10：[ZooKeeper 命名服务是什么？](/docs/Zookeeper/2021年最新最全面%20Zookeeper%20面试题（总结全面的面试题）.md#题10zookeeper-命名服务是什么)<br/>

命名服务是指通过指定的名字来获取资源或者服务的地址。

Zookeeper会在自己的文件系统上（树结构的文件系统）创建一个以路径为名称的节点，它可以指向提供的服务的地址、远程对象等。

简单来说使用Zookeeper做命名服务就是用路径作为名字，路径上的数据就是其名字指向的实体。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")