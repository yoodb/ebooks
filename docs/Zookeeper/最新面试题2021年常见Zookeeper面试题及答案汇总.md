# 最新面试题2021年常见Zookeeper面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Zookeeper

### 题1：[Zookeeper 常用命令都有哪些？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题1zookeeper-常用命令都有哪些)<br/>
**1、ZooKeeper服务命令**

1）启动ZK服务: bin/zkServer.sh start

2）查看ZK服务状态: bin/zkServer.sh status

3）停止ZK服务: bin/zkServer.sh stop

4）重启ZK服务: bin/zkServer.sh restart

5）连接服务器: zkCli.sh -server 127.0.0.1:2181

**2、连接ZooKeeper**

启动ZooKeeper服务后可以使用命令连接到ZooKeeper服务：
```shell
zkCli.sh -server 127.0.0.1:2181
```
**3、ZooKeeper客户端命令**

1）ls -- 查看某个目录包含的所有文件：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] ls /
```
2）ls2 -- 查看某个目录包含的所有文件，与ls不同的是它查看到time、version等信息：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] ls2 /
```
3）create -- 创建znode，并设置初始内容：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] create /test "test"
Created /test
```
创建一个新的znode节点“test”以及与它关联的字符串

4）get -- 获取znode的数据：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] get /test
```
5）set -- 修改znode内容：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] set /test "ricky"
```
6）delete -- 删除znode：
```shell
[zk: 127.0.0.1:2181(CONNECTED) 1] delete /test
```
7）quit -- 退出客户端

8）help -- 帮助命令

### 题2：[Zookeeper 集群最少要几台服务器，什么规则？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题2zookeeper-集群最少要几台服务器什么规则)<br/>
集群最少要3台服务器，集群规则为2N+1台，N>0，即3台。

### 题3：[Zookeeper 有哪几种部署模式？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题3zookeeper-有哪几种部署模式)<br/>
部署模式：单机模式、伪集群模式、集群模式。

### 题4：[ZooKeeper 中什么情况下删除临时节点？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题4zookeeper-中什么情况下删除临时节点)<br/>
如果连接断开后，ZK不会马上移除临时数据，只有当SESSIONEXPIRED之后，才会把这个会话建立的临时数据移除。因此，用户需要谨慎设置Session_TimeOut。

### 题5：[ZooKeeper 集群中如何实现服务器之间通信？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题5zookeeper-集群中如何实现服务器之间通信)<br/>
Leader服务器会和每一个Follower/Observer服务器都建立TCP连接，同时为每个F/O都创建一个叫做LearnerHandler的实体。

LearnerHandler主要负责Leader和F/O之间的网络通讯，包括数据同步，请求转发和Proposal提议的投票等。

Leader服务器保存了所有F/O的LearnerHandler。

### 题6：[Zookeeper 中支持自动清理日志？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题6zookeeper-中支持自动清理日志)<br/>
Zookeeper不支持自动清理日志，需要运维人员手动或编写shell脚本清理日志。

### 题7：[Zookeeper 中什么是 ZAB 协议？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题7zookeeper-中什么是-zab-协议)<br/>
ZAB协议是为分布式协调服务Zookeeper专门设计的一种支持崩溃恢复的原子广播协议。

ZAB协议包括两种基本的模式：崩溃恢复和消息广播。

崩溃恢复：在正常情况下运行非常良好，一旦Leader出现崩溃或者由于网络原因导致Leader服务器失去了与过半Follower的联系，那么就会进入崩溃恢复模式。为了程序的正确运行，整个恢复过程后需要选举出一个新的Leader,因此需要一个高效可靠的选举方法快速选举出一个Leader。

消息广播：类似一个两阶段提交过程，针对客户端的事务请求， Leader服务器会为其生成对应的事务Proposal,并将其发送给集群中的其余所有机器，再分别收集各自的选票，最后进行事务提交。

当整个zookeeper集群刚刚启动或者Leader服务器宕机、重启或者网络故障导致不存在过半的服务器与Leader服务器保持正常通信时，所有进程（服务器）进入崩溃恢复模式，首先选举产生新的Leader服务器，然后集群中Follower服务器开始与新的Leader服务器进行数据同步，当集群中超过半数机器与该Leader服务器完成数据同步之后，退出恢复模式进入消息广播模式，Leader服务器开始接收客户端的事务请求生成事物提案来进行事务请求处理。

### 题8：[ZooKeeper 中支持临时节点创建子节点吗？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题8zookeeper-中支持临时节点创建子节点吗)<br/>
ZooKeeper不支持临时节点创建子节点。

### 题9：[说一说 Zookeeper 中数据同步流程？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题9说一说-zookeeper-中数据同步流程)<br/>
整个集群完成Leader选举之后，Learner（Follower和Observer的统称）回向Leader服务器进行注册。当Learner服务器想Leader服务器完成注册后，进入数据同步环节。

数据同步流程：（均以消息传递的方式进行）

>1）Learner向Learder注册
2）数据同步
3）同步确认

Zookeeper的数据同步通常分为四类：

>直接差异化同步（DIFF同步）
先回滚再差异化同步（TRUNC+DIFF同步）
仅回滚同步（TRUNC同步）
全量同步（SNAP同步）
在进行数据同步前，Leader服务器会完成数据同步初始化：

peerLastZxid：从learner服务器注册时发送的ACKEPOCH消息中提取lastZxid（该Learner服务器最后处理的ZXID）

minCommittedLog：Leader服务器Proposal缓存队列committedLog中最小ZXID

maxCommittedLog：Leader服务器Proposal缓存队列committedLog中最大ZXID

**直接差异化同步（DIFF同步）**

场景：peerLastZxid介于minCommittedLog和maxCommittedLog之间

**先回滚再差异化同步（TRUNC+DIFF同步）**

场景：当新的Leader服务器发现某个Learner服务器包含了一条自己没有的事务记录，那么就需要让该Learner服务器进行事务回滚--回滚到Leader服务器上存在的，同时也是最接近于peerLastZxid的ZXID

**仅回滚同步（TRUNC同步）**

场景：peerLastZxid 大于 maxCommittedLog

**全量同步（SNAP同步）**

场景一：peerLastZxid 小于 minCommittedLog

场景二：Leader服务器上没有Proposal缓存队列且peerLastZxid不等于lastProcessZxid

### 题10：[ZooKeeper 中 什么是 ACL 权限控制机制？](/docs/Zookeeper/最新面试题2021年常见Zookeeper面试题及答案汇总.md#题10zookeeper-中-什么是-acl-权限控制机制)<br/>
**什么是ACL？**

ACL全称Access Control List即访问控制列表，用于控制资源的访问权限。

ZooKeeper利用ACL策略控制节点的访问权限，如节点数据读写、节点创建、节点删除、读取子节点列表、设置节点权限等。

在传统文件系统中，ACL分为两个维度。一个是属组，一个是权限。属组包含多个权限，一个文件或目录拥有某个组的权限即拥有了组里的所有权限，文件或子目录默认会继承自父目录的ACL。

Zookeeper中znode的ACL是没有继承关系的，每个znode的权限都是独立控制的，只有客户端满足znode设置的权限要求时，才能完成相应的操作。

Zookeeper的ACL，分为三个维度：scheme、id、permission。

通常表示方式：

```shell
scheme:id:permission
```
schema代表授权策略，id代表授权对象，permission代表权限。

ACL（Access Control List）访问控制列表，包括三个方面：

**权限模式（Scheme）**

IP：从IP地址粒度进行权限控制。

Digest：最常用，用类似于username:password的权限标识来进行权限配置，便于区分不同应用来进行权限控制。

World：最开放的权限控制方式，是一种特殊的digest模式，只有一个权限标识“world:anyone”。

Super：超级用户。

**授权对象（Id）**

授权对象指权限赋予的用户或一个指定实体。

**权限（Permission）**

CREATE：数据节点创建权限，允许授权对象在该Znode下创建子节点。

DELETE：子节点删除权限，允许授权对象删除该数据节点的子节点。

READ：数据节点的读取权限，允许授权对象访问该数据节点并读取其数据内容或子节点列表等。

WRITE：数据节点更新权限，允许授权对象对该数据节点进行更新操作。

ADMIN：数据节点管理权限，允许授权对象对该数据节点进行ACL相关设置操作。

### 题11：zookeeper-怎么保证主从节点的状态同步<br/>


### 题12：zookeeper-中节点增多时什么情况导致-ptbalancer-速度变慢<br/>


### 题13：zookeeper-集群支持动态添加服务器吗<br/>


### 题14：zookeeper-中-watcher-工作机制和特性<br/>


### 题15：zookeeper-中-chroot-特性有什么作用<br/>


### 题16：zookeeper-节点宕机如何处理<br/>


### 题17：zookeeper-提供了什么<br/>


### 题18：什么是-zookeeper<br/>


### 题19：zookeeper-是如何保证事务的顺序一致性的<br/>


### 题20：zookeeper-中如何实现通知机制的<br/>


### 题21：zookeeper-和文件系统有哪些区别<br/>


### 题22：zookeeper-中数据复制有什么优点<br/>


### 题23：zookeeper-中对节点是永久watch监听通知吗<br/>


### 题24：zookeeper-节点存储数据有没有限制<br/>


### 题25：zookeeper-中什么情况下导致-zab-进入恢复模式并选取新的-leader?<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")