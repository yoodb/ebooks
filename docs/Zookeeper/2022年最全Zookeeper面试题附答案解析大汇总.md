# 2022年最全Zookeeper面试题附答案解析大汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Zookeeper

### 题1：[Paxos 和 ZAB 算法有什么区别和联系？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题1paxos-和-zab-算法有什么区别和联系)<br/>
**相同点**

两者都存在一个类似于Leader进程的角色，由其负责协调多个Follower进程的运行；

Leader进程都会等待超过半数的Follower做出正确的反馈后，才会将一个提案进行提交；

ZAB协议中每个Proposal中都包含一个epoch值来代表当前的Leader周期，而Paxos中名字为Ballot。

**不同点**

Paxos是用来构建分布式一致性状态机系统，而ZAB用来构建高可用的分布式数据主备系统（Zookeeper）。

### 题2：[Zookeeper 中 Watcher 工作机制和特性？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题2zookeeper-中-watcher-工作机制和特性)<br/>
Zookeeper允许客户端向服务端的某个Znode注册一个Watcher监听，当服务端的一些指定事件触发了这个Watcher，服务端会向指定客户端发送一个事件通知来实现分布式的通知功能，然后客户端根据Watcher通知状态和事件类型做出业务上的改变。

**工作机制**

客户端注册watcher
服务端处理watcher
客户端回调watcher

**Watcher特性**

1）一次性

无论是服务端还是客户端，一旦一个Watcher被触发，Zookeeper都会将其从相应的存储中移除。这样的设计有效的减轻了服务端的压力，不然对于更新非常频繁的节点，服务端会不断的向客户端发送事件通知，无论对于网络还是服务端的压力都非常大。

2）客户端串行执行

客户端Watcher回调的过程是一个串行同步的过程。

3）轻量

Watcher通知非常简单，只会告诉客户端发生了事件，而不会说明事件的具体内容。
客户端向服务端注册Watcher的时候，并不会把客户端真实的Watcher对象实体传递到服务端，仅仅是在客户端请求中使用boolean类型属性进行了标记。

watcher event异步发送watcher的通知事件从server发送到client是异步的，这就存在一个问题，不同的客户端和服务器之间通过socket进行通信，由于网络延迟或其他因素导致客户端在不通的时刻监听到事件，由于Zookeeper本身提供了ordering guarantee，即客户端监听事件后，才会感知它所监视znode发生了变化。所以我们使用Zookeeper不能期望能够监控到节点每次的变化。Zookeeper只能保证最终的一致性，而无法保证强一致性。

注册watcher getData、exists、getChildren

触发watcher create、delete、setData

当一个客户端连接到一个新的服务器上时，watch将会被以任意会话事件触发。当与一个服务器失去连接的时候，是无法接收到watch的。而当client重新连接时，如果需要的话，所有先前注册过的watch，都会被重新注册。通常这是完全透明的。只有在一个特殊情况下，watch可能会丢失：对于一个未创建的znode的exist watch，如果在客户端断开连接期间被创建了，并且随后在客户端连接上之前又删除了，这种情况下，这个watch事件可能会被丢失。

### 题3：[Zookeeper 中 Java 客户端都有哪些？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题3zookeeper-中-java-客户端都有哪些)<br/>
Java客户端：

1）zk自带的zkclient

2）Apache开源的Curator

### 题4：[Zookeeper 中数据复制有什么优点？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题4zookeeper-中数据复制有什么优点)<br/>
Zookeeper作为一个集群提供一致的数据服务，自然，它要在所有机器间做数据复制。

数据复制的有点：

1、容错：一个节点出错，不致于让整个系统停止工作，别的节点可以接管它的工作；

2、提高系统的扩展能力 ：把负载分布到多个节点上，或者增加节点来提高系统的负载能力；

3、提高性能：让客户端本地访问就近的节点，提高用户访问速度。

### 题5：[ZooKeeper 中 什么是 ACL 权限控制机制？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题5zookeeper-中-什么是-acl-权限控制机制)<br/>
**什么是ACL？**

ACL全称Access Control List即访问控制列表，用于控制资源的访问权限。

ZooKeeper利用ACL策略控制节点的访问权限，如节点数据读写、节点创建、节点删除、读取子节点列表、设置节点权限等。

在传统文件系统中，ACL分为两个维度。一个是属组，一个是权限。属组包含多个权限，一个文件或目录拥有某个组的权限即拥有了组里的所有权限，文件或子目录默认会继承自父目录的ACL。

Zookeeper中znode的ACL是没有继承关系的，每个znode的权限都是独立控制的，只有客户端满足znode设置的权限要求时，才能完成相应的操作。

Zookeeper的ACL，分为三个维度：scheme、id、permission。

通常表示方式：

```shell
scheme:id:permission
```
schema代表授权策略，id代表授权对象，permission代表权限。

ACL（Access Control List）访问控制列表，包括三个方面：

**权限模式（Scheme）**

IP：从IP地址粒度进行权限控制。

Digest：最常用，用类似于username:password的权限标识来进行权限配置，便于区分不同应用来进行权限控制。

World：最开放的权限控制方式，是一种特殊的digest模式，只有一个权限标识“world:anyone”。

Super：超级用户。

**授权对象（Id）**

授权对象指权限赋予的用户或一个指定实体。

**权限（Permission）**

CREATE：数据节点创建权限，允许授权对象在该Znode下创建子节点。

DELETE：子节点删除权限，允许授权对象删除该数据节点的子节点。

READ：数据节点的读取权限，允许授权对象访问该数据节点并读取其数据内容或子节点列表等。

WRITE：数据节点更新权限，允许授权对象对该数据节点进行更新操作。

ADMIN：数据节点管理权限，允许授权对象对该数据节点进行ACL相关设置操作。

### 题6：[分布式集群中为什么会有 Master？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题6分布式集群中为什么会有-master)<br/>
Master选举原理是有多个Master，其每次只有一个Master负责主要的工作，剩余的Master作为备份并负责监听工作的Master，一旦负责工作的Master挂掉，其他的Master就会收到监听的事件，从而去抢夺负责工作的权利，其他没有争夺到负责主要工作的Master转而去监听负责工作的新Master。

在分布式环境中，一般采用master-salve模式（主从模式），正常情况下主机提供服务，备机负责监听主机状态，当主机异常时自动切换到备机继续提供服务，也就是只需要集群中的某一台服务器进行执行，其他的服务器共享这个结果，从而减少服务器的重复计算，提高性能。其切换过程中选出下一个Master的过程就是master选举，因此需要进行leader选举。



### 题7：[Zookeeper 节点宕机如何处理？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题7zookeeper-节点宕机如何处理)<br/>
Zookeeper本身是集群模式，官方推荐不少于3台服务器配置。Zookeeper自身保证当一个节点宕机时，其他节点会继续提供服务。

假设其中一个Follower宕机，还有2台服务器提供访问，因为Zookeeper上的数据是有多个副本的，并不会丢失数据；

如果是一个Leader宕机，Zookeeper会选举出新的Leader。

Zookeeper集群的机制是只要超过半数的节点正常，集群就可以正常提供服务。只有Zookeeper节点挂得太多，只剩不到一半节点提供服务，才会导致Zookeeper集群失效。

**Zookeeper集群节点配置原则**

3个节点的cluster可以挂掉1个节点（leader可以得到2节 > 1.5）。

2个节点的cluster需保证不能挂掉任何1个节点（leader可以得到 1节 <=1）。

### 题8：[ZooKeeper 中支持临时节点创建子节点吗？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题8zookeeper-中支持临时节点创建子节点吗)<br/>
ZooKeeper不支持临时节点创建子节点。

### 题9：[Zookeeper 和文件系统有哪些区别？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题9zookeeper-和文件系统有哪些区别)<br/>
Zookeeper提供一个多层级的节点命名空间（节点称为znode）。与文件系统不同的是，这些节点都可以设置关联的数据，而文件系统中只有文件节点可以存放数据而目录节点不行。

Zookeeper为了保证高吞吐和低延迟，在内存中维护了这个树状的目录结构，这种特性使得Zookeeper 不能用于存放大量的数据，每个节点的存放数据上限为1M。

### 题10：[ZooKeeper 中是否支持禁止某一 IP 访问？](/docs/Zookeeper/2022年最全Zookeeper面试题附答案解析大汇总.md#题10zookeeper-中是否支持禁止某一-ip-访问)<br/>
ZK本身不提供这样的功能，它仅仅提供了对单个IP的连接数的限制。

但是可以通过修改iptables等方式来实现对单个ip的限制。

### 题11：zookeeper-中定义了几种操作权限<br/>


### 题12：zookeeper-队列有哪些类型<br/>


### 题13：zookeeper-集群支持动态添加服务器吗<br/>


### 题14：zookeeper-命名服务是什么<br/>


### 题15：zookeeper-中什么情况下导致-zab-进入恢复模式并选取新的-leader?<br/>


### 题16：zookeeper-中会话管理使用什么策略和分配原则<br/>


### 题17：zookeeper-中对节点是永久watch监听通知吗<br/>


### 题18：zookeeper--中-server-都有哪些工作状态<br/>


### 题19：zookeeper-常用命令都有哪些<br/>


### 题20：zookeeper-有哪些典型应用场景<br/>


### 题21：zookeeper-中如何识别请求的先后顺序<br/>


### 题22：说一说-zookeeper-中数据同步流程<br/>


### 题23：zookeeper-中-chroot-特性有什么作用<br/>


### 题24：zookeeper-是如何保证事务的顺序一致性的<br/>


### 题25：zookeeper-集群最少要几台服务器什么规则<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")