# 最新Spring Cloud面试题及答案附答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Cloud

### 题1：[Ribbon 和 Feign 有什么区别？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题1ribbon-和-feign-有什么区别)<br/>
Ribbon添加maven依赖spring-starter-ribbon使用@RibbonClient(value="服务名称") 使用RestTemplate调用远程服务对应的方法。

Feign添加maven依赖 spring-starter-feign服务提供方提供对外接口 调用方使用在接口上使用@FeignClient("指定服务名")。

**Ribbon和Feign的区别：**

Ribbon和Feign都是用于调用其他服务的，不过方式不同。

1、启动类使用的注解不同，Ribbon用的是@RibbonClient，Feign用的是@EnableFeignClients。

2、服务的指定位置不同，Ribbon是在@RibbonClient注解上声明，Feign则是在定义抽象方法的接口中使用@FeignClient声明。

3、调用方式不同，Ribbon需要自己构建http请求，模拟http请求然后使用RestTemplate发送给其他服务，步骤相当繁琐。

Feign则是在Ribbon的基础上进行了一次改进，采用接口的方式，将需要调用的其他服务的方法定义成抽象方法即可，不需要自己构建http请求。不过要注意的是抽象方法的注解、方法签名要和提供服务的方法完全一致。

### 题2：[Ribbon  和 Nginx  负载均衡有什么区别？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题2ribbon--和-nginx--负载均衡有什么区别)<br/>
Nginx是服务器负载均衡，客户端所有请求都会交给NGINX，然后又NGINX实现转发请求。即负载均衡是有服务端实现的。

Ribbon本地负载均衡，在调用微服务接口时候，会在注册中心获取注册信息服务列表之后缓存到JVM本地，从而在本地实现RPC远程服务调用技术。

### 题3：[什么是 Spring Cloud OpenFeign？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题3什么是-spring-cloud-openfeign)<br/>
基于Ribbon和Hystrix的声明式服务调用组件，可以动态创建基于Spring MVC注解的接口实现用于服务调用，在Spring Cloud 2.0中已经取代Feign成为了优选。

### 题4：[什么是 Spring Cloud Sleuth？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题4什么是-spring-cloud-sleuth)<br/>
Spring Cloud应用程序的分布式请求链路跟踪，支持使用Zipkin、HTrace和基于日志（例如ELK）的跟踪。

### 题5：[Spring Cloud 框架有哪些优缺点？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题5spring-cloud-框架有哪些优缺点)<br/>
**Spring Cloud优点：**

1）服务拆分粒度更细，有利于资源重复利用，有利于提高开发效率，每个模块可以独立开发和部署、代码耦合度低；

2）可以更精准的制定优化服务方案，提高系统的可维护性，每个服务可以单独进行部署，升级某个模块的时候只需要单独部署对应的模块服务即可，效率更高；

3）微服务架构采用去中心化思想，服务之间采用Restful等轻量级通讯，比ESB更轻量，模块专一性提升，每个模块只需要关心自己模块所负责的功能即可，不需要关心其他模块业务，专一性更高，更便于功能模块开发和拓展；

4）技术选型不再单一，由于每个模块是单独开发并且部署，所以每个模块可以有更多的技术选型方案，如模块1数据库选择mysql，模块2选择用oracle也是可以的；


5）适于互联网时代，产品迭代周期更短。系统稳定性以及性能提升，由于微服务是几个服务共同组成的项目或者流程，因此相比传统单一项目的优点就在于某个模块提供的服务宕机过后不至于整个系统瘫痪掉，而且微服务里面的容灾和服务降级机制也能大大提高项目的稳定性；从性能而言，由于每个服务是单独部署，所以每个模块都可以有自己的一套运行环境，当某个服务性能低下的时候可以对单个服务进行配置或者代码上的升级，从而达到提升性能的目的。

**Spring Cloud缺点：**

1）微服务过多，治理成本高，不利于维护系统，服务之间接口调用成本增加，相比以往单项目的时候调用某个方法或者接口可以直接通过本地方法调用就能够完成，但是当切换成微服务的时候，调用方式就不能用以前的方式进行调试、目前主流采用的技术有http api接口调用、RPC、WebService等方式进行调用，调用成本比单个项目的时候有所增加；

2）分布式系统开发的成本高（容错，分布式事务等）对团队挑战大

2）独立的数据库，微服务产生事务一致性的问题，由于各个模块用的技术都各不相同、而且每个服务都会高并发进行调用，就会存在分布式事务一致性的问题；

3）分布式部署，造成运营的成本增加、相比较单个应用的时候，运营人员只需要对单个项目进行部署、负载均衡等操作，但是微服务的每个模块都需要这样的操作，增加了运行时的成本；

4）由于整个系统是通过各个模块组合而成的，因此当某个服务进行变更时需要对前后涉及的所有功能进行回归测试，测试功能不能仅限于当个模块，增加了测试难度和测试成本；

总体来说优点大过于缺点，目前看来Spring Cloud是一套非常完善的微服务框架，目前很多企业开始用微服务，Spring Cloud的优势是显而易见的。

### 题6：[什么是 Hystrix？如何实现容错机制？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题6什么是-hystrix如何实现容错机制)<br/>
Hystrix是一个延迟和容错库，旨在隔离远程系统，服务和第三方库的访问点，当出现故障是不可避免的故障时，停止级联故障并在复杂的分布式系统中实现弹性。

通常对于使用微服务架构开发的系统，涉及到许多微服务。这些微服务彼此协作。

### 题7：[Spring Cloud 中为什么要使用 Feign？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题7spring-cloud-中为什么要使用-feign)<br/>
Feign简化了RestTemplate代码，实现了Ribbon负载均衡，使代码变得更加简洁，也少了客户端调用的代码，所以Feign负载均衡是首选的。

### 题8：[Spring Cloud Eureka 自我保护机制是什么？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题8spring-cloud-eureka-自我保护机制是什么)<br/>
1）假设某一时间某个微服务宕机了，而Eureka不会自动清除，依然对微服务的信息进行保存。
 
2）在默认的情况系，Eureka Server在一定的时间内没有接受到微服务的实例心跳（默认为90秒），Eureka Server将会注销该实例。
 
3）但是在网络发生故障时微服务在Eureka Server之间是无法通信的，因为微服务本身实例是健康的，此刻本不应该注销这个微服务。那么Eureka自我保护模式就解决了这个问题。
 
4）当Eureka Server节点在短时间内丢失过客户端时包含发生的网络故障，那么节点就会进行自我保护。
 
5）一但进入自我保护模式，Eureka Server就会保护服务注册表中的信息，不再删除服务注册表中的数据注销任何微服务。
 
6）当网络故障恢复后，这个Eureka Server节点会自动的退出自我保护机制。
 
7）在自我保护模式中Eureka Server会保护服务注册表中的信息，不在注销任何服务实例，当重新收到心跳恢复阀值以上时，这个Eureka Server节点就会自动的退出自我保护模式，这种设计模式是为了宁可保留错误的服务注册信息，也不盲目的注销删除任何可能健康的服务实例。
 
8）自我保护机制就是一种对网络异常的安全保护实施，它会保存所有的微服务，不管健康或不健康的微服务都会保存，而不会盲目的删除任何微服务，可以让Eureka集群更加的健壮、稳定。

9）在Eureka Server端可取消自我保护机制，但是不建议取消此功能。

### 题9：[Spring Cloud 如何实现服务的注册？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题9spring-cloud-如何实现服务的注册)<br/>
1、当服务发布时，指定对应的服务名，将服务注册到注册中心，比如Eureka、Zookeeper等。

2、注册中心加@EnableEurekaServer注解，服务使用@EnableDiscoveryClient注解，然后使用Ribbon或Feign进行服务直接的调用发现。

**服务发现组件的功能**

1）服务注册表

服务注册表是一个记录当前可用服务实例的网络信息的数据库，是服务发现机制的核心。

服务注册表提供查询API和管理API，使用查询API获得可用的服务实例，使用管理API实现注册和注销；

2）服务注册

3）健康检查

### 题10：[Spring Boot 和 Spring Cloud 之间有什么联系？](/docs/Spring%20Cloud/最新Spring%20Cloud面试题及答案附答案汇总.md#题10spring-boot-和-spring-cloud-之间有什么联系)<br/>
Spring Boot是Spring推出用于解决传统框架配置文件冗余，装配组件繁杂的基于Maven的解决方案，旨在快速搭建单个微服务。

Spring Cloud专注于解决各个微服务之间的协调与配置，整合并管理各个微服务，为各个微服务之间提供配置管理、服务发现、断路器、路由、事件总线等集成服务。

Spring Cloud是依赖于Spring Boot，而Spring Boot并不依赖与Spring Cloud。

Spring Boot专注于快速、方便的开发单个的微服务个体，Spring Cloud是关注全局的服务治理框架。

### 题11：什么是-spring-cloud-框架<br/>


### 题12：什么是-spring-cloud-bus<br/>


### 题13：什么是-spring-cloud-netflix<br/>


### 题14：spring-cloud-断路器的作用是什么<br/>


### 题15：微服务有哪些优缺点<br/>


### 题16：什么是-spring-cloud-gateway<br/>


### 题17：什么是-spring-cloud-config<br/>


### 题18：什么是-spring-cloud-stream<br/>


### 题19：什么是微服务<br/>


### 题20：什么是服务熔断什么是服务降级<br/>


### 题21：什么是-spring-cloud-ribbon<br/>


### 题22：微服务通信方式有哪几种<br/>


### 题23：eureka-和-zookeeper-有哪些区别<br/>


### 题24：什么是雪崩效应<br/>


### 题25：什么是-spring-cloud-security<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")