# 最新面试题2021年Spring Cloud面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Cloud

### 题1：[什么是 Spring Cloud Task？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题1什么是-spring-cloud-task)<br/>
用于快速构建短暂、有限数据处理任务的微服务框架，用于向应用中添加功能性和非功能性的特性。

### 题2：[Spring Cloud 如何实现服务的注册？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题2spring-cloud-如何实现服务的注册)<br/>
1、当服务发布时，指定对应的服务名，将服务注册到注册中心，比如Eureka、Zookeeper等。

2、注册中心加@EnableEurekaServer注解，服务使用@EnableDiscoveryClient注解，然后使用Ribbon或Feign进行服务直接的调用发现。

**服务发现组件的功能**

1）服务注册表

服务注册表是一个记录当前可用服务实例的网络信息的数据库，是服务发现机制的核心。

服务注册表提供查询API和管理API，使用查询API获得可用的服务实例，使用管理API实现注册和注销；

2）服务注册

3）健康检查

### 题3：[什么是 Hystrix？如何实现容错机制？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题3什么是-hystrix如何实现容错机制)<br/>
Hystrix是一个延迟和容错库，旨在隔离远程系统，服务和第三方库的访问点，当出现故障是不可避免的故障时，停止级联故障并在复杂的分布式系统中实现弹性。

通常对于使用微服务架构开发的系统，涉及到许多微服务。这些微服务彼此协作。

### 题4：[什么是 Spring Cloud Ribbon？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题4什么是-spring-cloud-ribbon)<br/>
Spring Cloud Ribbon是基于Netflix Ribbon实现的一套客户端负载均衡的工具。

简单的说，Ribbon是Netflixf发布的开源项目，主要功能是提供醍醐的的软件负载均衡算法和服务调用。Ribbon客户端组件提供一系列完善的配置项如：连接超时，重试等。简单的说，就是在配置文件中列出Load Balancer(简称LB)后面所有的机器，Ribbon会自动的帮助你基于某种规则（如简单轮询、随机连接等）去连接这些机器。我们很容易的使用Ribbon实现自定义的负载均衡算法。

### 题5：[什么是 Spring Cloud Gateway？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题5什么是-spring-cloud-gateway)<br/>
API网关组件，对请求提供路由及过滤功能。

Spring Cloud Gateway是Spring Cloud官方推出的第二代网关框架，取代Zuul网关。网关作为流量的，在微服务系统中有着非常作用，网关常见的功能有路由转发、权限校验、限流控制等作用。

使用了一个RouteLocatorBuilder的bean去创建路由，除了创建路由RouteLocatorBuilder可以让你添加各种predicates和filters，predicates断言的意思，顾名思义就是根据具体的请求的规则，由具体的route去处理，filters是各种过滤器，用来对请求做各种判断和修改。

### 题6：[Spring Cloud 核心组件有哪些？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题6spring-cloud-核心组件有哪些)<br/>
Eureka：服务注册与发现，Eureka服务端称服务注册中心，Eureka客户端主要处理服务的注册与发现。

Feign：基于Feign的动态代理机制，根据注解和选择的机器，拼接请求url地址，发起请求。

Ribbon：负载均衡，服务间发起请求时基于Ribbon实现负载均衡，从一个服务的多台机器中选择一台。

Hystrix：提供服务隔离、熔断、降级机制，发起请求是通过Hystrix提供的线程池，实现不同服务调用之间的隔离，避免服务雪崩问题。

Zuul：服务网关，前端调用后端服务，统一由Zuul网关转发请求给对应的服务。

### 题7：[什么是雪崩效应？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题7什么是雪崩效应)<br/>
分布式系统架构中多个系统之间通常是通过远程RPC调用进行通信，也就是A系统调用B系统服务，B系统调用C系统服务等，实现方式有 Spring Boot+Dubbo实现微服务调用，以及各个公司自研的一些RPC框架等。当下游应用 C发生故障，而系统B没有服务降级的时候就可能会导致B，甚至系统A瘫痪，这种现象被称为雪崩效应。

### 题8：[你都知道哪些微服务技术栈？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题8你都知道哪些微服务技术栈)<br/>
|微服务描述|技术名称|
|-|-|
|服务开发|Springboot、Spring、SpringMVC| 
|服务配置与管理|Netflix公司的Archaius、阿里的Diamond等| 
|服务注册与发现|Eureka、Consul、Zookeeper等| 
|服务调用|REST、RPC、gRPC| 
|服务熔断器|Hystrix、Envoy等| 
|负载均衡|Ribbon、Nginx等| 
|服务接口调用（客户端调用服务发简单工具）|Feign等| 
|消息队列|kafka、RabbitMQ、ActiveMQ等| 
|服务配置中心管理|SpringCloudConfig、Chef等| 
|服务路由（API网关）|Zuul等| 
|服务监控|Zabbix、Nagios、Metrics、Spectator等| 
|全链路追踪|Zipkin、Brave、Dapper等| 
|服务部署|Docker、OpenStack、Kubernetes等| 
|数据流操作开发包|SpringCloud Stream（封装与Redis，Rabbit、Kafka等发送接收消息）| 
|事件消息总线|Spring Cloud Bus|

### 题9：[Ribbon  和 Nginx  负载均衡有什么区别？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题9ribbon--和-nginx--负载均衡有什么区别)<br/>
Nginx是服务器负载均衡，客户端所有请求都会交给NGINX，然后又NGINX实现转发请求。即负载均衡是有服务端实现的。

Ribbon本地负载均衡，在调用微服务接口时候，会在注册中心获取注册信息服务列表之后缓存到JVM本地，从而在本地实现RPC远程服务调用技术。

### 题10：[Eureka 和 Zookeeper 有哪些区别？](/docs/Spring%20Cloud/最新面试题2021年Spring%20Cloud面试题及答案汇总.md#题10eureka-和-zookeeper-有哪些区别)<br/>
1）Zookeeper是CP原则，强一致性和分区容错性。

2）Eureka是AP原则 可用性和分区容错性。

3）Zookeeper当主节点故障时，zk会在剩余节点重新选择主节点，耗时过长，虽然最终能够恢复，但是选取主节点期间会导致服务不可用，这是不能容忍的。

4）Eureka各个节点是平等的，一个节点挂掉，其他节点仍会正常保证服务。

### 题11：微服务有哪些优缺点<br/>


### 题12：雪崩效应有哪些常见的解决方案<br/>


### 题13：什么是微服务<br/>


### 题14：spring-cloud-中为什么要使用-feign<br/>


### 题15：分布式事务是什么<br/>


### 题16：spring-cloud-eureka-自我保护机制是什么<br/>


### 题17：什么是服务熔断什么是服务降级<br/>


### 题18：spring-cloud-zuul-和-spring-cloud-gateway-有什么区别<br/>


### 题19：什么是-spring-cloud-stream<br/>


### 题20：spring-cloud-断路器的作用是什么<br/>


### 题21：雪崩效应都有哪些常见场景<br/>


### 题22：断路器有几种熔断状态<br/>


### 题23：什么是-spring-cloud-框架<br/>


### 题24：什么是-spring-cloud-consul<br/>


### 题25：什么是-spring-cloud-zookeeper<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")