# 掌握2022年最新Spring Cloud面试题汇总（含答案）

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Cloud

### 题1：[Ribbon  和 Nginx  负载均衡有什么区别？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题1ribbon--和-nginx--负载均衡有什么区别)<br/>
Nginx是服务器负载均衡，客户端所有请求都会交给NGINX，然后又NGINX实现转发请求。即负载均衡是有服务端实现的。

Ribbon本地负载均衡，在调用微服务接口时候，会在注册中心获取注册信息服务列表之后缓存到JVM本地，从而在本地实现RPC远程服务调用技术。

### 题2：[Spring Cloud 如何实现服务的注册？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题2spring-cloud-如何实现服务的注册)<br/>
1、当服务发布时，指定对应的服务名，将服务注册到注册中心，比如Eureka、Zookeeper等。

2、注册中心加@EnableEurekaServer注解，服务使用@EnableDiscoveryClient注解，然后使用Ribbon或Feign进行服务直接的调用发现。

**服务发现组件的功能**

1）服务注册表

服务注册表是一个记录当前可用服务实例的网络信息的数据库，是服务发现机制的核心。

服务注册表提供查询API和管理API，使用查询API获得可用的服务实例，使用管理API实现注册和注销；

2）服务注册

3）健康检查

### 题3：[什么是 Spring Cloud Gateway？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题3什么是-spring-cloud-gateway)<br/>
API网关组件，对请求提供路由及过滤功能。

Spring Cloud Gateway是Spring Cloud官方推出的第二代网关框架，取代Zuul网关。网关作为流量的，在微服务系统中有着非常作用，网关常见的功能有路由转发、权限校验、限流控制等作用。

使用了一个RouteLocatorBuilder的bean去创建路由，除了创建路由RouteLocatorBuilder可以让你添加各种predicates和filters，predicates断言的意思，顾名思义就是根据具体的请求的规则，由具体的route去处理，filters是各种过滤器，用来对请求做各种判断和修改。

### 题4：[什么是 Spring Cloud 框架？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题4什么是-spring-cloud-框架)<br/>
Spring Cloud是一系列框架的有序集合，它利用Spring Boot的开发便利性巧妙地简化了分布式系统基础设施的开发，如服务发现注册、配置中心、消息总线、负载均衡、断路器、数据监控等，都可以用Spring Boot的开发风格做到一键启动和部署。

Spring Cloud并没有重复制造轮子，它只是将各家公司开发的比较成熟、经得起实际考验的服务框架组合起来，通过Spring Boot风格进行再封装屏蔽掉了复杂的配置和实现原理，最终给开发者留出了一套简单易懂、易部署和易维护的分布式系统开发工具包。

Spring Cloud的子项目，大致可分成两大类：

一类是对现有成熟框架“Spring Boot化”的封装和抽象，也是数量最多的项目；

第二类是开发一部分分布式系统的基础设施的实现，如Spring Cloud Stream扮演的是kafka, ActiveMQ这样的角色。

对于快速实践微服务的开发者来说，第一类子项目已经基本足够使用，如：

1）Spring Cloud Netflix是对Netflix开发的一套分布式服务框架的封装，包括服务的发现和注册，负载均衡、断路器、REST客户端、请求路由等；

2）Spring Cloud Config将配置信息中央化保存, 配置Spring Cloud Bus可以实现动态修改配置文件；

3）Spring Cloud Bus分布式消息队列，是对Kafka, MQ的封装；

4）Spring Cloud Security对Spring Security的封装，并能配合Netflix使用；

5）Spring Cloud Zookeeper对Zookeeper的封装，使之能配置其它Spring Cloud的子项目使用；

6）Spring Cloud Eureka是Spring Cloud Netflix微服务套件中的一部分，它基于Netflix Eureka做了二次封装，主要负责完成微服务架构中的服务治理功能。注意的是从2.x起，官方不会继续开源，若需要使用2.x，风险还是有的。但是我觉得问题并不大，eureka目前的功能已经非常稳定，就算不升级，服务注册/发现这些功能已经够用。consul是个不错的替代品，还有其他替代组件，后续篇幅会有详细赘述或关注微信公众号“Java精选”，有详细替代方案源码分享。

### 题5：[什么是 Spring Cloud Consul？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题5什么是-spring-cloud-consul)<br/>
基于Hashicorp Consul的服务治理组件。


### 题6：[你都知道哪些微服务技术栈？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题6你都知道哪些微服务技术栈)<br/>
|微服务描述|技术名称|
|-|-|
|服务开发|Springboot、Spring、SpringMVC| 
|服务配置与管理|Netflix公司的Archaius、阿里的Diamond等| 
|服务注册与发现|Eureka、Consul、Zookeeper等| 
|服务调用|REST、RPC、gRPC| 
|服务熔断器|Hystrix、Envoy等| 
|负载均衡|Ribbon、Nginx等| 
|服务接口调用（客户端调用服务发简单工具）|Feign等| 
|消息队列|kafka、RabbitMQ、ActiveMQ等| 
|服务配置中心管理|SpringCloudConfig、Chef等| 
|服务路由（API网关）|Zuul等| 
|服务监控|Zabbix、Nagios、Metrics、Spectator等| 
|全链路追踪|Zipkin、Brave、Dapper等| 
|服务部署|Docker、OpenStack、Kubernetes等| 
|数据流操作开发包|SpringCloud Stream（封装与Redis，Rabbit、Kafka等发送接收消息）| 
|事件消息总线|Spring Cloud Bus|

### 题7：[Spring Cloud 框架有哪些优缺点？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题7spring-cloud-框架有哪些优缺点)<br/>
**Spring Cloud优点：**

1）服务拆分粒度更细，有利于资源重复利用，有利于提高开发效率，每个模块可以独立开发和部署、代码耦合度低；

2）可以更精准的制定优化服务方案，提高系统的可维护性，每个服务可以单独进行部署，升级某个模块的时候只需要单独部署对应的模块服务即可，效率更高；

3）微服务架构采用去中心化思想，服务之间采用Restful等轻量级通讯，比ESB更轻量，模块专一性提升，每个模块只需要关心自己模块所负责的功能即可，不需要关心其他模块业务，专一性更高，更便于功能模块开发和拓展；

4）技术选型不再单一，由于每个模块是单独开发并且部署，所以每个模块可以有更多的技术选型方案，如模块1数据库选择mysql，模块2选择用oracle也是可以的；


5）适于互联网时代，产品迭代周期更短。系统稳定性以及性能提升，由于微服务是几个服务共同组成的项目或者流程，因此相比传统单一项目的优点就在于某个模块提供的服务宕机过后不至于整个系统瘫痪掉，而且微服务里面的容灾和服务降级机制也能大大提高项目的稳定性；从性能而言，由于每个服务是单独部署，所以每个模块都可以有自己的一套运行环境，当某个服务性能低下的时候可以对单个服务进行配置或者代码上的升级，从而达到提升性能的目的。

**Spring Cloud缺点：**

1）微服务过多，治理成本高，不利于维护系统，服务之间接口调用成本增加，相比以往单项目的时候调用某个方法或者接口可以直接通过本地方法调用就能够完成，但是当切换成微服务的时候，调用方式就不能用以前的方式进行调试、目前主流采用的技术有http api接口调用、RPC、WebService等方式进行调用，调用成本比单个项目的时候有所增加；

2）分布式系统开发的成本高（容错，分布式事务等）对团队挑战大

2）独立的数据库，微服务产生事务一致性的问题，由于各个模块用的技术都各不相同、而且每个服务都会高并发进行调用，就会存在分布式事务一致性的问题；

3）分布式部署，造成运营的成本增加、相比较单个应用的时候，运营人员只需要对单个项目进行部署、负载均衡等操作，但是微服务的每个模块都需要这样的操作，增加了运行时的成本；

4）由于整个系统是通过各个模块组合而成的，因此当某个服务进行变更时需要对前后涉及的所有功能进行回归测试，测试功能不能仅限于当个模块，增加了测试难度和测试成本；

总体来说优点大过于缺点，目前看来Spring Cloud是一套非常完善的微服务框架，目前很多企业开始用微服务，Spring Cloud的优势是显而易见的。

### 题8：[雪崩效应都有哪些常见场景？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题8雪崩效应都有哪些常见场景)<br/>
1、硬件故障：如服务器宕机，机房断电，光纤被挖断等。

2、流量激增：如异常流量，重试加大流量等。

3、缓存穿透：一般发生在应用重启，所有缓存失效时，以及短时间内大量缓存失效时。大量的缓存不命中，使请求直击后端服务，造成服务提供者超负荷运行，引起服务不可用。

4、程序BUG：如程序逻辑导致内存泄漏，JVM 长时间 FullGC 等。

5、同步等待：服务间采用同步调用模式，同步等待造成的资源耗尽。

### 题9：[什么是 Spring Cloud Zookeeper？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题9什么是-spring-cloud-zookeeper)<br/>
基于Apache Zookeeper的服务治理组件。

### 题10：[微服务有哪些优缺点？](/docs/Spring%20Cloud/掌握2022年最新Spring%20Cloud面试题汇总（含答案）.md#题10微服务有哪些优缺点)<br/>
优点：松耦合，聚焦单一业务功能，无关开发语言，团队规模降低。在开发中，不需要了解多有业务，只专注于当前功能，便利集中，功能小而精。微服务一个功能受损，对其他功能影响并不是太大，可以快速定位问题。微服务只专注于当前业务逻辑代码，不会和 html、css 或其他界面进行混合。可以灵活搭配技术，独立性比较舒服。

缺点：随着服务数量增加，管理复杂，部署复杂，服务器需要增多，服务通信和调用压力增大，运维工程师压力增大，人力资源增多，系统依赖增强，数据一致性，性能监控。

### 题11：微服务通信方式有哪几种<br/>


### 题12：什么是-hystrix如何实现容错机制<br/>


### 题13：雪崩效应有哪些常见的解决方案<br/>


### 题14：什么是雪崩效应<br/>


### 题15：spring-boot-和-spring-cloud-之间有什么联系<br/>


### 题16：什么是-spring-cloud-bus<br/>


### 题17：什么是-spring-cloud-task<br/>


### 题18：什么是-spring-cloud-ribbon<br/>


### 题19：什么是微服务架构<br/>


### 题20：load-balancer-负载均衡是什么<br/>


### 题21：eureka-和-zookeeper-有哪些区别<br/>


### 题22：什么是-spring-cloud-netflix<br/>


### 题23：spring-cloud-核心组件有哪些<br/>


### 题24：什么是-spring-cloud-security<br/>


### 题25：什么是-spring-cloud-config<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")