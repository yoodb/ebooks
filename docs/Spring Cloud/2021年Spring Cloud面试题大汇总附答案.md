# 2021年Spring Cloud面试题大汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Cloud

### 题1：[分布式事务是什么？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题1分布式事务是什么)<br/>
​分布式系统会把一个应用系统拆分为可独立部署的多个服务，因此需要服务与服务之间远程协作才能完成事务操作，这种分布式系统环境下由不同的服务之间通过网络远程协作完成事务称之为分布式事务。

举例：用户注册送积分事务、创建订单减库存事务，银行转账事务等都是分布式事务。

简单来说就是分布式事务用于在分布式系统中保证不同节点之间的数据一致性。

### 题2：[什么是 Spring Cloud Ribbon？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题2什么是-spring-cloud-ribbon)<br/>
Spring Cloud Ribbon是基于Netflix Ribbon实现的一套客户端负载均衡的工具。

简单的说，Ribbon是Netflixf发布的开源项目，主要功能是提供醍醐的的软件负载均衡算法和服务调用。Ribbon客户端组件提供一系列完善的配置项如：连接超时，重试等。简单的说，就是在配置文件中列出Load Balancer(简称LB)后面所有的机器，Ribbon会自动的帮助你基于某种规则（如简单轮询、随机连接等）去连接这些机器。我们很容易的使用Ribbon实现自定义的负载均衡算法。

### 题3：[Spring Cloud 框架有哪些优缺点？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题3spring-cloud-框架有哪些优缺点)<br/>
**Spring Cloud优点：**

1）服务拆分粒度更细，有利于资源重复利用，有利于提高开发效率，每个模块可以独立开发和部署、代码耦合度低；

2）可以更精准的制定优化服务方案，提高系统的可维护性，每个服务可以单独进行部署，升级某个模块的时候只需要单独部署对应的模块服务即可，效率更高；

3）微服务架构采用去中心化思想，服务之间采用Restful等轻量级通讯，比ESB更轻量，模块专一性提升，每个模块只需要关心自己模块所负责的功能即可，不需要关心其他模块业务，专一性更高，更便于功能模块开发和拓展；

4）技术选型不再单一，由于每个模块是单独开发并且部署，所以每个模块可以有更多的技术选型方案，如模块1数据库选择mysql，模块2选择用oracle也是可以的；


5）适于互联网时代，产品迭代周期更短。系统稳定性以及性能提升，由于微服务是几个服务共同组成的项目或者流程，因此相比传统单一项目的优点就在于某个模块提供的服务宕机过后不至于整个系统瘫痪掉，而且微服务里面的容灾和服务降级机制也能大大提高项目的稳定性；从性能而言，由于每个服务是单独部署，所以每个模块都可以有自己的一套运行环境，当某个服务性能低下的时候可以对单个服务进行配置或者代码上的升级，从而达到提升性能的目的。

**Spring Cloud缺点：**

1）微服务过多，治理成本高，不利于维护系统，服务之间接口调用成本增加，相比以往单项目的时候调用某个方法或者接口可以直接通过本地方法调用就能够完成，但是当切换成微服务的时候，调用方式就不能用以前的方式进行调试、目前主流采用的技术有http api接口调用、RPC、WebService等方式进行调用，调用成本比单个项目的时候有所增加；

2）分布式系统开发的成本高（容错，分布式事务等）对团队挑战大

2）独立的数据库，微服务产生事务一致性的问题，由于各个模块用的技术都各不相同、而且每个服务都会高并发进行调用，就会存在分布式事务一致性的问题；

3）分布式部署，造成运营的成本增加、相比较单个应用的时候，运营人员只需要对单个项目进行部署、负载均衡等操作，但是微服务的每个模块都需要这样的操作，增加了运行时的成本；

4）由于整个系统是通过各个模块组合而成的，因此当某个服务进行变更时需要对前后涉及的所有功能进行回归测试，测试功能不能仅限于当个模块，增加了测试难度和测试成本；

总体来说优点大过于缺点，目前看来Spring Cloud是一套非常完善的微服务框架，目前很多企业开始用微服务，Spring Cloud的优势是显而易见的。

### 题4：[什么是 Spring Cloud OpenFeign？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题4什么是-spring-cloud-openfeign)<br/>
基于Ribbon和Hystrix的声明式服务调用组件，可以动态创建基于Spring MVC注解的接口实现用于服务调用，在Spring Cloud 2.0中已经取代Feign成为了优选。

### 题5：[断路器有几种熔断状态？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题5断路器有几种熔断状态)<br/>
Close：关闭状态，所有的请求都能正常的访问。

Open：打开状态，所有的请求都会被降级。Hystrix会对请求情况进行计数，当一定时间内请求次数达到20次以上并且失败次数的阈值达到了50%，则触发熔断，断路器会被打开；所有的请求都不能正常的访问。

Half-Open：半打开状态；Open的状态不是永远的，当休眠5秒后会自动进去半打开状态；会释放部分请求通过，如果这些请求是成功的，那么就是完全关闭断路器；如果不成功，那么继续进行休眠5秒，重复流程。

### 题6：[微服务有哪些优缺点？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题6微服务有哪些优缺点)<br/>
优点：松耦合，聚焦单一业务功能，无关开发语言，团队规模降低。在开发中，不需要了解多有业务，只专注于当前功能，便利集中，功能小而精。微服务一个功能受损，对其他功能影响并不是太大，可以快速定位问题。微服务只专注于当前业务逻辑代码，不会和 html、css 或其他界面进行混合。可以灵活搭配技术，独立性比较舒服。

缺点：随着服务数量增加，管理复杂，部署复杂，服务器需要增多，服务通信和调用压力增大，运维工程师压力增大，人力资源增多，系统依赖增强，数据一致性，性能监控。

### 题7：[你都知道哪些微服务技术栈？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题7你都知道哪些微服务技术栈)<br/>
|微服务描述|技术名称|
|-|-|
|服务开发|Springboot、Spring、SpringMVC| 
|服务配置与管理|Netflix公司的Archaius、阿里的Diamond等| 
|服务注册与发现|Eureka、Consul、Zookeeper等| 
|服务调用|REST、RPC、gRPC| 
|服务熔断器|Hystrix、Envoy等| 
|负载均衡|Ribbon、Nginx等| 
|服务接口调用（客户端调用服务发简单工具）|Feign等| 
|消息队列|kafka、RabbitMQ、ActiveMQ等| 
|服务配置中心管理|SpringCloudConfig、Chef等| 
|服务路由（API网关）|Zuul等| 
|服务监控|Zabbix、Nagios、Metrics、Spectator等| 
|全链路追踪|Zipkin、Brave、Dapper等| 
|服务部署|Docker、OpenStack、Kubernetes等| 
|数据流操作开发包|SpringCloud Stream（封装与Redis，Rabbit、Kafka等发送接收消息）| 
|事件消息总线|Spring Cloud Bus|

### 题8：[Ribbon 和 Feign 有什么区别？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题8ribbon-和-feign-有什么区别)<br/>
Ribbon添加maven依赖spring-starter-ribbon使用@RibbonClient(value="服务名称") 使用RestTemplate调用远程服务对应的方法。

Feign添加maven依赖 spring-starter-feign服务提供方提供对外接口 调用方使用在接口上使用@FeignClient("指定服务名")。

**Ribbon和Feign的区别：**

Ribbon和Feign都是用于调用其他服务的，不过方式不同。

1、启动类使用的注解不同，Ribbon用的是@RibbonClient，Feign用的是@EnableFeignClients。

2、服务的指定位置不同，Ribbon是在@RibbonClient注解上声明，Feign则是在定义抽象方法的接口中使用@FeignClient声明。

3、调用方式不同，Ribbon需要自己构建http请求，模拟http请求然后使用RestTemplate发送给其他服务，步骤相当繁琐。

Feign则是在Ribbon的基础上进行了一次改进，采用接口的方式，将需要调用的其他服务的方法定义成抽象方法即可，不需要自己构建http请求。不过要注意的是抽象方法的注解、方法签名要和提供服务的方法完全一致。

### 题9：[Spring Cloud Eureka 自我保护机制是什么？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题9spring-cloud-eureka-自我保护机制是什么)<br/>
1）假设某一时间某个微服务宕机了，而Eureka不会自动清除，依然对微服务的信息进行保存。
 
2）在默认的情况系，Eureka Server在一定的时间内没有接受到微服务的实例心跳（默认为90秒），Eureka Server将会注销该实例。
 
3）但是在网络发生故障时微服务在Eureka Server之间是无法通信的，因为微服务本身实例是健康的，此刻本不应该注销这个微服务。那么Eureka自我保护模式就解决了这个问题。
 
4）当Eureka Server节点在短时间内丢失过客户端时包含发生的网络故障，那么节点就会进行自我保护。
 
5）一但进入自我保护模式，Eureka Server就会保护服务注册表中的信息，不再删除服务注册表中的数据注销任何微服务。
 
6）当网络故障恢复后，这个Eureka Server节点会自动的退出自我保护机制。
 
7）在自我保护模式中Eureka Server会保护服务注册表中的信息，不在注销任何服务实例，当重新收到心跳恢复阀值以上时，这个Eureka Server节点就会自动的退出自我保护模式，这种设计模式是为了宁可保留错误的服务注册信息，也不盲目的注销删除任何可能健康的服务实例。
 
8）自我保护机制就是一种对网络异常的安全保护实施，它会保存所有的微服务，不管健康或不健康的微服务都会保存，而不会盲目的删除任何微服务，可以让Eureka集群更加的健壮、稳定。

9）在Eureka Server端可取消自我保护机制，但是不建议取消此功能。

### 题10：[什么是 Spring Cloud Gateway？](/docs/Spring%20Cloud/2021年Spring%20Cloud面试题大汇总附答案.md#题10什么是-spring-cloud-gateway)<br/>
API网关组件，对请求提供路由及过滤功能。

Spring Cloud Gateway是Spring Cloud官方推出的第二代网关框架，取代Zuul网关。网关作为流量的，在微服务系统中有着非常作用，网关常见的功能有路由转发、权限校验、限流控制等作用。

使用了一个RouteLocatorBuilder的bean去创建路由，除了创建路由RouteLocatorBuilder可以让你添加各种predicates和filters，predicates断言的意思，顾名思义就是根据具体的请求的规则，由具体的route去处理，filters是各种过滤器，用来对请求做各种判断和修改。

### 题11：什么是-spring-cloud-框架<br/>


### 题12：什么是-spring-cloud-stream<br/>


### 题13：什么是-spring-cloud-security<br/>


### 题14：什么是雪崩效应<br/>


### 题15：什么是-spring-cloud-zookeeper<br/>


### 题16：微服务通信方式有哪几种<br/>


### 题17：什么是-spring-cloud-netflix<br/>


### 题18：spring-cloud-中为什么要使用-feign<br/>


### 题19：什么是-spring-cloud-bus<br/>


### 题20：spring-cloud-zuul-和-spring-cloud-gateway-有什么区别<br/>


### 题21：什么是微服务架构<br/>


### 题22：雪崩效应都有哪些常见场景<br/>


### 题23：雪崩效应有哪些常见的解决方案<br/>


### 题24：eureka-和-zookeeper-有哪些区别<br/>


### 题25：ribbon--和-nginx--负载均衡有什么区别<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")