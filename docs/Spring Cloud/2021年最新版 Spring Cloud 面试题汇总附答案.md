# 2021年最新版 Spring Cloud 面试题汇总附答案

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Cloud

### 题1：[什么是 Spring Cloud OpenFeign？](/docs/Spring%20Cloud/2021年最新版%20Spring%20Cloud%20面试题汇总附答案.md#题1什么是-spring-cloud-openfeign)<br/>
基于Ribbon和Hystrix的声明式服务调用组件，可以动态创建基于Spring MVC注解的接口实现用于服务调用，在Spring Cloud 2.0中已经取代Feign成为了优选。

### 题2：[分布式事务是什么？](/docs/Spring%20Cloud/2021年最新版%20Spring%20Cloud%20面试题汇总附答案.md#题2分布式事务是什么)<br/>
​分布式系统会把一个应用系统拆分为可独立部署的多个服务，因此需要服务与服务之间远程协作才能完成事务操作，这种分布式系统环境下由不同的服务之间通过网络远程协作完成事务称之为分布式事务。

举例：用户注册送积分事务、创建订单减库存事务，银行转账事务等都是分布式事务。

简单来说就是分布式事务用于在分布式系统中保证不同节点之间的数据一致性。

### 题3：[什么是 Spring Cloud Ribbon？](/docs/Spring%20Cloud/2021年最新版%20Spring%20Cloud%20面试题汇总附答案.md#题3什么是-spring-cloud-ribbon)<br/>
Spring Cloud Ribbon是基于Netflix Ribbon实现的一套客户端负载均衡的工具。

简单的说，Ribbon是Netflixf发布的开源项目，主要功能是提供醍醐的的软件负载均衡算法和服务调用。Ribbon客户端组件提供一系列完善的配置项如：连接超时，重试等。简单的说，就是在配置文件中列出Load Balancer(简称LB)后面所有的机器，Ribbon会自动的帮助你基于某种规则（如简单轮询、随机连接等）去连接这些机器。我们很容易的使用Ribbon实现自定义的负载均衡算法。

### 题4：[什么是 Spring Cloud Consul？](/docs/Spring%20Cloud/2021年最新版%20Spring%20Cloud%20面试题汇总附答案.md#题4什么是-spring-cloud-consul)<br/>
基于Hashicorp Consul的服务治理组件。


### 题5：[什么是 Spring Cloud Zookeeper？](/docs/Spring%20Cloud/2021年最新版%20Spring%20Cloud%20面试题汇总附答案.md#题5什么是-spring-cloud-zookeeper)<br/>
基于Apache Zookeeper的服务治理组件。

### 题6：[Ribbon 和 Feign 有什么区别？](/docs/Spring%20Cloud/2021年最新版%20Spring%20Cloud%20面试题汇总附答案.md#题6ribbon-和-feign-有什么区别)<br/>
Ribbon添加maven依赖spring-starter-ribbon使用@RibbonClient(value="服务名称") 使用RestTemplate调用远程服务对应的方法。

Feign添加maven依赖 spring-starter-feign服务提供方提供对外接口 调用方使用在接口上使用@FeignClient("指定服务名")。

**Ribbon和Feign的区别：**

Ribbon和Feign都是用于调用其他服务的，不过方式不同。

1、启动类使用的注解不同，Ribbon用的是@RibbonClient，Feign用的是@EnableFeignClients。

2、服务的指定位置不同，Ribbon是在@RibbonClient注解上声明，Feign则是在定义抽象方法的接口中使用@FeignClient声明。

3、调用方式不同，Ribbon需要自己构建http请求，模拟http请求然后使用RestTemplate发送给其他服务，步骤相当繁琐。

Feign则是在Ribbon的基础上进行了一次改进，采用接口的方式，将需要调用的其他服务的方法定义成抽象方法即可，不需要自己构建http请求。不过要注意的是抽象方法的注解、方法签名要和提供服务的方法完全一致。

### 题7：[你都知道哪些微服务技术栈？](/docs/Spring%20Cloud/2021年最新版%20Spring%20Cloud%20面试题汇总附答案.md#题7你都知道哪些微服务技术栈)<br/>
|微服务描述|技术名称|
|-|-|
|服务开发|Springboot、Spring、SpringMVC| 
|服务配置与管理|Netflix公司的Archaius、阿里的Diamond等| 
|服务注册与发现|Eureka、Consul、Zookeeper等| 
|服务调用|REST、RPC、gRPC| 
|服务熔断器|Hystrix、Envoy等| 
|负载均衡|Ribbon、Nginx等| 
|服务接口调用（客户端调用服务发简单工具）|Feign等| 
|消息队列|kafka、RabbitMQ、ActiveMQ等| 
|服务配置中心管理|SpringCloudConfig、Chef等| 
|服务路由（API网关）|Zuul等| 
|服务监控|Zabbix、Nagios、Metrics、Spectator等| 
|全链路追踪|Zipkin、Brave、Dapper等| 
|服务部署|Docker、OpenStack、Kubernetes等| 
|数据流操作开发包|SpringCloud Stream（封装与Redis，Rabbit、Kafka等发送接收消息）| 
|事件消息总线|Spring Cloud Bus|

### 题8：[Load Balancer 负载均衡是什么？](/docs/Spring%20Cloud/2021年最新版%20Spring%20Cloud%20面试题汇总附答案.md#题8load-balancer-负载均衡是什么)<br/>
简单的说就是讲用户的请求平摊分配到多个服务上，从而达到系统的HA（高可用）。

常见的负载均衡软件有Nginx、LVS，硬件有F5等。

### 题9：[什么是 Spring Cloud Task？](/docs/Spring%20Cloud/2021年最新版%20Spring%20Cloud%20面试题汇总附答案.md#题9什么是-spring-cloud-task)<br/>
用于快速构建短暂、有限数据处理任务的微服务框架，用于向应用中添加功能性和非功能性的特性。

### 题10：[什么是 Spring Cloud Bus？](/docs/Spring%20Cloud/2021年最新版%20Spring%20Cloud%20面试题汇总附答案.md#题10什么是-spring-cloud-bus)<br/>
用于传播集群状态变化的消息总线，使用轻量级消息代理链接分布式系统中的节点，可以用来动态刷新集群中的服务配置。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")