# 最新2021年Spring Cloud面试题及答案汇总版

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Cloud

### 题1：[微服务通信方式有哪几种？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题1微服务通信方式有哪几种)<br/>
**同步通讯**

RPC、REST等，比如Dobbo通过RPC远程过程调用；Spring Cloud通过REST接口json调用等。

**异步通讯**

消息队列，考虑消息的可靠传输、高性能，以及编程模型的变化等，比如：RabbitMq、ActiveMq、Kafka等。

### 题2：[Eureka 和 Zookeeper 有哪些区别？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题2eureka-和-zookeeper-有哪些区别)<br/>
1）Zookeeper是CP原则，强一致性和分区容错性。

2）Eureka是AP原则 可用性和分区容错性。

3）Zookeeper当主节点故障时，zk会在剩余节点重新选择主节点，耗时过长，虽然最终能够恢复，但是选取主节点期间会导致服务不可用，这是不能容忍的。

4）Eureka各个节点是平等的，一个节点挂掉，其他节点仍会正常保证服务。

### 题3：[雪崩效应都有哪些常见场景？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题3雪崩效应都有哪些常见场景)<br/>
1、硬件故障：如服务器宕机，机房断电，光纤被挖断等。

2、流量激增：如异常流量，重试加大流量等。

3、缓存穿透：一般发生在应用重启，所有缓存失效时，以及短时间内大量缓存失效时。大量的缓存不命中，使请求直击后端服务，造成服务提供者超负荷运行，引起服务不可用。

4、程序BUG：如程序逻辑导致内存泄漏，JVM 长时间 FullGC 等。

5、同步等待：服务间采用同步调用模式，同步等待造成的资源耗尽。

### 题4：[雪崩效应有哪些常见的解决方案？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题4雪崩效应有哪些常见的解决方案)<br/>
针对不同场景分别有不同的解决方案，如下所示：

1、硬件故障：多机房容灾，跨机房路由，异地多活等。

2、流量激增：采用自动扩缩容以应对突发流量，或在负载均衡器上安装限流模块。

3、缓存穿透：缓存预加载、缓存异步加载等。

4、程序BUG：修改程序bug、及时释放资源等。

5、同步等待：资源隔离、MQ解耦、不可用服务调用快速失败等。资源隔离通常指不同服务调用采用不同的线程池；不可用服务调用快速失败一般通过超时机制，熔断器以及熔断后降级方法等方案实现。

**流量控制**的具体措施包括：

1）网关限流。

2）用户交互限流，采用加载动画，提高用户的忍耐等待时间；提交按钮添加强制等待时间机制。

3）关闭重试。

服务调用者**降级服务**的措施包括：

1）资源隔离，主要是对调用服务的线程池进行隔离。

2）对依赖服务进行分类。

3）不可用服务的调用快速失败。

### 题5：[Ribbon  和 Nginx  负载均衡有什么区别？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题5ribbon--和-nginx--负载均衡有什么区别)<br/>
Nginx是服务器负载均衡，客户端所有请求都会交给NGINX，然后又NGINX实现转发请求。即负载均衡是有服务端实现的。

Ribbon本地负载均衡，在调用微服务接口时候，会在注册中心获取注册信息服务列表之后缓存到JVM本地，从而在本地实现RPC远程服务调用技术。

### 题6：[什么是 Spring Cloud Task？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题6什么是-spring-cloud-task)<br/>
用于快速构建短暂、有限数据处理任务的微服务框架，用于向应用中添加功能性和非功能性的特性。

### 题7：[什么是 Spring Cloud Sleuth？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题7什么是-spring-cloud-sleuth)<br/>
Spring Cloud应用程序的分布式请求链路跟踪，支持使用Zipkin、HTrace和基于日志（例如ELK）的跟踪。

### 题8：[什么是 Hystrix？如何实现容错机制？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题8什么是-hystrix如何实现容错机制)<br/>
Hystrix是一个延迟和容错库，旨在隔离远程系统，服务和第三方库的访问点，当出现故障是不可避免的故障时，停止级联故障并在复杂的分布式系统中实现弹性。

通常对于使用微服务架构开发的系统，涉及到许多微服务。这些微服务彼此协作。

### 题9：[什么是 Spring Cloud Ribbon？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题9什么是-spring-cloud-ribbon)<br/>
Spring Cloud Ribbon是基于Netflix Ribbon实现的一套客户端负载均衡的工具。

简单的说，Ribbon是Netflixf发布的开源项目，主要功能是提供醍醐的的软件负载均衡算法和服务调用。Ribbon客户端组件提供一系列完善的配置项如：连接超时，重试等。简单的说，就是在配置文件中列出Load Balancer(简称LB)后面所有的机器，Ribbon会自动的帮助你基于某种规则（如简单轮询、随机连接等）去连接这些机器。我们很容易的使用Ribbon实现自定义的负载均衡算法。

### 题10：[Spring Cloud 核心组件有哪些？](/docs/Spring%20Cloud/最新2021年Spring%20Cloud面试题及答案汇总版.md#题10spring-cloud-核心组件有哪些)<br/>
Eureka：服务注册与发现，Eureka服务端称服务注册中心，Eureka客户端主要处理服务的注册与发现。

Feign：基于Feign的动态代理机制，根据注解和选择的机器，拼接请求url地址，发起请求。

Ribbon：负载均衡，服务间发起请求时基于Ribbon实现负载均衡，从一个服务的多台机器中选择一台。

Hystrix：提供服务隔离、熔断、降级机制，发起请求是通过Hystrix提供的线程池，实现不同服务调用之间的隔离，避免服务雪崩问题。

Zuul：服务网关，前端调用后端服务，统一由Zuul网关转发请求给对应的服务。

### 题11：什么是服务熔断什么是服务降级<br/>


### 题12：断路器有几种熔断状态<br/>


### 题13：ribbon-和-feign-有什么区别<br/>


### 题14：load-balancer-负载均衡是什么<br/>


### 题15：spring-cloud-zuul-和-spring-cloud-gateway-有什么区别<br/>


### 题16：什么是-spring-cloud-consul<br/>


### 题17：spring-cloud-框架有哪些优缺点<br/>


### 题18：什么是-spring-cloud-config<br/>


### 题19：spring-cloud-断路器的作用是什么<br/>


### 题20：什么是-spring-cloud-netflix<br/>


### 题21：spring-boot-和-spring-cloud-之间有什么联系<br/>


### 题22：什么是-spring-cloud-框架<br/>


### 题23：微服务有哪些优缺点<br/>


### 题24：spring-cloud-eureka-自我保护机制是什么<br/>


### 题25：分布式事务是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")