# 2022年最全Elasticsearch面试题附答案解析大汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Elasticsearch

### 题1：[Beats 如何与 Elasticsearch 结合使用？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题1beats-如何与-elasticsearch-结合使用)<br/>
Beats是一种开源工具，可以将数据直接传输到Elasticsearch或通过logstash，在使用Kibana进行查看之前，可以对数据进行处理或过滤。

传输的数据类型包含：审核数据，日志文件，云数据，网络流量和窗口事件日志等。

### 题2：[Elasticsearch 中常用的 cat 命令有哪些？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题2elasticsearch-中常用的-cat-命令有哪些)<br/>
面试时说几个核心的就可以，包含但不限于：

|含义|命令|
|-|-|
|别名|GET _cat/aliases?v|
|分配相关|GET _cat/allocation|
|计数|GET _cat/count?v|
|字段数据|GET _cat/fielddata?v|
|运行状况|GET_cat/health?v|
|索引相关|GET _cat/indices?v|
|主节点相关|GET _cat/master?v|
|节点属性|GET _cat/nodeattrs?v|
|节点|GET _cat/nodes?v|
|待处理任务|GET _cat/pending_tasks?v|
|插件|GET _cat/plugins?v|
|恢复|GET _cat / recovery?v|
|存储库|GET _cat /repositories?v|
|段|GET _cat /segments?v|
|分片|GET _cat/shards?v|
|快照|GET _cat/snapshots?v|
|任务|GET _cat/tasks?v|
|模板|GET _cat/templates?v|
|线程池|GET _cat/thread_pool?v|

### 题3：[Elasticsearch 对于大数据量（上亿量级）的聚合如何实现？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题3elasticsearch-对于大数据量上亿量级的聚合如何实现)<br/>
Elasticsearch提供的首个近似聚合是cardinality度量。它提供一个字段的基数，即该字段的distinct或者unique值的数目。它是基于HLL算法的。HLL会先对我们的输入作哈希运算，然后根据哈希运算的结果中的bits做概率估算从而得到基数。其特点是：

1）支持配置精度，用来控制内存的使用（精度高＝多内存）；

2）小数据集精度非常高；

3）可以通过配置参数，来设置去重需要的固定内存使用量。

无论数千还是数十亿的唯一值，内存使用量只与ES配置的精确度相关。

### 题4：[在索引中更新 Mapping 的语法？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题4在索引中更新-mapping-的语法)<br/>
```shell
PUT test_001/_mapping
{
  "properties": {
    "title":{
      "type":"keyword"
    }
  }
}
```

### 题5：[列出 10 家使用 Elasticsearch 作为其应用程序的搜索引擎和数据库的公司？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题5列出-10-家使用-elasticsearch-作为其应用程序的搜索引擎和数据库的公司)<br/>
参与过Elastic中文社区活动或者经常关注社区动态的人都知道，使用的公司太多了，列举如下（排名不分先后）：

阿里、腾讯、字节跳动、百度、京东、美团、小米、滴滴、携程、贝壳找房、360、IBM、顺丰快递等等，几乎能想到的互联网公司都在使用Elasticsearch。

### 题6：[Elasticsearch 中的集群、节点、索引、文档、类型是什么？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题6elasticsearch-中的集群节点索引文档类型是什么)<br/>
群集：一个或多个节点（服务器）的集合，它们共同保存您的整个数据，并提供跨所有节点的联合索引和搜索功能。群集由唯一名称标识，默认情况下为“elasticsearch”。此名称很重要，因为如果节点设置为按名称加入群集，则该节点只能是群集的一部分。　　
 
节点：属于集群一部分的单个服务器。它存储数据并参与群集索引和搜索功能。　　
 
索引：就像关系数据库中的“数据库”。它有一个定义多种类型的映射。索引是逻辑名称空间，映射到一个或多个主分片，并且可以有零个或多个副本分片。
eg: MySQL =>数据库 　　 ElasticSearch =>索引 　　
 
文档：类似于关系数据库中的一行。不同之处在于索引中的每个文档可以具有不同的结构（字段），但是对于通用字段应该具有相同的数据类型。
MySQL=>Databases =>Tables =>Columns/Rows ElasticSearch=>Indices=>Types =>具有属性的文档
 
类型：是索引的逻辑类别/分区，其语义完全取决于用户。

### 题7：[Elasticsearch 安装前需要什么环境？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题7elasticsearch-安装前需要什么环境)<br/>
ElasticSearch是基于lucence开发的，也就是运行时需要java jdk支持。所以要先安装JAVA环境。

注意：由于ElasticSearch 5.x往后依赖于JDK 1.8及以上版本，安装高版本ES需要考虑与JDK版本的兼容性问题。ElasticSearch早期版本需要JDK，在7.X版本后已经集成JDK，已无需第三方依赖。

### 题8：[Elasticsearch 索引数据过多，如何调优和部署？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题8elasticsearch-索引数据过多如何调优和部署)<br/>
索引数据的规划，应在前期做好规划，正所谓“设计先行，编码在后”，这样才能有效的避免突如其来的数据激增导致集群处理能力不足引发的线上客户检索或者其他业务受到影响。

1、动态索引层面

基于模板+时间+rollover api滚动创建索引。

举例：设计阶段定义blog索引的模板格式为：blog_index_时间戳的形式，每天递增数据。

好处：不至于数据量激增导致单个索引数据量非常大，接近于上线2的32次幂-1，索引存储达到了TB+甚至更大。一旦单个索引很大，存储等各种风险也随之而来，所以要提前考虑，及早避免。

2、存储层面

冷热数据分离存储，热数据（比如最近3天或者一周数据），其余为冷数据。对于冷数据不会再写入新数据，可以考虑定期force_merge加shrink压缩操作，节省存储空间和检索效率。

3、部署层面

如果前期没有规划，这里就属于应急策略。结合ES自身的支持动态扩展的特点，动态新增机器的方式可以缓解集群压力。

注意：如果之前主节点等规划合理，不需要重启集群也能完成动态新增。

### 题9：[Elasticsearch 部署时，Linux 设置有哪些优化方法？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题9elasticsearch-部署时linux-设置有哪些优化方法)<br/>
1）关闭缓存swap；

2）堆内存设置为：Min（节点内存/2, 32GB）；

3）设置最大文件句柄数；

4）线程池+队列大小根据业务需要做调整；

5）磁盘存储raid方式：存储有条件使用RAID10，增加单节点性能以及避免单节点存储故障。

### 题10：[ElasticSearch 中是否了解字典树？](/docs/Elasticsearch/2022年最全Elasticsearch面试题附答案解析大汇总.md#题10elasticsearch-中是否了解字典树)<br/>
|数据结构|优缺点|
|-|-|
|Array/List|使用二分法查找，不平衡|
|HashMap/TreeMap|性能高，内存消耗大，几乎是原始数据的三倍|
|Skip List|跳跃表，可快速查找词语，在lucene,redis,HBase中有实现|
|Trie|适合英文词典，如果系统中存在大量字符串且这些字符串基本没有公共前缀|
|Double Array Trie|适合做中文词典，内存占用小，很多分词工具军采用此种算法|
|Ternary Search Tree|一种有状态的转移机，Lucene 4有开源实现，并大量使用|

Trie的核心思想是空间换时间，利用字符串的公共前缀来降低查询时间的开销以

达到提高效率的目的。它有3个基本性质：

1、根节点不包含字符，除根节点外每一个节点都只包含一个字符。

2、从根节点到某一节点，路径上经过的字符连接起来，为该节点对应的字符串。

3、每个节点的所有子节点包含的字符都不相同。

### 题11：什么是副本replica它有什么作用<br/>


### 题12：详细描述一下-elasticsearch-索引文档的过程<br/>


### 题13：elasticsearch-中的属性-enabledindex-和-store-的功能是什么<br/>


### 题14：解释一下-elasticsearch-集群中索引的概念-<br/>


### 题15：elasticsearch-集群中增加和创建索引的步骤是什么<br/>


### 题16：列出与-elasticsearch-有关的主要可用字段数据类型<br/>


### 题17：elasticsearch-中按-id-检索文档的语法是什么<br/>


### 题18：elasticsearch-中列出集群的所有索引的语法是什么<br/>


### 题19：elasticsearch中-cat-api-的功能是什么<br/>


### 题20：elasticsearch-中-term-和-match-有什么区别<br/>


### 题21：说说-elasticsearch-生产集群节点分配方案<br/>


### 题22：elasticsearch-中内置分词器有哪些<br/>


### 题23：elasticsearch-中索引在写入阶段如何调优<br/>


### 题24：master-节点和-候选-master-节点有什么区别<br/>


### 题25：如何使用-elasticsearch-analyzer-中的字符过滤器<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")