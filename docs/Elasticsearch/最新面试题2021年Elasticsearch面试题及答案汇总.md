# 最新面试题2021年Elasticsearch面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Elasticsearch

### 题1：[Elasticsearch中的 Ingest 节点如何工作？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题1elasticsearch中的-ingest-节点如何工作)<br/>
ingest节点可以看作是数据前置处理转换的节点，支持pipeline管道设置，可以使用ingest对数据进行过滤、转换等操作，类似于logstash中filter的作用，功能相当强大。

### 题2：[Elasticsearch 索引数据过多，如何调优和部署？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题2elasticsearch-索引数据过多如何调优和部署)<br/>
索引数据的规划，应在前期做好规划，正所谓“设计先行，编码在后”，这样才能有效的避免突如其来的数据激增导致集群处理能力不足引发的线上客户检索或者其他业务受到影响。

1、动态索引层面

基于模板+时间+rollover api滚动创建索引。

举例：设计阶段定义blog索引的模板格式为：blog_index_时间戳的形式，每天递增数据。

好处：不至于数据量激增导致单个索引数据量非常大，接近于上线2的32次幂-1，索引存储达到了TB+甚至更大。一旦单个索引很大，存储等各种风险也随之而来，所以要提前考虑，及早避免。

2、存储层面

冷热数据分离存储，热数据（比如最近3天或者一周数据），其余为冷数据。对于冷数据不会再写入新数据，可以考虑定期force_merge加shrink压缩操作，节省存储空间和检索效率。

3、部署层面

如果前期没有规划，这里就属于应急策略。结合ES自身的支持动态扩展的特点，动态新增机器的方式可以缓解集群压力。

注意：如果之前主节点等规划合理，不需要重启集群也能完成动态新增。

### 题3：[描述一下 Elasticsearch 更新和删除文档的过程？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题3描述一下-elasticsearch-更新和删除文档的过程)<br/>
1、删除和更新也都是写操作，但是Elasticsearch中的文档是不可变的，因此不能被删除或者改动以展示其变更；

2、磁盘上的每个段都有一个相应的.del文件。当删除请求发送后，文档并没有真的被删除，而是在.del文件中被标记为删除。该文档依然能匹配查询，但是会在结果中被过滤掉。当段合并时，在.del文件中被标记为删除的文档将不会被写入新段。

3、在新的文档被创建时，Elasticsearch会为该文档指定一个版本号，当执行更新时，旧版本的文档在.del文件中被标记为删除，新版本的文档被索引到一个新段。旧版本的文档依然能匹配查询，但是会在结果中被过滤掉。

### 题4：[迁移 Migration API 如何用作 Elasticsearch？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题4迁移-migration-api-如何用作-elasticsearch)<br/>
迁移API简化了X-Pack索引从一个版本到另一个版本的升级。

点到即可，类似问题实际开发中如有发现，需现场排查，类似问题没有什么意义。

参考文献：https://www.elastic.co/guide/en/elasticsearch/reference/current/migration-api.html

### 题5：[Elasticsearch 中的集群、节点、索引、文档、类型是什么？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题5elasticsearch-中的集群节点索引文档类型是什么)<br/>
群集：一个或多个节点（服务器）的集合，它们共同保存您的整个数据，并提供跨所有节点的联合索引和搜索功能。群集由唯一名称标识，默认情况下为“elasticsearch”。此名称很重要，因为如果节点设置为按名称加入群集，则该节点只能是群集的一部分。　　
 
节点：属于集群一部分的单个服务器。它存储数据并参与群集索引和搜索功能。　　
 
索引：就像关系数据库中的“数据库”。它有一个定义多种类型的映射。索引是逻辑名称空间，映射到一个或多个主分片，并且可以有零个或多个副本分片。
eg: MySQL =>数据库 　　 ElasticSearch =>索引 　　
 
文档：类似于关系数据库中的一行。不同之处在于索引中的每个文档可以具有不同的结构（字段），但是对于通用字段应该具有相同的数据类型。
MySQL=>Databases =>Tables =>Columns/Rows ElasticSearch=>Indices=>Types =>具有属性的文档
 
类型：是索引的逻辑类别/分区，其语义完全取决于用户。

### 题6：[Java 中常用的搜索引擎框架有哪些？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题6java-中常用的搜索引擎框架有哪些)<br/>
1、Java 全文搜索引擎框架Lucene

官网：http://lucene.apache.org/

2、开源Java搜索引擎Nutch

官网：http://nutch.apache.org/

3、分布式搜索引擎 ElasticSearch

官网：http://www.elasticsearch.org/

4、实时分布式搜索引擎 Solandra

官网：https://github.com/tjake/Solandra

5、IndexTank

官网：https://github.com/linkedin/indextank-engine

6、搜索引擎 Compass

官网：http://www.compass-project.org/

7、Java全文搜索服务器 Solr

官网：http://lucene.apache.org/solr/

8、Lucene图片搜索 LIRE

官网：http://www.semanticmetadata.net/lire/

9、全文本搜索引擎 Egothor

官网：http://www.egothor.org/cms/

### 题7：[列出 10 家使用 Elasticsearch 作为其应用程序的搜索引擎和数据库的公司？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题7列出-10-家使用-elasticsearch-作为其应用程序的搜索引擎和数据库的公司)<br/>
参与过Elastic中文社区活动或者经常关注社区动态的人都知道，使用的公司太多了，列举如下（排名不分先后）：

阿里、腾讯、字节跳动、百度、京东、美团、小米、滴滴、携程、贝壳找房、360、IBM、顺丰快递等等，几乎能想到的互联网公司都在使用Elasticsearch。

### 题8：[如何使用 Elasticsearch Tokenizer？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题8如何使用-elasticsearch-tokenizer)<br/>
Tokenizer接收字符流（如果包含了字符过滤，则接收过滤后的字符流；否则，接收原始字符流），将其分词。

同时记录分词后的顺序或位置(position)，以及开始值（start_offset）和偏移值(end_offset-start_offset)。

### 题9：[描述一下 Elasticsearch 搜索的过程？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题9描述一下-elasticsearch-搜索的过程)<br/>
1、搜索被执行成一个两阶段过程，称为Query Then Fetch；

2、在初始查询阶段时，查询会广播到索引中每一个分片拷贝（主分片或者副本分片）。 每个分片在本地执行搜索并构建一个匹配文档的大小为from+size的优先队列。

PS：在搜索的时候是会查询Filesystem Cache的，但是有部分数据还在Memory Buffer，所以搜索是近实时的。

3、每个分片返回各自优先队列中 所有文档的ID和排序值给协调节点，它合并这些值到自己的优先队列中来产生一个全局排序后的结果列表。

4、接下来就是取回阶段，协调节点辨别出哪些文档需要被取回并向相关的分片提交多个GET请求。每个分片加载并丰富文档，如果有需要的话，接着返回文档给协调节点。一旦所有的文档都被取回了，协调节点返回结果给客户端。

5、补充：Query Then Fetch的搜索类型在文档相关性打分的时候参考的是本分片的数据，这样在文档数量较少的时候可能不够准确，DFS Query Then Fetch 增 加了一个预查询的处理，询问Term和Document frequency，这个评分更准确，但是性能会变差。

![16377447431.jpg](https://jingxuan.yoodb.com/upload/images/386a8c662f214b989e4b05962c900076.jpg)

### 题10：[Elasticsearch 中列出集群的所有索引的语法是什么？](/docs/Elasticsearch/最新面试题2021年Elasticsearch面试题及答案汇总.md#题10elasticsearch-中列出集群的所有索引的语法是什么)<br/>
```shell
GET /_<索引名称>
```

![image.png](https://jingxuan.yoodb.com/upload/images/d9317a7ce95a424fb11d6c5b39dea1fc.png)

GET index_name，在上述情况下index_name是.kibana。

### 题11：说说-elasticsearch-生产集群节点分配方案<br/>


### 题12：elasticsearch中按-id-检索文档的语法是什么<br/>


### 题13：elasticsearch-中是否了解字典树<br/>


### 题14：如何使用-elasticsearch-analyzer-中的字符过滤器<br/>


### 题15：你可以列出-elasticsearch-各种类型的分析器吗<br/>


### 题16：解释一下-elasticsearch-集群中索引的概念-<br/>


### 题17：kibana-在-elasticsearch-的哪些地方以及如何使用<br/>


### 题18：如何监控-elasticsearch-集群状态<br/>


### 题19：elasticsearch-中-term-和-match-有什么区别<br/>


### 题20：elasticsearch-中按-id-检索文档的语法是什么<br/>


### 题21：拼写纠错是如何实现的<br/>


### 题22：elasticsearch-安装前需要什么环境<br/>


### 题23：在索引中更新-mapping-的语法<br/>


### 题24：beats-如何与-elasticsearch-结合使用<br/>


### 题25：elasticsearch中-cat-api-的功能是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")