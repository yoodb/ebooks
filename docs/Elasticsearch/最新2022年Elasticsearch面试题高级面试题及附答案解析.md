# 最新2022年Elasticsearch面试题高级面试题及附答案解析

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Elasticsearch

### 题1：[Elasticsearch 中索引在设计阶段如何调优？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题1elasticsearch-中索引在设计阶段如何调优)<br/>
1）根据业务增量需求，采取基于日期模板创建索引，通过roll over API滚动索引；

2）使用别名进行索引管理；

3）每天凌晨定时对索引做force_merge操作，以释放空间；

4）采取冷热分离机制，热数据存储到SSD，提高检索效率；冷数据定期进行shrink操作，以缩减存储；

5）采取curator进行索引的生命周期管理；

5）仅针对需要分词的字段，合理的设置分词器；

6）Mapping阶段充分结合各个字段的属性，是否需要检索、是否需要存储等。

### 题2：[Elasticsearch 是如何实现 Master 选举的？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题2elasticsearch-是如何实现-master-选举的)<br/>
前置条件：

1）只有是候选主节点（master：true）的节点才能成为主节点。

2）最小主节点数（min_master_nodes）的目的是防止脑裂。

Elasticsearch 的选主是 ZenDiscovery 模块负责的，主要包含 Ping（节点之间通过这个RPC来发现彼此）和 Unicast（单播模块包含一个主机列表以控制哪些节点需要 ping 通）这两部分；

获取主节点的核心入口为findMaster，选择主节点成功返回对应Master，否则返回null。

选举流程大致描述如下：

第一步：确认候选主节点数达标，elasticsearch.yml设置的值discovery.zen.minimum_master_nodes。

第二步：对所有候选主节点根据nodeId字典排序，每次选举每个节点都把自己所知道节点排一次序，然后选出第一个（第0位）节点，暂且认为它是master节点。

第三步：如果对某个节点的投票数达到一定的值（候选主节点数n/2+1）并且该节点自己也选举自己，那这个节点就是master。否则重新选举一直到满足上述条件。

选举流程总结：

1、Elasticsearch的选主是ZenDiscovery模块负责的，主要包含Ping（节点之间通过这个RPC来发现彼此）和Unicast（单播模块包含一个主机列表以控制哪些节点需要ping通）这两部分；

2、对所有可以成为master的节点（node.master:true）根据nodeId字典排序，每次选举每个节点都把自己所知道节点排一次序，然后选出第一个（第0位）节点，暂且认为它是master节点。

3、如果对某个节点的投票数达到一定的值（可以成为master节点数n/2+1）并且该节点自己也选举自己，那这个节点就是master。否则重新选举一直到满足上述条件。

4、master节点的职责主要包括集群、节点和索引的管理，不负责文档级别的管理；data节点可以关闭http功能*。


### 题3：[列出与 Elasticsearch 有关的主要可用字段数据类型？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题3列出与-elasticsearch-有关的主要可用字段数据类型)<br/>

1、字符串数据类型，包括支持全文检索的text类型 和 精准匹配的keyword类型。

2、数值数据类型，例如字节、短整数、长整数、浮点数、双精度数、half_float、scaled_float。

3、日期类型，日期纳秒Date nanoseconds，布尔值，二进制（Base64编码的字符串）等。

4、范围（整数范围integer_range，长范围long_range，双精度范围double_range，浮动范围float_range，日期范围 date_range）。

5、包含对象的复杂数据类型，nested、Object。

6、GEO地理位置相关类型。

7、特定类型如：数组（数组中的值应具有相同的数据类型）。

### 题4：[你可以列出 Elasticsearch 各种类型的分析器吗？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题4你可以列出-elasticsearch-各种类型的分析器吗)<br/>
Elasticsearch Analyzer的类型为内置分析器和自定义分析器。

**Standard Analyzer：** 标准分析器是默认分词器，如果未指定，则使用该分词器。

它基于Unicode文本分割算法，适用于大多数语言。

**Whitespace Analyzer：** 基于空格字符切词。

**Stop Analyzer：** 在simple Analyzer的基础上，移除停用词。

**Keyword Analyzer：** 不切词，将输入的整个串一起返回。

**自定义分词器的模板：** 自定义分词器的在Mapping的Setting部分设置：

```shell
PUT my_custom_index
{
 "settings":{
  "analysis":{
  "char_filter":{},
  "tokenizer":{},
  "filter":{},
  "analyzer":{}
  }
 }
}
```

其中参数含义如下：

“char_filter”:{},——对应字符过滤部分；

“tokenizer”:{},——对应文本切分为分词部分；

“filter”:{},——对应分词后再过滤部分；

“analyzer”:{}——对应分词器组成部分，其中会包含：1. 2. 3。

### 题5：[ElasticSearch 中是否了解字典树？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题5elasticsearch-中是否了解字典树)<br/>
|数据结构|优缺点|
|-|-|
|Array/List|使用二分法查找，不平衡|
|HashMap/TreeMap|性能高，内存消耗大，几乎是原始数据的三倍|
|Skip List|跳跃表，可快速查找词语，在lucene,redis,HBase中有实现|
|Trie|适合英文词典，如果系统中存在大量字符串且这些字符串基本没有公共前缀|
|Double Array Trie|适合做中文词典，内存占用小，很多分词工具军采用此种算法|
|Ternary Search Tree|一种有状态的转移机，Lucene 4有开源实现，并大量使用|

Trie的核心思想是空间换时间，利用字符串的公共前缀来降低查询时间的开销以

达到提高效率的目的。它有3个基本性质：

1、根节点不包含字符，除根节点外每一个节点都只包含一个字符。

2、从根节点到某一节点，路径上经过的字符连接起来，为该节点对应的字符串。

3、每个节点的所有子节点包含的字符都不相同。

### 题6：[描述一下 Elasticsearch 更新和删除文档的过程？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题6描述一下-elasticsearch-更新和删除文档的过程)<br/>
1、删除和更新也都是写操作，但是Elasticsearch中的文档是不可变的，因此不能被删除或者改动以展示其变更；

2、磁盘上的每个段都有一个相应的.del文件。当删除请求发送后，文档并没有真的被删除，而是在.del文件中被标记为删除。该文档依然能匹配查询，但是会在结果中被过滤掉。当段合并时，在.del文件中被标记为删除的文档将不会被写入新段。

3、在新的文档被创建时，Elasticsearch会为该文档指定一个版本号，当执行更新时，旧版本的文档在.del文件中被标记为删除，新版本的文档被索引到一个新段。旧版本的文档依然能匹配查询，但是会在结果中被过滤掉。

### 题7：[Elasticsearch中按 ID 检索文档的语法是什么？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题7elasticsearch中按-id-检索文档的语法是什么)<br/>
GET API从索引中检索指定的JSON文档。

句法：

```shell
GET <index_name>/_doc/<_id>
```
实例：

```shell
GET test_001/_doc/1
```

### 题8：[如何使用 Elasticsearch Tokenizer？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题8如何使用-elasticsearch-tokenizer)<br/>
Tokenizer接收字符流（如果包含了字符过滤，则接收过滤后的字符流；否则，接收原始字符流），将其分词。

同时记录分词后的顺序或位置(position)，以及开始值（start_offset）和偏移值(end_offset-start_offset)。

### 题9：[什么是 Elasticsearch？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题9什么是-elasticsearch)<br/>
ES是一种开源、RESTful、可扩展的基于文档的搜索引擎，它构建在Lucene库上。

用户使用Kibana就可以可视化使用数据，同时Kibana也提供交互式的数据状态呈现和数据分析。

Apache Lucene搜索引擎基于JSON文档来进行搜索管理和快速搜索。

Elasticsearch，可简称为ES（官方未给出简称名字，很多人都这么叫而已）一种开源、RESTful、可扩展的基于文档的搜索引擎，构建是在Apache Lucene库的基础上的搜索引擎，无论在开源还是专有领域，Lucene可以被认为是迄今为止最先进、性能最好的、功能最全的搜索引擎库。 但是，Lucene只是一个库。想要发挥其强大的作用，需使用Java并要将其集成到应用中。

Elasticsearch是使用Java编写并使用Lucene来建立索引并实现搜索功能，但是它的目的是通过简单连贯的RESTful API让全文搜索变得简单并隐藏Lucene的复杂性。 

用户通过JSON格式的请求，使用CRUD的REST API就可以完成存储和管理文本、数值、地理空间、结构化或者非结构化的数据。

Elasticsearch不仅是Lucene和全文搜索引擎，它还提供：

1、分布式的实时文件存储，每个字段都被索引并可被搜索
2、实时分析的分布式搜索引擎
3、可以扩展到上百台服务器，处理PB级结构化或非结构化数据，而且所有的这些功能被集成到一台服务器，应用可以通过简单的RESTful API、各种语言的客户端甚至命令行与之交互。

Elasticsearch非常简单，它提供了许多合理的缺省值，并对初学者隐藏了复杂的搜索引擎理论。开箱即用（安装即可使用），只需很少的学习既可在生产环境中使用。Elasticsearch在Apache 2 license下许可使用，可以免费下载、使用和修改。 

### 题10：[Elasticsearch 部署时，Linux 设置有哪些优化方法？](/docs/Elasticsearch/最新2022年Elasticsearch面试题高级面试题及附答案解析.md#题10elasticsearch-部署时linux-设置有哪些优化方法)<br/>
1）关闭缓存swap；

2）堆内存设置为：Min（节点内存/2, 32GB）；

3）设置最大文件句柄数；

4）线程池+队列大小根据业务需要做调整；

5）磁盘存储raid方式：存储有条件使用RAID10，增加单节点性能以及避免单节点存储故障。

### 题11：如何使用-elastic-reporting<br/>


### 题12：解释一下-elasticsearch-集群中索引的概念-<br/>


### 题13：elasticsearch-中常见的分词过滤器有哪些<br/>


### 题14：elasticsearch-中-term-和-match-有什么区别<br/>


### 题15：elasticsearch-中内置分词器有哪些<br/>


### 题16：elasticsearch中的-ingest-节点如何工作<br/>


### 题17：elasticsearch-中索引在查询阶段如何调优<br/>


### 题18：能否列出-与-elk-日志分析相关的应用场景<br/>


### 题19：列出-10-家使用-elasticsearch-作为其应用程序的搜索引擎和数据库的公司<br/>


### 题20：elasticsearch-中分析器由哪几部分组成<br/>


### 题21：kibana-在-elasticsearch-的哪些地方以及如何使用<br/>


### 题22：请解释一下-elasticsearch-中聚合的工作原理<br/>


### 题23：elasticsearch-中常用的-cat-命令有哪些<br/>


### 题24：什么是副本replica它有什么作用<br/>


### 题25：elasticsearch-支持哪些配置管理工具<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")