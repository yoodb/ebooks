# 经典面试题2021年常见Elasticsearch面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Elasticsearch

### 题1：[Elasticsearch中的 Ingest 节点如何工作？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题1elasticsearch中的-ingest-节点如何工作)<br/>
ingest节点可以看作是数据前置处理转换的节点，支持pipeline管道设置，可以使用ingest对数据进行过滤、转换等操作，类似于logstash中filter的作用，功能相当强大。

### 题2：[在索引中更新 Mapping 的语法？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题2在索引中更新-mapping-的语法)<br/>
```shell
PUT test_001/_mapping
{
  "properties": {
    "title":{
      "type":"keyword"
    }
  }
}
```

### 题3：[REST API 相对于 Elasticsearch 有哪些优势？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题3rest-api-相对于-elasticsearch-有哪些优势)<br/>
REST API是使用超文本传输​​协议的系统之间的通信，它以XML和JSON格式传输数据请求。

REST协议是无状态的，并且与服务器和存储数据的用户界面分离，从而增强了用户界面与任何类型平台的可移植性。它还提高了可扩展性，允许独立实现组件，因此应用程序变得更加灵活。

REST API独立于平台和语言，只是用于数据交换的语言是XML或JSON。

### 题4：[你可以列出 Elasticsearch 各种类型的分析器吗？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题4你可以列出-elasticsearch-各种类型的分析器吗)<br/>
Elasticsearch Analyzer的类型为内置分析器和自定义分析器。

**Standard Analyzer：** 标准分析器是默认分词器，如果未指定，则使用该分词器。

它基于Unicode文本分割算法，适用于大多数语言。

**Whitespace Analyzer：** 基于空格字符切词。

**Stop Analyzer：** 在simple Analyzer的基础上，移除停用词。

**Keyword Analyzer：** 不切词，将输入的整个串一起返回。

**自定义分词器的模板：** 自定义分词器的在Mapping的Setting部分设置：

```shell
PUT my_custom_index
{
 "settings":{
  "analysis":{
  "char_filter":{},
  "tokenizer":{},
  "filter":{},
  "analyzer":{}
  }
 }
}
```

其中参数含义如下：

“char_filter”:{},——对应字符过滤部分；

“tokenizer”:{},——对应文本切分为分词部分；

“filter”:{},——对应分词后再过滤部分；

“analyzer”:{}——对应分词器组成部分，其中会包含：1. 2. 3。

### 题5：[介绍一下常见电商搜索的整体技术架构？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题5介绍一下常见电商搜索的整体技术架构)<br/>
![16377450381.jpg](https://jingxuan.yoodb.com/upload/images/78e36b2ce4cb46caadbaa3c186c4cef4.jpg)

### 题6：[Elasticsearch 中按 ID 检索文档的语法是什么？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题6elasticsearch-中按-id-检索文档的语法是什么)<br/>
```shell
GET test_001/_doc/1
```

### 题7：[Elasticsearch 中什么是分词器？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题7elasticsearch-中什么是分词器)<br/>
把全文本转换成一系列单词（term/token）的过程，叫做分词。

举一个分词简单的例子：比如输入 Hello World，会自动分成两个单词，一个是hello，另一个是world，可以看出单词也被转化成了小写。

分词器（Tokenizers）接收一个字符流，将其分解为单个标记(通常是单个单词)，并输出一个标记流。例如，当看到任何空白时，whitespace分词器就会将文本分解为标记。它会将文本“Quick brown fox!”转换为“Quick, brown, fox!” 。

分词器（Tokenizers）还负责记录每个词的顺序或位置(用于短语和词接近查询)，以及该词表示的原始词的开头和结尾字符偏移量(用于突出显示搜索片段)。

分词器（Tokenizers）主要还是用来创建分析器（Analyzer）的。ES有很多内置的分词器，以供用户来自定义分析器。

分词器是专门处理分词的组件，分词器由以下三部分组成：

**Character Filters：** 针对原始文本处理，比如去除HTML标签。

**Tokenizer：** 按照规则切分为单词，比如按照空格切分。

**Token Filters：** 将切分的单词进行加工，比如大写转小写，删除stopwords，增加同义语。

### 题8：[如何监控 Elasticsearch 集群状态？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题8如何监控-elasticsearch-集群状态)<br/>
Marvel可以很简单的通过Kibana监控Elasticsearch。支持实时查看集群健康状态和性能，也可以分析过去的集群、索引和节点指标。

### 题9：[Elasticsearch 客户端和集群连接时，如何选择特定的节点执行请求的？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题9elasticsearch-客户端和集群连接时如何选择特定的节点执行请求的)<br/>
1、TransportClient利用transport模块远程连接一个elasticsearch集群。

2、TransportClient并不加入到集群中，只是简单的获得一个或者多个初始化的transport地址，并以轮询的方式与这些地址进行通信。

### 题10：[能否列出 与 ELK 日志分析相关的应用场景？](/docs/Elasticsearch/经典面试题2021年常见Elasticsearch面试题及答案汇总.md#题10能否列出-与-elk-日志分析相关的应用场景)<br/>
电子商务搜索解决方案

欺诈识别

市场情报

风险管理

安全分析等。

### 题11：如何在-elasticsearch-中搜索数据<br/>


### 题12：elasticsearch-中索引在写入阶段如何调优<br/>


### 题13：拼写纠错是如何实现的<br/>


### 题14：elasticsearch-中的属性-enabledindex-和-store-的功能是什么<br/>


### 题15：elasticsearch-中-term-和-match-有什么区别<br/>


### 题16：elasticsearch-中常用的-cat-命令有哪些<br/>


### 题17：elasticsearch-的节点类型有什么区别<br/>


### 题18：描述一下-elasticsearch-更新和删除文档的过程<br/>


### 题19：安装-elasticsearch-时请解释不同的包及其重要性<br/>


### 题20：对于-gc-方面使用-elasticsearch-时要注意什么<br/>


### 题21：elasticsearch-部署时linux-设置有哪些优化方法<br/>


### 题22：描述一下-elasticsearch-搜索的过程<br/>


### 题23：elasticsearch-中解释一下-nrt-是什么<br/>


### 题24：说说-elasticsearch-生产集群节点分配方案<br/>


### 题25：elasticsearch-对于大数据量上亿量级的聚合如何实现<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")