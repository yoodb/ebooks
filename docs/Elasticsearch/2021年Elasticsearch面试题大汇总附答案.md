# 2021年Elasticsearch面试题大汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Elasticsearch

### 题1：[在使用 Elasticsearch 时要注意什么？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题1在使用-elasticsearch-时要注意什么)<br/>
由于ES使用的Java写的，所有注意的是GC方面的问题

1、倒排词典的索引需要常驻内存，无法GC，需要监控data node上segmentmemory增长趋势。

2、各类缓存，field cache、filter cache、indexing cache、bulk queue等等，要设置合理的大小，并且要应该根据最坏的情况来看 heap 是否够用，也就是各类缓存全部占满的时候，还有heap 空间可以分配给其他任务吗？避免采用clear cache等“自欺欺人”的方式来释放内存。

3、避免返回大量结果集的搜索与聚合。确实需要大量拉取数据的场景，可以采用scan&scroll api来实现。

4、cluster stats驻留内存并无法水平扩展，超大规模集群可以考虑分拆成多个集群通过tribe node连接。

5、想知道heap够不够，必须结合实际应用场景，并对集群的heap使用情况做持续的监控。

### 题2：[ElasticSearch 的节点类型有什么区别？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题2elasticsearch-的节点类型有什么区别)<br/>
节点是指ElasticSearch的实例。当启动Elasticsearch的实例，就会启动至少一个节点。

相同集群名的多个节点的连接就组成了一个集群，在默认情况下，集群中的每个节点都可以处理http请求和集群节点间的数据传输，集群中所有的节点都知道集群中其他所有的节点，可以将客户端请求转发到适当的节点。

节点有以下类型：

主(master)节点：在一个节点上当node.master设置为True（默认）的时候，它有资格被选作为主节点，控制整个集群。

数据(data)节点：在一个节点上node.data设置为True（默认）的时候。该节点保存数据和执行数据相关的操作，如增删改查，搜索，和聚合。

客户端节点：当一个节点的node.master和node.data都设置为false的时候，它既不能保持数据也不能成为主节点，该节点可以作为客户端节点，可以响应用户的情况，并把相关操作发送到其他节点。

部落节点：当一个节点配置tribe.*的时候，它是一个特殊的客户端，它可以连接多个集群，在所有连接的集群上执行搜索和其他操作。

### 题3：[Elasticsearch 中分析器的工作过程原理？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题3elasticsearch-中分析器的工作过程原理)<br/>
分析器的工作过程大概分成两步：

**分词（Tokenization）：** 根据停止词把文本分割成很多的小的token，比如the quick fox会被分成the、quick、fox，其中的停止词就是空格，还有很多其他的停止词比如&或者#，大多数的标点符号都是停止词

**归一化（Normalization）：** 把分隔的token变成统一的形式方便匹配，比如下面几种

把单词变成小写，Quick会变成quick。

提取词干，foxes变成fox。

合并同义词，jump和leap是同义词，会被统一索引成jump。

Elasticsearch自带了一个分析器，是系统默认的标准分析器，只对英文语句做分词，中文不支持，每个中文字都会被拆分为独立的个体。

### 题4：[如何使用 Elastic Reporting？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题4如何使用-elastic-reporting)<br/>
收费功能，只是了解，点到为止。

Reporting API有助于将检索结果生成PDF格式，图像PNG格式以及电子表格CSV格式的数据，并可根据需要进行共享或保存。

### 题5：[描述一下 Elasticsearch 搜索的过程？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题5描述一下-elasticsearch-搜索的过程)<br/>
1、搜索被执行成一个两阶段过程，称为Query Then Fetch；

2、在初始查询阶段时，查询会广播到索引中每一个分片拷贝（主分片或者副本分片）。 每个分片在本地执行搜索并构建一个匹配文档的大小为from+size的优先队列。

PS：在搜索的时候是会查询Filesystem Cache的，但是有部分数据还在Memory Buffer，所以搜索是近实时的。

3、每个分片返回各自优先队列中 所有文档的ID和排序值给协调节点，它合并这些值到自己的优先队列中来产生一个全局排序后的结果列表。

4、接下来就是取回阶段，协调节点辨别出哪些文档需要被取回并向相关的分片提交多个GET请求。每个分片加载并丰富文档，如果有需要的话，接着返回文档给协调节点。一旦所有的文档都被取回了，协调节点返回结果给客户端。

5、补充：Query Then Fetch的搜索类型在文档相关性打分的时候参考的是本分片的数据，这样在文档数量较少的时候可能不够准确，DFS Query Then Fetch 增 加了一个预查询的处理，询问Term和Document frequency，这个评分更准确，但是性能会变差。

![16377447431.jpg](https://jingxuan.yoodb.com/upload/images/386a8c662f214b989e4b05962c900076.jpg)

### 题6：[列出 10 家使用 Elasticsearch 作为其应用程序的搜索引擎和数据库的公司？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题6列出-10-家使用-elasticsearch-作为其应用程序的搜索引擎和数据库的公司)<br/>
参与过Elastic中文社区活动或者经常关注社区动态的人都知道，使用的公司太多了，列举如下（排名不分先后）：

阿里、腾讯、字节跳动、百度、京东、美团、小米、滴滴、携程、贝壳找房、360、IBM、顺丰快递等等，几乎能想到的互联网公司都在使用Elasticsearch。

### 题7：[Elasticsearch 中列出集群的所有索引的语法是什么？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题7elasticsearch-中列出集群的所有索引的语法是什么)<br/>
```shell
GET /_<索引名称>
```

![image.png](https://jingxuan.yoodb.com/upload/images/d9317a7ce95a424fb11d6c5b39dea1fc.png)

GET index_name，在上述情况下index_name是.kibana。

### 题8：[Elasticsearch 中索引在写入阶段如何调优？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题8elasticsearch-中索引在写入阶段如何调优)<br/>
1）写入前副本数设置为0；

2）写入前关闭refresh_interval设置为-1，禁用刷新机制；

3）写入过程中：采取bulk批量写入；

4）写入后恢复副本数和刷新间隔；

5）尽量使用自动生成的id。

### 题9：[REST API 相对于 Elasticsearch 有哪些优势？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题9rest-api-相对于-elasticsearch-有哪些优势)<br/>
REST API是使用超文本传输​​协议的系统之间的通信，它以XML和JSON格式传输数据请求。

REST协议是无状态的，并且与服务器和存储数据的用户界面分离，从而增强了用户界面与任何类型平台的可移植性。它还提高了可扩展性，允许独立实现组件，因此应用程序变得更加灵活。

REST API独立于平台和语言，只是用于数据交换的语言是XML或JSON。

### 题10：[对于 GC 方面，使用 Elasticsearch 时要注意什么？](/docs/Elasticsearch/2021年Elasticsearch面试题大汇总附答案.md#题10对于-gc-方面使用-elasticsearch-时要注意什么)<br/>
1）倒排词典的索引需要常驻内存，无法GC，需要监控data node上segment memory增长趋势。

2）各类缓存，field cache、filter cache、indexing cache、bulk queue等等，要设置合理的大小，并且要应该根据最坏的情况来看heap是否够用，也就是各类缓存全部占满的时候，还有heap空间可以分配给其他任务吗？避免采用clear cache等“自欺欺人”的方式来释放内存。

3）避免返回大量结果集的搜索与聚合。确实需要大量拉取数据的场景，可以采用scan & scroll api来实现。

4）cluster stats驻留内存并无法水平扩展，超大规模集群可以考虑分拆成多个集群通过tribe node连接。

5）想知道heap够不够，必须结合实际应用场景，并对集群的heap使用情况做持续的监控。

### 题11：我们可以在-elasticsearch-中执行搜索的各种可能方式有哪些<br/>


### 题12：能否列出-与-elk-日志分析相关的应用场景<br/>


### 题13：在索引中更新-mapping-的语法<br/>


### 题14：你能否列出与-elasticsearch-有关的主要可用字段数据类型<br/>


### 题15：elasticsearch-中-term-和-match-有什么区别<br/>


### 题16：在并发情况下elasticsearch-如何保证读写一致<br/>


### 题17：拼写纠错是如何实现的<br/>


### 题18：什么是副本replica它有什么作用<br/>


### 题19：如何解决-elasticsearch-集群的脑裂问题<br/>


### 题20：你能列出-x-pack-api-类型吗<br/>


### 题21：elasticsearch-分片是什么<br/>


### 题22：elasticsearch-中常用的-cat-命令有哪些<br/>


### 题23：elasticsearch-支持哪些配置管理工具<br/>


### 题24：如何监控-elasticsearch-集群状态<br/>


### 题25：说说-elasticsearch-生产集群节点分配方案<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")