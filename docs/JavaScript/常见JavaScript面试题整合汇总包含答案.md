# 常见JavaScript面试题整合汇总包含答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## JavaScript

### 题1：[如何将 JavaScript 代码分解成几行吗？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题1如何将-javascript-代码分解成几行吗)<br/>
在字符串语句中可以通过在第一行末尾使用反斜杠“\”来完成

例：
```javascript
document.write("This is \a program");
```

如果不是在字符串语句中更改为新行，那么javaScript会忽略行中的断点。

例：
```javascript
var x=1, y=2,
z=
x+y;
```

上面的代码是完美的，但并不建议这样做，因为阻碍了调试。

### 题2：[ViewState 和 SessionState 有什么区别？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题2viewstate-和-sessionstate-有什么区别)<br/>
ViewState特定于会话中的页面。

SessionState特定于可在Web应用程序中的所有页面上访问的用户特定数据。

### 题3：[解释事件冒泡以及如何阻止它？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题3解释事件冒泡以及如何阻止它)<br/>
事件冒泡是指嵌套最深的元素触发一个事件，然后这个事件顺着嵌套顺序在父元素上触发。

防止事件冒泡的一种方法是使用event.cancelBubble或event.stopPropagation()（低于 IE 9）。

### 题4：[JavaScript 中如何使用事件处理程序？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题4javascript-中如何使用事件处理程序)<br/>
事件是由用户生成活动（例如单击链接或填写表单）导致的操作。

需要一个事件处理程序来管理所有这些事件的正确执行。

事件处理程序是对象的额外属性。此属性包括事件的名称以及事件发生时采取的操作。

### 题5：[什么是 JavaScript？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题5什么是-javascript)<br/>
JavaScript（简称“JS”）是一种具有函数优先的轻量级，解释型或即时编译型的编程语言。虽然它是作为开发Web页面的脚本语言而出名，但是它也被用到了很多非浏览器环境中，JavaScript基于原型编程、多范式的动态脚本语言，并且支持面向对象、命令式、声明式、函数式编程范式。

JavaScript是客户端和服务器端脚本语言，可以插入到HTML页面中，并且是目前较热门的Web开发语言。同时，JavaScript也是面向对象编程语言。

### 题6：[“use strict”的作用是什么？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题6“use-strict”的作用是什么)<br/>
use strict出现在JavaScript代码的顶部或函数的顶部，可以帮助你写出更安全的JavaScript代码。如果你错误地创建了全局变量，它会通过抛出错误的方式来警告你。例如，以下程序将抛出错误：

```javascript
function doSomething(val) {
 "use strict"; 
 x = val + 10;
}
```

它会抛出一个错误，因为x没有被定义，并使用了全局作用域中的某个值对其进行赋值，而use strict不允许这样做。下面的小改动修复了这个错误：

```javascript
function doSomething(val) {
 "use strict"; 
 var x = val + 10;
}
```

### 题7：[什么是负无穷大？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题7什么是负无穷大)<br/>
负无穷大是JavaScript中的一个数字，可以通过将负数除以零来得到。

### 题8：[JavaScript 中的循环结构都有什么？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题8javascript-中的循环结构都有什么)<br/>
For、While、do-while loops

### 题9：[escape 字符是用来做什么的？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题9escape-字符是用来做什么的)<br/>
escape方法返回一个包含charstring内容的字符串值（Unicode格式）。所有空格、标点、重音符号以及其他非ASCII字符都用\%xx 编码代替，其中xx等于表示该字符的十六进制数。例如，空格返回的是"%20" 。

字符值大于255的以%uxxxx格式存储。

### 题10：[web-garden 和 web-farm 之间有何不同？](/docs/JavaScript/常见JavaScript面试题整合汇总包含答案.md#题10web-garden-和-web-farm-之间有何不同)<br/>
web-garden和web-farm都是网络托管系统。

唯一的区别是web-garden是在单个服务器中包含许多处理器的设置，而web-farm是使用多个服务器的较大设置。

### 题11：javascript-中-.call()-和.apply()-之间有什么区别<br/>


### 题12：3--2-"7"-的结果是什么<br/>


### 题13：什么是-===-运算符<br/>


### 题14：javascript-中不同类型的错误有几种<br/>


### 题15：javascript-中-void(0)-如何使用<br/>


### 题16：javascript-中的强制转型是指什么<br/>


### 题17：什么是全局变量这些变量如何声明使用全局变量有哪些问题<br/>


### 题18：如何在不支持-javascript-的旧浏览器中隐藏-javascript-代码<br/>


### 题19：javascript-中的作用域scope是指什么<br/>


### 题20：编写一个可以执行如下操作的函数<br/>


### 题21：解释-javascript-中定时器的工作如果有也可以说明使用定时器的缺点<br/>


### 题22：0.1--0.2-===-0.3--输出的结果是什么<br/>


### 题23：javascript-中读取和写入文件的方法是什么<br/>


### 题24：描述一下-revealing-module-pattern-设计模式<br/>


### 题25：解释-javascript-中的值和类型<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")