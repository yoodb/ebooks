# 经典JavaScript面试题及答案附答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## JavaScript

### 题1：[JavaScript 中有哪些类型的弹出框？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题1javascript-中有哪些类型的弹出框)<br/>
Alert、Confirm、Prompt

### 题2：[描述一下 Revealing Module Pattern 设计模式？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题2描述一下-revealing-module-pattern-设计模式)<br/>
暴露模块模式（Revealing Module Pattern）是模块模式的一个变体，目的是维护封装性并暴露在对象中返回的某些变量和方法。如下所示：

```javascript
var Exposer = (function() {
 var privateVariable = 10;
 var privateMethod = function() {
 console.log('Inside a private method!');
 privateVariable++;
 }
 var methodToExpose = function() {
 console.log('This is a method I want to expose!');
 }
 var otherMethodIWantToExpose = function() {
 privateMethod();
 }
 return {
 first: methodToExpose,
 second: otherMethodIWantToExpose
 };
})();
Exposer.first(); // 输出: This is a method I want to expose!
Exposer.second(); // 输出: Inside a private method!
Exposer.methodToExpose; // undefined
```

它的一个明显的缺点是无法引用私有方法。

### 题3：[JavaScript 中的闭包是什么？举个例子？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题3javascript-中的闭包是什么举个例子)<br/>
闭包是在另一个函数（称为父函数）中定义的函数，并且可以访问在父函数作用域中声明和定义的变量。

闭包可以访问三个作用域中的变量：

1. 在自己作用域中声明的变量；
2. 在父函数中声明的变量；
3. 在全局作用域中声明的变量。

```javascript
var globalVar = "abc";
// 自调用函数
(function outerFunction (outerArg) { // outerFunction 作用域开始
 // 在 outerFunction 函数作用域中声明的变量
 var outerFuncVar = 'x'; 
 // 闭包自调用函数
 (function innerFunction (innerArg) { // innerFunction 作用域开始
 // 在 innerFunction 函数作用域中声明的变量
 var innerFuncVar = "y";
 console.log( 
 "outerArg = " + outerArg + "
" +
 "outerFuncVar = " + outerFuncVar + "
" +
 "innerArg = " + innerArg + "
" +
 "innerFuncVar = " + innerFuncVar + "
" +
 "globalVar = " + globalVar);
 // innerFunction 作用域结束
 })(5); // 将 5 作为参数
// outerFunction 作用域结束
})(7); // 将 7 作为参数
```
innerFunction是在outerFunction中定义的闭包，可以访问在outerFunction 作用域内声明和定义的所有变量。除此之外，闭包还可以访问在全局命名空间中声明的变量。

上述代码的输出结果：

```javascript
outerArg = 7
outerFuncVar = x
innerArg = 5
innerFuncVar = y
globalVar = abc
```

### 题4：[解释 JavaScript 中的 pop() 方法？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题4解释-javascript-中的-pop()-方法)<br/>
pop()方法与shift()方法类似，但不同之处在于Shift方法在数组的开头工作。此外，pop()方法将最后一个元素从给定的数组中取出并返回。然后改变被调用的数组。

例：
```javascript
var cloths = ["Shirt", "Pant", "TShirt"];
cloths.pop();
//Now cloth becomes Shirt,Pant
```

### 题5：[如何在 JavaScript 中比较两个对象？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题5如何在-javascript-中比较两个对象)<br/>
对于两个非原始值，比如两个对象（包括函数和数组），==和\===比较都只是检查它们的引用是否匹配，并不会检查实际引用的内容。

例如，默认情况下，数组将被强制转型成字符串，并使用逗号将数组的所有元素连接起来。所以，两个具有相同内容的数组进行 == 比较时不会相等：

```javascript
var a = [1,2,3];
var b = [1,2,3];
var c = "1,2,3";
a == c; // true
b == c; // true
a == b; // false
```

对于对象的深度比较，可以使用deep-equal 这个库，或者自己实现递归比较算法。

### 题6：[匿名函数和命名函数有什么区别？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题6匿名函数和命名函数有什么区别)<br/>
匿名函数通常是某一个事件触发后进行触发的。

命名函数可以进行预先的封装，在需要使用的地方通过调用函数名运行。

```javascript
var niming = function() { // 赋给变量niming的匿名函数
 // ..
};
var mingming = function bar(){ // 赋给变量mingming的命名函数bar
 // ..
};
niming(); // 实际执行函数
mingming();
```

### 题7：[JavaScript 中的作用域（scope）是指什么？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题7javascript-中的作用域scope是指什么)<br/>
在JavaScript中，每个函数都有自己的作用域。作用域基本上是变量以及如何通过名称访问这些变量的规则的集合。只有函数中的代码才能访问函数作用域内的变量。

同一个作用域中的变量名必须是唯一的。一个作用域可以嵌套在另一个作用域内。如果一个作用域嵌套在另一个作用域内，最内部作用域内的代码可以访问另一个作用域的变量。

### 题8：[解释 window.onload 和 onDocumentReady？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题8解释-window.onload-和-ondocumentready)<br/>
在载入页面的所有信息之前，不运行onload函数。这导致在执行任何代码之前会出现延迟。

onDocumentReady在加载DOM之后加载代码。这允许早期的代码操纵。

### 题9：[JavaScript 中获取 CheckBox 状态的方式是什么？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题9javascript-中获取-checkbox-状态的方式是什么)<br/>
```javascript
alert(document.getElementById('checkbox1').checked);
```

如果CheckBox被检查，此警报将返回TRUE。

### 题10：[JavaScript 中解释原型设计模式？](/docs/JavaScript/经典JavaScript面试题及答案附答案汇总.md#题10javascript-中解释原型设计模式)<br/>
原型模式可用于创建新对象，但它创建的不是非初始化的对象，而是使用原型对象（或样本对象）的值进行初始化的对象。原型模式也称为属性模式。

原型模式在初始化业务对象时非常有用，业务对象的值与数据库中的默认值相匹配。原型对象中的默认值被复制到新创建的业务对象中。

经典的编程语言很少使用原型模式，但作为原型语言的JavaScript在构造新对象及其原型时使用了这个模式。

### 题11：javascript-中-dataypes-的两个基本组是什么<br/>


### 题12：0.1--0.2-===-0.3--输出的结果是什么<br/>


### 题13：-“this”关键字的原理是什么请提供一些代码示例。<br/>


### 题14：什么是-iife立即调用函数表达式<br/>


### 题15：javascript-中-.call()-和.apply()-之间有什么区别<br/>


### 题16：解释-javascript-中的值和类型<br/>


### 题17：javascript-中如何使用事件处理程序<br/>


### 题18：什么是-javascript-中的提升操作<br/>


### 题19：列举-java-和-javascript-之间的区别<br/>


### 题20：解释-javascript-中的相等性<br/>


### 题21：javascript-中的-null-是什么意思<br/>


### 题22：javascript-中的各种功能组件是什么<br/>


### 题23：说一说-==-和-===-之间有什么区别<br/>


### 题24：什么是负无穷大<br/>


### 题25：如何检测客户端机器上的操作系统<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")