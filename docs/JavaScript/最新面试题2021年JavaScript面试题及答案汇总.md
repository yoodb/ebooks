# 最新面试题2021年JavaScript面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## JavaScript

### 题1：[解释 window.onload 和 onDocumentReady？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题1解释-window.onload-和-ondocumentready)<br/>
在载入页面的所有信息之前，不运行onload函数。这导致在执行任何代码之前会出现延迟。

onDocumentReady在加载DOM之后加载代码。这允许早期的代码操纵。

### 题2：[什么是 JavaScript 中的提升操作？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题2什么是-javascript-中的提升操作)<br/>
提升（hoisting）是JavaScript解释器将所有变量和函数声明移动到当前作用域顶部的操作。

有两种类型的提升：

>1）变量提升——非常少见
2）函数提升——常见

无论var（或函数声明）出现在作用域的什么地方，它都属于整个作用域，并且可以在该作用域内的任何地方访问它。

```javascript
var a = 2;
foo(); // 因为`foo()`声明被"提升"，所以可调用
function foo() {
 a = 3;
 console.log( a ); // 3
 var a; // 声明被"提升"到 foo() 的顶部
}
console.log( a ); // 2
```

### 题3：[delete 操作符的功能是什么？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题3delete-操作符的功能是什么)<br/>
delete操作符用于删除程序中的所有变量或对象，但不能删除使用VAR关键字声明的变量。

### 题4：[JavaScript 中 void(0) 如何使用？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题4javascript-中-void(0)-如何使用)<br/>
void(0)用于防止页面刷新，并在调用时传递参数“zero”。

void(0)用于调用另一种方法而不刷新页面。

### 题5：[JavaScript 中使用 innerHTML 的缺点是什么？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题5javascript-中使用-innerhtml-的缺点是什么)<br/>
如果不重新解析整个innerHTML，就没有附加支持。这使得直接更改innerHTML非常慢。

例如，要附加到html标签，需要执行以下操作：

```javascript
let myDiv = document.querySelector('#myDiv')
//重新解析整个myDiv标签。
myDiv.innerHTML += '<p>Added new tag</p>'
```

innerHTML不提供验证，因此我们可以潜在地在文档中插入有效和损坏的HTML并将其破坏。

### 题6：[JavaScript 中哪些关键字用于处理异常？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题6javascript-中哪些关键字用于处理异常)<br/>
try... catch-finally用于处理JavaScript中的异常。

### 题7：[解释一下 ES5 和 ES6 之间有什么区别？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题7解释一下-es5-和-es6-之间有什么区别)<br/>
ECMAScript 5（ES5）：ECMAScript的第5版，于2009年标准化。这个标准已在所有现代浏览器中完全实现。

ECMAScript 6（ES6）或ECMAScript 2015（ES2015）：第6版ECMAScript，于2015年标准化。这个标准已在大多数现代浏览器中部分实现。

以下是ES5和ES6之间的一些主要区别：

**箭头函数和字符串插值：**

```javascript
const greetings = (name) => {
 return `hello ${name}`;
}
const greetings = name => `hello ${name}`;
```

**常量**

常量在很多方面与其他语言中的常量一样，但有一些需要注意的地方。常量表示对值的“固定引用”。因此，在使用常量时，你实际上可以改变变量所引用的对象的属性，但无法改变引用本身。

```javascript
const NAMES = [];
NAMES.push("Jim");
console.log(NAMES.length === 1); // true
NAMES = ["Steve", "John"]; // error
```
**块作用域变量。**

新的 ES6 关键字 let 允许开发人员声明块级别作用域的变量。let 不像 var 那样可以进行提升。

**默认参数值**

默认参数允许我们使用默认值初始化函数。如果省略或未定义参数，则使用默认值，也就是说 null 是有效值。

```javascript
// 基本语法
function multiply (a, b = 2) {
 return a * b;
}
multiply(5); // 10
```

**类定义和继承**

ES6 引入了对类（关键字 class）、构造函数（关键字 constructor）和用于继承的 extend 关键字的支持。

**for…of 操作符**

for…of 语句将创建一个遍历可迭代对象的循环。

**用于对象合并的Spread操作**

```javascript
const obj1 = { a: 1, b: 2 }
const obj2 = { a: 2, c: 3, d: 4}
const obj3 = {...obj1, ...obj2}
```

**promise**

promise 提供了一种机制来处理异步操作结果。你可以使用回调来达到同样的目的，但是 promise 通过方法链接和简洁的错误处理带来了更高的可读性。

```javascript
const isGreater = (a, b) => {
return new Promise ((resolve, reject) => {
 if(a > b) {
 resolve(true)
 } else {
 reject(false)
 }
 })
}
isGreater(1, 2)
.then(result => {
 console.log('greater')
})
.catch(result => {
 console.log('smaller')
})
```

**模块导出和导入**

```javascript
const myModule = { x: 1, y: () => { console.log('This is ES5') }}
export default myModule;
import myModule from './myModule';
```

### 题8：[web-garden 和 web-farm 之间有何不同？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题8web-garden-和-web-farm-之间有何不同)<br/>
web-garden和web-farm都是网络托管系统。

唯一的区别是web-garden是在单个服务器中包含许多处理器的设置，而web-farm是使用多个服务器的较大设置。

### 题9：[列举 Java 和 JavaScript 之间的区别？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题9列举-java-和-javascript-之间的区别)<br/>
Java是一门十分完整、成熟的编程语言。相比之下，JavaScript是一个可以被引入HTML页面的编程语言。这两种语言并不完全相互依赖，而是针对不同的意图而设计的。

Java是一种面向对象编程（OOPS）或结构化编程语言，类似的如C ++或C，而JavaScript是客户端脚本语言，它被称为非结构化编程。

### 题10：[decodeURI() 和 encodeURI() 是什么？](/docs/JavaScript/最新面试题2021年JavaScript面试题及答案汇总.md#题10decodeuri()-和-encodeuri()-是什么)<br/>
EncodeURl()用于将URL转换为十六进制编码。而DecodeURI()用于将编码的URL转换回正常。

### 题11：javascript-中的闭包是什么举个例子<br/>


### 题12：javascript-中如何创建通用对象<br/>


### 题13：解释-javascript-中的相等性<br/>


### 题14：如何在-javascript-中创建私有变量<br/>


### 题15：什么是-javascript-中的-unshift-方法<br/>


### 题16：什么是未声明和未定义的变量<br/>


### 题17：javascript-中的各种功能组件是什么<br/>


### 题18：匿名函数和命名函数有什么区别<br/>


### 题19：javascript-中的循环结构都有什么<br/>


### 题20：javascript-中的-null-和-undefined有什么区别<br/>


### 题21：什么是负无穷大<br/>


### 题22：javascript-中的强制转型是指什么<br/>


### 题23：javascript-中-break-和-continue-语句的作用<br/>


### 题24：javascript-中如何使用-dom<br/>


### 题25：说一说-==-和-===-之间有什么区别<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")