# 2021年常见 JavaScript 面试题附答案

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## JavaScript

### 题1：[JavaScript 中哪些关键字用于处理异常？](/docs/JavaScript/2021年常见%20JavaScript%20面试题附答案.md#题1javascript-中哪些关键字用于处理异常)<br/>
try... catch-finally用于处理JavaScript中的异常。

### 题2：[JavaScript 中 void(0) 如何使用？](/docs/JavaScript/2021年常见%20JavaScript%20面试题附答案.md#题2javascript-中-void(0)-如何使用)<br/>
void(0)用于防止页面刷新，并在调用时传递参数“zero”。

void(0)用于调用另一种方法而不刷新页面。

### 题3：[如何使用 JavaScript 提交表单？](/docs/JavaScript/2021年常见%20JavaScript%20面试题附答案.md#题3如何使用-javascript-提交表单)<br/>
要使用JavaScript提交表单，请使用

```javascript
document.form [0].submit();
```

### 题4：[什么是 JavaScript 中的 unshift 方法？](/docs/JavaScript/2021年常见%20JavaScript%20面试题附答案.md#题4什么是-javascript-中的-unshift-方法)<br/>
unshift方法就像在数组开头工作的push方法。该方法用于将一个或多个元素添加到数组的开头。

### 题5：[JavaScript 中如何使用事件处理程序？](/docs/JavaScript/2021年常见%20JavaScript%20面试题附答案.md#题5javascript-中如何使用事件处理程序)<br/>
事件是由用户生成活动（例如单击链接或填写表单）导致的操作。

需要一个事件处理程序来管理所有这些事件的正确执行。

事件处理程序是对象的额外属性。此属性包括事件的名称以及事件发生时采取的操作。

### 题6：[一个特定的框架如何使用 JavaScript 中的超链接定位？](/docs/JavaScript/2021年常见%20JavaScript%20面试题附答案.md#题6一个特定的框架如何使用-javascript-中的超链接定位)<br/>
可以通过使用“target”属性在超链接中包含所需帧的名称来实现。

```javascript
<a href="newpage.htm" target="newframe">New Page</a>
```

### 题7：[什么是全局变量？这些变量如何声明，使用全局变量有哪些问题？](/docs/JavaScript/2021年常见%20JavaScript%20面试题附答案.md#题7什么是全局变量这些变量如何声明使用全局变量有哪些问题)<br/>
全局变量是整个代码长度可用的变量，也就是说这些变量没有任何作用域。var关键字用于声明局部变量或对象。如果省略var关键字，则声明一个全局变量。

例：

```javascript
Declare a global globalVariable = "Test";
```

使用全局变量所面临的问题是本地和全局变量名称的冲突。此外，很难调试和测试依赖于全局变量的代码。

### 题8：[什么是 JavaScript Cookie？](/docs/JavaScript/2021年常见%20JavaScript%20面试题附答案.md#题8什么是-javascript-cookie)<br/>
Cookie是用来存储计算机中的小型测试文件，当用户访问网站以存储他们需要的信息时，它将被创建。

### 题9：[JavaScript 中读取和写入文件的方法是什么？](/docs/JavaScript/2021年常见%20JavaScript%20面试题附答案.md#题9javascript-中读取和写入文件的方法是什么)<br/>
可以通过使用JavaScript扩展（从JavaScript编辑器运行），打开文件的示例来完成：

```javascript
fh = fopen(getScriptPath(), 0);
```

### 题10：[JavaScript 中的作用域（scope）是指什么？](/docs/JavaScript/2021年常见%20JavaScript%20面试题附答案.md#题10javascript-中的作用域scope是指什么)<br/>
在JavaScript中，每个函数都有自己的作用域。作用域基本上是变量以及如何通过名称访问这些变量的规则的集合。只有函数中的代码才能访问函数作用域内的变量。

同一个作用域中的变量名必须是唯一的。一个作用域可以嵌套在另一个作用域内。如果一个作用域嵌套在另一个作用域内，最内部作用域内的代码可以访问另一个作用域的变量。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")