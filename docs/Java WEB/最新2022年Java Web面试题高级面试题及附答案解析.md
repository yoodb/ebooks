# 最新2022年Java Web面试题高级面试题及附答案解析

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java WEB

### 题1：[JSP 中 request 对象有什么作用？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题1jsp-中-request-对象有什么作用)<br/>
request对象代表了客户端的请求信息，主要用于接受通过HTTP协议传送到服务器的数据。（包括头信息、系统信息、请求方式以及请求参数等）。request对象的作用域为一次请求。

当Request对象获取客户提交的汉字字符时，会出现乱码问题，必须进行特殊处理。首先，将获取的字符串用ISO-8859－1进行编码，并将编码存发岛一个字节数组中，然后再将这个数组转化为字符串对象。

**Request常用方法**

getParameter(String strTextName)：获取表单提交的信息。

getProtocol()：获取客户使用的协议。

getServletPath()：获取客户提交信息的页面。

getMethod()：获取客户提交信息的方式。

getHeader()：获取HTTP头文件中的accept,accept-encoding和Host的值。

getRermoteAddr()：获取客户的IP地址。

getRemoteHost()：获取客户机的名称。

getServerName()：获取服务器名称。

getServerPort()：获取服务器的端口号。

getParameterNames()：获取客户端提交的所有参数的名字。

### 题2：[什么情况下调用 doGet() 和 doPost()？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题2什么情况下调用-doget()-和-dopost())<br/>
默认情况是调用doGet()方法，JSP页面中的Form表单的method属性设置为post的时候，调用的为doPost()方法；为get的时候，调用deGet()方法。

### 题3：[JSP 中 pageContext 对象有什么作用？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题3jsp-中-pagecontext-对象有什么作用)<br/>
pageContext对象的作用是取得任何范围的参数，通过它可以获取JSP页面的out、request、reponse、session、application等对象。

pageContext对象的创建和初始化都是由容器来完成的，在JSP页面中可以直接使用pageContext对象。

page对象代表JSP本身，只有在JSP页面内才是合法的。page隐含对象本质上包含当前Servlet接口引用的变量，类似于Java编程中的this指针。

### 题4：[JSP 中 session 对象有什么作用？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题4jsp-中-session-对象有什么作用)<br/>
Session对象是一个JSP内置对象，它在第一个JSP页面被装载时自动创建，完成会话期管理。从一个客户打开浏览器并连接到服务器开始，到客户关闭浏览器离开这个服务器结束，被称为一个会话。当一个客户访问一个服务器时，可能会在这个服务器的几个页面之间切换，服务器应当通过某种办法知道这是一个客户，就需要Session对象。

当一个客户首次访问服务器上的一个JSP页面时，JSP引擎产生一个Session对象，同时分配一个String类型的ID号，JSP引擎同时将这换个ID号发送到客户端，存放在Cookie中，这样Session对象，直到客户关闭浏览器后，服务器端该客户的Session对象才取消，并且和客户的会话对应关系消失。当客户重新打开浏览器再连接到该服务器时，服务器为该客户再创建一个新的Session对象。

session 对象是由服务器自动创建的与用户请求相关的对象。服务器为每个用户都生成一个session对象，用于保存该用户的信息，跟踪用户的操作状态。

session对象内部使用Map类来保存数据，因此保存数据的格式为 “Key/value”。 session对象的value可以使复杂的对象类型，而不仅仅局限于字符串类型。

public String getId()：获取Session对象编号。

public void setAttribute(String key,Object obj)：将参数Object指定的对象obj添加到Session对象中，并为添加的对象指定一个索引关键字。

public Object getAttribute(String key)：获取Session对象中含有关键字的对象。

public Boolean isNew()：判断是否是一个新的客户。

### 题5：[编写 Servlet 需要继承什么类？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题5编写-servlet-需要继承什么类)<br/>
编写Servlet需要继承HttpServlet类。

HttpServlet抽象类是继承自GenericServlet，而GenericServlet是实现了Servlet、ServletConfig、Serializable接口。

Servlet接口及其抽象类：
```java
public interface Servlet {

   public void init(ServletConfig config) throws ServletException;

   public ServletConfig getServletConfig();

   public void service(ServletRequest req, ServletResponse res )throws ServletException, IOException;

    public String getServletInfo();

    public void destroy();

}
```

```java
public abstract class GenericServlet implements Servlet, ServletConfig,java.io.Serializable {}

public abstract class HttpServlet extends GenericServlet {}
```

### 题6：[什么是 ServletContext？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题6什么是-servletcontext)<br/>
ServletContext是一个接口，它表示Servlet上下文对象一个工程或者一个模块只会有一个ServletContext对象实例，即意思是无论获取到多少个ServletContext对象其实就是一个对象，只是名字可能不一样。

ServletContext也可以理解成是一块所有客户都可以访问的共享的服务器端空间，类似Session。

### 题7：[JSP 内置对象都有什么作用？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题7jsp-内置对象都有什么作用)<br/>
1）Request：本质上就是HttpServletRequest，包含用户端请求的信息，就是请求对象

2）Response：本质上就是HttpServletResponse，包含服务器传回客户端的响应信息，就是响应对象

3）Session：是HttpSession，是一个会话对象，主要用于保存状态

4）Application：是servletContext，指的的整个web应用

5）Page：指整个jsp页面，类似this伪对象

6）PageContext：主要用于管理整个jsp页面

7）Exception：异常对象，jsp页面上的异常都会封装在这里面

8）Config：本质上就是servletConfig对象

9）Out：主要用于向客户端输出数据

### 题8：[说一说 Servlet 容器对 url 匹配过程？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题8说一说-servlet-容器对-url-匹配过程)<br/>
当一个请求发送到servlet容器的时候，容器先会将请求的url减去当前应用上下文的路径作为servlet的映射url，比如我访问的是http://localhost/test/aaa.html，我的应用上下文是test，容器会将http://localhost/test去掉，剩下的/aaa.html部分拿来做servlet的映射匹配。这个映射匹配过程是有顺序的，而且当有一个servlet匹配成功以后，就不会去理会剩下的servlet了（filter不同，后文会提到）。

匹配规则和顺序如下：

1）精确路径匹配。例子：比如servletA 的url-pattern为 /test，servletB的url-pattern为 /* ，这个时候，如果我访问的url为http://localhost/test ，这个时候容器就会先 进行精确路径匹配，发现/test正好被servletA精确匹配，那么就去调用servletA，也不会去理会其他的servlet了。

2）最长路径匹配。例子：servletA的url-pattern为/test/*，而servletB的url-pattern为/test/a/*，此时访问http://localhost/test/a时，容器会选择路径最长的servlet来匹配，也就是这里的servletB。

3）扩展匹配，如果url最后一段包含扩展，容器将会根据扩展选择合适的servlet。例子：servletA的url-pattern：*.action

4）如果前面三条规则都没有找到一个servlet，容器会根据url选择对应的请求资源。如果应用定义了一个default servlet，则容器会将请求丢给default servlet（什么是default servlet?后面会讲）。

根据这个规则表，就能很清楚的知道servlet的匹配过程，所以定义servlet的时候也要考虑url-pattern的写法，以免出错。

对于filter，不会像servlet那样只匹配一个servlet，因为filter的集合是一个链，所以只会有处理的顺序不同，而不会出现只选择一个filter。Filter的处理顺序和filter-mapping在web.xml中定义的顺序相同。

**url-pattern详解**

在web.xml文件中，以下语法用于定义映射：

>以”/’开头和以”/*”结尾的是用来做路径映射的。
以前缀”*.”开头的是用来做扩展映射的。
“/” 是用来定义default servlet映射的。
剩下的都是用来定义详细映射的。比如： /aa/bb/cc.action

所以，为什么定义”/*.action”这样一个看起来很正常的匹配会错?因为这个匹配即属于路径映射，也属于扩展映射，导致容器无法判断。


### 题9：[Servlet 是线程安全的吗？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题9servlet-是线程安全的吗)<br/>
Servlet不是线程安全的。

多线程并发的读写会导致数据不同步的问题。

解决的办法是尽量不要定义name属性，而是要把name变量分别定义在doGet()和doPost()方法内。

虽然使用synchronized(name){}语句块可以解决问题，但是会造成线程的等待，不是很科学的办法。

注意的是多线程的并发的读写Servlet类属性会导致数据不同步。但是如果只是并发地读取属性而不写入，则不存在数据不同步的问题。因此Servlet里的只读属性最好定义为final类型的。

### 题10：[JSP 中 <%…%> 和 <%!…%> 有什么区别？](/docs/Java%20WEB/最新2022年Java%20Web面试题高级面试题及附答案解析.md#题10jsp-中-<%…%>-和-<%!…%>-有什么区别)<br/>
<%…%>用于在JSP页面中嵌入Java脚本

<%!…%>用于在JSP页面中申明变量或方法，可以在该页面中的<%…%>脚本中调用，声明的变量相当于Servlet中的定义的成员变量。

### 题11：jsp-中内置对象映射表<br/>


### 题12：什么是-token<br/>


### 题13：doget-和-dopost-方法的两个参数是什么<br/>


### 题14：java-中如何获取-servletcontext-实例<br/>


### 题15：jsp-中-7-个动作指令和作用<br/>


### 题16：java-中-servletcontext-应用场景有哪些<br/>


### 题17：serlvet-中-init()-方法执行次数是多少<br/>


### 题18：servlet-是单例还是多例<br/>


### 题19：jsp-中-config-对象有什么作用<br/>


### 题20：jsp-中如何解决中文乱码问题<br/>


### 题21：jsp-中动态-include-和静态-include-有什么区别<br/>


### 题22：servlet-中如何获取-session-对象<br/>


### 题23：什么是-servlet<br/>


### 题24：jsp-中静态包含和动态包含有什么区别<br/>


### 题25：java-中-servlet-主要功能作用是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")