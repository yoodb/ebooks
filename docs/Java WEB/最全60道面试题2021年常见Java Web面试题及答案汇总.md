# 最全60道面试题2021年常见Java Web面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java WEB

### 题1：[Java 中如何获取 ServletContext 实例？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题1java-中如何获取-servletcontext-实例)<br/>
1、javax.servlet.Filter中直接获取

```java
ServletContext context = config.getServletContext();
```

2、HttpServlet中直接获取

```java
this.getServletContext()
```


3、在其他方法中通过HttpRequest获得
```java
request.getSession().getServletContext();
```

### 题2：[什么是 Servlet？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题2什么是-servlet)<br/>
Servlet是用Java编写的服务器端程序, 其主要功能在于交互式地浏览和修改数据，生成动态Web内容。

狭义的Servlet是指Java语言实现的一个接口，广义的Servlet是指任何实现了这个Servlet接口的类，一般情况下，我们一般将Servlet理解为后者。

### 题3：[JSP 中 config 对象有什么作用？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题3jsp-中-config-对象有什么作用)<br/>
config对象的主要作用是取得服务器的配置信息。

通过pageConext对象的getServletConfig()方法可以获取一个config对象。

当一个Servlet初始化时，容器把某些信息通过config对象传递给这个Servlet。

开发者可以在web.xml文件中为应用程序环境中的Servlet程序和JSP页面提供初始化参数。

### 题4：[JSP 中如何解决中文乱码问题？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题4jsp-中如何解决中文乱码问题)<br/>
1、JSP页面显示乱码。

```jsp
<%@ page contentType="text/html; charset=gb2312"%>
```
2、表单提交中文时出现乱码。

```java
request.seCharacterEncoding("gb2312")
```

对请求进行统一编码

3、数据库连接出现乱码

在数据库的数据库URL中加上，增加配置如下：

```xml
useUnicode=true&characterEncoding=GBK
```

4、通过过滤器完成。

5、在server.xml中的设置编码格式。

### 题5：[什么是 B/S 和 C/S？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题5什么是-b/s-和-c/s)<br/>
Browser/Server 浏览器/服务器（瘦客户端）

Custom/Server 客户端/服务器（胖客户端）

### 题6：[Serlvet 中 init() 方法执行次数是多少？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题6serlvet-中-init()-方法执行次数是多少)<br/>
在Servlet的生命周期中，该方法执行一次。

### 题7：[Servlet 中如何实现自动刷新（Refresh）？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题7servlet-中如何实现自动刷新refresh)<br/>
自动刷新不仅可以实现一段时间之后自动跳转到另一个页面，还可以实现一段时间之后自动刷新本页面。Servlet中通过HttpServletResponse对象设置Header属性实现自动刷新例如：

```java
Response.setHeader("Refresh","1000;URL=http://localhost:8080/servlet/example.htm");
```

其中1000为时间，单位为毫秒。URL指定就是要跳转的页面，如果设置自己的路径，就会实现没过一秒自动刷新本页面一次。

### 题8：[Servlet 执行时一般实现哪几个方法？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题8servlet-执行时一般实现哪几个方法)<br/>
init()方法在servlet的生命周期中仅执行一次，在服务器装载servlet时执行。缺省的init()方法通常是符合要求的，不过也可以根据需要进行 override，比如管理服务器端资源，一次性装入GIF图像，初始化数据库连接等，缺省的inti()方法设置了servlet的初始化参数，并用它的ServeltConfig对象参数来启动配置，所以覆盖init()方法时，应调用super.init()以确保仍然执行这些任务。
```java
public void init(ServletConfig config)
```

getServletConfig()方法返回一个servletConfig对象，该对象用来返回初始化参数和servletContext。servletContext接口提供有关servlet的环境信息。

```java
public ServletConfig getServletConfig()
```
getServletInfo()方法提供有关servlet的信息，如作者，版本，版权。

```java
public String getServletInfo()
```
service()方法是servlet的核心，在调用service()方法之前，应确保已完成init()方法。对于HttpServlet，每当客户请求一个HttpServlet对象，该对象的service()方法就要被调用，HttpServlet缺省的service()方法的服务功能就是调用与HTTP请求的方法相应的do功能，doPost()和doGet()，所以对于HttpServlet，一般都是重写doPost()和doGet()方法。

```java
public void service(ServletRequest request,ServletResponse response)
```

destroy()方法在servlet的生命周期中也仅执行一次，即在服务器停止卸载servlet时执行，把servlet作为服务器进程的一部分关闭。缺省的destroy()方法通常是符合要求的，但也可以override，比如在卸载servlet时将统计数字保存在文件中，或是关闭数据库连接。

```java
public void destroy()
```

### 题9：[什么情况下调用 doGet() 和 doPost()？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题9什么情况下调用-doget()-和-dopost())<br/>
默认情况是调用doGet()方法，JSP页面中的Form表单的method属性设置为post的时候，调用的为doPost()方法；为get的时候，调用deGet()方法。

### 题10：[说一说 Servlet 容器对 url 匹配过程？](/docs/Java%20WEB/最全60道面试题2021年常见Java%20Web面试题及答案汇总.md#题10说一说-servlet-容器对-url-匹配过程)<br/>
当一个请求发送到servlet容器的时候，容器先会将请求的url减去当前应用上下文的路径作为servlet的映射url，比如我访问的是http://localhost/test/aaa.html，我的应用上下文是test，容器会将http://localhost/test去掉，剩下的/aaa.html部分拿来做servlet的映射匹配。这个映射匹配过程是有顺序的，而且当有一个servlet匹配成功以后，就不会去理会剩下的servlet了（filter不同，后文会提到）。

匹配规则和顺序如下：

1）精确路径匹配。例子：比如servletA 的url-pattern为 /test，servletB的url-pattern为 /* ，这个时候，如果我访问的url为http://localhost/test ，这个时候容器就会先 进行精确路径匹配，发现/test正好被servletA精确匹配，那么就去调用servletA，也不会去理会其他的servlet了。

2）最长路径匹配。例子：servletA的url-pattern为/test/*，而servletB的url-pattern为/test/a/*，此时访问http://localhost/test/a时，容器会选择路径最长的servlet来匹配，也就是这里的servletB。

3）扩展匹配，如果url最后一段包含扩展，容器将会根据扩展选择合适的servlet。例子：servletA的url-pattern：*.action

4）如果前面三条规则都没有找到一个servlet，容器会根据url选择对应的请求资源。如果应用定义了一个default servlet，则容器会将请求丢给default servlet（什么是default servlet?后面会讲）。

根据这个规则表，就能很清楚的知道servlet的匹配过程，所以定义servlet的时候也要考虑url-pattern的写法，以免出错。

对于filter，不会像servlet那样只匹配一个servlet，因为filter的集合是一个链，所以只会有处理的顺序不同，而不会出现只选择一个filter。Filter的处理顺序和filter-mapping在web.xml中定义的顺序相同。

**url-pattern详解**

在web.xml文件中，以下语法用于定义映射：

>以”/’开头和以”/*”结尾的是用来做路径映射的。
以前缀”*.”开头的是用来做扩展映射的。
“/” 是用来定义default servlet映射的。
剩下的都是用来定义详细映射的。比如： /aa/bb/cc.action

所以，为什么定义”/*.action”这样一个看起来很正常的匹配会错?因为这个匹配即属于路径映射，也属于扩展映射，导致容器无法判断。


### 题11：java-中-servletcontext-的生命周期<br/>


### 题12：jsp-中-session-对象有什么作用<br/>


### 题13：jsp-中内置对象映射表<br/>


### 题14：什么是跨域<br/>


### 题15：如何保存会话状态有哪些方式区别<br/>


### 题16：session-和-token-有什么区别<br/>


### 题17：什么是-servlet-链<br/>


### 题18：什么是-web-service<br/>


### 题19：java-中有哪些会话跟踪技术作用域<br/>


### 题20：编写-servlet-通常需要重写哪两个方法<br/>


### 题21：servlet-中如何获取客户端机器的信息<br/>


### 题22：doget-和-dopost-方法的两个参数是什么<br/>


### 题23：什么是-token<br/>


### 题24：jsp-中-cookie-对象有什么作用<br/>


### 题25：如何发布一个-web-service-服务<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")