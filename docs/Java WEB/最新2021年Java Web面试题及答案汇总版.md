# 最新2021年Java Web面试题及答案汇总版

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java WEB

### 题1：[Servlet 中过滤器有什么作用？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题1servlet-中过滤器有什么作用)<br/>
Servlet监听器对特定的事件进行监听，当产生这些事件的时候，会执行监听器的代码。可以对应用的加载、卸载，对session的初始化、销毁，对session中值变化等事件进行监听。

```java
void doFilter(..) { 
    // do stuff before servlet gets called

    // invoke the servlet, or any other filters mapped to the target servlet
    chain.doFilter(..);

    // do stuff after the servlet finishes
}
```

### 题2：[session 和 token 有什么区别？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题2session-和-token-有什么区别)<br/>
session机制存在服务器压力增大，CSRF跨站伪造请求攻击，扩展性不强等问题；

session存储在服务器端，token存储在客户端；

token提供认证和授权功能，作为身份认证，token安全性比session好；

session这种会话存储方式方式只适用于客户端代码和服务端代码运行在同一台服务器上，token适用于项目级的前后端分离（前后端代码运行在不同的服务器下）。

### 题3：[Servlet 和 GCI 有什么区别？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题3servlet-和-gci-有什么区别)<br/>
1、Servlet是基于Java编写的，处于服务器进程中，他能够通过多线程方式运行service()方法，一个实例可以服务于多个请求，而且一般不会销毁；

2、CGI对每个请求都产生新的进程，服务完成后销毁，所以效率上低于Servlet。

### 题4：[谈谈 MVC 架构模式中的三个角色？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题4谈谈-mvc-架构模式中的三个角色)<br/>
**Model（模型端）**

Mod封装的是数据源和所有基于对这些数据的操作。在一个组件中，Model往往表示组件的状态和操作这些状态的方法，往往是一系列的公开方法。通过这些公开方法，便可以取得模型端的所有功能。
 
在这些公开方法中，有些是取值方法，让系统其他部分可以得到模型端的内部状态参数，其他的改值方法则允许外部修改模型端的内部状态。模型端还必须有方法登记视图，以便在模型端的内部状态发生变化时，可以通知视图端。我们可以自己定义一个Subject接口来提供登记和通知视图所需的接口或者继承 Java.util.Observable类，让父类完成这件事。

**多个View（视图端）**

View封装的是对数据源Model的一种显示。一个模型可以由多个视图，并且可以在需要的时候动态地登记上所需的视图。而一个视图理论上也可以同不同的模型关联起来。
 
在前言里提到了，MVC模式用到了合成模式，这是因为在视图端里，视图可以嵌套，比如说在一个JFrame组件里面，可以有菜单组件，很多按钮组件等。

**多个Controller（控制器端）**

封装的是外界作用于模型的操作。通常，这些操作会转发到模型上，并调用模型中相应的一个或者多个方法（这个方法就是前面在介绍模型的时候说的改值方法）。一般Controller在Model和View之间起到了沟通的作用，处理用户在View上的输入，并转发给Model来更改其状态值。这样 Model和View两者之间可以做到松散耦合，甚至可以彼此不知道对方，而由Controller连接起这两个部分。也在前言里提到，MVC用到了策略模式，这是因为View用一个特定的Controller的实例来实现一个特定的响应策略，更换不同的Controller，可以改变View对用户输入的响应。

MVC (Model-View-Controller) : 模型利用"观察者"让控制器和视图可以随最新的状态改变而更新。另一方面，视图和控制器则实现了"策略模式"。控制器是视图的行为; 视图内部使用"组合模"式来管理显示组件。

以下的MVC解释图很好的标示了这种模式：

模型使用观察者模式，以便观察者更新，同时保持两者之间的解耦。
控制器是视图的策略，视图可以使用不同的控制器实现，得到不同的行为。
视图使用组合模式实现用户界面，用户界面通常组合了嵌套的组件，像面板、框架和按钮。
这些模式携手合作，把MVC模式的三层解耦，这样可以保持设计干净又有弹性。

### 题5：[什么是 Cookie？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题5什么是-cookie)<br/>
Cookie是客户端保存用户信息的一种机制，用来记录用户的一些信息，它的过期时间可以任意设置，如果不主动清除，很长一段时间都能保留。

### 题6：[什么是 B/S 和 C/S？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题6什么是-b/s-和-c/s)<br/>
Browser/Server 浏览器/服务器（瘦客户端）

Custom/Server 客户端/服务器（胖客户端）

### 题7：[什么是跨域？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题7什么是跨域)<br/>
跨域是指浏览器不能执行其他网站的脚本。它是由浏览器的同源策略造成的，是浏览器对JavaScript实施的安全限制。

当一个请求url的协议、域名、端口三者之间任意一个与当前页面url不同即为跨域

|当前页面url|被请求页面url|是否跨域|原因|
|-|-|-|-|
|http://blog.yoodb.com/|http://blog.yoodb.com/index.html|否|同源（协议、域名、端口号相同）|
|http://blog.yoodb.com/|https://blog.yoodb.com/index.html|跨域|协议不同（http/https）|
|http://blog.yoodb.com/|http://www.baidu.com/|跨域|主域名不同（test/baidu）|
|http://blog.yoodb.com/|http://blog.test.com/|跨域|子域名不同（www/blog）|
|http://blog.yoodb.com:8080/|http://blog.yoodb.com:7001/|跨域|端口号不同（8080/7001）|


### 题8：[转发（Forward）和重定向（Redirect）有什么区别？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题8转发forward和重定向redirect有什么区别)<br/>
转发是服务器行为，重定向是客户端行为。

**转发（Forword）**

通过RequestDispatcher对象的forward（HttpServletRequest request,HttpServletResponse response）方法实现的。RequestDispatcher可以通过HttpServletRequest 的getRequestDispatcher()方法获得。例如下面的代码就是跳转到login_success.jsp页面。

```java
request.getRequestDispatcher("login_success.jsp").forward(request, response);
```

**重定向（Redirect）**

是利用服务器返回的状态吗来实现的。客户端浏览器请求服务器的时候，服务器会返回一个状态码。服务器通过HttpServletRequestResponse的setStatus(int status)方法设置状态码。如果服务器返回301或者302，则浏览器会到新的网址重新请求该资源。

**区别**

1、从地址栏显示来说

forward是服务器请求资源，服务器直接访问目标地址的URL，把那个URL的响应内容读取过来，然后把这些内容再发给浏览器。浏览器根本不知道服务器发送的内容从哪里来的，所以它的地址栏还是原来的地址。

redirect是服务端根据逻辑，发送一个状态码,告诉浏览器重新去请求那个地址，所以地址栏显示的是新的URL。

2、从数据共享来说

forward：转发页面和转发到的页面可以共享request里面的数据。
redirect：不能共享数据。

3、从运用地方来说

forward：一般用于用户登陆的时候，根据角色转发到相应的模块。
redirect：一般用于用户注销登陆时返回主页面和跳转到其它的网站等。

4、从效率来说

forward：高。
redirect：低。

### 题9：[JSP 中如何解决中文乱码问题？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题9jsp-中如何解决中文乱码问题)<br/>
1、JSP页面显示乱码。

```jsp
<%@ page contentType="text/html; charset=gb2312"%>
```
2、表单提交中文时出现乱码。

```java
request.seCharacterEncoding("gb2312")
```

对请求进行统一编码

3、数据库连接出现乱码

在数据库的数据库URL中加上，增加配置如下：

```xml
useUnicode=true&characterEncoding=GBK
```

4、通过过滤器完成。

5、在server.xml中的设置编码格式。

### 题10：[Servlet 是线程安全的吗？](/docs/Java%20WEB/最新2021年Java%20Web面试题及答案汇总版.md#题10servlet-是线程安全的吗)<br/>
Servlet不是线程安全的。

多线程并发的读写会导致数据不同步的问题。

解决的办法是尽量不要定义name属性，而是要把name变量分别定义在doGet()和doPost()方法内。

虽然使用synchronized(name){}语句块可以解决问题，但是会造成线程的等待，不是很科学的办法。

注意的是多线程的并发的读写Servlet类属性会导致数据不同步。但是如果只是并发地读取属性而不写入，则不存在数据不同步的问题。因此Servlet里的只读属性最好定义为final类型的。

### 题11：jsp-中-response-对象有什么作用<br/>


### 题12：什么是-web-service<br/>


### 题13：java-中-servlet-主要功能作用是什么<br/>


### 题14：jsp/servlet-中如何保证-browser-保存在-cache-中<br/>


### 题15：servlet-中如何获取-session-对象<br/>


### 题16：jsp-和-html之间有什么关系<br/>


### 题17：jsp-中隐含对象都有哪些<br/>


### 题18：servlet-是单例还是多例<br/>


### 题19：servlet-的生命周期有哪几个阶段<br/>


### 题20：如何发布一个-web-service-服务<br/>


### 题21：什么时候用-assert<br/>


### 题22：如何保存会话状态有哪些方式区别<br/>


### 题23：servlet-中如何实现自动刷新refresh<br/>


### 题24：session-和-cookie-有什么区别<br/>


### 题25：servlet-接口的层次结构<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")