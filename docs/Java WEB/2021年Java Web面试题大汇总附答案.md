# 2021年Java Web面试题大汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java WEB

### 题1：[如何读取 Servlet 初始化参数？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题1如何读取-servlet-初始化参数)<br/>
ServletConfig中定义了如下的方法用来读取初始化参数的信息：

```java
public String getInitParameter(String name)
```

参数：初始化参数的名称。

返回：初始化参数的值，如果没有配置，返回null。

### 题2：[什么是跨域？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题2什么是跨域)<br/>
跨域是指浏览器不能执行其他网站的脚本。它是由浏览器的同源策略造成的，是浏览器对JavaScript实施的安全限制。

当一个请求url的协议、域名、端口三者之间任意一个与当前页面url不同即为跨域

|当前页面url|被请求页面url|是否跨域|原因|
|-|-|-|-|
|http://blog.yoodb.com/|http://blog.yoodb.com/index.html|否|同源（协议、域名、端口号相同）|
|http://blog.yoodb.com/|https://blog.yoodb.com/index.html|跨域|协议不同（http/https）|
|http://blog.yoodb.com/|http://www.baidu.com/|跨域|主域名不同（test/baidu）|
|http://blog.yoodb.com/|http://blog.test.com/|跨域|子域名不同（www/blog）|
|http://blog.yoodb.com:8080/|http://blog.yoodb.com:7001/|跨域|端口号不同（8080/7001）|


### 题3：[Java 中 Request 对象都有哪些方法？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题3java-中-request-对象都有哪些方法)<br/>
setAttribute(String name,Object)：设置名字为name的request的参数值

getAttribute(String name)：返回由name指定的属性值 

getAttributeNames()：返回request对象所有属性的名字集合，结果是一个枚举的实例 

getCookies()：返回客户端的所有Cookie对象，结果是一个Cookie数组 

getCharacterEncoding()：返回请求中的字符编码方式 

getContentLength()：返回请求的Body的长度 getHeader(String name)：获得HTTP协议定义的文件头信息 

getHeaders(String name)：返回指定名字的request Header的所有值，结果是一个枚举的实例
 
getHeaderNames()：返回所以request Header的名字，结果是一个枚举的实例 

getInputStream()：返回请求的输入流，用于获得请求中的数据 

getMethod()：获得客户端向服务器端传送数据的方法 

getParameter(String name)：获得客户端传送给服务器端的有name指定的参数值 

getParameterNames()：获得客户端传送给服务器端的所有参数的名字，结果是一个枚举的实例
 
getParameterValues(String name)：获得有name指定的参数的所有值 

getProtocol()：获取客户端向服务器端传送数据所依据的协议名称 

getQueryString()：获得查询字符串 

getRequestURI()：获取发出请求字符串的客户端地址 

getRemoteAddr()：获取客户端的IP地址 

getRemoteHost()：获取客户端的名字 

getSession([Boolean create])：返回和请求相关Session 

getServerName()：获取服务器的名字 

getServletPath()：获取客户端所请求的脚本文件的路径 

getServerPort()：获取服务器的端口号 

removeAttribute(String name)：删除请求中的一个属性

### 题4：[什么是 ServletContext？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题4什么是-servletcontext)<br/>
ServletContext是一个接口，它表示Servlet上下文对象一个工程或者一个模块只会有一个ServletContext对象实例，即意思是无论获取到多少个ServletContext对象其实就是一个对象，只是名字可能不一样。

ServletContext也可以理解成是一块所有客户都可以访问的共享的服务器端空间，类似Session。

### 题5：[JSP 中动态 include 和静态 include 有什么区别？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题5jsp-中动态-include-和静态-include-有什么区别)<br/>
include指令用于把另一个页面包含到当前页面中，在转换成servlet时包含进去。

动态include用jsp:include动作实现 <jsp:include page="jingxuan.jsp" flush="true" />它总是会检查所含文件中的变化，适合用于包含动态页面，并且可以带参数。

静态include用include伪码实现,定不会检查所含文件的变化,适用于包含静态页面<%@ include file="jingxuan.htm" %>。


### 题6：[doGet 和 doPost 方法的两个参数是什么？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题6doget-和-dopost-方法的两个参数是什么)<br/>
doGet()和doPost()方法的两个参数为HttpServletRequest和HttpServletResponse对象。

HttpServletRequest：封装了与请求相关的信息。

HttpServletResponse：封装了与响应相关的信息。

### 题7：[ServletContext 接口包括哪些功能？分别用代码示例。](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题7servletcontext-接口包括哪些功能分别用代码示例。)<br/>
**1、获取web应用的初始化参数**

使用getInitParameterNames()和getInitParameter(String name)来获得web应用中的初始化参数。

```java
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	request.setCharacterEncoding("utf-8");
	response.setContentType("text/html;charset=utf-8");
	PrintWriter outPrintWriter = response.getWriter();
	ServletContext context = this.getServletContext();                  //定义一个ServletContext对象
	Enumeration<String> enumeration = context.getInitParameterNames();  //用集合得方式存储配置文件中所有的name
	while(enumeration.hasMoreElements()){                              //判断是否为空
		String nameString = enumeration.nextElement();                 //跨过头部  提取第一个name
		String passString = context.getInitParameter(nameString);    //用getInitParameter(name)提取value
		outPrintWriter.print(nameString+"  "+passString);            //输出
	}
}
``` 

**2、实现多个servlet的数据共享**

```java
setAttribute(String name,String value) 来设置共享的数据 
getAttribute(String name) 来获得共享得数据值
removeAttribute（String name） 删除
getAttributeNames()
```
写数据：ServletSet类

```java
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// TODO Auto-generated method stub
	ServletContext context = this.getServletContext();
	context.setAttribute("公众号：Java精选", "123456");
}
```

读数据：ServletGet类

```java
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// TODO Auto-generated method stub
	request.setCharacterEncoding("utf-8");
	response.setContentType("text/html;charset=utf-8");
	PrintWriter out = response.getWriter();
	ServletContext context = this.getServletContext();
	String pasString = (String) context.getAttribute("公众号：Java精选"); //要强制类型转换getAttribute的返回值为object
	out.print(pasString);
}
```

**3、获取web应用下的资源文件（配置文件或图片）**

```java
InputStream getResourceAsInputstream(String path)
String getRealPath(String path)
URL getResource(String path)
```
1）在src路径下创建配置文件创建文件source.properties（项目被发布后资源文件的路径发生改变）

2）获取资源文件定义的InoutStream and load()方法getProperty()方法

```java
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();
	ServletContext context = this.getServletContext();
	InputStream in = context.getResourceAsStream("D:/source.properties");
	Properties pros = new Properties();   //定义内容对象
	pros.load(in);     加载InputStream对象
	out.print(pros.getProperty("name"));  //使用getProperty函数输出文件里的哈希值对
	out.print(pros.getProperty("password"));
}
```

### 题8：[Serlvet 中 init() 方法执行次数是多少？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题8serlvet-中-init()-方法执行次数是多少)<br/>
在Servlet的生命周期中，该方法执行一次。

### 题9：[JSP 中 exception 对象有什么作用？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题9jsp-中-exception-对象有什么作用)<br/>
exception对象的作用是显示异常信息，只有在包含 isErrorPage="true"的页面中才可以被使用，在一般的JSP页面中使用该对象将无法编译JSP文件。

excepation对象和Java的所有对象一样，都具有系统提供的继承 结构。

exception对象几乎定义了所有异常情况。在Java程序中，可以使用try/catch关键字来处理异常情况； 如果在JSP页面中出现没有捕获到的异常，就会生成exception对象，并把exception对象传送到在page指令中设定的错误页面中，然后在错误页面中处理相应的exception对象。


### 题10：[什么是 Cookie？](/docs/Java%20WEB/2021年Java%20Web面试题大汇总附答案.md#题10什么是-cookie)<br/>
Cookie是客户端保存用户信息的一种机制，用来记录用户的一些信息，它的过期时间可以任意设置，如果不主动清除，很长一段时间都能保留。

### 题11：如何请求一个-web-service-服务<br/>


### 题12：jsp-中内置对象映射表<br/>


### 题13：java-中-servletcontext-应用场景有哪些<br/>


### 题14：如何实现-servlet-单线程模式<br/>


### 题15：什么时候用-assert<br/>


### 题16：说一说-servlet-容器对-url-匹配过程<br/>


### 题17：servlet-中如何获取-session-对象<br/>


### 题18：web-service-的核心组成包括哪些内容<br/>


### 题19：什么是-web-service<br/>


### 题20：jsp/servlet-中如何保证-browser-保存在-cache-中<br/>


### 题21：jsp-中-response-对象有什么作用<br/>


### 题22：javaweb-中四大域对象及作用范围<br/>


### 题23：转发forward和重定向redirect有什么区别<br/>


### 题24：什么是-servlet-链<br/>


### 题25：servlet-中如何返回响应信息<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")