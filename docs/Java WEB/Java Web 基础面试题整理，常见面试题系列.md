# Java Web 基础面试题整理，常见面试题系列

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java WEB

### 题1：[Java 中如何获取 ServletContext 实例？](/docs/Java%20WEB/Java%20Web%20基础面试题整理，常见面试题系列.md#题1java-中如何获取-servletcontext-实例)<br/>
1、javax.servlet.Filter中直接获取

```java
ServletContext context = config.getServletContext();
```

2、HttpServlet中直接获取

```java
this.getServletContext()
```


3、在其他方法中通过HttpRequest获得
```java
request.getSession().getServletContext();
```

### 题2：[谈谈 MVC 架构模式中的三个角色？](/docs/Java%20WEB/Java%20Web%20基础面试题整理，常见面试题系列.md#题2谈谈-mvc-架构模式中的三个角色)<br/>
**Model（模型端）**

Mod封装的是数据源和所有基于对这些数据的操作。在一个组件中，Model往往表示组件的状态和操作这些状态的方法，往往是一系列的公开方法。通过这些公开方法，便可以取得模型端的所有功能。
 
在这些公开方法中，有些是取值方法，让系统其他部分可以得到模型端的内部状态参数，其他的改值方法则允许外部修改模型端的内部状态。模型端还必须有方法登记视图，以便在模型端的内部状态发生变化时，可以通知视图端。我们可以自己定义一个Subject接口来提供登记和通知视图所需的接口或者继承 Java.util.Observable类，让父类完成这件事。

**多个View（视图端）**

View封装的是对数据源Model的一种显示。一个模型可以由多个视图，并且可以在需要的时候动态地登记上所需的视图。而一个视图理论上也可以同不同的模型关联起来。
 
在前言里提到了，MVC模式用到了合成模式，这是因为在视图端里，视图可以嵌套，比如说在一个JFrame组件里面，可以有菜单组件，很多按钮组件等。

**多个Controller（控制器端）**

封装的是外界作用于模型的操作。通常，这些操作会转发到模型上，并调用模型中相应的一个或者多个方法（这个方法就是前面在介绍模型的时候说的改值方法）。一般Controller在Model和View之间起到了沟通的作用，处理用户在View上的输入，并转发给Model来更改其状态值。这样 Model和View两者之间可以做到松散耦合，甚至可以彼此不知道对方，而由Controller连接起这两个部分。也在前言里提到，MVC用到了策略模式，这是因为View用一个特定的Controller的实例来实现一个特定的响应策略，更换不同的Controller，可以改变View对用户输入的响应。

MVC (Model-View-Controller) : 模型利用"观察者"让控制器和视图可以随最新的状态改变而更新。另一方面，视图和控制器则实现了"策略模式"。控制器是视图的行为; 视图内部使用"组合模"式来管理显示组件。

以下的MVC解释图很好的标示了这种模式：

模型使用观察者模式，以便观察者更新，同时保持两者之间的解耦。
控制器是视图的策略，视图可以使用不同的控制器实现，得到不同的行为。
视图使用组合模式实现用户界面，用户界面通常组合了嵌套的组件，像面板、框架和按钮。
这些模式携手合作，把MVC模式的三层解耦，这样可以保持设计干净又有弹性。

### 题3：[Servlet 中如何实现自动刷新（Refresh）？](/docs/Java%20WEB/Java%20Web%20基础面试题整理，常见面试题系列.md#题3servlet-中如何实现自动刷新refresh)<br/>
自动刷新不仅可以实现一段时间之后自动跳转到另一个页面，还可以实现一段时间之后自动刷新本页面。Servlet中通过HttpServletResponse对象设置Header属性实现自动刷新例如：

```java
Response.setHeader("Refresh","1000;URL=http://localhost:8080/servlet/example.htm");
```

其中1000为时间，单位为毫秒。URL指定就是要跳转的页面，如果设置自己的路径，就会实现没过一秒自动刷新本页面一次。

### 题4：[Servlet 接口的层次结构？](/docs/Java%20WEB/Java%20Web%20基础面试题整理，常见面试题系列.md#题4servlet-接口的层次结构)<br/>
Servlet->genericServlet->HttpServlet->自定义servlet

GenericServlet实现Servlet接口，同时为它的子类屏蔽了不常用的方法，子类只需要重写service方法即可。

HttpServlet继承GenericServlet，根据请求类型进行分发处理，GET进入doGet方法，POST进入doPost方法。

开发者自定义的Servlet类只需要继承HttpServlet即可，重新doGet和doPost。

### 题5：[Java 中自定义标签要继承哪个类？](/docs/Java%20WEB/Java%20Web%20基础面试题整理，常见面试题系列.md#题5java-中自定义标签要继承哪个类)<br/>
继承TagSupport或者BodyTagSupport。

两者的差别是前者适用于没有主体的标签，而后者适用于有主体的标签。

继承TagSupport，可以实现doStartTag和doEndTag两个方法实现Tag的功能

继承BodyTagSupport，可以实现doAfterBody这个方法。

### 题6：[Servlet 和 JSP 有什么区别？](/docs/Java%20WEB/Java%20Web%20基础面试题整理，常见面试题系列.md#题6servlet-和-jsp-有什么区别)<br/>
Servlet是服务器端的程序，动态生成html页面发送到客户端，但是这样程序里会有很多out.println()。

java与html语言混在一起很乱，所以后来sun公司推出了JSP。

其实JSP就是Servlet，每次运行的时候JSP都首先被编译成servlet文件，然后再被编译成.class文件运行。

有了jsp，在MVC项目中servlet不再负责动态生成页面，转而去负责控制程序逻辑的作用，控制jsp与javabean之间的流转。

### 题7：[Serlvet 中 init() 方法执行次数是多少？](/docs/Java%20WEB/Java%20Web%20基础面试题整理，常见面试题系列.md#题7serlvet-中-init()-方法执行次数是多少)<br/>
在Servlet的生命周期中，该方法执行一次。

### 题8：[session 和 token 有什么区别？](/docs/Java%20WEB/Java%20Web%20基础面试题整理，常见面试题系列.md#题8session-和-token-有什么区别)<br/>
session机制存在服务器压力增大，CSRF跨站伪造请求攻击，扩展性不强等问题；

session存储在服务器端，token存储在客户端；

token提供认证和授权功能，作为身份认证，token安全性比session好；

session这种会话存储方式方式只适用于客户端代码和服务端代码运行在同一台服务器上，token适用于项目级的前后端分离（前后端代码运行在不同的服务器下）。

### 题9：[Java 中 Request 对象都有哪些方法？](/docs/Java%20WEB/Java%20Web%20基础面试题整理，常见面试题系列.md#题9java-中-request-对象都有哪些方法)<br/>
setAttribute(String name,Object)：设置名字为name的request的参数值

getAttribute(String name)：返回由name指定的属性值 

getAttributeNames()：返回request对象所有属性的名字集合，结果是一个枚举的实例 

getCookies()：返回客户端的所有Cookie对象，结果是一个Cookie数组 

getCharacterEncoding()：返回请求中的字符编码方式 

getContentLength()：返回请求的Body的长度 getHeader(String name)：获得HTTP协议定义的文件头信息 

getHeaders(String name)：返回指定名字的request Header的所有值，结果是一个枚举的实例
 
getHeaderNames()：返回所以request Header的名字，结果是一个枚举的实例 

getInputStream()：返回请求的输入流，用于获得请求中的数据 

getMethod()：获得客户端向服务器端传送数据的方法 

getParameter(String name)：获得客户端传送给服务器端的有name指定的参数值 

getParameterNames()：获得客户端传送给服务器端的所有参数的名字，结果是一个枚举的实例
 
getParameterValues(String name)：获得有name指定的参数的所有值 

getProtocol()：获取客户端向服务器端传送数据所依据的协议名称 

getQueryString()：获得查询字符串 

getRequestURI()：获取发出请求字符串的客户端地址 

getRemoteAddr()：获取客户端的IP地址 

getRemoteHost()：获取客户端的名字 

getSession([Boolean create])：返回和请求相关Session 

getServerName()：获取服务器的名字 

getServletPath()：获取客户端所请求的脚本文件的路径 

getServerPort()：获取服务器的端口号 

removeAttribute(String name)：删除请求中的一个属性

### 题10：[JavaWeb 中四大域对象及作用范围？](/docs/Java%20WEB/Java%20Web%20基础面试题整理，常见面试题系列.md#题10javaweb-中四大域对象及作用范围)<br/>
PageContext：作用范围在整个页面（一个页面）。

HttpRequest：作用范围在一次请求。

HttpSession：这是JavaWeb的一种会话机制，作用在整个会话中。

ServletContext：作用范围在整个web应用。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")