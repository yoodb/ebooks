# 最新Java Web面试题及答案附答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java WEB

### 题1：[get 和 post 请求有什么区别？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题1get-和-post-请求有什么区别)<br/>
1、get一般用于从服务器上获取数据，而post一般用来向服务器传送数据；

2、get将表单中数据按照variable=value的形式，添加到action所指向的URL后面，并且两者用"？"连接，变量之间用"&"连接；而post是将表单中的数据放在form的数据体中，按照变量与值对应的方式，传递到action所指定的URL。

3、get是不安全的，因为在传输过程中，数据是被放在请求的URL中，而post的所有操作对用户来说都是不可见的。

4、get传输的数据量小，这主要应为受url长度限制，而post可以传输大量的数据，所有上传文件只能用post提交。

5、get限制form表单的数据集必须为ASCII字符，而post支持整个IS01 0646字符集。

6、get是form表单的默认方法。

### 题2：[JSP 中 pageContext 对象有什么作用？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题2jsp-中-pagecontext-对象有什么作用)<br/>
pageContext对象的作用是取得任何范围的参数，通过它可以获取JSP页面的out、request、reponse、session、application等对象。

pageContext对象的创建和初始化都是由容器来完成的，在JSP页面中可以直接使用pageContext对象。

page对象代表JSP本身，只有在JSP页面内才是合法的。page隐含对象本质上包含当前Servlet接口引用的变量，类似于Java编程中的this指针。

### 题3：[编写 Servlet 需要继承什么类？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题3编写-servlet-需要继承什么类)<br/>
编写Servlet需要继承HttpServlet类。

HttpServlet抽象类是继承自GenericServlet，而GenericServlet是实现了Servlet、ServletConfig、Serializable接口。

Servlet接口及其抽象类：
```java
public interface Servlet {

   public void init(ServletConfig config) throws ServletException;

   public ServletConfig getServletConfig();

   public void service(ServletRequest req, ServletResponse res )throws ServletException, IOException;

    public String getServletInfo();

    public void destroy();

}
```

```java
public abstract class GenericServlet implements Servlet, ServletConfig,java.io.Serializable {}

public abstract class HttpServlet extends GenericServlet {}
```

### 题4：[JSP 中 7 个动作指令和作用？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题4jsp-中-7-个动作指令和作用)<br/>
jsp:forward-执行页面转向，把请求转发到下一个页面；

jsp:param-用于传递参数，必须与其他支持参数的标签一起使用；

jsp:include-用于动态引入一个JSP页面；

jsp:plugin-用于下载JavaBean或Applet到客户端执行；

jsp:useBean-寻求或者实例化一个JavaBean；

jsp:setProperty-设置JavaBean的属性值；

jsp:getProperty-获取JavaBean的属性值。

### 题5：[Servlet 执行时一般实现哪几个方法？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题5servlet-执行时一般实现哪几个方法)<br/>
init()方法在servlet的生命周期中仅执行一次，在服务器装载servlet时执行。缺省的init()方法通常是符合要求的，不过也可以根据需要进行 override，比如管理服务器端资源，一次性装入GIF图像，初始化数据库连接等，缺省的inti()方法设置了servlet的初始化参数，并用它的ServeltConfig对象参数来启动配置，所以覆盖init()方法时，应调用super.init()以确保仍然执行这些任务。
```java
public void init(ServletConfig config)
```

getServletConfig()方法返回一个servletConfig对象，该对象用来返回初始化参数和servletContext。servletContext接口提供有关servlet的环境信息。

```java
public ServletConfig getServletConfig()
```
getServletInfo()方法提供有关servlet的信息，如作者，版本，版权。

```java
public String getServletInfo()
```
service()方法是servlet的核心，在调用service()方法之前，应确保已完成init()方法。对于HttpServlet，每当客户请求一个HttpServlet对象，该对象的service()方法就要被调用，HttpServlet缺省的service()方法的服务功能就是调用与HTTP请求的方法相应的do功能，doPost()和doGet()，所以对于HttpServlet，一般都是重写doPost()和doGet()方法。

```java
public void service(ServletRequest request,ServletResponse response)
```

destroy()方法在servlet的生命周期中也仅执行一次，即在服务器停止卸载servlet时执行，把servlet作为服务器进程的一部分关闭。缺省的destroy()方法通常是符合要求的，但也可以override，比如在卸载servlet时将统计数字保存在文件中，或是关闭数据库连接。

```java
public void destroy()
```

### 题6：[Servlet 如何获取传递的参数信息？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题6servlet-如何获取传递的参数信息)<br/>
HttpServletRequest对象的getParameter()方法和getParameterValues()方法。

getParameter()方法用于获取单值表单元素的值，而getParameterValues()方法用于获取多值的情况，典型的复选框。

getParameter()方法返回的是一个字符串，而getParameterValues()方法返回的是字符串数组。

如果参数指定的表单元素不存在，返回null。

### 题7：[什么是 Token？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题7什么是-token)<br/>
Token的引入：Token是在客户端频繁向服务端请求数据，服务端频繁的去数据库查询用户名和密码并进行对比，判断用户名和密码正确与否，并作出相应提示，在这样的背景下，Token便应运而生。

Token的定义：Token是服务端生成的一串字符串，以作客户端进行请求的一个令牌，当第一次登录后，服务器生成一个Token便将此Token返回给客户端，以后客户端只需带上这个Token前来请求数据即可，无需再次带上用户名和密码。

使用Token的目的：Token的目的是为了减轻服务器的压力，减少频繁的查询数据库，使服务器更加健壮。

Token 是在服务端产生的。如果前端使用用户名/密码向服务端请求认证，服务端认证成功，那么在服务端会返回 Token 给前端。前端可以在每次请求的时候带上 Token 证明自己的合法地位。

### 题8：[Session 和 Cookie 有什么区别？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题8session-和-cookie-有什么区别)<br/>
Session和Cookie都是一种会话技术。

**Session**

数据存放在服务端，安全（只存放和状态相关的）。

session不仅仅是存放字符串，还可以存放对象。

session是域对象（session本身是不能跨域的，但可以通过相应技术来解决）。

sessionID的传输默认是需要cookie支持的。


**Cookie**

数据存放在客户端，不安全（存放的数据一定是和安全无关紧要的数据）。

cookie只能存放字符串，不能存放对象。

cookie不是域对象（Cookie是支持跨域的）。

### 题9：[JSP 中 request 对象有什么作用？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题9jsp-中-request-对象有什么作用)<br/>
request对象代表了客户端的请求信息，主要用于接受通过HTTP协议传送到服务器的数据。（包括头信息、系统信息、请求方式以及请求参数等）。request对象的作用域为一次请求。

当Request对象获取客户提交的汉字字符时，会出现乱码问题，必须进行特殊处理。首先，将获取的字符串用ISO-8859－1进行编码，并将编码存发岛一个字节数组中，然后再将这个数组转化为字符串对象。

**Request常用方法**

getParameter(String strTextName)：获取表单提交的信息。

getProtocol()：获取客户使用的协议。

getServletPath()：获取客户提交信息的页面。

getMethod()：获取客户提交信息的方式。

getHeader()：获取HTTP头文件中的accept,accept-encoding和Host的值。

getRermoteAddr()：获取客户的IP地址。

getRemoteHost()：获取客户机的名称。

getServerName()：获取服务器名称。

getServerPort()：获取服务器的端口号。

getParameterNames()：获取客户端提交的所有参数的名字。

### 题10：[说一说 Servlet 容器对 url 匹配过程？](/docs/Java%20WEB/最新Java%20Web面试题及答案附答案汇总.md#题10说一说-servlet-容器对-url-匹配过程)<br/>
当一个请求发送到servlet容器的时候，容器先会将请求的url减去当前应用上下文的路径作为servlet的映射url，比如我访问的是http://localhost/test/aaa.html，我的应用上下文是test，容器会将http://localhost/test去掉，剩下的/aaa.html部分拿来做servlet的映射匹配。这个映射匹配过程是有顺序的，而且当有一个servlet匹配成功以后，就不会去理会剩下的servlet了（filter不同，后文会提到）。

匹配规则和顺序如下：

1）精确路径匹配。例子：比如servletA 的url-pattern为 /test，servletB的url-pattern为 /* ，这个时候，如果我访问的url为http://localhost/test ，这个时候容器就会先 进行精确路径匹配，发现/test正好被servletA精确匹配，那么就去调用servletA，也不会去理会其他的servlet了。

2）最长路径匹配。例子：servletA的url-pattern为/test/*，而servletB的url-pattern为/test/a/*，此时访问http://localhost/test/a时，容器会选择路径最长的servlet来匹配，也就是这里的servletB。

3）扩展匹配，如果url最后一段包含扩展，容器将会根据扩展选择合适的servlet。例子：servletA的url-pattern：*.action

4）如果前面三条规则都没有找到一个servlet，容器会根据url选择对应的请求资源。如果应用定义了一个default servlet，则容器会将请求丢给default servlet（什么是default servlet?后面会讲）。

根据这个规则表，就能很清楚的知道servlet的匹配过程，所以定义servlet的时候也要考虑url-pattern的写法，以免出错。

对于filter，不会像servlet那样只匹配一个servlet，因为filter的集合是一个链，所以只会有处理的顺序不同，而不会出现只选择一个filter。Filter的处理顺序和filter-mapping在web.xml中定义的顺序相同。

**url-pattern详解**

在web.xml文件中，以下语法用于定义映射：

>以”/’开头和以”/*”结尾的是用来做路径映射的。
以前缀”*.”开头的是用来做扩展映射的。
“/” 是用来定义default servlet映射的。
剩下的都是用来定义详细映射的。比如： /aa/bb/cc.action

所以，为什么定义”/*.action”这样一个看起来很正常的匹配会错?因为这个匹配即属于路径映射，也属于扩展映射，导致容器无法判断。


### 题11：b/s-和-c/s-有什么联系与区别<br/>


### 题12：jsp-和-html之间有什么关系<br/>


### 题13：servlet-中如何返回响应信息<br/>


### 题14：如何请求一个-web-service-服务<br/>


### 题15：jsp-中如何解决中文乱码问题<br/>


### 题16：什么是-servlet<br/>


### 题17：什么是-web-service<br/>


### 题18：转发forward和重定向redirect有什么区别<br/>


### 题19：什么是跨域<br/>


### 题20：为什么要使用-servlet<br/>


### 题21：java-中-servletcontext-的生命周期<br/>


### 题22：jsp-中-config-对象有什么作用<br/>


### 题23：什么是-servletcontext<br/>


### 题24：jsp-中动态-include-和静态-include-有什么区别<br/>


### 题25：servlet-中如何实现自动刷新refresh<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")