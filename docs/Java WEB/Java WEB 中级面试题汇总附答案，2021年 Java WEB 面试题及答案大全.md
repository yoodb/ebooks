# Java WEB 中级面试题汇总附答案，2021年 Java WEB 面试题及答案大全

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java WEB

### 题1：[如何保存会话状态？有哪些方式、区别？](/docs/Java%20WEB/Java%20WEB%20中级面试题汇总附答案，2021年%20Java%20WEB%20面试题及答案大全.md#题1如何保存会话状态有哪些方式区别)<br/>
1、cookie保存在客户端，容易被篡改；

2、session保存在服务端，连接较大的话会造成服务端压力过大，分布式的情况下可以存储子数据库中。

**优点：**

1）简单且高性能；

2）支持分布式与集群；

3）支持服务器断电和重启；

4）支持 tomcat、jetty 等运行容器重启。

**缺点：**

1）需要检查和维护session过期，手动维护cookie；

2）不能有频繁的session数据存取。

3、token多终端或者app应用，推荐使用token

随着互联网技术的发展，分布式web应用的普及，通过session管理用户登录状态成本越来越高，因此慢慢发展成为token的方式做登录身份校验，通过token去取redis中缓存的用户信息。之后jwt的出现，校验方式更加简单便捷化，无需通过redis缓存，而是直接根据token取出保存的用户信息，以及对token可用性校验，单点登录更为简单。

JWT的token包含三部分数据：

Header：头部，通常头部有两部分信息：1）声明类型，这里是JWT；2）加密算法，自定义。一般对头部进行base64加密（可解密），得到第一部分数据。

Payload：载荷，就是有效数据，一般包含下面信息：用户身份信息（需注意的是，这里因采用base64加密，可解密，因此不要存放敏感信息）。

注册声明：如token的签发时间，过期时间，签发人等，此部分采用base64加密，得到第二部分数据。

Signature：签名，是整个数据的认证信息。一般根据前两步的数据， 再加上服务的密钥（secret）。（保密性，周期性更换），通过加密算法生成。用于验证整个数据完整和可靠性（保密性，周期性更换），通过加密算法生成。用于验证整个数据完整和可靠性。

### 题2：[谈谈 MVC 架构模式中的三个角色？](/docs/Java%20WEB/Java%20WEB%20中级面试题汇总附答案，2021年%20Java%20WEB%20面试题及答案大全.md#题2谈谈-mvc-架构模式中的三个角色)<br/>
**Model（模型端）**

Mod封装的是数据源和所有基于对这些数据的操作。在一个组件中，Model往往表示组件的状态和操作这些状态的方法，往往是一系列的公开方法。通过这些公开方法，便可以取得模型端的所有功能。
 
在这些公开方法中，有些是取值方法，让系统其他部分可以得到模型端的内部状态参数，其他的改值方法则允许外部修改模型端的内部状态。模型端还必须有方法登记视图，以便在模型端的内部状态发生变化时，可以通知视图端。我们可以自己定义一个Subject接口来提供登记和通知视图所需的接口或者继承 Java.util.Observable类，让父类完成这件事。

**多个View（视图端）**

View封装的是对数据源Model的一种显示。一个模型可以由多个视图，并且可以在需要的时候动态地登记上所需的视图。而一个视图理论上也可以同不同的模型关联起来。
 
在前言里提到了，MVC模式用到了合成模式，这是因为在视图端里，视图可以嵌套，比如说在一个JFrame组件里面，可以有菜单组件，很多按钮组件等。

**多个Controller（控制器端）**

封装的是外界作用于模型的操作。通常，这些操作会转发到模型上，并调用模型中相应的一个或者多个方法（这个方法就是前面在介绍模型的时候说的改值方法）。一般Controller在Model和View之间起到了沟通的作用，处理用户在View上的输入，并转发给Model来更改其状态值。这样 Model和View两者之间可以做到松散耦合，甚至可以彼此不知道对方，而由Controller连接起这两个部分。也在前言里提到，MVC用到了策略模式，这是因为View用一个特定的Controller的实例来实现一个特定的响应策略，更换不同的Controller，可以改变View对用户输入的响应。

MVC (Model-View-Controller) : 模型利用"观察者"让控制器和视图可以随最新的状态改变而更新。另一方面，视图和控制器则实现了"策略模式"。控制器是视图的行为; 视图内部使用"组合模"式来管理显示组件。

以下的MVC解释图很好的标示了这种模式：

模型使用观察者模式，以便观察者更新，同时保持两者之间的解耦。
控制器是视图的策略，视图可以使用不同的控制器实现，得到不同的行为。
视图使用组合模式实现用户界面，用户界面通常组合了嵌套的组件，像面板、框架和按钮。
这些模式携手合作，把MVC模式的三层解耦，这样可以保持设计干净又有弹性。

### 题3：[Web Service 中有哪些常用开发框架？](/docs/Java%20WEB/Java%20WEB%20中级面试题汇总附答案，2021年%20Java%20WEB%20面试题及答案大全.md#题3web-service-中有哪些常用开发框架)<br/>
Apache Axis1、Apache Axis2、Codehaus XFire、Apache CXF、Apache Wink、Jboss RESTEasy、sun JAX-WS（最简单、方便）、阿里巴巴Dubbo等。

### 题4：[如何请求一个 Web Service 服务？](/docs/Java%20WEB/Java%20WEB%20中级面试题汇总附答案，2021年%20Java%20WEB%20面试题及答案大全.md#题4如何请求一个-web-service-服务)<br/>
1、根据wsdl文档生成客户端代码

使用JDK中wsimport生成Web Service客户端Java类，命令“jdk wsimport -keep wsdl路径”。

使用apache-cxf中cxf生成We bService客户端Java类，命令“cxf wsdl2java wsdl路径”。

2、根据生成的代码调用Web Service

找到wsdl文档中service标签的name属性对应的类，找到这个port标签的name属性，调用该方法即可。

### 题5：[如何发布一个 Web Service 服务？](/docs/Java%20WEB/Java%20WEB%20中级面试题汇总附答案，2021年%20Java%20WEB%20面试题及答案大全.md#题5如何发布一个-web-service-服务)<br/>
1、定义SEI（接口）使用@webservice（类）和@webMethod（暴露的方法）注解。

2、定义SEI的实现。

3、发布Endpoint.publish（url,new SEI的实现对象）。

### 题6：[Web Service 中 SEI 指什么？](/docs/Java%20WEB/Java%20WEB%20中级面试题汇总附答案，2021年%20Java%20WEB%20面试题及答案大全.md#题6web-service-中-sei-指什么)<br/>
SEI全称是WebService EndPoint Interface，即Web Service服务器端用来处理请求的接口。

### 题7：[Web Service 的核心组成包括哪些内容？](/docs/Java%20WEB/Java%20WEB%20中级面试题汇总附答案，2021年%20Java%20WEB%20面试题及答案大全.md#题7web-service-的核心组成包括哪些内容)<br/>
1、soap：简单对象访问协议，它是轻型协议，用于分散的、分布式计算环境中交换信息。SOAP有助于以独立于平台的方式访问对象、服务和服务器。它借助于XML，提供了HTTP所需的扩展，即http+xml。

2、XML+XSD：Web Service平台中表示数据的格式是XML，XML解决了数据表示的问题，但它没有定义一套标准的数据类型，更没有说怎么去扩展这套数据类型，而XSD就是专门解决这个问题的一套标准。它定义了一套标准的数据类型，并给出了一种语言来扩展这套数据类型。

3、wsdl：基于XML用于描述Web Service及其函数、参数和返回值的文件。

Web Service服务器端通过一个WSDL文件来说明自身对外提供什么服务，该服务包括什么方法、参数、返回值等。WSDL文件保存在Web服务器上，通过一个url地址就可以访问到它。客户端要调用一个Web Service服务之前，首先要知道该服务的WSDL文件的地址。

Web Service服务的WSDL文件地址可以通过两种方式来暴露：

1）注册到UDDI服务器，以便被人查找；

2）直接告诉给客户端调用者。

4、uddi:它是目录服务，通过该服务可以注册和发布web Servcie，以便第三方的调用者统一调用。

### 题8：[什么是 Web Service？](/docs/Java%20WEB/Java%20WEB%20中级面试题汇总附答案，2021年%20Java%20WEB%20面试题及答案大全.md#题8什么是-web-service)<br/>
Web Service是一个SOA（面向服务的编程）的架构，它是不依赖于语言，不依赖于平台，可以实现不同的语言间的相互调用，通过Internet进行基于Http协议的网络应用间的交互。

一句话概括：Web Service是一种跨编程语言和跨操作系统平台的远程调用技术。

Web Service实现不同语言间的调用，是依托于一个标准，Web service是需要遵守WSDL（web服务定义语言）/SOAP（简单请求协议）规范的。 

Web Service = WSDL+SOAP+UDDI（webservice的注册）Soap是由Soap的part和0个或多个附件组成，一般只有part，在part中有Envelope和Body。

Web Service是通过提供标准的协议和接口，可以让不同的程序集成的一种SOA架构。



### 题9：[session 和 token 有什么区别？](/docs/Java%20WEB/Java%20WEB%20中级面试题汇总附答案，2021年%20Java%20WEB%20面试题及答案大全.md#题9session-和-token-有什么区别)<br/>
session机制存在服务器压力增大，CSRF跨站伪造请求攻击，扩展性不强等问题；

session存储在服务器端，token存储在客户端；

token提供认证和授权功能，作为身份认证，token安全性比session好；

session这种会话存储方式方式只适用于客户端代码和服务端代码运行在同一台服务器上，token适用于项目级的前后端分离（前后端代码运行在不同的服务器下）。

### 题10：[什么是 Token？](/docs/Java%20WEB/Java%20WEB%20中级面试题汇总附答案，2021年%20Java%20WEB%20面试题及答案大全.md#题10什么是-token)<br/>
Token的引入：Token是在客户端频繁向服务端请求数据，服务端频繁的去数据库查询用户名和密码并进行对比，判断用户名和密码正确与否，并作出相应提示，在这样的背景下，Token便应运而生。

Token的定义：Token是服务端生成的一串字符串，以作客户端进行请求的一个令牌，当第一次登录后，服务器生成一个Token便将此Token返回给客户端，以后客户端只需带上这个Token前来请求数据即可，无需再次带上用户名和密码。

使用Token的目的：Token的目的是为了减轻服务器的压力，减少频繁的查询数据库，使服务器更加健壮。

Token 是在服务端产生的。如果前端使用用户名/密码向服务端请求认证，服务端认证成功，那么在服务端会返回 Token 给前端。前端可以在每次请求的时候带上 Token 证明自己的合法地位。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")