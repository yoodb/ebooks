# 80道Java Web大厂必备面试题整理汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java WEB

### 题1：[JSP 中 request 对象有什么作用？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题1jsp-中-request-对象有什么作用)<br/>
request对象代表了客户端的请求信息，主要用于接受通过HTTP协议传送到服务器的数据。（包括头信息、系统信息、请求方式以及请求参数等）。request对象的作用域为一次请求。

当Request对象获取客户提交的汉字字符时，会出现乱码问题，必须进行特殊处理。首先，将获取的字符串用ISO-8859－1进行编码，并将编码存发岛一个字节数组中，然后再将这个数组转化为字符串对象。

**Request常用方法**

getParameter(String strTextName)：获取表单提交的信息。

getProtocol()：获取客户使用的协议。

getServletPath()：获取客户提交信息的页面。

getMethod()：获取客户提交信息的方式。

getHeader()：获取HTTP头文件中的accept,accept-encoding和Host的值。

getRermoteAddr()：获取客户的IP地址。

getRemoteHost()：获取客户机的名称。

getServerName()：获取服务器名称。

getServerPort()：获取服务器的端口号。

getParameterNames()：获取客户端提交的所有参数的名字。

### 题2：[什么是 B/S 和 C/S？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题2什么是-b/s-和-c/s)<br/>
Browser/Server 浏览器/服务器（瘦客户端）

Custom/Server 客户端/服务器（胖客户端）

### 题3：[Servlet 中过滤器有什么作用？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题3servlet-中过滤器有什么作用)<br/>
Servlet监听器对特定的事件进行监听，当产生这些事件的时候，会执行监听器的代码。可以对应用的加载、卸载，对session的初始化、销毁，对session中值变化等事件进行监听。

```java
void doFilter(..) { 
    // do stuff before servlet gets called

    // invoke the servlet, or any other filters mapped to the target servlet
    chain.doFilter(..);

    // do stuff after the servlet finishes
}
```

### 题4：[Java 中如何获取 ServletContext 实例？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题4java-中如何获取-servletcontext-实例)<br/>
1、javax.servlet.Filter中直接获取

```java
ServletContext context = config.getServletContext();
```

2、HttpServlet中直接获取

```java
this.getServletContext()
```


3、在其他方法中通过HttpRequest获得
```java
request.getSession().getServletContext();
```

### 题5：[如何发布一个 Web Service 服务？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题5如何发布一个-web-service-服务)<br/>
1、定义SEI（接口）使用@webservice（类）和@webMethod（暴露的方法）注解。

2、定义SEI的实现。

3、发布Endpoint.publish（url,new SEI的实现对象）。

### 题6：[JSP 中隐含对象都有哪些？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题6jsp-中隐含对象都有哪些)<br/>
JSP隐含对象：没有声明就可以使用的对象。JSP有9个隐含对象。

```java
request
response
session
application
out
pagecontext
config
page
exception
```
request：HttpServletRequest 的一个实例，代表了客户端的请求信息，主要用于接受通过HTTP协议传送到服务器的数据。（包括头信息、系统信息、请求方式以及请求参数等）作用域为一次请求。

response：HttpServletResponse的一个实例，代表是对客户端的响应，主要是将JSP容器处理过的对象传回到客户端。

session：HttpSession的一个实例，是由服务器自动创建的与用户请求相关的对象，作用域为一次会话（浏览器打开直到关闭称为一次会话）。

application：ServletContext的一个实例（表示当前web应用），开始于服务器启动，直到服务器关闭，作用域为当前web应用。

out：jspWriter的一个实例（用于浏览器输出数据）。

pagecontext：作用是取得任何范围的参数（页面的上下文）作用域为当前JSP =页面
config：ServletConfig 的一个实例（主要作用是取得服务器的配置信息）。

page：page 对象代表JSP本身，只有在JSP页面内才是合法的。

exception：显示异常信息。

### 题7：[jsp/servlet 中如何保证 browser 保存在 cache 中？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题7jsp/servlet-中如何保证-browser-保存在-cache-中)<br/>
JSP文件头部增加如下信息即可：

```jsp
<%
response.setHeader("Cache-Control","no-store"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
```

### 题8：[JSP 中 <%…%> 和 <%!…%> 有什么区别？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题8jsp-中-<%…%>-和-<%!…%>-有什么区别)<br/>
<%…%>用于在JSP页面中嵌入Java脚本

<%!…%>用于在JSP页面中申明变量或方法，可以在该页面中的<%…%>脚本中调用，声明的变量相当于Servlet中的定义的成员变量。

### 题9：[什么是 Servlet？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题9什么是-servlet)<br/>
Servlet是用Java编写的服务器端程序, 其主要功能在于交互式地浏览和修改数据，生成动态Web内容。

狭义的Servlet是指Java语言实现的一个接口，广义的Servlet是指任何实现了这个Servlet接口的类，一般情况下，我们一般将Servlet理解为后者。

### 题10：[什么是跨域？](/docs/Java%20WEB/80道Java%20Web大厂必备面试题整理汇总附答案.md#题10什么是跨域)<br/>
跨域是指浏览器不能执行其他网站的脚本。它是由浏览器的同源策略造成的，是浏览器对JavaScript实施的安全限制。

当一个请求url的协议、域名、端口三者之间任意一个与当前页面url不同即为跨域

|当前页面url|被请求页面url|是否跨域|原因|
|-|-|-|-|
|http://blog.yoodb.com/|http://blog.yoodb.com/index.html|否|同源（协议、域名、端口号相同）|
|http://blog.yoodb.com/|https://blog.yoodb.com/index.html|跨域|协议不同（http/https）|
|http://blog.yoodb.com/|http://www.baidu.com/|跨域|主域名不同（test/baidu）|
|http://blog.yoodb.com/|http://blog.test.com/|跨域|子域名不同（www/blog）|
|http://blog.yoodb.com:8080/|http://blog.yoodb.com:7001/|跨域|端口号不同（8080/7001）|


### 题11：如何实现-servlet-单线程模式<br/>


### 题12：servlet-的生命周期有哪几个阶段<br/>


### 题13：为什么会出现跨域问题<br/>


### 题14：什么是-token<br/>


### 题15：java-中有哪些会话跟踪技术作用域<br/>


### 题16：如何请求一个-web-service-服务<br/>


### 题17：什么情况下调用-doget()-和-dopost()<br/>


### 题18：web-service-的核心组成包括哪些内容<br/>


### 题19：servlet-执行时一般实现哪几个方法<br/>


### 题20：servlet-中如何实现自动刷新refresh<br/>


### 题21：什么是-web-service<br/>


### 题22：java-中-request-对象都有哪些方法<br/>


### 题23：jsp-中-application-对象有什么作用<br/>


### 题24：web-service-中-sei-指什么<br/>


### 题25：如何实现自定义一个-servlet<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")