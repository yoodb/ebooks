# 2021年最新版 Spring Boot 面试题汇总附答案

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Boot

### 题1：[如何重新加载 Spring Boot 上的更改内容，而无需重启服务？](/docs/Spring%20Boot/2021年最新版%20Spring%20Boot%20面试题汇总附答案.md#题1如何重新加载-spring-boot-上的更改内容而无需重启服务)<br/>
可以使用DEV工具来实现。通过这种依赖关系，可以节省任何更改，嵌入式tomcat将重新启动。

Spring Boot有一个开发工具（DevTools）模块，它有助于提高开发人员的生产力。

Java开发人员面临的一个主要挑战是将文件更改自动部署到服务器并自动重启服务器。

开发人员可以重新加载Spring Boot上的更改内容，而无需重启服务。消除了每次手动部署更改的需要。

Spring Boot在发布它的第一个版本时没有这个功能。这是开发人员最需要的功能。

DevTools模块完全满足开发人员的需求。该模块将在生产环境中被禁用。它还提供H2数据库控制台以更好地测试应用程序。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <optional>true</optional>
</dependency>
```

### 题2：[如何自定义端口运行 Spring Boot 应用程序？](/docs/Spring%20Boot/2021年最新版%20Spring%20Boot%20面试题汇总附答案.md#题2如何自定义端口运行-spring-boot-应用程序)<br/>
Spring Boot应用程序自定义端口可以在application.properties中指定端口。

```xml
server.port = 8090
```

### 题3：[Spring Boot 如何禁用某些自动配置特性？](/docs/Spring%20Boot/2021年最新版%20Spring%20Boot%20面试题汇总附答案.md#题3spring-boot-如何禁用某些自动配置特性)<br/>
禁用某些自动配置特性，可以使用@EnableAutoConfiguration注解的exclude属性来指明。

例如，下面的代码段是使DataSourceAutoConfiguration无效：

```java
// other annotations
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class MyConfiguration { }
```

**使用@SpringBootApplication注解**

将@EnableAutoConfiguration作为元注解的项，来启用自动化配置，能够使用相同名字的属性来禁用自动化配置：

```java
// other annotations
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class MyConfiguration { }
```
也可以使用spring.autoconfigure.exclude环境属性来禁用自动化配置。application.properties文件中增加如下配置内容：

```xml
spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
```

### 题4：[Spring Boot 中 Actuator 有什么作用？](/docs/Spring%20Boot/2021年最新版%20Spring%20Boot%20面试题汇总附答案.md#题4spring-boot-中-actuator-有什么作用)<br/>
本质上Actuator通过启用production-ready功能使得SpringBoot应用程序变得更有生命力。这些功能允许对生产环境中的应用程序进行监视和管理。

集成SpringBoot Actuator到项目中非常简单。只需要做的是将spring-boot-starter-actuator starter引入到POM.xml文件当中：
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

SpringBoot Actuaor可以使用HTTP或者JMX endpoints来浏览操作信息。大多数应用程序都是用HTTP，作为endpoint的标识以及使用/actuator前缀作为URL路径。

一些常用的内置endpoints Actuator：

>auditevents：查看 audit 事件信息
env：查看 环境变量
health：查看应用程序健康信息
httptrace：展示 HTTP 路径信息
info：展示 arbitrary 应用信息
metrics：展示 metrics 信息
loggers：显示并修改应用程序中日志器的配置
mappings：展示所有 @RequestMapping 路径信息
scheduledtasks：展示应用程序中的定时任务信息
threaddump：执行 Thread Dump

### 题5：[Spring Boot 如何注册一个定制的自动化配置？](/docs/Spring%20Boot/2021年最新版%20Spring%20Boot%20面试题汇总附答案.md#题5spring-boot-如何注册一个定制的自动化配置)<br/>
为了注册一个自动化配置类，必须在META-INF/spring.factories文件中的EnableAutoConfiguration键下列出它的全限定名：

```xml
org.springframework.boot.autoconfigure.EnableAutoConfiguration=com.baeldung.autoconfigure.CustomAutoConfiguration
```

如果使用Maven构建项目，这个文件需要放置在package阶段被写入完成的resources/META-INF目录中。

### 题6：[Spring Boot 的目录结构是怎样的？](/docs/Spring%20Boot/2021年最新版%20Spring%20Boot%20面试题汇总附答案.md#题6spring-boot-的目录结构是怎样的)<br/>
**1、代码层的结构**

根目录：com.springboot

1）工程启动类(ApplicationServer.java)置于com.springboot.build包下

2）实体类(domain)置于com.springboot.domain

3）数据访问层(Dao)置于com.springboot.repository

4）数据服务层(Service)置于com,springboot.service,数据服务的实现接口(serviceImpl)至于com.springboot.service.impl

5）前端控制器(Controller)置于com.springboot.controller

6）工具类(utils)置于com.springboot.utils

7）常量接口类(constant)置于com.springboot.constant

8）配置信息类(config)置于com.springboot.config

9）数据传输类(vo)置于com.springboot.vo

**2、资源文件的结构**

根目录：src/main/resources

1）配置文件(.properties/.json等)置于config文件夹下

2）国际化(i18n)置于i18n文件夹下

3）spring.xml置于META-INF/spring文件夹下

4）页面以及js/css/image等置于static文件夹下的各自文件下

### 题7：[什么是 YAML？](/docs/Spring%20Boot/2021年最新版%20Spring%20Boot%20面试题汇总附答案.md#题7什么是-yaml)<br/>
YAML是一种人类可读的数据序列化语言。

它通常用于配置文件。

与属性文件相比，如果我们想要在配置文件中添加复杂的属性，YAML文件就更加结构化，而且更少混淆。可以看出YAML具有分层配置数据。

### 题8：[Spring Boot 中如何解决跨域问题？](/docs/Spring%20Boot/2021年最新版%20Spring%20Boot%20面试题汇总附答案.md#题8spring-boot-中如何解决跨域问题)<br/>
跨域可以在前端通过JSONP来解决，但是JSONP只可以发送GET请求，无法发送其他类型的请求。

在RESTful风格的应用中，就显得非常鸡肋，因此推荐在后端通过（CORS，Cross-origin resource sharing）来解决跨域问题。

这种解决方案并非Spring Boot特有的，在传统的SSM框架中，就可以通过CORS来解决跨域问题，只不过之前是在XML文件中配置CORS，现在可以通过实现WebMvcConfigurer接口然后重写addCorsMappings方法解决跨域问题。

```java
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                .maxAge(3600);
    }
}
```

项目中前后端分离部署，所以需要解决跨域的问题。使用cookie存放用户登录的信息，在spring拦截器进行权限控制，当权限不符合时，直接返回给用户固定的json结果。

注意：当用户退出登录状态时或者token过期时，由于拦截器和跨域的顺序有问题，出现了跨域的现象。http请求先经过filter，到达servlet后才进行拦截器的处理，如果把cors放在filter中就可以优先于权限拦截器执行。

```java
@Configuration
public class CorsConfig {
    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(urlBasedCorsConfigurationSource);
    }
}
```

### 题9：[什么是 Swagger？Spring Boot 如何实现 Swagger？](/docs/Spring%20Boot/2021年最新版%20Spring%20Boot%20面试题汇总附答案.md#题9什么是-swaggerspring-boot-如何实现-swagger)<br/>
Swagger广泛用于可视化API，使用Swagger UI为前端开发人员提供在线沙箱。

Swagger是用于生成RESTful Web服务的可视化表示的工具，规范和完整框架实现。它使文档能够以与服务器相同的速度更新。当通过 Swagger正确定义时，消费者可以使用最少量的实现逻辑来理解远程服务并与其进行交互。因此，Swagger消除了调用服务时的猜测。

### 题10：[Spring boot 中当 bean 存在时如何置后执行自动配置？](/docs/Spring%20Boot/2021年最新版%20Spring%20Boot%20面试题汇总附答案.md#题10spring-boot-中当-bean-存在时如何置后执行自动配置)<br/>
当bean已存在的时候通知自动配置类置后执行，可以使用@ConditionalOnMissingBean注解。这个注解需要注意的属性是：

value：被检查的beans的类型

name：被检查的beans的名字

当将@Bean修饰到方法时，目标类型默认为方法的返回类型：

```java
@Configuration
public class CustomConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public CustomService service() { ... }
}
```

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")