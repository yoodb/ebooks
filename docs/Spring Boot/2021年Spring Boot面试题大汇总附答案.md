# 2021年Spring Boot面试题大汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Boot

### 题1：[Spring Boot 支持松绑定表示什么含义？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题1spring-boot-支持松绑定表示什么含义)<br/>
SpringBoot中的松绑定适用于配置属性的类型安全绑定。使用松绑定，环境属性的键不需要与属性名完全匹配。这样就可以用驼峰式、短横线式、蛇形式或者下划线分割来命名。

例如，在一个有@ConfigurationProperties声明的bean类中带有一个名为myProp的属性，它可以绑定到以下任何一个参数中，myProp、my-prop、my_prop或者MY_PROP。

### 题2：[如何自定义端口运行 Spring Boot 应用程序？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题2如何自定义端口运行-spring-boot-应用程序)<br/>
Spring Boot应用程序自定义端口可以在application.properties中指定端口。

```xml
server.port = 8090
```

### 题3：[Spring Boot 如何禁用某些自动配置特性？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题3spring-boot-如何禁用某些自动配置特性)<br/>
禁用某些自动配置特性，可以使用@EnableAutoConfiguration注解的exclude属性来指明。

例如，下面的代码段是使DataSourceAutoConfiguration无效：

```java
// other annotations
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class MyConfiguration { }
```

**使用@SpringBootApplication注解**

将@EnableAutoConfiguration作为元注解的项，来启用自动化配置，能够使用相同名字的属性来禁用自动化配置：

```java
// other annotations
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class MyConfiguration { }
```
也可以使用spring.autoconfigure.exclude环境属性来禁用自动化配置。application.properties文件中增加如下配置内容：

```xml
spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
```

### 题4：[Spring Boot 中如何实现全局异常处理？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题4spring-boot-中如何实现全局异常处理)<br/>
Spring提供了一种使用ControllerAdvice处理异常的非常有用的方法。通过实现一个ControlerAdvice类，来处理控制器类抛出的所有异常。

### 题5：[常见的系统架构风格有哪些?各有什么优缺点?](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题5常见的系统架构风格有哪些?各有什么优缺点?)<br/>
**1、单体架构**

单体架构也称之为单体系统或者是单体应用。就是一种把系统中所有的功能、模块耦合在一个应用中的架构方式。

单体架构特点：打包成一个独立的单元(导成一个唯一的jar包或者是war包)，会一个进程的方式来运行。

单体架构的优点、缺点

优点：

项目易于管理

部署简单

缺点：

测试成本高

可伸缩性差

可靠性差

迭代困难

跨语言程度差

团队协作难


**2、MVC架构**

MVC架构特点：

MVC是模型(Model)、视图(View)、控制器(Controller)3个单词的缩写。 下面我们从这3个方面来讲解MVC中的三个要素。

Model是指数据模型，是对客观事物的抽象。 如一篇博客文章，我们可能会以一个Post类来表示，那么，这个Post类就是数据对象。 同时，博客文章还有一些业务逻辑，如发布、回收、评论等，这一般表现为类的方法，这也是model的内容和范畴。 对于Model，主要是数据、业务逻辑和业务规则。相对而言，这是MVC中比较稳定的部分，一般成品后不会改变。 开发初期的最重要任务，主要也是实现Model的部分。这一部分写得好，后面就可以改得少，开发起来就快。

View是指视图，也就是呈现给用户的一个界面，是model的具体表现形式，也是收集用户输入的地方。 如你在某个博客上看到的某一篇文章，就是某个Post类的表现形式。 View的目的在于提供与用户交互的界面。换句话说，对于用户而言，只有View是可见的、可操作的。 事实上也是如此，你不会让用户看到Model，更不会让他直接操作Model。 你只会让用户看到你想让他看的内容。 这就是View要做的事，他往往是MVC中变化频繁的部分，也是客户经常要求改来改去的地方。 今天你可能会以一种形式来展示你的博文，明天可能就变成别的表现形式了。

Contorller指的是控制器，主要负责与model和view打交道。 换句话说，model和view之间一般不直接打交道，他们老死不相往来。view中不会对model作任何操作， model不会输出任何用于表现的东西，如HTML代码等。这俩甩手不干了，那总得有人来干吧，只能Controller上了。 Contorller用于决定使用哪些Model，对Model执行什么操作，为视图准备哪些数据，是MVC中沟通的桥梁。

MVC架构优缺点

优点：

各施其职，互不干涉。

在MVC模式中，三个层各施其职，所以如果一旦哪一层的需求发生了变化，就只需要更改相应的层中的代码而不会影响到其它层中的代码。

有利于开发中的分工。

在MVC模式中，由于按层把系统分开，那么就能更好的实现开发中的分工。网页设计人员可以进行开发视图层中的JSP，对业务熟悉的开发人员可开发业务层，而其它开发人员可开发控制层。

有利于组件的重用。

分层后更有利于组件的重用。如控制层可独立成一个能用的组件，视图层也可做成通用的操作界面。

缺点：

增加了系统结构和实现的复杂性。

视图与控制器间的过于紧密的连接。

视图对模型数据的低效率访问。


**3、面向服务架构(SOA)**

面向服务的架构(SOA)是一个组件模型，它将应用程序拆分成不同功能单元(称为服务)通过这些服务之间定义良好的接口和契约联系起来。接口是采用中立的方式进行定义的，它应该独立于实现服务的硬件平台、操作系统和编程语言。这使得构建在各种各样的系统中的服务可以以一种统一和通用的方式进行交互。

面向服务架构特点：

系统是由多个服务构成

每个服务可以单独独立部署

每个服务之间是松耦合的。服务内部是高内聚的，外部是低耦合的。高内聚就是每个服务只关注完成一个功能。

服务的优点、缺点

优点：

测试容易

可伸缩性强

可靠性强

跨语言程度会更加灵活

团队协作容易

系统迭代容易

缺点：

运维成本过高，部署数量较多

接口兼容多版本

分布式系统的复杂性

分布式事务

### 题6：[什么是 WebSocket？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题6什么是-websocket)<br/>
WebSocket是一种计算机通信协议，通过单个 TCP 连接提供全双工通信信道。

1、WebSocket是双向的—使用WebSocket客户端或服务器可以发起消息发送。

2、WebSocket是全双工的—客户端和服务器通信是相互独立的。

3、单个TCP连接—初始连接使用HTTP，然后将此连接升级到基于套接字的连接。然后这个单一连接用于所有未来的通信

4、Light与http相比，WebSocket消息数据交换要轻得多。

### 题7：[Spring Boot 需要独立的容器运行吗？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题7spring-boot-需要独立的容器运行吗)<br/>
Spring Boot项目可以不需要，内置了Tomcat/Jetty等容器，默认Tomcat。

Spring Boot不需要独立的容器就可以运行，因为在Spring Boot工程发布的jar文件里已经包含了tomcat插件的jar文件。

Spring Boot运行时创建tomcat对象实现web服务功能，另外也可以将Spring Boot编译成war包文件放到tomcat中运行。

### 题8：[Spring Boot 支持哪几种内嵌容器？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题8spring-boot-支持哪几种内嵌容器)<br/>
Spring Boot支持的内嵌容器有Tomcat（默认）、Jetty、Undertow和Reactor Netty（v2.0+），借助可插拔（SPI）机制的实现，开发者可以轻松进行容器间的切换。

### 题9：[什么是 Spring Boot Stater？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题9什么是-spring-boot-stater)<br/>
Spring Boot在配置上相比Spring要简单许多，其核心在于Spring Boot Stater。

Spring Boot内嵌容器支持Tomcat、Jetty、Undertow等应用服务的starter启动器，在应用启动时被加载，可以快速的处理应用所需要的一些基础环境配置。

starter解决的是依赖管理配置复杂的问题，可以理解成通过pom.xml文件配置很多jar包组合的maven项目，用来简化maven依赖配置，starter可以被继承也可以依赖于别的starter。

比如spring-boot-starter-web包含以下依赖：

```java
org.springframework.boot:spring-boot-starter
org.springframework.boot:spring-boot-starter-tomcat
org.springframework.boot:spring-boot-starter-validation
com.fasterxml.jackson.core:jackson-databind
org.springframework:spring-web
org.springframework:spring-webmvc
```

starter负责配与Sping整合相关的配置依赖等，使用者无需关心框架整合带来的问题。

比如使用Sping和JPA访问数据库，只需要项目包含spring-boot-starter-data-jpa依赖就可以完美执行。

### 题10：[Spring Boot 如何注册一个定制的自动化配置？](/docs/Spring%20Boot/2021年Spring%20Boot面试题大汇总附答案.md#题10spring-boot-如何注册一个定制的自动化配置)<br/>
为了注册一个自动化配置类，必须在META-INF/spring.factories文件中的EnableAutoConfiguration键下列出它的全限定名：

```xml
org.springframework.boot.autoconfigure.EnableAutoConfiguration=com.baeldung.autoconfigure.CustomAutoConfiguration
```

如果使用Maven构建项目，这个文件需要放置在package阶段被写入完成的resources/META-INF目录中。

### 题11：spring-boot-中如何实现定时任务<br/>


### 题12：如何使用-maven-来构建一个-spring-boot-程序<br/>


### 题13：如何使用-spring-boot-实现分页和排序<br/>


### 题14：spring-boot-运行方式有哪几种<br/>


### 题15：spring-boot-的目录结构是怎样的<br/>


### 题16：spring-boot-内嵌容器默认是什么<br/>


### 题17：spring-boot-2.x-有什么新特性与-1.x-有什么区别<br/>


### 题18：spring-boot-热部署有几种方式<br/>


### 题19：spring-boot-中-actuator-有什么作用<br/>


### 题20：spring-boot-和-spring-有什么区别<br/>


### 题21：spring-boot-如何编写一个集成测试<br/>


### 题22：什么是-swaggerspring-boot-如何实现-swagger<br/>


### 题23：bootstrap.properties-和-application.properties-有何区别<br/>


### 题24：什么是-yaml<br/>


### 题25：如何重新加载-spring-boot-上的更改内容而无需重启服务<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")