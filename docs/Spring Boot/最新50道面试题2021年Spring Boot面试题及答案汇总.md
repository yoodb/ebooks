# 最新50道面试题2021年Spring Boot面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Boot

### 题1：[如何重新加载 Spring Boot 上的更改内容，而无需重启服务？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题1如何重新加载-spring-boot-上的更改内容而无需重启服务)<br/>
可以使用DEV工具来实现。通过这种依赖关系，可以节省任何更改，嵌入式tomcat将重新启动。

Spring Boot有一个开发工具（DevTools）模块，它有助于提高开发人员的生产力。

Java开发人员面临的一个主要挑战是将文件更改自动部署到服务器并自动重启服务器。

开发人员可以重新加载Spring Boot上的更改内容，而无需重启服务。消除了每次手动部署更改的需要。

Spring Boot在发布它的第一个版本时没有这个功能。这是开发人员最需要的功能。

DevTools模块完全满足开发人员的需求。该模块将在生产环境中被禁用。它还提供H2数据库控制台以更好地测试应用程序。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <optional>true</optional>
</dependency>
```

### 题2：[Spring Boot 中如何解决跨域问题？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题2spring-boot-中如何解决跨域问题)<br/>
跨域可以在前端通过JSONP来解决，但是JSONP只可以发送GET请求，无法发送其他类型的请求。

在RESTful风格的应用中，就显得非常鸡肋，因此推荐在后端通过（CORS，Cross-origin resource sharing）来解决跨域问题。

这种解决方案并非Spring Boot特有的，在传统的SSM框架中，就可以通过CORS来解决跨域问题，只不过之前是在XML文件中配置CORS，现在可以通过实现WebMvcConfigurer接口然后重写addCorsMappings方法解决跨域问题。

```java
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                .maxAge(3600);
    }
}
```

项目中前后端分离部署，所以需要解决跨域的问题。使用cookie存放用户登录的信息，在spring拦截器进行权限控制，当权限不符合时，直接返回给用户固定的json结果。

注意：当用户退出登录状态时或者token过期时，由于拦截器和跨域的顺序有问题，出现了跨域的现象。http请求先经过filter，到达servlet后才进行拦截器的处理，如果把cors放在filter中就可以优先于权限拦截器执行。

```java
@Configuration
public class CorsConfig {
    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(urlBasedCorsConfigurationSource);
    }
}
```

### 题3：[如何监视所有 Spring Boot 微服务？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题3如何监视所有-spring-boot-微服务)<br/>
Spring Boot提供监视器端点以监控各个微服务的度量。这些端点对于获取有关应用程序的信息（如它们是否已启动）以及它们的组件（如数据库等）是否正常运行很有帮助。

但是，使用监视器的一个主要缺点或困难是，我们必须单独打开应用程序的知识点以了解其状态或健康状况。想象一下涉及50个应用程序的微服务，管理员将不得不击中所有50个应用程序的执行终端。

为了帮助我们处理这种情况，我们将使用位于的开源项目。它建立在Spring Boot Actuator之上，它提供了一个Web UI，使我们能够可视化多个应用程序的度量。

### 题4：[Spring Boot 2.X 有什么新特性？与 1.X 有什么区别？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题4spring-boot-2.x-有什么新特性与-1.x-有什么区别)<br/>
1、配置变更。

2、JDK版本升级。

3、第三方类库升级。

4、响应式Spring编程支持。

5、HTTP/2支持。

6、配置属性绑定。

7、更多改进与加强。

### 题5：[Spring Boot 支持松绑定表示什么含义？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题5spring-boot-支持松绑定表示什么含义)<br/>
SpringBoot中的松绑定适用于配置属性的类型安全绑定。使用松绑定，环境属性的键不需要与属性名完全匹配。这样就可以用驼峰式、短横线式、蛇形式或者下划线分割来命名。

例如，在一个有@ConfigurationProperties声明的bean类中带有一个名为myProp的属性，它可以绑定到以下任何一个参数中，myProp、my-prop、my_prop或者MY_PROP。

### 题6：[Spring Boot  jar 和普通 jar 有什么区别？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题6spring-boot--jar-和普通-jar-有什么区别)<br/>
Spring Boot项目最终打包成的jar是可执行jar，这种jar可以直接通过java-jarxxx.jar命令来运行，这种jar不可以作为普通的jar被其他项目依赖，即使依赖了也无法使用其中的类。

Spring Boot的jar无法被其他项目依赖，主要还是他和普通jar的结构不同。普通的jar包，解压后直接就是包名，包里就是开发的代码，而Spring Boot打包成的可执行jar解压后，在\BOOT-INF\classes目录下才是开发的代码，因此无法被直接引用。

如果非要引用，可以在pom.xml文件中增加配置，将Spring Boot项目打包成两个jar，一个可执行，一个可引用。

### 题7：[如何使用 Spring Boot 实现分页和排序？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题7如何使用-spring-boot-实现分页和排序)<br/>
使用Spring Boot实现分页非常简单。使用Spring Data JPA可以实现将可分页的传递给存储库方法。


### 题8：[Spring Boot 的目录结构是怎样的？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题8spring-boot-的目录结构是怎样的)<br/>
**1、代码层的结构**

根目录：com.springboot

1）工程启动类(ApplicationServer.java)置于com.springboot.build包下

2）实体类(domain)置于com.springboot.domain

3）数据访问层(Dao)置于com.springboot.repository

4）数据服务层(Service)置于com,springboot.service,数据服务的实现接口(serviceImpl)至于com.springboot.service.impl

5）前端控制器(Controller)置于com.springboot.controller

6）工具类(utils)置于com.springboot.utils

7）常量接口类(constant)置于com.springboot.constant

8）配置信息类(config)置于com.springboot.config

9）数据传输类(vo)置于com.springboot.vo

**2、资源文件的结构**

根目录：src/main/resources

1）配置文件(.properties/.json等)置于config文件夹下

2）国际化(i18n)置于i18n文件夹下

3）spring.xml置于META-INF/spring文件夹下

4）页面以及js/css/image等置于static文件夹下的各自文件下

### 题9：[如何自定义端口运行 Spring Boot 应用程序？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题9如何自定义端口运行-spring-boot-应用程序)<br/>
Spring Boot应用程序自定义端口可以在application.properties中指定端口。

```xml
server.port = 8090
```

### 题10：[Spring Boot 热部署有几种方式？](/docs/Spring%20Boot/最新50道面试题2021年Spring%20Boot面试题及答案汇总.md#题10spring-boot-热部署有几种方式)<br/>
1）spring-boot-devtools

通过Springboot提供的开发者工具spring-boot-devtools来实现，在pom.xml引用其依赖。

然后在Settings→Build→Compiler中将Build project automatically勾选上，最后按ctrl+shift+alt+/ 选择registy，将compiler.automake.allow.when.app.running勾选。

2）Spring Loaded

Spring官方提供的热部署程序，实现修改类文件的热部署

下载Spring Loaded（项目地址https://github.com/spring-projects/spring-loaded）

添加运行时参数：-javaagent:C:/springloaded-1.2.5.RELEASE.jar –noverify

3）JRebel

收费的一个热部署软件，安装插件使用即可。

### 题11：spring-boot-核心注解都有哪些<br/>


### 题12：bootstrap.properties-和-application.properties-有何区别<br/>


### 题13：什么是-websocket<br/>


### 题14：什么是-spring-boot-stater<br/>


### 题15：什么是-spring-boot-框架<br/>


### 题16：spring-boot-stater-有什么命名规范<br/>


### 题17：spring-boot-中如何实现定时任务<br/>


### 题18：spring-boot-自动配置原理是什么<br/>


### 题19：常见的系统架构风格有哪些?各有什么优缺点?<br/>


### 题20：spring-boot-运行方式有哪几种<br/>


### 题21：spring-boot-如何注册一个定制的自动化配置<br/>


### 题22：spring-boot-中核心配置文件是什么<br/>


### 题23：什么是-akf-拆分原则<br/>


### 题24：spring-security-和-shiro-对比有什么优缺点<br/>


### 题25：spring-boot-如何禁用某些自动配置特性<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")