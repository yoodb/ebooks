# 最新Spring Boot面试题大全带答案持续更新

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Boot

### 题1：[Spring Boot 热部署有几种方式？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题1spring-boot-热部署有几种方式)<br/>
1）spring-boot-devtools

通过Springboot提供的开发者工具spring-boot-devtools来实现，在pom.xml引用其依赖。

然后在Settings→Build→Compiler中将Build project automatically勾选上，最后按ctrl+shift+alt+/ 选择registy，将compiler.automake.allow.when.app.running勾选。

2）Spring Loaded

Spring官方提供的热部署程序，实现修改类文件的热部署

下载Spring Loaded（项目地址https://github.com/spring-projects/spring-loaded）

添加运行时参数：-javaagent:C:/springloaded-1.2.5.RELEASE.jar –noverify

3）JRebel

收费的一个热部署软件，安装插件使用即可。

### 题2：[什么是 Spring Boot 框架？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题2什么是-spring-boot-框架)<br/>
Spring Boot是由Pivotal团队提供的全新框架，其设计目的是用来简化新Spring应用的初始搭建以及开发过程。

Spring Boot框架使用了特定的方式来进行配置，从而使开发人员不再需要定义样板化的配置。通过这种方式，Spring Boot致力于在蓬勃发展的快速应用开发领域（rapid application development）成为领导者。

2014年4月发布第一个版本的全新开源的Spring Boot轻量级框架。它基于Spring4.0设计，不仅继承了Spring框架原有的优秀特性，而且还通过简化配置来进一步简化了Spring应用的整个搭建和开发过程。

另外Spring Boot通过集成大量的框架使得依赖包的版本冲突，以及引用的不稳定性等问题得到了很好的解决。

### 题3：[什么是 JavaConfig？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题3什么是-javaconfig)<br/>
Spring JavaConfig是Spring社区的产品，它提供了配置Spring IoC容器的纯Java方法。因此它有助于避免使用XML配置。

使用JavaConfig的优点在于：
 
1）面向对象的配置。由于配置被定义为JavaConfig中的类，因此用户可以充分利用Java 中的面向对象功能。一个配置类可以继承另一个，重写它的@Bean方法等。

2）减少或消除 XML配置。基于依赖注入原则的外化配置的好处已被证明。但是，许多开发人员不希望在XML和Java之间来回切换。JavaConfig为开发人员提供了一种纯Java方法来配置与 XML 配置概念相似的Spring容器。从技术角度来讲，只使用JavaConfig配置类来配置容器是可行的，但实际上很多人认为将JavaConfig与XML混合匹配是理想的。

3）类型安全和重构友好。JavaConfig提供了一种类型安全的方法来配置Spring容器。由于Java 5.0对泛型的支持，现在可以按类型而不是按名称检索bean，不需要任何强制转换或基于字符串的查找。

### 题4：[什么是 Spring Profiles？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题4什么是-spring-profiles)<br/>
Spring Profiles允许用户根据配置文件（dev，test，prod等）来注册bean。

因此，当应用程序在开发中运行时，只有某些 bean可以加载，而在PRODUCTION中，某些其他bean可以加载。

假设要求是Swagger文档仅适用于QA环境，并且禁用所有其他文档。这就可以使用配置文件来完成。

Spring Boot使得使用配置文件非常简单。

### 题5：[如何重新加载 Spring Boot 上的更改内容，而无需重启服务？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题5如何重新加载-spring-boot-上的更改内容而无需重启服务)<br/>
可以使用DEV工具来实现。通过这种依赖关系，可以节省任何更改，嵌入式tomcat将重新启动。

Spring Boot有一个开发工具（DevTools）模块，它有助于提高开发人员的生产力。

Java开发人员面临的一个主要挑战是将文件更改自动部署到服务器并自动重启服务器。

开发人员可以重新加载Spring Boot上的更改内容，而无需重启服务。消除了每次手动部署更改的需要。

Spring Boot在发布它的第一个版本时没有这个功能。这是开发人员最需要的功能。

DevTools模块完全满足开发人员的需求。该模块将在生产环境中被禁用。它还提供H2数据库控制台以更好地测试应用程序。

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <optional>true</optional>
</dependency>
```

### 题6：[Spring Boot 的目录结构是怎样的？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题6spring-boot-的目录结构是怎样的)<br/>
**1、代码层的结构**

根目录：com.springboot

1）工程启动类(ApplicationServer.java)置于com.springboot.build包下

2）实体类(domain)置于com.springboot.domain

3）数据访问层(Dao)置于com.springboot.repository

4）数据服务层(Service)置于com,springboot.service,数据服务的实现接口(serviceImpl)至于com.springboot.service.impl

5）前端控制器(Controller)置于com.springboot.controller

6）工具类(utils)置于com.springboot.utils

7）常量接口类(constant)置于com.springboot.constant

8）配置信息类(config)置于com.springboot.config

9）数据传输类(vo)置于com.springboot.vo

**2、资源文件的结构**

根目录：src/main/resources

1）配置文件(.properties/.json等)置于config文件夹下

2）国际化(i18n)置于i18n文件夹下

3）spring.xml置于META-INF/spring文件夹下

4）页面以及js/css/image等置于static文件夹下的各自文件下

### 题7：[常见的系统架构风格有哪些?各有什么优缺点?](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题7常见的系统架构风格有哪些?各有什么优缺点?)<br/>
**1、单体架构**

单体架构也称之为单体系统或者是单体应用。就是一种把系统中所有的功能、模块耦合在一个应用中的架构方式。

单体架构特点：打包成一个独立的单元(导成一个唯一的jar包或者是war包)，会一个进程的方式来运行。

单体架构的优点、缺点

优点：

项目易于管理

部署简单

缺点：

测试成本高

可伸缩性差

可靠性差

迭代困难

跨语言程度差

团队协作难


**2、MVC架构**

MVC架构特点：

MVC是模型(Model)、视图(View)、控制器(Controller)3个单词的缩写。 下面我们从这3个方面来讲解MVC中的三个要素。

Model是指数据模型，是对客观事物的抽象。 如一篇博客文章，我们可能会以一个Post类来表示，那么，这个Post类就是数据对象。 同时，博客文章还有一些业务逻辑，如发布、回收、评论等，这一般表现为类的方法，这也是model的内容和范畴。 对于Model，主要是数据、业务逻辑和业务规则。相对而言，这是MVC中比较稳定的部分，一般成品后不会改变。 开发初期的最重要任务，主要也是实现Model的部分。这一部分写得好，后面就可以改得少，开发起来就快。

View是指视图，也就是呈现给用户的一个界面，是model的具体表现形式，也是收集用户输入的地方。 如你在某个博客上看到的某一篇文章，就是某个Post类的表现形式。 View的目的在于提供与用户交互的界面。换句话说，对于用户而言，只有View是可见的、可操作的。 事实上也是如此，你不会让用户看到Model，更不会让他直接操作Model。 你只会让用户看到你想让他看的内容。 这就是View要做的事，他往往是MVC中变化频繁的部分，也是客户经常要求改来改去的地方。 今天你可能会以一种形式来展示你的博文，明天可能就变成别的表现形式了。

Contorller指的是控制器，主要负责与model和view打交道。 换句话说，model和view之间一般不直接打交道，他们老死不相往来。view中不会对model作任何操作， model不会输出任何用于表现的东西，如HTML代码等。这俩甩手不干了，那总得有人来干吧，只能Controller上了。 Contorller用于决定使用哪些Model，对Model执行什么操作，为视图准备哪些数据，是MVC中沟通的桥梁。

MVC架构优缺点

优点：

各施其职，互不干涉。

在MVC模式中，三个层各施其职，所以如果一旦哪一层的需求发生了变化，就只需要更改相应的层中的代码而不会影响到其它层中的代码。

有利于开发中的分工。

在MVC模式中，由于按层把系统分开，那么就能更好的实现开发中的分工。网页设计人员可以进行开发视图层中的JSP，对业务熟悉的开发人员可开发业务层，而其它开发人员可开发控制层。

有利于组件的重用。

分层后更有利于组件的重用。如控制层可独立成一个能用的组件，视图层也可做成通用的操作界面。

缺点：

增加了系统结构和实现的复杂性。

视图与控制器间的过于紧密的连接。

视图对模型数据的低效率访问。


**3、面向服务架构(SOA)**

面向服务的架构(SOA)是一个组件模型，它将应用程序拆分成不同功能单元(称为服务)通过这些服务之间定义良好的接口和契约联系起来。接口是采用中立的方式进行定义的，它应该独立于实现服务的硬件平台、操作系统和编程语言。这使得构建在各种各样的系统中的服务可以以一种统一和通用的方式进行交互。

面向服务架构特点：

系统是由多个服务构成

每个服务可以单独独立部署

每个服务之间是松耦合的。服务内部是高内聚的，外部是低耦合的。高内聚就是每个服务只关注完成一个功能。

服务的优点、缺点

优点：

测试容易

可伸缩性强

可靠性强

跨语言程度会更加灵活

团队协作容易

系统迭代容易

缺点：

运维成本过高，部署数量较多

接口兼容多版本

分布式系统的复杂性

分布式事务

### 题8：[Spring Boot 支持哪几种内嵌容器？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题8spring-boot-支持哪几种内嵌容器)<br/>
Spring Boot支持的内嵌容器有Tomcat（默认）、Jetty、Undertow和Reactor Netty（v2.0+），借助可插拔（SPI）机制的实现，开发者可以轻松进行容器间的切换。

### 题9：[Spring Boot  jar 和普通 jar 有什么区别？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题9spring-boot--jar-和普通-jar-有什么区别)<br/>
Spring Boot项目最终打包成的jar是可执行jar，这种jar可以直接通过java-jarxxx.jar命令来运行，这种jar不可以作为普通的jar被其他项目依赖，即使依赖了也无法使用其中的类。

Spring Boot的jar无法被其他项目依赖，主要还是他和普通jar的结构不同。普通的jar包，解压后直接就是包名，包里就是开发的代码，而Spring Boot打包成的可执行jar解压后，在\BOOT-INF\classes目录下才是开发的代码，因此无法被直接引用。

如果非要引用，可以在pom.xml文件中增加配置，将Spring Boot项目打包成两个jar，一个可执行，一个可引用。

### 题10：[什么是 WebSocket？](/docs/Spring%20Boot/最新Spring%20Boot面试题大全带答案持续更新.md#题10什么是-websocket)<br/>
WebSocket是一种计算机通信协议，通过单个 TCP 连接提供全双工通信信道。

1、WebSocket是双向的—使用WebSocket客户端或服务器可以发起消息发送。

2、WebSocket是全双工的—客户端和服务器通信是相互独立的。

3、单个TCP连接—初始连接使用HTTP，然后将此连接升级到基于套接字的连接。然后这个单一连接用于所有未来的通信

4、Light与http相比，WebSocket消息数据交换要轻得多。

### 题11：spring-boot-2.x-有什么新特性与-1.x-有什么区别<br/>


### 题12：spring-boot-需要独立的容器运行吗<br/>


### 题13：-springspring-mvc-和-spring-boot-有什么区别<br/>


### 题14：spring-boot-自动配置原理是什么<br/>


### 题15：spring-boot-中如何实现全局异常处理<br/>


### 题16：如何使用-spring-boot-实现分页和排序<br/>


### 题17：spring-boot-中如何禁用-actuator-端点安全性<br/>


### 题18：spring-boot-中当-bean-存在时如何置后执行自动配置<br/>


### 题19：什么是-spring-batch<br/>


### 题20：spring-boot-启动器都有哪些<br/>


### 题21：spring-boot-有什么外部配置的可能来源<br/>


### 题22：spring-boot-中如何实现兼容老-spring-项目<br/>


### 题23：什么是-akf-拆分原则<br/>


### 题24：spring-boot-如何编写一个集成测试<br/>


### 题25：如何使用-maven-来构建一个-spring-boot-程序<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")