# 最新2021年Spring Boot面试题及答案汇总版

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spring Boot

### 题1：[Spring Boot 和 Spring 有什么区别？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题1spring-boot-和-spring-有什么区别)<br/>
Spring框架提供多种特性使得web应用开发变得更简便，包括依赖注入、数据绑定、切面编程、数据存取等等。

随着时间推移，Spring生态变得越来越复杂了，并且应用程序所必须的配置文件也令人觉得可怕。这就是Spirng Boot派上用场的地方了，它使得Spring的配置变得更轻而易举。

实际上Spring是unopinionated（予以配置项多，倾向性弱）的，Spring Boot在平台和库的做法中更opinionated，使得我们更容易上手。

这里有两条SpringBoot带来的好处：

1）根据classpath中的artifacts的自动化配置应用程序；

2）提供非功能性特性例如安全和健康检查给到生产环境中的应用程序。

### 题2：[Spring Boot 中如何实现兼容老 Spring 项目？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题2spring-boot-中如何实现兼容老-spring-项目)<br/>
通过使用@ImportResource注解导入旧配置文件（注解写在启动类），方式如下：

```java
@SpringBootApplication
@ImportResource(locations = {"classpath:spring.xml"})
public class Application{
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
```

### 题3：[Spring Boot 自动配置原理是什么？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题3spring-boot-自动配置原理是什么)<br/>
Spring Boot的启动类中使用了@SpringBootApplication注解，里面的@EnableAutoConfiguration注解是自动配置的核心，注解内部使用@Import(AutoConfigurationImportSelector.class)（class文件用来哪些加载配置类）注解来加载配置类，并不是所有的bean都会被加载，在配置类或bean中使用@Condition来加载满足条件的bean。

@EnableAutoConfiguration给容器导入META-INF/spring.factories中定义的自动配置类，筛选有效的自动配置类。每一个自动配置类结合对应的xxxProperties.java读取配置文件进行自动配置功能

### 题4：[Spring Boot 如何禁用某些自动配置特性？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题4spring-boot-如何禁用某些自动配置特性)<br/>
禁用某些自动配置特性，可以使用@EnableAutoConfiguration注解的exclude属性来指明。

例如，下面的代码段是使DataSourceAutoConfiguration无效：

```java
// other annotations
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class MyConfiguration { }
```

**使用@SpringBootApplication注解**

将@EnableAutoConfiguration作为元注解的项，来启用自动化配置，能够使用相同名字的属性来禁用自动化配置：

```java
// other annotations
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class MyConfiguration { }
```
也可以使用spring.autoconfigure.exclude环境属性来禁用自动化配置。application.properties文件中增加如下配置内容：

```xml
spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
```

### 题5：[什么是 Spring Batch？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题5什么是-spring-batch)<br/>
Spring Boot Batch提供可重用的函数，这些函数在处理大量记录时非常重要，包括日志/跟踪，事务管理，作业处理统计信息，作业重新启动，跳过和资源管理。

Spring Boot Batch还提供了更先进的技术服务和功能，通过优化和分区技术，可以实现极高批量和高性能批处理作业。简单以及复杂的大批量批处理作业可以高度可扩展的方式利用框架处理重要大量的信息。

### 题6：[如何使用 Maven 来构建一个 Spring Boot 程序？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题6如何使用-maven-来构建一个-spring-boot-程序)<br/>
就像引入其他库一样，可以在Maven工程中加入SpringBoot依赖。然而，最好是从spring-boot-starter-parent 项目中继承以及声明依赖到Spring Boot starters。这样做可以使得项目可以重用SpringBoot的默认配置。

继承spring-boot-starter-parent项目依赖很简单，只需要在pom.xml中定义一个parent节点：

```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.1.1.RELEASE</version>
</parent>
```

可以在Maven central中找到spring-boot-starter-parent的最新版本。

### 题7：[Spring Boot  jar 和普通 jar 有什么区别？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题7spring-boot--jar-和普通-jar-有什么区别)<br/>
Spring Boot项目最终打包成的jar是可执行jar，这种jar可以直接通过java-jarxxx.jar命令来运行，这种jar不可以作为普通的jar被其他项目依赖，即使依赖了也无法使用其中的类。

Spring Boot的jar无法被其他项目依赖，主要还是他和普通jar的结构不同。普通的jar包，解压后直接就是包名，包里就是开发的代码，而Spring Boot打包成的可执行jar解压后，在\BOOT-INF\classes目录下才是开发的代码，因此无法被直接引用。

如果非要引用，可以在pom.xml文件中增加配置，将Spring Boot项目打包成两个jar，一个可执行，一个可引用。

### 题8：[如何实现 Spring Boot 应用程序的安全性？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题8如何实现-spring-boot-应用程序的安全性)<br/>
为了实现Spring Boot的安全性，可以使用spring-boot-starter-security依赖项，并且必须添加安全配置。

spring-boot-starter-security只需要很少的代码。

配置类必须扩展WebSecurityConfigurerAdapter并覆盖其方法。

### 题9：[Spring Boot 中如何解决跨域问题？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题9spring-boot-中如何解决跨域问题)<br/>
跨域可以在前端通过JSONP来解决，但是JSONP只可以发送GET请求，无法发送其他类型的请求。

在RESTful风格的应用中，就显得非常鸡肋，因此推荐在后端通过（CORS，Cross-origin resource sharing）来解决跨域问题。

这种解决方案并非Spring Boot特有的，在传统的SSM框架中，就可以通过CORS来解决跨域问题，只不过之前是在XML文件中配置CORS，现在可以通过实现WebMvcConfigurer接口然后重写addCorsMappings方法解决跨域问题。

```java
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                .maxAge(3600);
    }
}
```

项目中前后端分离部署，所以需要解决跨域的问题。使用cookie存放用户登录的信息，在spring拦截器进行权限控制，当权限不符合时，直接返回给用户固定的json结果。

注意：当用户退出登录状态时或者token过期时，由于拦截器和跨域的顺序有问题，出现了跨域的现象。http请求先经过filter，到达servlet后才进行拦截器的处理，如果把cors放在filter中就可以优先于权限拦截器执行。

```java
@Configuration
public class CorsConfig {
    @Bean
    public CorsFilter corsFilter() {
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.addAllowedOrigin("*");
        corsConfiguration.addAllowedHeader("*");
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.setAllowCredentials(true);
        UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource = new UrlBasedCorsConfigurationSource();
        urlBasedCorsConfigurationSource.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(urlBasedCorsConfigurationSource);
    }
}
```

### 题10：[Spring Boot 核心注解都有哪些？](/docs/Spring%20Boot/最新2021年Spring%20Boot面试题及答案汇总版.md#题10spring-boot-核心注解都有哪些)<br/>
1）@SpringBootApplication*

用于Spring主类上最最最核心的注解，自动化配置文件，表示这是一个SpringBoot项目，用于开启SpringBoot的各项能力。

相当于@SpringBootConfigryation、@EnableAutoConfiguration、@ComponentScan三个注解的组合。

2）@EnableAutoConfiguration

允许SpringBoot自动配置注解，开启这个注解之后，SpringBoot就能根据当前类路径下的包或者类来配置Spring Bean。

如当前路径下有MyBatis这个Jar包，MyBatisAutoConfiguration 注解就能根据相关参数来配置Mybatis的各个Spring Bean。

3）@Configuration

Spring 3.0添加的一个注解，用来代替applicationContext.xml配置文件，所有这个配置文件里面能做到的事情都可以通过这个注解所在的类来进行注册。

4）@SpringBootConfiguration

@Configuration注解的变体，只是用来修饰Spring Boot的配置而已。

5）@ComponentScan

Spring 3.1添加的一个注解，用来代替配置文件中的component-scan配置，开启组件扫描，自动扫描包路径下的@Component注解进行注册bean实例放到context(容器)中。

6）@Conditional

Spring 4.0添加的一个注解，用来标识一个Spring Bean或者Configuration配置文件，当满足指定条件才开启配置

7）@ConditionalOnBean

组合@Conditional注解，当容器中有指定Bean才开启配置。

8）@ConditionalOnMissingBean

组合@Conditional注解，当容器中没有值当Bean才可开启配置。

9）@ConditionalOnClass

组合@Conditional注解，当容器中有指定Class才可开启配置。

10）@ConditionalOnMissingClass

组合@Conditional注解，当容器中没有指定Class才可开启配置。

11）@ConditionOnWebApplication

组合@Conditional注解，当前项目类型是WEB项目才可开启配置。

项目有以下三种类型：

① ANY：任意一个Web项目

② SERVLET： Servlet的Web项目

③ REACTIVE ：基于reactive-base的Web项目

12） @ConditionOnNotWebApplication

组合@Conditional注解，当前项目类型不是WEB项目才可开启配置。

13）@ConditionalOnProperty

组合@Conditional注解，当指定的属性有指定的值时才可开启配置。

14）@ConditionalOnExpression

组合@Conditional注解，当SpEl表达式为true时才可开启配置。

15）@ConditionOnJava

组合@Conditional注解，当运行的Java JVM在指定的版本范围时才开启配置。

16）@ConditionalResource

组合@Conditional注解，当类路径下有指定的资源才开启配置。

17）@ConditionOnJndi

组合@Conditional注解，当指定的JNDI存在时才开启配置。

18）@ConditionalOnCloudPlatform

组合@Conditional注解，当指定的云平台激活时才可开启配置。

19）@ConditiomalOnSingleCandidate

组合@Conditional注解，当制定的Class在容器中只有一个Bean，或者同时有多个但为首选时才开启配置。

20）@ConfigurationProperties

用来加载额外的配置(如.properties文件)，可用在@Configuration注解类或者@Bean注解方法上面。可看一看Spring Boot读取配置文件的几种方式。

21）@EnableConfigurationProperties

一般要配合@ConfigurationProperties注解使用，用来开启@ConfigurationProperties注解配置Bean的支持。

22）@AntoConfigureAfter

用在自动配置类上面，便是该自动配置类需要在另外指定的自动配置类配置完之后。如Mybatis的自动配置类，需要在数据源自动配置类之后。

23）@AutoConfigureBefore

用在自动配置类上面，便是该自动配置类需要在另外指定的自动配置类配置完之前。

24）@Import

Spring 3.0添加注解，用来导入一个或者多个@Configuration注解修饰的配置类。

25）@IMportReSource

Spring 3.0添加注解，用来导入一个或者多个Spring配置文件，这对Spring Boot兼容老项目非常有用，一位内有些配置文件无法通过java config的形式来配置

### 题11：spring-boot-中当-bean-存在时如何置后执行自动配置<br/>


### 题12：spring-security-和-shiro-对比有什么优缺点<br/>


### 题13：什么是-websocket<br/>


### 题14：spring-boot-需要独立的容器运行吗<br/>


### 题15：spring-boot-中如何实现定时任务<br/>


### 题16：spring-boot-支持松绑定表示什么含义<br/>


### 题17：什么是-yaml<br/>


### 题18：常见的系统架构风格有哪些?各有什么优缺点?<br/>


### 题19：spring-boot-中如何禁用-actuator-端点安全性<br/>


### 题20：如何自定义端口运行-spring-boot-应用程序<br/>


### 题21：什么是-spring-boot-框架<br/>


### 题22：什么是-javaconfig<br/>


### 题23：spring-boot-框架的优缺点<br/>


### 题24：spring-boot-是否可以使用-xml-配置<br/>


### 题25：spring-boot-支持哪几种内嵌容器<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")