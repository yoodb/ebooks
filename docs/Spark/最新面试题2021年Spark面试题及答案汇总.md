# 最新面试题2021年Spark面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spark

### 题1：[Spark sql 使用过吗？在什么项目中？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题1spark-sql-使用过吗在什么项目中)<br/>
离线ETL之类的，结合机器学习等。

### 题2：[Spark 中 Driver 功能是什么？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题2spark-中-driver-功能是什么)<br/>
1）一个Spark作业运行时包括一个Driver进程，也是作业的主进程，具有main函数，并且有SparkContext的实例，是程序的入口点。

2）功能：负责向集群申请资源，向master注册信息，负责了作业的调度，，负责作业的解析、生成Stage并调度Task到Executor上。包括DAGScheduler，TaskScheduler。

### 题3：[Spark 是什么？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题3spark-是什么)<br/>
Spark是一种快速、通用、可扩展的大数据分析引擎。

2009年诞生于加州大学伯克利分校AMPLab。2010年开源，2013年6月成为Apache孵化项目。2014年2月成为Apache顶级项目。

目前，Spark生态系统已经发展成为一个包含多个子项目的集合，其中包含SparkSQL、SparkStreaming、GraphX、MLlib等子项目。

Spark是基于内存计算的大数据并行计算框架。Spark基于内存计算，提高了在大数据环境下数据处理的实时性，同时保证了高容错性和高可伸缩性，允许用户将Spark部署在大量廉价硬件之上，形成集群。

Spark得到了众多大数据公司的支持，这些公司包括Hortonworks、IBM、Intel、Cloudera、MapR、Pivotal、百度、阿里、腾讯、京东、携程、优酷土豆。

当前百度的Spark已应用于凤巢、大搜索、直达号、百度大数据等业务；阿里利用GraphX构建了大规模的图计算和图挖掘系统，实现了很多生产系统的推荐算法；腾讯Spark集群达到8000台的规模，是当前已知的世界上最大的Spark集群。

### 题4：[Spark 中 worker 的主要工作是什么？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题4spark-中-worker-的主要工作是什么)<br/>
管理当前节点内存，CPU的使用情况，接受master发送过来的资源指令，通过executorRunner启动程序分配任务，worker就类似于包工头，管理分配新进程，做计算的服务，相当于process服务，需要注意的是：

1）worker会不会汇报当前信息给master？

worker心跳给master主要只有workid，不会以心跳的方式发送资源信息给master，这样master就知道worker是否存活，只有故障的时候才会发送资源信息；

2）worker不会运行代码，具体运行的是executor，可以运行具体application斜的业务逻辑代码，操作代码的节点，不会去运行代码。

### 题5：[Spark 程序执行时，为什么默认有时产生很多 task，如何修改 task 个数？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题5spark-程序执行时为什么默认有时产生很多-task如何修改-task-个数)<br/>
1）因为输入数据有很多task，尤其是有很多小文件的时候，有多少个输入block就会有多少个task启动；

2）spark中有partition的概念，每个partition都会对应一个task，task越多，在处理大规模数据的时候，就会越有效率。不过task并不是越多越好，如果平时测试，或者数据量没有那么大，则没有必要task数量太多。

3）参数可以通过spark_home/conf/spark-default.conf配置文件设置:

```xml
spark.sql.shuffle.partitions 50
spark.default.parallelism 10
```

**spark.sql.shuffle.partitions** 设置的是 RDD1做shuffle处理后生成的结果RDD2的分区数，默认值200。

**spark.default.parallelism** 是指RDD任务的默认并行度，Spark中所谓的并行度是指RDD中的分区数，即RDD中的Task数。

当初始RDD没有设置分区数（numPartitions或numSlice）时，则分区数采用spark.default.parallelism的取值。

### 题6：[如何解决 Spark 中的数据倾斜问题？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题6如何解决-spark-中的数据倾斜问题)<br/>
当发现数据倾斜的时候，不要急于提高executor的资源，修改参数或是修改程序。

**一、数据问题造成的数据倾斜**

找出异常的key，如果任务长时间卡在最后最后1个(几个)任务，首先要对key进行抽样分析，判断是哪些key造成的。 选取key，对数据进行抽样，统计出现的次数，根据出现次数大小排序取出前几个。

比如

```java
df.select("key").sample(false,0.1)
.(k=>(k,1)).reduceBykey(+)
.map(k=>(k._2,k._1))
.sortByKey(false).take(10)
```

如果发现多数数据分布都较为平均，而个别数据比其他数据大上若干个数量级，则说明发生了数据倾斜。
 
经过分析，倾斜的数据主要有以下三种情况: 

1）null（空值）或是一些无意义的信息()之类的，大多是这个原因引起。

2）无效数据，大量重复的测试数据或是对结果影响不大的有效数据。

3）有效数据，业务导致的正常数据分布。

**解决办法**

第1，2种情况，直接对数据进行过滤即可（因为该数据对当前业务不会产生影响）。

第3种情况则需要进行一些特殊操作，常见的有以下几种做法

1）隔离执行，将异常的key过滤出来单独处理，最后与正常数据的处理结果进行union操作。

2）对key先添加随机值，进行操作后，去掉随机值，再进行一次操作。

3）使用reduceByKey 代替 groupByKey（reduceByKey用于对每个key对应的多个value进行merge操作，最重要的是它能够在本地先进行merge操作，并且merge操作可以通过函数自定义）

4）使用map join。

**案例**

如果使用reduceByKey因为数据倾斜造成运行失败的问题。具体操作流程如下: 

1）将原始的 key 转化为 key + 随机值(例如Random.nextInt)

2）对数据进行 reduceByKey(func)

3）将key+随机值 转成 key

4）再对数据进行reduceByKey(func)

案例操作流程分析：

假设说有倾斜的Key，给所有的Key加上一个随机数，然后进行reduceByKey操作；此时同一个Key会有不同的随机数前缀，在进行reduceByKey操作的时候原来的一个非常大的倾斜的Key就分而治之变成若干个更小的Key，不过此时结果和原来不一样，怎么解决？进行map操作，目的是把随机数前缀去掉，然后再次进行reduceByKey操作，这样就可以把原本倾斜的Key通过分而治之方案分散开来，最后又进行了全局聚合。

注意1: 如果此时依旧存在问题，建议筛选出倾斜的数据单独处理。最后将这份数据与正常的数据进行union即可。

注意2: 单独处理异常数据时，可以配合使用Map Join解决。

**二、Spark使用不当造成的数据倾斜**

**提高shuffle并行度**

1）dataFrame和sparkSql可以设置spark.sql.shuffle.partitions参数控制shuffle的并发度，默认为200。

2）rdd操作可以设置spark.default.parallelism控制并发度，默认参数由不同的Cluster Manager控制。

3）局限性: 只是让每个task执行更少的不同的key。无法解决个别key特别大的情况造成的倾斜，如果某些key的大小非常大，即使一个task单独执行它，也会受到数据倾斜的困扰。

**使用map join 代替reduce join**

在小表不是特别大（取决于executor大小）的情况下使用，可以使程序避免shuffle的过程，自然也就没有数据倾斜的困扰。这是因为局限性的问题，也就是先将小数据发送到每个executor上，所以数据量不能太大。

### 题7：[为什么要使用 Yarn 部署 Spark？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题7为什么要使用-yarn-部署-spark)<br/>
这是因为Yarn支持动态资源配置。Standalone模式只支持简单的固定资源分配策略，每个任务固定数量的core，各Job按顺序依次分配在资源，资源不够的时候就排队。这种模式比较适合单用户的情况，多用户的情境下，会有可能有些用户的任务得不到资源。

Yarn作为通用的种子资源调度平台，除了Spark提供调度服务之外，还可以为其他系统提供调度，如Hadoop MapReduce、Hive等。

### 题8：[Spark 技术栈有哪些组件，适合什么应用场景？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题8spark-技术栈有哪些组件适合什么应用场景)<br/>
1）Spark core：是其它组件的基础，spark的内核，主要包含：有向循环图、RDD、Lingage、Cache、broadcast等，并封装了底层通讯框架，是Spark的基础。

2）SparkStreaming是一个对实时数据流进行高通量、容错处理的流式处理系统，可以对多种数据源（如Kdfka、Flume、Twitter、Zero和TCP 套接字）进行类似Map、Reduce和Join等复杂操作，将流式计算分解成一系列短小的批处理作业。

3）Spark sql：Shark是SparkSQL的前身，Spark SQL的一个重要特点是其能够统一处理关系表和RDD，使得开发人员可以轻松地使用SQL命令进行外部查询，同时进行更复杂的数据分析

4）BlinkDB：是一个用于在海量数据上运行交互式 SQL 查询的大规模并行查询引擎，它允许用户通过权衡数据精度来提升查询响应时间，其数据的精度被控制在允许的误差范围内。

5）MLBase是Spark生态圈的一部分专注于机器学习，让机器学习的门槛更低，让一些可能并不了解机器学习的用户也能方便地使用MLbase。MLBase分为四部分：MLlib，MLI、ML Optimizer和MLRuntime。

6）GraphX是Spark中用于图和图并行计算。

### 题9：[Spark 中 collect 功能是什么，其底层是如何实现的？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题9spark-中-collect-功能是什么其底层是如何实现的)<br/>
driver通过collect把集群中各个节点的内容收集过来汇总成结果，collect返回结果是Array类型的，collect把各个节点上的数据抓过来，抓过来数据是Array型，collect对Array抓过来的结果进行合并，合并后Array中只有一个元素，是tuple类型（KV类型的）的。

### 题10：[Spark 中 RDD 通过 Linage（记录数据更新）的方式为何很高效？](/docs/Spark/最新面试题2021年Spark面试题及答案汇总.md#题10spark-中-rdd-通过-linage记录数据更新的方式为何很高效)<br/>
1）lazy记录了数据的来源，RDD是不可变的，且是lazy级别的，且rDD之间构成了链条，lazy是弹性的基石。由于RDD不可变，所以每次操作就产生新的rdd，不存在全局修改的问题，控制难度下降，所有有计算链条将复杂计算链条存储下来，计算的时候从后往前回溯900步是上一个stage的结束，要么就checkpoint。

2）记录原数据，是每次修改都记录，代价很大。如果修改一个集合，代价就很小，官方说rdd是粗粒度的操作，是为了效率，为了简化，每次都是操作数据集合，写或者修改操作，都是基于集合的rdd的写操作是粗粒度的，rdd的读操作既可以是粗粒度的也可以是细粒度，读可以读其中的一条条的记录。

3）简化复杂度，是高效率的一方面，写的粗粒度限制了使用场景，如网络爬虫，现实世界中，大多数写是粗粒度的场景。

### 题11：spark-中列举一些你常用的-action<br/>


### 题12：spark-中-rdddagstage-如何理解<br/>


### 题13：spark-中-cache-和-persist-有什么区别<br/>


### 题14：spark-为什么要持久化一般什么场景下要进行-persist-操作<br/>


### 题15：spark-中-map-和-flatmap-有什么区别<br/>


### 题16：spark-中调优方式都有哪些<br/>


### 题17：spark-中-rdd-有哪些不足之处<br/>


### 题18：为什么-spark-比-mapreduce-快<br/>


### 题19：概述一下-spark-中的常用算子区别<br/>


### 题20：spark-streaming-工作流程和-storm-有什么区别<br/>


### 题21：spark-中-rdd-弹性表现在哪几点<br/>


### 题22：spark.sql.shuffle.partitions-和-spark.default.parallelism-有什么区别和联系<br/>


### 题23：spark-运行架构的特点是什么<br/>


### 题24：spark-中-ml-和-mllib-两个包区别和联系<br/>


### 题25：spark-中宽依赖窄依赖如何理解<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")