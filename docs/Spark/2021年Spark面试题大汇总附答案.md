# 2021年Spark面试题大汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spark

### 题1：[Spark 中 worker 的主要工作是什么？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题1spark-中-worker-的主要工作是什么)<br/>
管理当前节点内存，CPU的使用情况，接受master发送过来的资源指令，通过executorRunner启动程序分配任务，worker就类似于包工头，管理分配新进程，做计算的服务，相当于process服务，需要注意的是：

1）worker会不会汇报当前信息给master？

worker心跳给master主要只有workid，不会以心跳的方式发送资源信息给master，这样master就知道worker是否存活，只有故障的时候才会发送资源信息；

2）worker不会运行代码，具体运行的是executor，可以运行具体application斜的业务逻辑代码，操作代码的节点，不会去运行代码。

### 题2：[说一说 Spark 中 yarn-cluster 和 yarn-client 有什么异同点？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题2说一说-spark-中-yarn-cluster-和-yarn-client-有什么异同点)<br/>
1、cluster模式会在集群的某个节点上为Spark程序启动一个称为Master的进程，然后Driver程序会运行正在这个Master进程内部，由这种进程来启动Driver程序，客户端完成提交的步骤后就可以退出，不需要等待Spark程序运行结束，这是四一职中适合生产环境的运行方式。

2、client模式也有一个Master进程，但是Driver程序不会运行在这个Master进程内部，而是运行在本地，只是通过Master来申请资源，直到运行结束，这种模式非常适合需要交互的计算。显然Driver在client模式下会对本地资源造成一定的压力。

### 题3：[Spark 中 ML 和 MLLib 两个包区别和联系？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题3spark-中-ml-和-mllib-两个包区别和联系)<br/>
1、技术角度上，面向的数据集类型不同

ML的API是面向Dataset的（Dataframe 是 Dataset的子集，也就是Dataset[Row]）， mllib是面对RDD的。

Dataset和RDD有啥不一样呢？

Dataset的底端是RDD。Dataset对RDD进行了更深一层的优化，比如说有sql语言类似的黑魔法，Dataset支持静态类型分析所以在compile time就能报错，各种combinators（map，foreach 等）性能会更好，等等。

2、编程过程上，构建机器学习算法的过程不同

ML提倡使用pipelines，把数据想成水，水从管道的一段流入，从另一端流出。ML是1.4比 Mllib更高抽象的库，它解决如果简洁的设计一个机器学习工作流的问题，而不是具体的某种机器学习算法。未来这两个库会并行发展。


### 题4：[Spark 程序执行时，为什么默认有时产生很多 task，如何修改 task 个数？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题4spark-程序执行时为什么默认有时产生很多-task如何修改-task-个数)<br/>
1）因为输入数据有很多task，尤其是有很多小文件的时候，有多少个输入block就会有多少个task启动；

2）spark中有partition的概念，每个partition都会对应一个task，task越多，在处理大规模数据的时候，就会越有效率。不过task并不是越多越好，如果平时测试，或者数据量没有那么大，则没有必要task数量太多。

3）参数可以通过spark_home/conf/spark-default.conf配置文件设置:

```xml
spark.sql.shuffle.partitions 50
spark.default.parallelism 10
```

**spark.sql.shuffle.partitions** 设置的是 RDD1做shuffle处理后生成的结果RDD2的分区数，默认值200。

**spark.default.parallelism** 是指RDD任务的默认并行度，Spark中所谓的并行度是指RDD中的分区数，即RDD中的Task数。

当初始RDD没有设置分区数（numPartitions或numSlice）时，则分区数采用spark.default.parallelism的取值。

### 题5：[Spark 中宽依赖、窄依赖如何理解？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题5spark-中宽依赖窄依赖如何理解)<br/>
1、窄依赖指的是每一个parent RDD的partition最多被子RDD的一个partition使用（一子一亲）

2、宽依赖指的是多个子RDD的partition会依赖同一个parent RDD的partition（多子一亲）
RDD作为数据结构，本质上是一个只读的分区记录集合。一个RDD可以包含多个分区，每个分区就是一个dataset片段。RDD可以相互依赖。

首先，窄依赖可以支持在同一个cluster node上，以pipeline形式执行多条命令（也叫同一个 stage 的操作），例如在执行了map后，紧接着执行filter。相反，宽依赖需要所有的父分区都是可用的，可能还需要调用类似 MapReduce 之类的操作进行跨节点传递。

其次，则是从失败恢复的角度考虑。窄依赖的失败恢复更有效，因为它只需要重新计算丢失的parent partition即可，而且可以并行地在不同节点进行重计算（一台机器太慢就会分配到多个节点进行），相反的是宽依赖牵涉RDD各级的多个parent partition。

### 题6：[Spark 中 collect 功能是什么，其底层是如何实现的？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题6spark-中-collect-功能是什么其底层是如何实现的)<br/>
driver通过collect把集群中各个节点的内容收集过来汇总成结果，collect返回结果是Array类型的，collect把各个节点上的数据抓过来，抓过来数据是Array型，collect对Array抓过来的结果进行合并，合并后Array中只有一个元素，是tuple类型（KV类型的）的。

### 题7：[Spark 为什么要进行序列化？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题7spark-为什么要进行序列化)<br/>
序列化可以减少数据的体积，减少存储空间，高效存储和传输数据，不好的是使用的时候要反序列化，非常消耗CPU。

### 题8：[Spark 为什么要持久化，一般什么场景下要进行 persist 操作？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题8spark-为什么要持久化一般什么场景下要进行-persist-操作)<br/>
**为什么要进行持久化？**

spark所有复杂一点的算法都会有persist身影,spark默认数据放在内存，spark很多内容都是放在内存的，非常适合高速迭代，1000个步骤。

只有第一个输入数据，中间不产生临时数据，但分布式系统风险很高，所以容易出错，就要容错，rdd出错或者分片可以根据血统算出来，如果没有对父rdd进行persist 或者cache的化，就需要重头做。

**使用persist场景**

1）某个步骤计算非常耗时，需要进行persist持久化

2）计算链条非常长，重新恢复要算很多步骤，很好使，persist

3）checkpoint所在的rdd要持久化persist

lazy级别，框架发现有checnkpoint，checkpoint时单独触发一个job，需要重算一遍，checkpoint前

要持久化，写个rdd.cache或者rdd.persist，将结果保存起来，再写checkpoint操作，这样执行起来会非常快，不需要重新计算rdd链条了。checkpoint之前一定会进行persist。

4）shuffle之后为什么要persist？shuffle要进性网络传输，风险很大，数据丢失重来，恢复代价很大

5）shuffle之前进行persist，框架默认将数据持久化到磁盘，这个是框架自动做的。

### 题9：[Spark 中 RDD 弹性表现在哪几点？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题9spark-中-rdd-弹性表现在哪几点)<br/>
1）自动的进行内存和磁盘的存储切换；

2）基于Lingage的高效容错；

3）task如果失败会自动进行特定次数的重试；

4）stage如果失败会自动进行特定次数的重试，而且只会计算失败的分片；

5）checkpoint和persist，数据计算之后持久化缓存；

6）数据调度弹性，DAG TASK调度和资源无关；

7）数据分片的高度弹性

a. 分片很多碎片可以合并成大的

b. par

### 题10：[Spark 中常规的容错方式有哪几种类型？](/docs/Spark/2021年Spark面试题大汇总附答案.md#题10spark-中常规的容错方式有哪几种类型)<br/>
1）数据检查点，会发生拷贝，浪费资源。

2）记录数据的更新，每次更新都会记录下来，比较复杂且比较消耗性能。

### 题11：spark-有什么优越性<br/>


### 题12：spark-中-rdd-有几种操作类型<br/>


### 题13：spark-有几种部署模式各自都有什么特点<br/>


### 题14：spark-中-rdd-是什么<br/>


### 题15：spark-中-rdd-通过-linage记录数据更新的方式为何很高效<br/>


### 题16：spark-中-rdd-有哪些不足之处<br/>


### 题17：spark-是什么<br/>


### 题18：为什么-spark-比-mapreduce-快<br/>


### 题19：spark-技术栈有哪些组件适合什么应用场景<br/>


### 题20：spark-streaming-工作流程和-storm-有什么区别<br/>


### 题21：spark-中-driver-功能是什么<br/>


### 题22：spark-中如何实现获取-topn<br/>


### 题23：spark-中-cache-和-persist-有什么区别<br/>


### 题24：spark-sql-使用过吗在什么项目中<br/>


### 题25：hadoop-和-spark-的-shuffle-有什么差异<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")