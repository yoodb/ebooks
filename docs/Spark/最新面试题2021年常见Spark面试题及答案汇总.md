# 最新面试题2021年常见Spark面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spark

### 题1：[Spark 中 RDD 通过 Linage（记录数据更新）的方式为何很高效？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题1spark-中-rdd-通过-linage记录数据更新的方式为何很高效)<br/>
1）lazy记录了数据的来源，RDD是不可变的，且是lazy级别的，且rDD之间构成了链条，lazy是弹性的基石。由于RDD不可变，所以每次操作就产生新的rdd，不存在全局修改的问题，控制难度下降，所有有计算链条将复杂计算链条存储下来，计算的时候从后往前回溯900步是上一个stage的结束，要么就checkpoint。

2）记录原数据，是每次修改都记录，代价很大。如果修改一个集合，代价就很小，官方说rdd是粗粒度的操作，是为了效率，为了简化，每次都是操作数据集合，写或者修改操作，都是基于集合的rdd的写操作是粗粒度的，rdd的读操作既可以是粗粒度的也可以是细粒度，读可以读其中的一条条的记录。

3）简化复杂度，是高效率的一方面，写的粗粒度限制了使用场景，如网络爬虫，现实世界中，大多数写是粗粒度的场景。

### 题2：[Spark 中 cache 和 persist 有什么区别？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题2spark-中-cache-和-persist-有什么区别)<br/>
**cache：** 缓存数据，默认是缓存在内存中，其本质还是调用 persist；

**persist：** 缓存数据，有丰富的数据缓存策略。数据可以保存在内存也可以保存在磁盘中，使用的时候指定对应的缓存级别即可。

### 题3：[Spark 中主要包括哪些组件？ ](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题3spark-中主要包括哪些组件-)<br/>
1）master：管理集群和节点，不参与计算。 

2）worker：计算节点，进程本身不参与计算，和master汇报。 

3）Driver：运行程序的main方法，创建spark context对象。 

4）spark context：控制整个application的生命周期，包括dagsheduler和task scheduler等组件。 

5）client：用户提交程序的入口。

### 题4：[Spark 中 Driver 功能是什么？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题4spark-中-driver-功能是什么)<br/>
1）一个Spark作业运行时包括一个Driver进程，也是作业的主进程，具有main函数，并且有SparkContext的实例，是程序的入口点。

2）功能：负责向集群申请资源，向master注册信息，负责了作业的调度，，负责作业的解析、生成Stage并调度Task到Executor上。包括DAGScheduler，TaskScheduler。

### 题5：[Spark Streaming 工作流程和 Storm 有什么区别？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题5spark-streaming-工作流程和-storm-有什么区别)<br/>
Spark Streaming与Storm都可以用于进行实时流计算。但是他们两者的区别是非常大的。

Spark Streaming和Storm的计算模型完全不一样，Spark Streaming是基于RDD的，因此需要将一小段时间内的，比如1秒内的数据，收集起来，作为一个RDD，然后再针对这个batch的数据进行处理。而Storm却可以做到每来一条数据，都可以立即进行处理和计算。因此，Spark Streaming实际上严格意义上来说，只能称作准实时的流计算框架；而Storm是真正意义上的实时计算框架。 

Storm支持的一项高级特性，是Spark Streaming暂时不具备的，即Storm支持在分布式流式计算程序（Topology）在运行过程中，可以动态地调整并行度，从而动态提高并发处理能力。而Spark Streaming是无法动态调整并行度的。但是Spark Streaming也有其优点，首先Spark Streaming由于是基于batch进行处理的，因此相较于 Storm 基于单条数据进行处理，具有数倍甚至数十倍的吞吐量。

Spark Streaming由于也身处于Spark生态圈内，因此Spark Streaming可以与Spark Core、Spark SQL，甚至是Spark MLlib、Spark GraphX进行无缝整合。流式处理完的数据，可以立即进行各种map、reduce转换操作，可以立即使用sql进行查询，甚至可以立即使用machine learning或者图计算算法进行处理。这种一站式的大数据处理功能和优势，是Storm无法匹敌的。 因此，综合上述来看，通常在对实时性要求特别高，而且实时数据量不稳定，比如在白天有高峰期的情况下，可以选择使用Storm。但是如果是对实时性要求一般，允许1秒的准实时处理，而且不要求动态调整并行度的话，选择Spark Streaming是更好的选择。

|对比点|Storm|Spark Streaming|
|-|-|-|
|实时计算模型|纯实时，来一条数据，处理一条数据|准实时，对一个时间段内的数据收集起来，作为一个RDD，再处理|
|实时计算延迟度|毫秒级|秒级|
|吞吐量|低|高|
|事务机制|支持完善|支持，但不够完善|
|健壮性 / 容错性|ZooKeeper，Acker，非常强|Checkpoint，WAL，一般|
|动态调整并行度|支持|不支持|


### 题6：[Spark 中宽依赖、窄依赖如何理解？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题6spark-中宽依赖窄依赖如何理解)<br/>
1、窄依赖指的是每一个parent RDD的partition最多被子RDD的一个partition使用（一子一亲）

2、宽依赖指的是多个子RDD的partition会依赖同一个parent RDD的partition（多子一亲）
RDD作为数据结构，本质上是一个只读的分区记录集合。一个RDD可以包含多个分区，每个分区就是一个dataset片段。RDD可以相互依赖。

首先，窄依赖可以支持在同一个cluster node上，以pipeline形式执行多条命令（也叫同一个 stage 的操作），例如在执行了map后，紧接着执行filter。相反，宽依赖需要所有的父分区都是可用的，可能还需要调用类似 MapReduce 之类的操作进行跨节点传递。

其次，则是从失败恢复的角度考虑。窄依赖的失败恢复更有效，因为它只需要重新计算丢失的parent partition即可，而且可以并行地在不同节点进行重计算（一台机器太慢就会分配到多个节点进行），相反的是宽依赖牵涉RDD各级的多个parent partition。

### 题7：[Spark 是什么？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题7spark-是什么)<br/>
Spark是一种快速、通用、可扩展的大数据分析引擎。

2009年诞生于加州大学伯克利分校AMPLab。2010年开源，2013年6月成为Apache孵化项目。2014年2月成为Apache顶级项目。

目前，Spark生态系统已经发展成为一个包含多个子项目的集合，其中包含SparkSQL、SparkStreaming、GraphX、MLlib等子项目。

Spark是基于内存计算的大数据并行计算框架。Spark基于内存计算，提高了在大数据环境下数据处理的实时性，同时保证了高容错性和高可伸缩性，允许用户将Spark部署在大量廉价硬件之上，形成集群。

Spark得到了众多大数据公司的支持，这些公司包括Hortonworks、IBM、Intel、Cloudera、MapR、Pivotal、百度、阿里、腾讯、京东、携程、优酷土豆。

当前百度的Spark已应用于凤巢、大搜索、直达号、百度大数据等业务；阿里利用GraphX构建了大规模的图计算和图挖掘系统，实现了很多生产系统的推荐算法；腾讯Spark集群达到8000台的规模，是当前已知的世界上最大的Spark集群。

### 题8：[Spark 有什么优越性？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题8spark-有什么优越性)<br/>
1）更高的性能。因为数据被加载到集群主机的分布式内存中。数据可以被快速的转换迭代，并缓存用以后续的频繁访问需求。在数据全部加载到内存的情况下，Spark可以比Hadoop快100倍，在内存不够存放所有数据的情况下快hadoop10倍。 

2）通过建立在Java、Scala、Python、SQL（应对交互式查询）的标准API以方便各行各业使用，同时还含有大量开箱即用的机器学习库。

3）与现有Hadoop 1和2.x(YARN)生态兼容，因此机构可以无缝迁移。

4）方便下载和安装。方便的shell（REPL: Read-Eval-Print-Loop）可以对API进行交互式的学习。 

5）借助高等级的架构提高生产力，从而可以讲精力放到计算上。

### 题9：[Spark 技术栈有哪些组件，适合什么应用场景？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题9spark-技术栈有哪些组件适合什么应用场景)<br/>
1）Spark core：是其它组件的基础，spark的内核，主要包含：有向循环图、RDD、Lingage、Cache、broadcast等，并封装了底层通讯框架，是Spark的基础。

2）SparkStreaming是一个对实时数据流进行高通量、容错处理的流式处理系统，可以对多种数据源（如Kdfka、Flume、Twitter、Zero和TCP 套接字）进行类似Map、Reduce和Join等复杂操作，将流式计算分解成一系列短小的批处理作业。

3）Spark sql：Shark是SparkSQL的前身，Spark SQL的一个重要特点是其能够统一处理关系表和RDD，使得开发人员可以轻松地使用SQL命令进行外部查询，同时进行更复杂的数据分析

4）BlinkDB：是一个用于在海量数据上运行交互式 SQL 查询的大规模并行查询引擎，它允许用户通过权衡数据精度来提升查询响应时间，其数据的精度被控制在允许的误差范围内。

5）MLBase是Spark生态圈的一部分专注于机器学习，让机器学习的门槛更低，让一些可能并不了解机器学习的用户也能方便地使用MLbase。MLBase分为四部分：MLlib，MLI、ML Optimizer和MLRuntime。

6）GraphX是Spark中用于图和图并行计算。

### 题10：[为什么要使用 Yarn 部署 Spark？](/docs/Spark/最新面试题2021年常见Spark面试题及答案汇总.md#题10为什么要使用-yarn-部署-spark)<br/>
这是因为Yarn支持动态资源配置。Standalone模式只支持简单的固定资源分配策略，每个任务固定数量的core，各Job按顺序依次分配在资源，资源不够的时候就排队。这种模式比较适合单用户的情况，多用户的情境下，会有可能有些用户的任务得不到资源。

Yarn作为通用的种子资源调度平台，除了Spark提供调度服务之外，还可以为其他系统提供调度，如Hadoop MapReduce、Hive等。

### 题11：spark-中-rdd-有哪些不足之处<br/>


### 题12：spark-中常规的容错方式有哪几种类型<br/>


### 题13：spark-为什么要持久化一般什么场景下要进行-persist-操作<br/>


### 题14：概述一下-spark-中的常用算子区别<br/>


### 题15：spark-如何处理不能被序列化的对象<br/>


### 题16：spark-有几种部署模式各自都有什么特点<br/>


### 题17：spark-为什么要进行序列化<br/>


### 题18：spark-中-worker-的主要工作是什么<br/>


### 题19：spark-中列举一些你常用的-action<br/>


### 题20：spark-中-collect-功能是什么其底层是如何实现的<br/>


### 题21：说一说-cogroup-rdd-实现原理在什么场景下使用过-rdd<br/>


### 题22：spark-程序执行时为什么默认有时产生很多-task如何修改-task-个数<br/>


### 题23：spark-中-rdd-弹性表现在哪几点<br/>


### 题24：spark-中常见的-join-操作优化有哪些分类<br/>


### 题25：spark-中-map-和-mappartitions-有什么区别<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")