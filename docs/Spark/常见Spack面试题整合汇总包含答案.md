# 常见Spack面试题整合汇总包含答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Spark

### 题1：[Spark 有什么优越性？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题1spark-有什么优越性)<br/>
1）更高的性能。因为数据被加载到集群主机的分布式内存中。数据可以被快速的转换迭代，并缓存用以后续的频繁访问需求。在数据全部加载到内存的情况下，Spark可以比Hadoop快100倍，在内存不够存放所有数据的情况下快hadoop10倍。 

2）通过建立在Java、Scala、Python、SQL（应对交互式查询）的标准API以方便各行各业使用，同时还含有大量开箱即用的机器学习库。

3）与现有Hadoop 1和2.x(YARN)生态兼容，因此机构可以无缝迁移。

4）方便下载和安装。方便的shell（REPL: Read-Eval-Print-Loop）可以对API进行交互式的学习。 

5）借助高等级的架构提高生产力，从而可以讲精力放到计算上。

### 题2：[Spark 运行架构的特点是什么？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题2spark-运行架构的特点是什么)<br/>
每个Application获取专属的executor进程，该进程在Application期间一直驻留，并以多线程方式运行tasks。

Spark任务与资源管理器无关，只要能够获取 executor进程，并能保持相互通信就可以了。

提交SparkContext的Client应该靠近Worker节点（运行 Executor 的节点)，最好是在同一个Rack里，因为Spark程序运行过程中SparkContext和Executor之间有大量的信息交换；如果想在远程集群中运行，最好使用 RPC将SparkContext提交给集群，不要远离Worker运行SparkContext。Task采用了数据本地性和推测执行的优化机制。

### 题3：[Spark 技术栈有哪些组件，适合什么应用场景？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题3spark-技术栈有哪些组件适合什么应用场景)<br/>
1）Spark core：是其它组件的基础，spark的内核，主要包含：有向循环图、RDD、Lingage、Cache、broadcast等，并封装了底层通讯框架，是Spark的基础。

2）SparkStreaming是一个对实时数据流进行高通量、容错处理的流式处理系统，可以对多种数据源（如Kdfka、Flume、Twitter、Zero和TCP 套接字）进行类似Map、Reduce和Join等复杂操作，将流式计算分解成一系列短小的批处理作业。

3）Spark sql：Shark是SparkSQL的前身，Spark SQL的一个重要特点是其能够统一处理关系表和RDD，使得开发人员可以轻松地使用SQL命令进行外部查询，同时进行更复杂的数据分析

4）BlinkDB：是一个用于在海量数据上运行交互式 SQL 查询的大规模并行查询引擎，它允许用户通过权衡数据精度来提升查询响应时间，其数据的精度被控制在允许的误差范围内。

5）MLBase是Spark生态圈的一部分专注于机器学习，让机器学习的门槛更低，让一些可能并不了解机器学习的用户也能方便地使用MLbase。MLBase分为四部分：MLlib，MLI、ML Optimizer和MLRuntime。

6）GraphX是Spark中用于图和图并行计算。

### 题4：[Spark 程序执行时，为什么默认有时产生很多 task，如何修改 task 个数？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题4spark-程序执行时为什么默认有时产生很多-task如何修改-task-个数)<br/>
1）因为输入数据有很多task，尤其是有很多小文件的时候，有多少个输入block就会有多少个task启动；

2）spark中有partition的概念，每个partition都会对应一个task，task越多，在处理大规模数据的时候，就会越有效率。不过task并不是越多越好，如果平时测试，或者数据量没有那么大，则没有必要task数量太多。

3）参数可以通过spark_home/conf/spark-default.conf配置文件设置:

```xml
spark.sql.shuffle.partitions 50
spark.default.parallelism 10
```

**spark.sql.shuffle.partitions** 设置的是 RDD1做shuffle处理后生成的结果RDD2的分区数，默认值200。

**spark.default.parallelism** 是指RDD任务的默认并行度，Spark中所谓的并行度是指RDD中的分区数，即RDD中的Task数。

当初始RDD没有设置分区数（numPartitions或numSlice）时，则分区数采用spark.default.parallelism的取值。

### 题5：[spark.sql.shuffle.partitions 和 spark.default.parallelism 有什么区别和联系？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题5spark.sql.shuffle.partitions-和-spark.default.parallelism-有什么区别和联系)<br/>
spark.default.parallelism只有在处理RDD时有效；而spark.sql.shuffle.partitions则是只对SparkSQL有效。

**spark.sql.shuffle.partitions：** 设置的是 RDD1做shuffle处理后生成的结果RDD2的分区数。

默认值：200

**spark.default.parallelism: ** 设置的是 RDD1做shuffle处理/并行处理(窄依赖算子)后生成的结果RDD2的分区数。

默认值：

对于分布式的shuffle算子, 默认值使用了结果RDD2所依赖的所有父RDD中分区数最大的, 作为自己的分区数。

对于并行处理算子（窄依赖的），有父依赖的，结果RDD分区数=父RDD分区数，没有父依赖的看集群配置：

Local mode:给定的core个数

Mesos fine grained mode: 8

Others: max(RDD分区数为总core数, 2)

### 题6：[Spark 中 RDD 有哪些不足之处？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题6spark-中-rdd-有哪些不足之处)<br/>
1）不支持细粒度的写和更新操作（如网络爬虫），spark写数据是粗粒度的。

所谓粗粒度，就是批量写入数据，为了提高效率。但是读数据是细粒度的也就是说可以一条条的读。

2）不支持增量迭代计算，Flink支持。

### 题7：[Spark 中调优方式都有哪些？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题7spark-中调优方式都有哪些)<br/>
**资源参数调优**

1、num-executors：设置Spark作业总共要用多少个Executor进程来执行

2、executor-memory：设置每个Executor进程的内存

3、executor-cores：设置每个Executor进程的CPU core数量

4、driver-memory：设置Driver进程的内存

5、spark.default.parallelism：设置每个stage的默认task数量

**开发调优**

1、避免创建重复的RDD

2、尽可能复用同一个RDD

3、对多次使用的RDD进行持久化

4、尽量避免使用shuffle类算子

5、使用map-side预聚合的shuffle操作

6、使用高性能的算子

①使用reduceByKey/aggregateByKey替代groupByKey

②使用mapPartitions替代普通map 

③使用foreachPartitions替代foreach 

④使用filter之后进行coalesce操作

⑤使用repartitionAndSortWithinPartitions替代repartition与sort类操作

7、广播大变量：在算子函数中使用到外部变量时，默认情况下，Spark会将该变量复制多个副本，通过网络传输到task中，此时每个task都有一个变量副本。如果变量本身比较大的话（比如100M，甚至1G），那么大量的变量副本在网络中传输的性能开销，以及在各个节点的Executor中占用过多内存导致的频繁GC(垃圾回收)，都会极大地影响性能。

8、使用Kryo优化序列化性能

9、优化数据结构：在可能以及合适的情况下，使用占用内存较少的数据结构，但是前提是要保证代码的可维护性。

### 题8：[Spark 中 RDD、DAG、Stage 如何理解？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题8spark-中-rdddagstage-如何理解)<br/>
DAG Spark中使用DAG对RDD的关系进行建模，描述了RDD的依赖关系，这种关系也被称之为lineage（血缘），RDD的依赖关系使用Dependency维护。DAG在Spark中的对应的实现为DAGScheduler。

RDD RDD是Spark的灵魂，也称为弹性分布式数据集。一个RDD代表一个可以被分区的只读数据集。RDD内部可以有许多分区(partitions)，每个分区又拥有大量的记录(records)。

Rdd的五个特征：
1）dependencies: 建立RDD的依赖关系，主要RDD之间是宽窄依赖的关系，具有窄依赖关系的RDD可以在同一个stage中进行计算。

2）partition: 一个RDD会有若干个分区，分区的大小决定了对这个RDD计算的粒度，每个RDD的分区的计算都在一个单独的任务中进行。

3）preferedlocations: 按照“移动数据不如移动计算”原则，在Spark进行任务调度的时候，优先将任务分配到数据块存储的位置。

4）compute: Spark中的计算都是以分区为基本单位的，compute函数只是对迭代器进行复合，并不保存单次计算的结果。

5）partitioner: 只存在于（K,V）类型的RDD中，非（K,V）类型的partitioner的值就是None。

RDD的算子主要分成2类，action和transformation。这里的算子概念，可以理解成就是对数据集的变换。action会触发真正的作业提交，而transformation算子是不会立即触发作业提交的。每一个transformation方法返回一个新的 RDD。只是某些transformation比较复杂，会包含多个子transformation，因而会生成多个RDD。这就是实际RDD个数比我们想象的多一些 的原因。通常是，当遇到action算子时会触发一个job的提交，然后反推回去看前面的transformation算子，进而形成一张有向无环图。

Stage在DAG中又进行stage的划分，划分的依据是依赖是否是shuffle的，每个stage又可以划分成若干task。接下来的事情就是driver发送task到executor，executor自己的线程池去执行这些task，完成之后将结果返回给driver。action算子是划分不同job的依据。

### 题9：[Spark 中 worker 的主要工作是什么？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题9spark-中-worker-的主要工作是什么)<br/>
管理当前节点内存，CPU的使用情况，接受master发送过来的资源指令，通过executorRunner启动程序分配任务，worker就类似于包工头，管理分配新进程，做计算的服务，相当于process服务，需要注意的是：

1）worker会不会汇报当前信息给master？

worker心跳给master主要只有workid，不会以心跳的方式发送资源信息给master，这样master就知道worker是否存活，只有故障的时候才会发送资源信息；

2）worker不会运行代码，具体运行的是executor，可以运行具体application斜的业务逻辑代码，操作代码的节点，不会去运行代码。

### 题10：[Spark 中宽依赖、窄依赖如何理解？](/docs/Spark/常见Spack面试题整合汇总包含答案.md#题10spark-中宽依赖窄依赖如何理解)<br/>
1、窄依赖指的是每一个parent RDD的partition最多被子RDD的一个partition使用（一子一亲）

2、宽依赖指的是多个子RDD的partition会依赖同一个parent RDD的partition（多子一亲）
RDD作为数据结构，本质上是一个只读的分区记录集合。一个RDD可以包含多个分区，每个分区就是一个dataset片段。RDD可以相互依赖。

首先，窄依赖可以支持在同一个cluster node上，以pipeline形式执行多条命令（也叫同一个 stage 的操作），例如在执行了map后，紧接着执行filter。相反，宽依赖需要所有的父分区都是可用的，可能还需要调用类似 MapReduce 之类的操作进行跨节点传递。

其次，则是从失败恢复的角度考虑。窄依赖的失败恢复更有效，因为它只需要重新计算丢失的parent partition即可，而且可以并行地在不同节点进行重计算（一台机器太慢就会分配到多个节点进行），相反的是宽依赖牵涉RDD各级的多个parent partition。

### 题11：spark-中-collect-功能是什么其底层是如何实现的<br/>


### 题12：spark-中常见的-join-操作优化有哪些分类<br/>


### 题13：spark-有几种部署模式各自都有什么特点<br/>


### 题14：说一说-spark-中-yarn-cluster-和-yarn-client-有什么异同点<br/>


### 题15：spark-中列举一些你常用的-action<br/>


### 题16：spark-如何处理不能被序列化的对象<br/>


### 题17：spark-中-map-和-mappartitions-有什么区别<br/>


### 题18：spark-中-ml-和-mllib-两个包区别和联系<br/>


### 题19：说一说-cogroup-rdd-实现原理在什么场景下使用过-rdd<br/>


### 题20：spark-中-map-和-flatmap-有什么区别<br/>


### 题21：spark-中主要包括哪些组件-<br/>


### 题22：spark-中-driver-功能是什么<br/>


### 题23：spark-为什么要持久化一般什么场景下要进行-persist-操作<br/>


### 题24：概述一下-spark-中的常用算子区别<br/>


### 题25：hadoop-和-spark-的-shuffle-有什么差异<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")