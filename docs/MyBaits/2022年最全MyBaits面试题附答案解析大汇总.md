# 2022年最全MyBaits面试题附答案解析大汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## MyBaits

### 题1：[Mybatis 是否可以映射 Enum 枚举类？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题1mybatis-是否可以映射-enum-枚举类)<br/>
Mybatis可以映射枚举类，不单可以映射枚举类，Mybatis可以映射任何对象到表的一列上。

映射方式为自定义一个TypeHandler，实现TypeHandler的setParameter()和getResult()接口方法。

TypeHandler有两个作用，一是完成从javaType至jdbcType的转换，二是完成jdbcType至javaType的转换，体现为setParameter()和getResult()两个方法，分别代表设置sql问号占位符参数和获取列查询结果。

### 题2：[MyBatis 是什么框架？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题2mybatis-是什么框架)<br/>
MyBatis框架是一个优秀的数据持久层框架，在实体类和SQL语句之间建立映射关系，是一种半自动化的ORM实现。其封装性要低于Hibernate，性能优秀，并且小巧。

ORM即对象/关系数据映射，也可以理解为一种数据持久化技术。

MyBatis的基本要素包括核心对象、核心配置文件、SQL映射文件。

数据持久化是将内存中的数据模型转换为存储模型，以及将存储模型转换为内存中的数据模型的统称。

### 题3：[Mybatis 中如何防止 SQL 注入的？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题3mybatis-中如何防止-sql-注入的)<br/>
首先看一下下面两个sql语句的区别：

```xml
<select id="selectByNameAndPassword" parameterType="java.util.Map" resultMap="BaseResultMap">
    select id, username, password
    from user
    where username = #{username,jdbcType=VARCHAR} and
    password = #{password,jdbcType=VARCHAR}
</select>

<select id="selectByNameAndPassword" parameterType="java.util.Map" resultMap="BaseResultMap">
    select id, username, password,
    from user
    where username = ${username,jdbcType=VARCHAR} and
    password = ${password,jdbcType=VARCHAR}
</select>
```
**mybatis中#和$的区别：**

#将传入的数据都当成一个字符串，会对自动传入的数据加一个双引号。 如：where username=#{username}，如果传入的值是111,那么解析成sql时的值为where username="111", 如果传入的值是id，则解析成的sql为where username="id"。

$将传入的数据直接显示生成在sql中。如：where username=${username}，如果传入的值是111,那么解析成sql时的值为where username=111；如果传入的值是：drop table user;，则解析成的sql为：select id, username, password, role from user where username=;drop table user;

#方式能够很大程度防止sql注入，$方式无法防止Sql注入。

$方式一般用于传入数据库对象，例如传入表名。

一般能用#的就别用$，若不得不使用“${xxx}”这样的参数，要手工地做好过滤工作，来防止sql注入攻击。
在MyBatis中，“${xxx}”这样格式的参数会直接参与SQL编译，从而不能避免注入攻击。但涉及到动态表名和列名时，只能使用“${xxx}”这样的参数格式。所以，这样的参数需要我们在代码中手工进行处理来防止注入。
sql注入：

SQL注入，大家都不陌生，是一种常见的攻击方式。攻击者在界面的表单信息或URL上输入一些奇怪的SQL片段（例如“or '1'='1'”这样的语句），有可能入侵参数检验不足的应用程序。所以，在应用中需要做一些工作，来防备这样的攻击方式。在一些安全性要求很高的应用中（比如银行软件），经常使用将SQL语句全部替换为存储过程这样的方式，来防止SQL注入。这当然是一种很安全的方式，但平时开发中，可能不需要这种死板的方式。

**mybatis是如何做到防止sql注入的？**

MyBatis框架作为一款半自动化的持久层框架，其SQL语句都要自己手动编写，这个时候当然需要防止SQL注入。其实，MyBatis的SQL是一个具有“输入+输出”的功能，类似于函数的结构，参考上面的两个例子。其中，parameterType表示了输入的参数类型，resultType表示了输出的参数类型。回应上文，如果我们想防止SQL注入，理所当然地要在输入参数上下功夫。上面代码中使用#的即输入参数在SQL中拼接的部分，传入参数后，打印出执行的SQL语句，会看到SQL是这样的：

```sql
select id, username, password from user where username=? and password=?
```

不管输入什么参数，打印出的SQL都是这样的。这是因为MyBatis启用了预编译功能，在SQL执行前，会先将上面的SQL发送给数据库进行编译；执行时，直接使用编译好的SQL，替换占位符“?”就可以了。因为SQL注入只能对编译过程起作用，所以这样的方式就很好地避免了SQL注入的问题。

**底层实现原理**

MyBatis是如何做到SQL预编译的呢？其实在框架底层，是JDBC中的PreparedStatement类在起作用，PreparedStatement是我们很熟悉的Statement的子类，它的对象包含了编译好的SQL语句。这种“准备好”的方式不仅能提高安全性，而且在多次执行同一个SQL时，能够提高效率。原因是SQL已编译好，再次执行时无需再编译。
结论：

>#{}：相当于JDBC中的PreparedStatement
${}：是输出变量的值

简单说，#{}是经过预编译的，是安全的；${}是未经过预编译的，仅仅是取变量的值，是非安全的，存在SQL注入。

### 题4：[Mybatis 中不同的 Xml 映射文件 ID 是否可以重复？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题4mybatis-中不同的-xml-映射文件-id-是否可以重复)<br/>
Mybatis中不同的Xml映射文件，如果配置了namespace，那么id可以重复；反之ID不能重复。

这是因为namespace+id是作为Map<String, MapperStatement>的key来使用，如果没有namespace空间，那么ID重复会导致数据互相覆盖。

通过设置namespace空间，ID相同但是namespace不同，namespace+id自然也就不同。目前新版本的namespace是必须的。

### 题5：[MyBatis 中实现一对多关系有几种方式？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题5mybatis-中实现一对多关系有几种方式)<br/>
MyBatis实现一对多关系有联合查询和嵌套查询两种方式。

联合查询是几个表联合查询，只查询一次，通过在resultMap里面的collection节点配置一对多的类就可以完成；

嵌套查询是先查一个表。根据这个表里面的 结果的外键id，去再另外一个表里面查询数据,也是通过配置collection，但另外一个表的查询通过select节点配置。

### 题6：[MyBatis 实现批量插入数据的方式有几种？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题6mybatis-实现批量插入数据的方式有几种)<br/>
MyBatis 实现批量插入数据的方式有几种？

**1、MyBatis foreach标签**

foreach主要用在构建in条件，在SQL语句中进行迭代一个集合。

foreach元素的属性主要有item，index，collection，open，separator，close。

>item表示集合中每一个元素进行迭代时的别名
index指定一个名字，用于表示在迭代过程中，每次迭代到的位置
open表示该语句以什么开始
separator表示在每次进行迭代之间以什么符号作为分隔符
close表示以什么结束

collection必须指定该属性，在不同情况下，值是不同的，主要体现3种情况：

若传入单参数且参数类型是List时，collection属性值为list

若传入单参数且参数类型是array数组时，collection的属性值为array

若传入参数是多个时，需要封装成Map

具体用法如下:

```xml
<insert id="insertForeach" parameterType="java.util.List" useGeneratedKeys="false">
	insert into t_userinfo
	(name, age, sex) values
	<foreach collection="list" item="item" index="index" separator=",">
		(#{item.name},#{item.age},#{item.sex})
	</foreach>		
</insert>
```
**2、MyBatis ExecutorType.BATCH**

Mybatis内置ExecutorType，默认是simple，该模式下它为每个语句的执行创建一个新的预处理语句，单条提交sql。

batch模式会重复使用已经预处理的语句，并批量执行所有更新语句。但batch模式Insert操作时，在事务没有提交前，是无法获取到自增的id。

### 题7：[Mybatis 中如何指定使用哪种 Executor 执行器？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题7mybatis-中如何指定使用哪种-executor-执行器)<br/>
Mybatis配置文件中，可以指定默认的ExecutorType执行器类型，也可以手动给DefaultSqlSessionFactory的创建SqlSession的方法传递ExecutorType类型参数。

### 题8：[如何解决 MyBatis 转义字符的问题？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题8如何解决-mybatis-转义字符的问题)<br/>
**xml配置文件使用转义字符**

```sql
SELECT * FROM test WHERE crate_time &lt;= #{crate_time} AND end_date &gt;= #{crate_time}
```

**xml转义字符关系表**

|字符|转义|备注|
|-|-|-|
|<|\&lt;| 小于号 |
|>|\&gt;|大于号|
|&|\&amp;|和|
|'|\&apos;|单引号|
|"|\&quot;|双引号|

注意：XML中只有”<”和”&”是非法的，其它三个都是合法存在的，使用时都可以把它们转义了，养成一个良好的习惯。

转义前后的字符都会被xml解析器解析，为了方便起见，使用
```xml
<![CDATA[...]]>
```
来包含不被xml解析器解析的内容。标记所包含的内容将表示为纯文本，其中...表示文本内容。

**但要注意的是：**

1）此部分不能再包含”]]>”；

2）不允许嵌套使用；

3)”]]>”这部分不能包含空格或者换行。

### 题9：[Mybatis 中有哪些动态 sql 标签，有什么作用？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题9mybatis-中有哪些动态-sql-标签有什么作用)<br/>
Mybatis动态sql可以在Xml映射文件内，以标签的形式编写动态sql，执行原理是根据表达式的值完成逻辑判断并动态拼接sql的功能。

Mybatis提供了9种动态sql标签：trim、where、set、foreach、if、choose、when、otherwise、bind。

### 题10：[什么是 MyBatis 接口绑定？有哪些实现方式？](/docs/MyBaits/2022年最全MyBaits面试题附答案解析大汇总.md#题10什么是-mybatis-接口绑定有哪些实现方式)<br/>
接口绑定是指在MyBatis中任意定义接口，然后把接口里面的方法和SQL语句绑定，我们直接调用接口方法就可以，这样比起原来了SqlSession提供的方法我们可以有更加灵活的选择和设置。

接口绑定有两种实现方式：

一种是通过注解绑定，就是在接口的方法上面加上@Select、@Update等注解，里面包含Sql语句来绑定；

另外一种就是通过xml里面写SQL来绑定，在这种情况下，要指定xml映射文件里面的namespace必须为接口的全路径名。当Sql语句比较简单时候，用注解绑定，当SQL语句比较复杂时候，用xml绑定，一般用xml绑定的比较多。


### 题11：mybatis-中实现一对一关系有几种方式<br/>


### 题12：mybatis-中如何获取自动生成的主键值<br/>


### 题13：通常一个mapper.xml文件都会对应一个dao接口这个dao接口的工作原理是什么dao接口里的方法参数不同时方法能重载吗<br/>


### 题14：mybatis-插件运行原理如何编写一个插件<br/>


### 题15：mybatis-中分页插件的原理是什么<br/>


### 题16：mybatis-中-${}-和-#{}-传参有什么区别<br/>


### 题17：mybatis-中如何解决实体类属性名和表字段名不一致问题<br/>


### 题18：mybatis映射文件中a标签使用include引用b标签内容b标签能否定义在a标签的后面还是说必须定义在a标签的前面<br/>


### 题19：mybatis-如何实现分页<br/>


### 题20：mybatis-中-mapper-编写有哪几种方式<br/>


### 题21：mybatis-中-integer-类型值是-0-为什么-!=-''-无法执行<br/>


### 题22：mybatis-中-模糊查询-like-语句如何使用<br/>


### 题23：mybatis-如何获取自动生成的主键-id-值<br/>


### 题24：xml-映射文件中除了常见的标签外还有哪些<br/>


### 题25：mybatis-是否支持延迟加载其原理是什么<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")