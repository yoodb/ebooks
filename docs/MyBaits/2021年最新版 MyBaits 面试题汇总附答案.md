# 2021年最新版 MyBaits 面试题汇总附答案

### 全部面试题答案，更新日期：12月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## MyBaits

### 题1：[MyBatis 中 Mapper 接口调用时有哪些要求？](/docs/MyBaits/2021年最新版%20MyBaits%20面试题汇总附答案.md#题1mybatis-中-mapper-接口调用时有哪些要求)<br/>
1）Mapper接口方法名和mapper.xml中定义的每个sql的id相同。

2）Mapper接口方法的输入参数类型和mapper.xml中定义的每个sql的parameterType的类型相同。

3）Mapper接口方法的输出参数类型和mapper.xml中定义的每个sql的resultType的类型相同。

4）Mapper.xml文件中的namespace即是mapper接口的类路径。


### 题2：[MyBatis 如何实现分页？](/docs/MyBaits/2021年最新版%20MyBaits%20面试题汇总附答案.md#题2mybatis-如何实现分页)<br/>
Mybatis使用rowbounds对象进行分页，它是针对resultset结果集执行的内存分页，而非物理分页。可以在sql内直接带有物理分页的参数来完成物理分页功能，也可以使用分页插件来完成物理分页。

对数据库表数据进行分页，依靠offset和limit两个参数，表示从第几条开始，取多少条。也就是常说的start和limit。

1）相对原始方法，使用limit分页，需要处理分页逻辑：

MySQL数据库使用limit，如：

select * from table limit 0,10; --返回0-10行

Oracle数据库使用rownum，如：

从表Sys_option（主键为sys_id)中从第10条记录开始检索20条记录，语句如下：

SELECT * FROM (SELECT ROWNUM R,t1.* From Sys_option where rownum < 30 ) t2 Where t2.R >= 10

2）拦截StatementHandler，其实质还是在最后生成limit语句。

3）使用PageHelper插件，目前比较常见的方法。

### 题3：[MyBatis 中 ${} 和 #{} 传参有什么区别？](/docs/MyBaits/2021年最新版%20MyBaits%20面试题汇总附答案.md#题3mybatis-中-${}-和-#{}-传参有什么区别)<br/>
1）“#”符号将传入的数据当成一个字符串并将传入的数据加上双引号。

如：order by #{userId}，如果传入的值是1，那么解析成sql时的值为order by "1"，如果传入的值是userId，则解析成的sql为order by "userId"。

2）“$”符号将传入的数据直接显示生成在sql语句中。

如：order by ${userId}，如果传入的值是1，那么解析成sql时的值为order by 1, 如果传入的值是userId，则解析成的sql为order by userId。

3）“#”符号能够很大程度防止sql注入，而“$”符号无法防止sql注入。

4）“$”符号方式一般用于传入数据库对象，例如传入表名。

5）一般能用“#”符号的就别用“$”符号

6）MyBatis排序时使用order by动态参数时需要注意使用“$”符号而不是“#”符号。

### 题4：[MyBatis 和 Hibernate 都有哪些区别？](/docs/MyBaits/2021年最新版%20MyBaits%20面试题汇总附答案.md#题4mybatis-和-hibernate-都有哪些区别)<br/>
1）Mybatis不完全是一个ORM框架，这是应为MyBatis需要开发人员自己来编写SQL语句。 

2）Mybatis直接编写原生态SQL语句，可以严格控制SQL语句执行，其性能、灵活度高，非常适合对关系数据模型要求不高的软件开发，因为这类软件需求变化频繁，一但需求变化要求迅速输出成果。但是灵活的前提是mybatis无法做到数据库无关性，如果需要实现支持多种数据库的软件，则需要自定义多套SQL映射文件，工作量大。

3）Hibernate对象/关系映射能力强，数据库无关性好，对于关系模型要求高的软件，如果用hibernate开发可以节省很多代码，提高效率。

### 题5：[MyBatis 框架适用哪些场景？](/docs/MyBaits/2021年最新版%20MyBaits%20面试题汇总附答案.md#题5mybatis-框架适用哪些场景)<br/>
1）MyBatis框架专注于SQL语句本身，是一个足够灵活的DAO层解决方案。

2）应用性能要求很高或需求变化较多的项目，例如电商项目、一元购项目等，MyBatis框架将是不错的选择。

### 题6：[Mybatis 是否可以映射 Enum 枚举类？](/docs/MyBaits/2021年最新版%20MyBaits%20面试题汇总附答案.md#题6mybatis-是否可以映射-enum-枚举类)<br/>
Mybatis可以映射枚举类，不单可以映射枚举类，Mybatis可以映射任何对象到表的一列上。

映射方式为自定义一个TypeHandler，实现TypeHandler的setParameter()和getResult()接口方法。

TypeHandler有两个作用，一是完成从javaType至jdbcType的转换，二是完成jdbcType至javaType的转换，体现为setParameter()和getResult()两个方法，分别代表设置sql问号占位符参数和获取列查询结果。

### 题7：[Mybatis 中分页插件的原理是什么？](/docs/MyBaits/2021年最新版%20MyBaits%20面试题汇总附答案.md#题7mybatis-中分页插件的原理是什么)<br/>
Mybatis分页插件的基本原理是使用mybatis提供的插件接口，实现自定义插件，在插件的拦截方法内拦截待执行的sql，然后重写sql，根据dialect方言，添加对应的物理分页语句和物理分页参数。

### 题8：[Xml 映射文件中除了常见的标签外，还有哪些？](/docs/MyBaits/2021年最新版%20MyBaits%20面试题汇总附答案.md#题8xml-映射文件中除了常见的标签外还有哪些)<br/>
Mybatis常见的select、insert、updae、delete标签。

除了常见标签外，还有\<resultMap>、\<parameterMap\>、\<sql>、\<include>、\<selectKey>，加上动态sql的9个标签，其中\<sql>为sql片段标签，通过\<include>标签引入sql片段，\<selectKey>为不支持自增的主键生成策略标签。

### 题9：[什么是 MyBatis 接口绑定？有哪些实现方式？](/docs/MyBaits/2021年最新版%20MyBaits%20面试题汇总附答案.md#题9什么是-mybatis-接口绑定有哪些实现方式)<br/>
接口绑定是指在MyBatis中任意定义接口，然后把接口里面的方法和SQL语句绑定，我们直接调用接口方法就可以，这样比起原来了SqlSession提供的方法我们可以有更加灵活的选择和设置。

接口绑定有两种实现方式：

一种是通过注解绑定，就是在接口的方法上面加上@Select、@Update等注解，里面包含Sql语句来绑定；

另外一种就是通过xml里面写SQL来绑定，在这种情况下，要指定xml映射文件里面的namespace必须为接口的全路径名。当Sql语句比较简单时候，用注解绑定，当SQL语句比较复杂时候，用xml绑定，一般用xml绑定的比较多。


### 题10：[MyBatis 中 模糊查询 like 语句如何使用？](/docs/MyBaits/2021年最新版%20MyBaits%20面试题汇总附答案.md#题10mybatis-中-模糊查询-like-语句如何使用)<br/>
**方式一：Java代码中添加sql通配符**

```java 
String mname = "%Java精选，微信公众号%";
List<UserInfo> list = mapper.selectUserInfo(mnane);
```
```xml
<select id="selectUserInfo">

    select * from t_userinfo where name like #{mname}

</select>
```

**方式二：sql语句中拼接通配符**

```java 
String name = "Java精选，微信公众号";
List<UserInfo> list = mapper.selectUserInfo(nane);
```
```xml
<select id="selectUserInfo">

    select * from t_userinfo where name like '%${name}%'

</select>
```

sql语句中拼接通配符需要注意sql注入的问题，在参数放入sql语句前校验是否合法。

![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")