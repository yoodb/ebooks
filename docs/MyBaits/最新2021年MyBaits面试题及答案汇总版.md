# 最新2021年MyBaits面试题及答案汇总版

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## MyBaits

### 题1：[MyBatis 中 mapper 如何实现传递多个参数？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题1mybatis-中-mapper-如何实现传递多个参数)<br/>
**方式一：**

1）DAO层的函数

```java
Public UserselectUser(String name,String area);
```

2）对应的xml,#{0}代表接收的是dao层中的第一个参数，#{1}代表dao层中第二参数，更多参数一致往后加即可。

```xml
<select id="selectUser"resultMap="BaseResultMap">  
    select *  fromuser_user_t   whereuser_name = #{0} anduser_area=#{1}  
</select>  
```

**方式二：使用 @param 注解**

```java
public interface Usermapper {
   user selectuser(@param("username") string username,@param("hashedpassword") string hashedpassword);
}
```

之后就可以在xml像下面这样使用（推荐第三种方式封装为map集合，作为单个参数传递给mapper）：
<select id="selectuser" resulttype="user">
         select id, username, hashedpassword
         from some_table
         where username = #{username}
         and hashedpassword = #{hashedpassword}
</select>
 
**方式三：多个参数封装成map**

```java
try{
//映射文件的命名空间.SQL片段的ID，就可以调用对应的映射文件中的SQL
//由于其参数超过了两个，而方法中只有一个Object参数收集，因此使用Map集合来装载所需的参数。
Map<String, Object> map = new HashMap();
     map.put("start", start);
     map.put("end", end);
     return sqlSession.selectList("StudentID.pagination", map);
 }catch(Exception e){
     e.printStackTrace();
     sqlSession.rollback();
    throw e; }
finally{
    MybatisUtil.closeSqlSession();
 }
```

### 题2：[Mybatis 中有哪些动态 sql 标签，有什么作用？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题2mybatis-中有哪些动态-sql-标签有什么作用)<br/>
Mybatis动态sql可以在Xml映射文件内，以标签的形式编写动态sql，执行原理是根据表达式的值完成逻辑判断并动态拼接sql的功能。

Mybatis提供了9种动态sql标签：trim、where、set、foreach、if、choose、when、otherwise、bind。

### 题3：[通常一个mapper.xml文件，都会对应一个Dao接口，这个Dao接口的工作原理是什么？Dao接口里的方法，参数不同时，方法能重载吗？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题3通常一个mapper.xml文件都会对应一个dao接口这个dao接口的工作原理是什么dao接口里的方法参数不同时方法能重载吗)<br/>
Mapper接口的工作原理是JDK动态代理，Mybatis运行时会使用JDK动态代理为Mapper接口生成代理对象proxy，代理对象会拦截接口方法，根据类的全限定名+方法名，唯一定位到一个MapperStatement并调用执行器执行所代表的sql，然后将sql执行结果返回。

Mapper接口里的方法，是不能重载的，因为是使用 全限名+方法名 的保存和寻找策略。

Dao接口即Mapper接口。接口的全限名，就是映射文件中的namespace的值；接口的方法名，就是映射文件中Mapper的Statement的id值；接口方法内的参数，就是传递给sql的参数。

当调用接口方法时，接口全限名+方法名拼接字符串作为key值，可唯一定位一个MapperStatement。在Mybatis中，每一个SQL标签，都会被解析为一个MapperStatement对象。

举例：com.mybatis3.mappers.StudentDao.findStudentById，可以唯一找到namespace为com.mybatis3.mappers.StudentDao下面 id 为 findStudentById 的 MapperStatement。

### 题4：[MyBatis 中实现一对一关系有几种方式？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题4mybatis-中实现一对一关系有几种方式)<br/>
MyBatis实现一对一关系有联合查询和嵌套查询两种方式。

联合查询是几个表联合查询，只查询一次，通过在resultMap里面配置association节点配置一对一的类就可以完成；

嵌套查询是先查一个表，根据这个表里面的结果的外键id，去再另外一个表里面查询数据,也是通过association配置，但另外一个表的查询通过select属性配置。

### 题5：[如何解决 MyBatis 转义字符的问题？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题5如何解决-mybatis-转义字符的问题)<br/>
**xml配置文件使用转义字符**

```sql
SELECT * FROM test WHERE crate_time &lt;= #{crate_time} AND end_date &gt;= #{crate_time}
```

**xml转义字符关系表**

|字符|转义|备注|
|-|-|-|
|<|\&lt;| 小于号 |
|>|\&gt;|大于号|
|&|\&amp;|和|
|'|\&apos;|单引号|
|"|\&quot;|双引号|

注意：XML中只有”<”和”&”是非法的，其它三个都是合法存在的，使用时都可以把它们转义了，养成一个良好的习惯。

转义前后的字符都会被xml解析器解析，为了方便起见，使用
```xml
<![CDATA[...]]>
```
来包含不被xml解析器解析的内容。标记所包含的内容将表示为纯文本，其中...表示文本内容。

**但要注意的是：**

1）此部分不能再包含”]]>”；

2）不允许嵌套使用；

3)”]]>”这部分不能包含空格或者换行。

### 题6：[MyBatis 中 Integer 类型值是 0 ，为什么 != '' 无法执行？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题6mybatis-中-integer-类型值是-0-为什么-!=-''-无法执行)<br/>
开发微信小程序“Java精选面试题”后台管理系统时，遇到根据状态判断是或否发布。

MySQL数据库中设计数据库表，其中某字段status使用tinyint数据类型，当修改状态的时候，赋值status属性的值为0，用于改变状态记录试题库中发布情况。

但是通过Debug模式查看Controller控制层明显已经获取到status等于0，但是在执行到MyBatis中xml文件SQL语句时，总是无法赋值成功，xml配置如下：
```xml
<update id="updateWarehouse" parameterType="Warehouse">
	update t_warehouse 
	<set>
		<if test="title != null and title != ''">title = #{title},</if>
		<if test="code != null and code != ''">code = #{code},</if>
		<if test="content != null and content != ''">content = #{content},</if>
		<if test="status != null and status != ''">status = #{status},</if>
		<if test="parentId != null and parentId != ''">parentId = #{parentId}</if>
	</set>
	where id = #{id}
</update>
```
分析：

通过分析表面上没有任何传参问题，通过上网查询MyBatis相关资料，终于弄明白什么原因。
此行代码中
```xml
<if test="status != null and status != ''">status = #{status},</if>
```
and status != ''，MyBatis中传参status的值为0时，因为数据类型为Integer类型，判断为false值。

MyBatis认定 0 = ''的，因此判断status != ''为false，这导致修改试题信息时状态值无法改变为0。

正确写法：
```xml
<update id="updateWarehouse" parameterType="Warehouse">
	update t_warehouse 
	<set>
		<if test="title != null and title != ''">title = #{title},</if>
		<if test="code != null and code != ''">code = #{code},</if>
		<if test="content != null and content != ''">content = #{content},</if>
		<if test="status != null">status = #{status},</if>
		<if test="parentId != null and parentId != ''">parentId = #{parentId}</if>
	</set>
	where id = #{id}
</update>
```

### 题7：[为什么说 MyBatis 是半自动 ORM 映射？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题7为什么说-mybatis-是半自动-orm-映射)<br/>
ORM是Object和Relation之间的映射，包括Object->Relation和Relation->Object两方面。Hibernate是个完整的ORM框架，而MyBatis完成的是Relation->Object，也就是其所说的Data Mapper Framework。

JPA是ORM映射标准，主流的ORM映射都实现了这个标准。MyBatis没有实现JPA，它和ORM框架的设计思路不完全一样。MyBatis是拥抱SQL，而ORM则更靠近面向对象，不建议写SQL，实在要写需用框架自带的类SQL代替。MyBatis是SQL映射而不是ORMORM映射，当然ORM和MyBatis都是持久层框架。

最典型的ORM映射是Hibernate，它是全自动ORM映射，而MyBatis是半自动的ORM映射。Hibernate完全可以通过对象关系模型实现对数据库的操作，拥有完整的JavaBean对象与数据库的映射结构来自动生成SQL。而MyBatis仅有基本的字段映射，对象数据以及对象实际关系仍然需要通过手写SQL来实现和管理。

Hibernate数据库移植性远大于MyBatis。Hibernate通过它强大的映射结构和HQL语言，大大降低了对象与数据库（oracle、mySQL等）的耦合性，而MyBatis由于需要手写SQL，因此与数据库的耦合性直接取决于程序员写SQL的方法，如果SQL不具通用性而用了很多某数据库特性的SQL语句的话，移植性也会随之降低很多，成本很高。

### 题8：[MyBatis 中 模糊查询 like 语句如何使用？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题8mybatis-中-模糊查询-like-语句如何使用)<br/>
**方式一：Java代码中添加sql通配符**

```java 
String mname = "%Java精选，微信公众号%";
List<UserInfo> list = mapper.selectUserInfo(mnane);
```
```xml
<select id="selectUserInfo">

    select * from t_userinfo where name like #{mname}

</select>
```

**方式二：sql语句中拼接通配符**

```java 
String name = "Java精选，微信公众号";
List<UserInfo> list = mapper.selectUserInfo(nane);
```
```xml
<select id="selectUserInfo">

    select * from t_userinfo where name like '%${name}%'

</select>
```

sql语句中拼接通配符需要注意sql注入的问题，在参数放入sql语句前校验是否合法。

### 题9：[MyBatis 如何实现分页？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题9mybatis-如何实现分页)<br/>
Mybatis使用rowbounds对象进行分页，它是针对resultset结果集执行的内存分页，而非物理分页。可以在sql内直接带有物理分页的参数来完成物理分页功能，也可以使用分页插件来完成物理分页。

对数据库表数据进行分页，依靠offset和limit两个参数，表示从第几条开始，取多少条。也就是常说的start和limit。

1）相对原始方法，使用limit分页，需要处理分页逻辑：

MySQL数据库使用limit，如：

select * from table limit 0,10; --返回0-10行

Oracle数据库使用rownum，如：

从表Sys_option（主键为sys_id)中从第10条记录开始检索20条记录，语句如下：

SELECT * FROM (SELECT ROWNUM R,t1.* From Sys_option where rownum < 30 ) t2 Where t2.R >= 10

2）拦截StatementHandler，其实质还是在最后生成limit语句。

3）使用PageHelper插件，目前比较常见的方法。

### 题10：[MyBatis 中实现一对多关系有几种方式？](/docs/MyBaits/最新2021年MyBaits面试题及答案汇总版.md#题10mybatis-中实现一对多关系有几种方式)<br/>
MyBatis实现一对多关系有联合查询和嵌套查询两种方式。

联合查询是几个表联合查询，只查询一次，通过在resultMap里面的collection节点配置一对多的类就可以完成；

嵌套查询是先查一个表。根据这个表里面的 结果的外键id，去再另外一个表里面查询数据,也是通过配置collection，但另外一个表的查询通过select节点配置。

### 题11：mybatis-中如何防止-sql-注入的<br/>


### 题12：mybatis-是什么框架<br/>


### 题13：mybatis-和-hibernate-都有哪些区别<br/>


### 题14：mybatis-是否可以映射-enum-枚举类<br/>


### 题15：mybatis-中如何指定使用哪种-executor-执行器<br/>


### 题16：mybatis-中-mapper-编写有哪几种方式<br/>


### 题17：xml-映射文件中除了常见的标签外还有哪些<br/>


### 题18：mybatis-中-mapper-编写有哪几种方式<br/>


### 题19：mybatis-中分页插件的原理是什么<br/>


### 题20：mybatis-中有哪些-executor-执行器它们之间有什么区别<br/>


### 题21：mybatis-框架适用哪些场景<br/>


### 题22：mybatis-插件运行原理如何编写一个插件<br/>


### 题23：mybatis-中一级缓存和二级缓存有什么区别<br/>


### 题24：mybatis-的-xml-映射文件和-mybatis-内部数据结构之间的映射关系<br/>


### 题25：mybatis-中如何获取自动生成的主键值<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")