# 最新30道面试题常见MyBaits面试题及答案汇总

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## MyBaits

### 题1：[MyBatis 中 Mapper 接口调用时有哪些要求？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题1mybatis-中-mapper-接口调用时有哪些要求)<br/>
1）Mapper接口方法名和mapper.xml中定义的每个sql的id相同。

2）Mapper接口方法的输入参数类型和mapper.xml中定义的每个sql的parameterType的类型相同。

3）Mapper接口方法的输出参数类型和mapper.xml中定义的每个sql的resultType的类型相同。

4）Mapper.xml文件中的namespace即是mapper接口的类路径。


### 题2：[通常一个mapper.xml文件，都会对应一个Dao接口，这个Dao接口的工作原理是什么？Dao接口里的方法，参数不同时，方法能重载吗？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题2通常一个mapper.xml文件都会对应一个dao接口这个dao接口的工作原理是什么dao接口里的方法参数不同时方法能重载吗)<br/>
Mapper接口的工作原理是JDK动态代理，Mybatis运行时会使用JDK动态代理为Mapper接口生成代理对象proxy，代理对象会拦截接口方法，根据类的全限定名+方法名，唯一定位到一个MapperStatement并调用执行器执行所代表的sql，然后将sql执行结果返回。

Mapper接口里的方法，是不能重载的，因为是使用 全限名+方法名 的保存和寻找策略。

Dao接口即Mapper接口。接口的全限名，就是映射文件中的namespace的值；接口的方法名，就是映射文件中Mapper的Statement的id值；接口方法内的参数，就是传递给sql的参数。

当调用接口方法时，接口全限名+方法名拼接字符串作为key值，可唯一定位一个MapperStatement。在Mybatis中，每一个SQL标签，都会被解析为一个MapperStatement对象。

举例：com.mybatis3.mappers.StudentDao.findStudentById，可以唯一找到namespace为com.mybatis3.mappers.StudentDao下面 id 为 findStudentById 的 MapperStatement。

### 题3：[Mybatis 是否可以映射 Enum 枚举类？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题3mybatis-是否可以映射-enum-枚举类)<br/>
Mybatis可以映射枚举类，不单可以映射枚举类，Mybatis可以映射任何对象到表的一列上。

映射方式为自定义一个TypeHandler，实现TypeHandler的setParameter()和getResult()接口方法。

TypeHandler有两个作用，一是完成从javaType至jdbcType的转换，二是完成jdbcType至javaType的转换，体现为setParameter()和getResult()两个方法，分别代表设置sql问号占位符参数和获取列查询结果。

### 题4：[Mybatis 中分页插件的原理是什么？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题4mybatis-中分页插件的原理是什么)<br/>
Mybatis分页插件的基本原理是使用mybatis提供的插件接口，实现自定义插件，在插件的拦截方法内拦截待执行的sql，然后重写sql，根据dialect方言，添加对应的物理分页语句和物理分页参数。

### 题5：[Mybatis 中如何指定使用哪种 Executor 执行器？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题5mybatis-中如何指定使用哪种-executor-执行器)<br/>
Mybatis配置文件中，可以指定默认的ExecutorType执行器类型，也可以手动给DefaultSqlSessionFactory的创建SqlSession的方法传递ExecutorType类型参数。

### 题6：[MyBatis 中实现一对一关系有几种方式？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题6mybatis-中实现一对一关系有几种方式)<br/>
MyBatis实现一对一关系有联合查询和嵌套查询两种方式。

联合查询是几个表联合查询，只查询一次，通过在resultMap里面配置association节点配置一对一的类就可以完成；

嵌套查询是先查一个表，根据这个表里面的结果的外键id，去再另外一个表里面查询数据,也是通过association配置，但另外一个表的查询通过select属性配置。

### 题7：[如何解决 MyBatis 转义字符的问题？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题7如何解决-mybatis-转义字符的问题)<br/>
**xml配置文件使用转义字符**

```sql
SELECT * FROM test WHERE crate_time &lt;= #{crate_time} AND end_date &gt;= #{crate_time}
```

**xml转义字符关系表**

|字符|转义|备注|
|-|-|-|
|<|\&lt;| 小于号 |
|>|\&gt;|大于号|
|&|\&amp;|和|
|'|\&apos;|单引号|
|"|\&quot;|双引号|

注意：XML中只有”<”和”&”是非法的，其它三个都是合法存在的，使用时都可以把它们转义了，养成一个良好的习惯。

转义前后的字符都会被xml解析器解析，为了方便起见，使用
```xml
<![CDATA[...]]>
```
来包含不被xml解析器解析的内容。标记所包含的内容将表示为纯文本，其中...表示文本内容。

**但要注意的是：**

1）此部分不能再包含”]]>”；

2）不允许嵌套使用；

3)”]]>”这部分不能包含空格或者换行。

### 题8：[Mybatis映射文件中A标签使用include引用B标签内容，B标签能否定义在A标签的后面，还是说必须定义在A标签的前面？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题8mybatis映射文件中a标签使用include引用b标签内容b标签能否定义在a标签的后面还是说必须定义在a标签的前面)<br/>
虽然Mybatis解析Xml映射文件是按照顺序解析的，但是，被引用的B标签依然可以定义在任何地方，Mybatis都可以正确识别。

原理是Mybatis解析A标签，发现A标签引用了B标签，但是B标签尚未解析到，尚不存在，此时，Mybatis会将A标签标记为未解析状态，然后继续解析余下的标签，包含B标签，待所有标签解析完毕，Mybatis会重新解析那些被标记为未解析的标签，此时再解析A标签时，B标签已经存在，A标签也就可以正常解析完成了。

### 题9：[Mybatis 中有哪些 Executor 执行器？它们之间有什么区别？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题9mybatis-中有哪些-executor-执行器它们之间有什么区别)<br/>
Mybatis有三种基本的Executor执行器，SimpleExecutor、ReuseExecutor、BatchExecutor。

**SimpleExecutor：** 每执行一次update或select，就开启一个Statement对象，用完立刻关闭Statement对象。

**ReuseExecutor：** 执行update或select，以sql作为key查找Statement对象，存在就使用，不存在就创建，用完后，不关闭Statement对象，而是放置于Map<String, Statement>内，供下一次使用。简言之，就是重复使用Statement对象。

**BatchExecutor：** 执行update（没有select，JDBC批处理不支持select），将所有sql都添加到批处理中（addBatch()），等待统一执行（executeBatch()），它缓存了多个Statement对象，每个Statement对象都是addBatch()完毕后，等待逐一执行executeBatch()批处理。与JDBC批处理相同。

作用范围：Executor的这些特点，都严格限制在SqlSession生命周期范围内。

### 题10：[Mybatis 中 Mapper 编写有哪几种方式？](/docs/MyBaits/最新30道面试题常见MyBaits面试题及答案汇总.md#题10mybatis-中-mapper-编写有哪几种方式)<br/>
**方式一：接口实现类继承SqlSessionDaoSupport**

使用此种方法需要编写mapper接口，mapper接口实现类、mapper.xml文件。

1）在sqlMapConfig.xml中配置mapper.xml的位置：
```xml
<mappers>
        <mapper resource="mapper.xml 文件的地址" />
        <mapper resource="mapper.xml 文件的地址" />
</mappers>
```
2）定义mapper接口：

3）实现类集成SqlSessionDaoSupport：mapper方法中可以this.getSqlSession()进行数据增删改查。

4）spring 配置：

```xml
<bean id="对象ID" class="mapper 接口的实现">
    <property name="sqlSessionFactory" ref="sqlSessionFactory"></property>
</bean>
```

**方式二：使用org.mybatis.spring.mapper.MapperFactoryBean**

1）在sqlMapConfig.xml中配置mapper.xml的位置，如果mapper.xml和mappre接口的名称相同且在同一个目录，这里可以不用配置
```xml
<mappers>
        <mapper resource="mapper.xml 文件的地址" />
        <mapper resource="mapper.xml 文件的地址" />
</mappers>
```
2）定义mapper接口：

① mapper.xml中的namespace为mapper接口的地址

② mapper接口中的方法名和mapper.xml中的定义的statement的id保持一致

③ Spring中定义：

```xml
<bean id="" class="org.mybatis.spring.mapper.MapperFactoryBean">
    <property name="mapperInterface" value="mapper 接口地址" />
    <property name="sqlSessionFactory" ref="sqlSessionFactory" />
</bean>
```

**方式三：使用mapper扫描器**

1）mapper.xml文件编写：

mapper.xml中的namespace为mapper接口的地址；

mapper接口中的方法名和mapper.xml中的定义的statement的id保持一致；

如果将mapper.xml和mapper接口的名称保持一致则不用在sqlMapConfig.xml中进行配置。 

2）定义mapper接口：

注意mapper.xml的文件名和mapper的接口名称保持一致，且放在同一个目录

3）配置mapper扫描器：

```xml
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
    <property name="basePackage" value="mapper接口包地址" />
    <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
</bean>
```

4）使用扫描器后从spring容器中获取mapper的实现对象。

### 题11：mybatis-中实现一对多关系有几种方式<br/>


### 题12：mybatis-中-${}-和-#{}-传参有什么区别<br/>


### 题13：mybatis-的-xml-映射文件和-mybatis-内部数据结构之间的映射关系<br/>


### 题14：mybatis-是否支持延迟加载其原理是什么<br/>


### 题15：mybatis-中-mapper-编写有哪几种方式<br/>


### 题16：mybatis-中如何获取自动生成的主键值<br/>


### 题17：mybatis-中一级缓存和二级缓存有什么区别<br/>


### 题18：mybatis-中-mapper-如何实现传递多个参数<br/>


### 题19：mybatis-中-integer-类型值是-0-为什么-!=-''-无法执行<br/>


### 题20：mybatis-框架适用哪些场景<br/>


### 题21：mybatis-中-模糊查询-like-语句如何使用<br/>


### 题22：mybatis-中如何解决实体类属性名和表字段名不一致问题<br/>


### 题23：mybatis-中有哪些动态-sql-标签有什么作用<br/>


### 题24：为什么说-mybatis-是半自动-orm-映射<br/>


### 题25：mybatis-中不同的-xml-映射文件-id-是否可以重复<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")