# 2021年最新版MyBaits面试题汇总附答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## MyBaits

### 题1：[Xml 映射文件中除了常见的标签外，还有哪些？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题1xml-映射文件中除了常见的标签外还有哪些)<br/>
Mybatis常见的select、insert、updae、delete标签。

除了常见标签外，还有\<resultMap>、\<parameterMap\>、\<sql>、\<include>、\<selectKey>，加上动态sql的9个标签，其中\<sql>为sql片段标签，通过\<include>标签引入sql片段，\<selectKey>为不支持自增的主键生成策略标签。

### 题2：[MyBatis 中实现一对多关系有几种方式？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题2mybatis-中实现一对多关系有几种方式)<br/>
MyBatis实现一对多关系有联合查询和嵌套查询两种方式。

联合查询是几个表联合查询，只查询一次，通过在resultMap里面的collection节点配置一对多的类就可以完成；

嵌套查询是先查一个表。根据这个表里面的 结果的外键id，去再另外一个表里面查询数据,也是通过配置collection，但另外一个表的查询通过select节点配置。

### 题3：[MyBatis 中 模糊查询 like 语句如何使用？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题3mybatis-中-模糊查询-like-语句如何使用)<br/>
**方式一：Java代码中添加sql通配符**

```java 
String mname = "%Java精选，微信公众号%";
List<UserInfo> list = mapper.selectUserInfo(mnane);
```
```xml
<select id="selectUserInfo">

    select * from t_userinfo where name like #{mname}

</select>
```

**方式二：sql语句中拼接通配符**

```java 
String name = "Java精选，微信公众号";
List<UserInfo> list = mapper.selectUserInfo(nane);
```
```xml
<select id="selectUserInfo">

    select * from t_userinfo where name like '%${name}%'

</select>
```

sql语句中拼接通配符需要注意sql注入的问题，在参数放入sql语句前校验是否合法。

### 题4：[MyBatis 中 Mapper 编写有哪几种方式？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题4mybatis-中-mapper-编写有哪几种方式)<br/>
**第一种：** 接口实现类继承SqlSessionDaoSupport：使用此种方法需要编写mapper接口，mapper接口实现类、mapper.xml文件。

1）在sqlMapConfig.xml中配置mapper.xml的位置：

```xml
<mappers>
        <mapper resource="mapper.xml 文件的地址" />
        <mapper resource="mapper.xml 文件的地址" />
</mappers>
```
2）定义mapper接口：

3）实现类集成SqlSessionDaoSupport：mapper方法中可以this.getSqlSession()进行数据增删改查。

4）spring 配置：

```xml
<bean id="对象ID" class="mapper 接口的实现">
    <property name="sqlSessionFactory" ref="sqlSessionFactory"></property>
</bean>
```
**第二种：** 使用org.mybatis.spring.mapper.MapperFactoryBean：

1）在sqlMapConfig.xml中配置mapper.xml的位置，如果mapper.xml和mappre接口的名称相同且在同一个目录，这里可以不用配置

```xml
<mappers>
        <mapper resource="mapper.xml 文件的地址" />
        <mapper resource="mapper.xml 文件的地址" />
</mappers>
```
2）定义mapper接口：

① mapper.xml中的namespace为mapper接口的地址

② mapper接口中的方法名和mapper.xml中的定义的statement的id保持一致

③ Spring中定义：

```xml
<bean id="" class="org.mybatis.spring.mapper.MapperFactoryBean">
    <property name="mapperInterface" value="mapper 接口地址" />
    <property name="sqlSessionFactory" ref="sqlSessionFactory" />
</bean>
```

**第三种：** 使用mapper扫描器：

1）mapper.xml文件编写：

mapper.xml中的namespace为mapper接口的地址；

mapper接口中的方法名和mapper.xml中的定义的statement的id保持一致；

如果将mapper.xml和mapper接口的名称保持一致则不用在sqlMapConfig.xml中进行配置。 

2）定义mapper接口：

注意mapper.xml的文件名和mapper的接口名称保持一致，且放在同一个目录

3）配置mapper扫描器：

```xml
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
    <property name="basePackage" value="mapper接口包地址" />
    <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
</bean>
```

4）使用扫描器后从spring容器中获取mapper的实现对象。

### 题5：[MyBatis 是什么框架？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题5mybatis-是什么框架)<br/>
MyBatis框架是一个优秀的数据持久层框架，在实体类和SQL语句之间建立映射关系，是一种半自动化的ORM实现。其封装性要低于Hibernate，性能优秀，并且小巧。

ORM即对象/关系数据映射，也可以理解为一种数据持久化技术。

MyBatis的基本要素包括核心对象、核心配置文件、SQL映射文件。

数据持久化是将内存中的数据模型转换为存储模型，以及将存储模型转换为内存中的数据模型的统称。

### 题6：[什么是 MyBatis 接口绑定？有哪些实现方式？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题6什么是-mybatis-接口绑定有哪些实现方式)<br/>
接口绑定是指在MyBatis中任意定义接口，然后把接口里面的方法和SQL语句绑定，我们直接调用接口方法就可以，这样比起原来了SqlSession提供的方法我们可以有更加灵活的选择和设置。

接口绑定有两种实现方式：

一种是通过注解绑定，就是在接口的方法上面加上@Select、@Update等注解，里面包含Sql语句来绑定；

另外一种就是通过xml里面写SQL来绑定，在这种情况下，要指定xml映射文件里面的namespace必须为接口的全路径名。当Sql语句比较简单时候，用注解绑定，当SQL语句比较复杂时候，用xml绑定，一般用xml绑定的比较多。


### 题7：[Mybatis 中如何指定使用哪种 Executor 执行器？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题7mybatis-中如何指定使用哪种-executor-执行器)<br/>
Mybatis配置文件中，可以指定默认的ExecutorType执行器类型，也可以手动给DefaultSqlSessionFactory的创建SqlSession的方法传递ExecutorType类型参数。

### 题8：[MyBatis 实现批量插入数据的方式有几种？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题8mybatis-实现批量插入数据的方式有几种)<br/>
MyBatis 实现批量插入数据的方式有几种？

**1、MyBatis foreach标签**

foreach主要用在构建in条件，在SQL语句中进行迭代一个集合。

foreach元素的属性主要有item，index，collection，open，separator，close。

>item表示集合中每一个元素进行迭代时的别名
index指定一个名字，用于表示在迭代过程中，每次迭代到的位置
open表示该语句以什么开始
separator表示在每次进行迭代之间以什么符号作为分隔符
close表示以什么结束

collection必须指定该属性，在不同情况下，值是不同的，主要体现3种情况：

若传入单参数且参数类型是List时，collection属性值为list

若传入单参数且参数类型是array数组时，collection的属性值为array

若传入参数是多个时，需要封装成Map

具体用法如下:

```xml
<insert id="insertForeach" parameterType="java.util.List" useGeneratedKeys="false">
	insert into t_userinfo
	(name, age, sex) values
	<foreach collection="list" item="item" index="index" separator=",">
		(#{item.name},#{item.age},#{item.sex})
	</foreach>		
</insert>
```
**2、MyBatis ExecutorType.BATCH**

Mybatis内置ExecutorType，默认是simple，该模式下它为每个语句的执行创建一个新的预处理语句，单条提交sql。

batch模式会重复使用已经预处理的语句，并批量执行所有更新语句。但batch模式Insert操作时，在事务没有提交前，是无法获取到自增的id。

### 题9：[MyBatis 中 ${} 和 #{} 传参有什么区别？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题9mybatis-中-${}-和-#{}-传参有什么区别)<br/>
1）“#”符号将传入的数据当成一个字符串并将传入的数据加上双引号。

如：order by #{userId}，如果传入的值是1，那么解析成sql时的值为order by "1"，如果传入的值是userId，则解析成的sql为order by "userId"。

2）“$”符号将传入的数据直接显示生成在sql语句中。

如：order by ${userId}，如果传入的值是1，那么解析成sql时的值为order by 1, 如果传入的值是userId，则解析成的sql为order by userId。

3）“#”符号能够很大程度防止sql注入，而“$”符号无法防止sql注入。

4）“$”符号方式一般用于传入数据库对象，例如传入表名。

5）一般能用“#”符号的就别用“$”符号

6）MyBatis排序时使用order by动态参数时需要注意使用“$”符号而不是“#”符号。

### 题10：[Mybatis 中 Mapper 编写有哪几种方式？](/docs/MyBaits/2021年最新版MyBaits面试题汇总附答案.md#题10mybatis-中-mapper-编写有哪几种方式)<br/>
**方式一：接口实现类继承SqlSessionDaoSupport**

使用此种方法需要编写mapper接口，mapper接口实现类、mapper.xml文件。

1）在sqlMapConfig.xml中配置mapper.xml的位置：
```xml
<mappers>
        <mapper resource="mapper.xml 文件的地址" />
        <mapper resource="mapper.xml 文件的地址" />
</mappers>
```
2）定义mapper接口：

3）实现类集成SqlSessionDaoSupport：mapper方法中可以this.getSqlSession()进行数据增删改查。

4）spring 配置：

```xml
<bean id="对象ID" class="mapper 接口的实现">
    <property name="sqlSessionFactory" ref="sqlSessionFactory"></property>
</bean>
```

**方式二：使用org.mybatis.spring.mapper.MapperFactoryBean**

1）在sqlMapConfig.xml中配置mapper.xml的位置，如果mapper.xml和mappre接口的名称相同且在同一个目录，这里可以不用配置
```xml
<mappers>
        <mapper resource="mapper.xml 文件的地址" />
        <mapper resource="mapper.xml 文件的地址" />
</mappers>
```
2）定义mapper接口：

① mapper.xml中的namespace为mapper接口的地址

② mapper接口中的方法名和mapper.xml中的定义的statement的id保持一致

③ Spring中定义：

```xml
<bean id="" class="org.mybatis.spring.mapper.MapperFactoryBean">
    <property name="mapperInterface" value="mapper 接口地址" />
    <property name="sqlSessionFactory" ref="sqlSessionFactory" />
</bean>
```

**方式三：使用mapper扫描器**

1）mapper.xml文件编写：

mapper.xml中的namespace为mapper接口的地址；

mapper接口中的方法名和mapper.xml中的定义的statement的id保持一致；

如果将mapper.xml和mapper接口的名称保持一致则不用在sqlMapConfig.xml中进行配置。 

2）定义mapper接口：

注意mapper.xml的文件名和mapper的接口名称保持一致，且放在同一个目录

3）配置mapper扫描器：

```xml
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
    <property name="basePackage" value="mapper接口包地址" />
    <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
</bean>
```

4）使用扫描器后从spring容器中获取mapper的实现对象。

### 题11：mybatis-中如何解决实体类属性名和表字段名不一致问题<br/>


### 题12：mybatis-如何实现分页<br/>


### 题13：mybatis-中有哪些动态-sql-标签有什么作用<br/>


### 题14：mybatis-中如何防止-sql-注入的<br/>


### 题15：mybatis-是否支持延迟加载其原理是什么<br/>


### 题16：mybatis-中一级缓存和二级缓存有什么区别<br/>


### 题17：mybatis-和-hibernate-都有哪些区别<br/>


### 题18：mybatis-中有哪些-executor-执行器它们之间有什么区别<br/>


### 题19：mybatis-中-mapper-如何实现传递多个参数<br/>


### 题20：mybatis-中不同的-xml-映射文件-id-是否可以重复<br/>


### 题21：mybatis-中-integer-类型值是-0-为什么-!=-''-无法执行<br/>


### 题22：通常一个mapper.xml文件都会对应一个dao接口这个dao接口的工作原理是什么dao接口里的方法参数不同时方法能重载吗<br/>


### 题23：mybatis-中如何获取自动生成的主键值<br/>


### 题24：mybatis-的-xml-映射文件和-mybatis-内部数据结构之间的映射关系<br/>


### 题25：mybatis-是否可以映射-enum-枚举类<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")