# 2022年各大厂最新Java并发题资料整合及答案

### 全部面试题答案，更新日期：01月30日，直接下载吧！

### 下载链接：[高清500+份面试题资料及电子书，累计 10000+ 页大厂面试题  PDF](/docs/index.md)

## Java 并发

### 题1：[什么是守护线程？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题1什么是守护线程)<br/>
守护线程（即daemon thread），是个服务线程，准确地来说就是服务其他的线程，而其他的线程只有一种，那就是用户线程。

java中线程可以划分为2种

>1、守护线程，比如垃圾回收线程，就是最典型的守护线程。
2、用户线程，就是应用程序里的自定义线程。
 
**守护线程**

1、守护线程，专门用于服务其他的线程，如果其他的线程（即用户自定义线程）都执行完毕，连main线程也执行完毕，那么jvm就会退出（即停止运行）——此时，连jvm都停止运行了，守护线程当然也就停止执行了。
 
2、如果存在用户自定义线程的话，jvm就不会退出，此时守护线程也不能退出，也就是它还要运行，就是为了执行垃圾回收的任务。
 
3、守护线程又被称为“服务进程”“精灵线程”“后台线程”，是指在程序运行是在后台提供一种通用的线程，这种线程并不属于程序不可或缺的部分。

通俗点讲，任何一个守护线程都是整个JVM中所有非守护线程的“保姆”。

### 题2：[虚拟机栈和本地方法栈为什么是私有的？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题2虚拟机栈和本地方法栈为什么是私有的)<br/>
**虚拟机栈**

每个Java方法在执行的同时会创建一个栈帧用于存储局部变量表、操作数栈、常量池引用等信息。从方法调用直至执行完成的过程，就对应着一个栈帧在Java虚拟机栈中入栈和出栈的过程。

**本地方法栈**

和虚拟机栈所发挥的作用非常相似，区别是： 虚拟机栈为虚拟机执行Java方法（也就是字节码）服务，而本地方法栈则为虚拟机使用到的Native方法服务。 在HotSpot虚拟机中和 Java 虚拟机栈合二为一。

因此，为了保证线程中的局部变量不被别的线程访问到，虚拟机栈和本地方法栈是线程私有的。

### 题3：[Java 中为什么代码会重排序？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题3java-中为什么代码会重排序)<br/>
在执行程序时，为了提供性能，处理器和编译器常常会对指令进行重排序，但是不能随意重排序，不是不是想怎么排序就怎么排序，需要满足以下两个条件：
>1）在单线程环境下不能改变程序运行的结果；
2）存在数据依赖关系的不允许重排序

需要注意的是：重排序不会影响单线程环境的执行结果，但是会破坏多线程的执行语义。

### 题4：[Java 中 volatile 和 synchronized 有什么区别？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题4java-中-volatile-和-synchronized-有什么区别)<br/>
volatile关键字能够保证数据的可见性，但不能保证数据的原子性。

synchronized关键字既能够保证数据的可见性，又能够保证数据的原子性，即保证资源的同步。

volatile关键字只能用于修饰变量，而synchronized关键字可以修饰方法以及代码块。

### 题5：[如何保证运行中的线程暂停一段时间？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题5如何保证运行中的线程暂停一段时间)<br/>
使用Thread类的sleep()方法可以让线程暂停一段时间。

需要注意的是，这并不会让线程终止，一旦从休眠中唤醒线程，线程的状态将会被改变为Runnable，并且根据线程调度，它将得到执行。

### 题6：[多线程同步和互斥有几种实现方法？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题6多线程同步和互斥有几种实现方法)<br/>
线程同步是指线程之间所具有的一种制约关系，一个线程的执行依赖另一个线程的消息，当它没有得到另一个线程的消息时应等待，直到消息到达时才被唤醒。

线程互斥是指对于共享的进程系统资源，在各单个线程访问时的排它性。当有若干个线程都要使用某一共享资源时，任何时刻最多只允许一个线程去使用，其它要使用该资源的线程必须等待，直到占用资源者释放该资源。线程互斥可以看成是一种特殊的线程同步。

线程间的同步方法大体可分为两类：用户模式和内核模式。顾名思义，内核模式就是指利用系统内核对象的单一性来进行同步，使用时需要切换内核态与用户态，而用户模式就是不需要切换到内核态，只在用户态完成操作。

用户模式下的方法有：原子操作（例如一个单一的全局变量），临界区。内核模式下的方法有：事件，信号量，互斥量。

### 题7：[Java Concurrency API 中有哪些原子类？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题7java-concurrency-api-中有哪些原子类)<br/>
java.util.concurrent包提供了一组原子类。其基本的特性就是在多线程环境下，当有多个线程同时执行这些类的实例包含的方法时，具有排他性，即当某个线程进入方法，执行其中的指令时，不会被其他线程打断，而别的线程就像自旋锁一样，一直等到该方法执行完成，才由JVM从等待队列中选择一个另一个线程进入，这只是一种逻辑上的理解。

原子类：AtomicBoolean，AtomicInteger，AtomicLong，AtomicReference

原子数组：AtomicIntegerArray，AtomicLongArray，AtomicReferenceArray

原子属性更新器：AtomicLongFieldUpdater，AtomicIntegerFieldUpdater，AtomicReferenceFieldUpdater

解决ABA问题的原子类：AtomicMarkableReference（通过引入一个boolean来反映中间有没有变过），AtomicStampedReference（通过引入一个int来累加来反映中间有没有变过）。

### 题8：[什么是不可变对象，对写并发应用有什么帮助？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题8什么是不可变对象对写并发应用有什么帮助)<br/>
不可变对象（Immutable Objects）即对象一旦被创建它的状态（对象的数据，也即对象属性值）就不能改变，反之即为可变对象（Mutable Objects）。

不可变对象的类即为不可变类（Immutable Class）。Java平台类库中包含许多不可变类，如String、基本类型的包装类、BigInteger和BigDecimal等。

不可变对象天生是线程安全的。它们的常量（域）是在构造函数中创建的。既然它们的状态无法修改，这些常量永远不会变。

不可变对象永远是线程安全的。

只有满足如下状态，一个对象才是不可变的。

>1）它的状态不能在创建后再被修改；
2）所有域都是final类型；
3）被正确创建（创建期间没有发生this引用的逸出）。

### 题9：[为什么 wait() 和 notify() 方法要在同步块中调用？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题9为什么-wait()-和-notify()-方法要在同步块中调用)<br/>
一方面是Java API强制要求这样做，如果不这么做的话，代码就会抛出IllegalMonitorStateException异常。

另外一方面是为了避免wait()和notify()方法之间产生竞争条件。

### 题10：[什么是线程池？](/docs/Java%20并发/2022年各大厂最新Java并发题资料整合及答案.md#题10什么是线程池)<br/>
线程池就是创建若干个可执行的线程放入一个池（容器）中，有任务需要处理时，会提交到线程池中的任务队列，处理完之后线程并不会被销毁，而是仍然在线程池中等待下一个任务。

### 题11：string变量可以存放在-delayqueue-队列吗<br/>


### 题12：如何检测一个线程是否拥有锁<br/>


### 题13：什么是aqs<br/>


### 题14：常用的并发工具类有哪些<br/>


### 题15：什么是线程安全<br/>


### 题16：什么是进程<br/>


### 题17：什么是-callable-和-future<br/>


### 题18：什么是线程<br/>


### 题19：java-中-volatile-关键字有什么作用<br/>


### 题20：java-中-aqs-底层原理是什么<br/>


### 题21：java-中如何实现多线程之间的通讯和协作<br/>


### 题22：并发和并行有什么区别<br/>


### 题23：synchronousqueue-队列的大小是多少<br/>


### 题24：创建线程池的有几种方式<br/>


### 题25：如何保证线程按顺序执行<br/>


![大厂面试题](../../imgs/pages.jpg "Java精选")

![大厂面试题](../../imgs/pdfs.png "Java精选")

![大厂面试题](../../imgs/weixin.png "Java精选")